import os

from ewokscore import Task
from silx.io.h5py_utils import retry, File, safe_top_level_names
from silx.utils.retry import RetryError

from blissdata.data.node import get_session_node
from blissdata.data.node import get_node
from blissoda.utils.directories import get_processed_dir
# from nexus_writer_service.utils.scan_utils import scan_uris, scan_info
from .convert.common import h5_to_ascii, h5_meta_to_ascii


END_STATES = ["DONE", "USER_ABORTED", "KILLED"]


@retry(retry_period=0.5)
def wait_scan_state_done(scan_name):
    node = get_node(scan_name)
    if node.info["state"] not in END_STATES:
        raise RetryError


@retry(retry_period=0.5)
def wait_scans_finished(filename, entries=None):
    if not entries:
        entries = safe_top_level_names(filename)
    with File(filename, mode="r") as fh:
        for entry in entries:
            if "end_time" not in fh[entry]:
                raise RetryError

@retry(retry_period=0.5)
def wait_end_scan_event(scan_name):
    scan = get_node(scan_name)
    if scan is None:
        raise RetryError
    for ev in scan.walk_events(wait=True):
        if ev.type == ev.type.END_SCAN:
            break


class WaitScansFinished(
    Task,
    input_names=["scan_name", "filename", "entry"],
    output_names=["scan_name", "filename", "entry"],
):
    def run(self):
        # is this necessary at all if we're waiting for the done state
        # afterwards?
        wait_end_scan_event(self.inputs.scan_name)
        scan = get_node(self.inputs.scan_name)

        print('0====', scan.info["state"])
        wait_scan_state_done(self.inputs.scan_name)
        print('1====', scan.info["state"])
        self.outputs.scan_name = self.get_input_value("scan_name", None)
        self.outputs.filename = self.get_input_value("filename", None)
        self.outputs.entry = self.get_input_value("entry", None)


class ScanToAscii(
    Task,
    input_names=["scan_name", "filename", "entry", "metadata_cfg", "data_cfg"],
    output_names=["output_files"],
):
    def run(self):
        wait_end_scan_event(self.inputs.scan_name)
        wait_scan_state_done(self.inputs.scan_name)
        node = get_node(self.inputs.scan_name)
        info = node.info
        state = info["state"]
        print(f"STATE {state}")
        if state == "DONE":
            processed_dir = get_processed_dir(self.inputs.filename)
            output_basename = os.path.splitext(
                                os.path.basename(self.inputs.filename))[0]
            if not os.path.isdir(processed_dir):
                os.path.makedirs(processed_dir)
            out_file_basename = os.path.splitext(
                os.path.basename(self.inputs.filename))[0]
            output_files = [
                h5_to_ascii(self.inputs.filename,
                            processed_dir,
                            out_file_basename,
                            scan_names=self.inputs.entry,
                            # filename_suffix=name,
                            **cfg)
                for name, cfg in self.inputs.data_cfg.items()]

            metadata_file = h5_meta_to_ascii(
                self.inputs.filename,
                processed_dir,
                out_file_basename,
                scan_names=self.inputs.entry,
                **self.inputs.metadata_cfg)
            # source,
            #          output_dir,
            #          out_file_basename,
            #          scan_names=None,
            #          meta_groups=None,
            #          exclude_groups=None,
            #          filename_suffix="metadata",
            #          filename_ext="dat"
            
            output_files += [metadata_file]
        self.outputs.output_files = output_files
        # scan = node.scan
        # print(scan)
        
        # self.outputs.filename = self.inputs.filename
        # self.outputs.scan_name = self.get_input_value("scan_name", None)

    # def h5toascii(self, h5filename, entry, processed_dir, output_basename):
    #     raise NotImplementedError


# class Xes5ScanToAscii(ScanToAscii):
#     def h5toascii(self, h5filename, entry, processed_dir, output_basename):
#         print(h5filename, entry, processed_dir)
#         print("=====", output_basename)