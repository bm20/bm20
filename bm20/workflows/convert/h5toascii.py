# from typing import List, Optional
# import os
# from ..resources.xrpd import RESOURCE_ROOT

# from ..xrpd.processor import XrpdProcessor

# from blissoda.persistent import ParameterInfo
# from blissoda.utils import directories
# from ewoksjob.client import submit, get_future
# import blissoda.persistent
# from blissoda.persistent import WithPersistentParameters, ParameterInfo, autocomplete_property

# from bliss.scanning import scan_meta

import os
from bliss import current_session
from bliss.scanning import scan_meta
from blissoda.utils import directories
from ewoksjob.client import submit


WORKFLOW = "/data/bm20/inhouse/ewoks/workflows/convert.json"


_METADATA_DEFAULTS = (
    ("filename_suffix", "metadata"),
    ("filename_ext", "dat"),
    ("meta_groups", None),
    ("exclude_groups", None)
)


_DATA_DEFAULTS = (
    ("filename_ext", "dat"),
    ("counters", None),
    ("positioners", None)
)


class AsciiWriter:
    name = property(lambda self: self._name)
    config = property(lambda self: self._config)
    # worker = property(lambda self: self._worker)

    def __init__(self, name, config):
        self._name = name
        self._config = config

        metadata_cfg = config.get("metadata")
        data_cfg = config.get("data")

        if metadata_cfg:
            metadata_cfg = {key: metadata_cfg.get(key, default)
                            for (key, default) in _METADATA_DEFAULTS}
        self._metadata_cfg = metadata_cfg

        _data_cfg = {}
        if data_cfg:
            for sub_cfg in data_cfg:
                name = sub_cfg["name"]
                _data_cfg[name] = {key: sub_cfg.get(key, default)
                                     for (key, default) in _DATA_DEFAULTS}
                _data_cfg[name]["filename_suffix"] = name
        self._data_cfg = _data_cfg

        if self._metadata_cfg["filename_suffix"] in self._data_cfg:
            raise RuntimeError("Data files can't have the same suffix"
                               " ({self._metadata_cfg['filename_suffix']}) as "
                               "the metadata file, file would be overwritten!")

        # self._worker = config["worker"]

    def get_filename(self, scan):
        filename = scan.scan_info.get("filename")
        if filename:
            return filename
        return current_session.scan_saving.filename
    
    def submit_workflow(self, scan):
        dfilename = self.get_filename(scan)
        root = directories.get_workflows_dir(dfilename)
        os.makedirs(root, exist_ok=True)

        # Trigger workflow from the current process.
        filename = self.get_filename(scan)
        scan_name = f"{scan.scan_info['scan_nb']}_{scan.name}"
        sinfo = scan.scan_info
        sav = scan.scan_saving
        try:
            db_name = [sav.session] + sav.base_path.lstrip(os.path.sep).split(os.path.sep) + \
                [sav.proposal_name, sav.collection_name,
                f"{sav.collection_name}_{sav.dataset_name}", scan_name]
            db_name = ":".join(db_name)
            scan_nb = scan.scan_info.get("scan_nb")
            entry = [f"{scan_nb}.1"]
            inputs = [{"name": "filename", "value": filename},
                      {"name": "scan_name", "value": db_name},
                      {"name": "entry", "value": entry},
                      {"name": "metadata_cfg", "value": self._metadata_cfg},
                      {"name": "data_cfg", "value": self._data_cfg}]
            future = submit(args=(WORKFLOW,), kwargs={"inputs":inputs}, queue=None)#self.worker)
        except Exception as ex:
            print("EEEEEEEK")
            print(ex)
            future = None
        print("future", future)
        # return future

    def enable(self):
        scan_meta_obj = scan_meta.get_user_scan_meta()
        if "workflows" not in scan_meta_obj.categories_names():
            scan_meta_obj.add_categories({"workflows"})
        scan_meta_obj.workflows.timing = scan_meta.META_TIMING.START
        scan_meta_obj.workflows.set("@NX_class", {"@NX_class": "NXcollection"})
        scan_meta_obj.workflows.set("processing", self.submit_workflow)

    def disable(self):
        scan_meta_obj = scan_meta.get_user_scan_meta()
        if "workflows" not in scan_meta_obj.categories_names():
            return
        processing = scan_meta_obj.workflows.metadata.get("processing")
        if processing == self.submit_workflow:
            scan_meta_obj.workflows.set("processing", None)
