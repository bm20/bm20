"""
Some functions that extract data and metadata from hdf5 (and hopefuly later from Scans)
and write them to ascii files.
"""

import os
import re
import time
from datetime import datetime
from collections import OrderedDict
from contextlib import contextmanager

import enum

# from silx.io.h5py_utils import retry


import ruamel
from tabulate import tabulate
import numpy as np
import h5py

try:
    from bliss.scanning.scan import Scan as BlissScan
except ImportError:
    BlissScan = None

from silx.io import spech5
from silx.io.h5py_utils import File, open_item


# def is_continuous_scan(meas_g):
#     return "axis:fbragg" in meas_g.keys()

# # def get_energy_axis(meas_g):
# #     keys = list(meas_g.keys())
# #     if is_continuous_scan(meas_g):
# #         return "energy_center"
# #     if energy in keys:
# #         return "energy"
# #     return None


def write_ascii(outfile, data, header, comments=None, com_char="#"):
    """
    Data: 2d array
    Comments: string or list of strings that will be written
        at the top of the file, each line prepended with the
        com_char character.
    """
    with open(outfile, 'w') as out_f:
        if comments:
            if isinstance(comments, (list, tuple,)):
                comments = f"\n{com_char} ".join(comments)
            out_f.write(f"{com_char} ")
            out_f.write(comments)
            out_f.write("\n")
        # out_f.write(com_char)
        np.savetxt(out_f, data.transpose(), header=", ".join(header), delimiter=", ")
        # out_f.write(tabulate(data_dict,
        #                      headers=data_dict.keys(),
        #                      tablefmt="plain"))
        out_f.write("\n")
    print(f"Wrote {outfile}.")


@contextmanager
def scan_ctx(obj, scan=None):
    try:
        yield obj
    except Exception as ex:
        raise


# @retry(retry_timeout=30, retry_period=0.5)
@contextmanager
def h5_ctx(obj, scan=None, retry=True):
    try:
        if scan:
            h5f = open_item(obj, scan, retry_invalid=retry, retry_timeout=10, retry_period=1)
        else:
            with File(obj, 'r') as h5f:
                yield h5f
            
        # with File(obj, 'r') as h5f:
        #     if scan:
        #         yield h5f[scan]
        #     else:
        #         yield h5f
    except Exception as ex:
        raise


@contextmanager
def spec_ctx(obj, scan=None):
    try:
        with spech5.SpecH5(obj) as spech5f:
            if scan:
                yield spech5f[scan]
            else:
                yield spech5f
    except Exception as ex:
        raise


def get_scan_names(group, entries=None):
    scan_names = list(group.keys())

    if entries is None:
        entries = []
    elif not isinstance(entries, (list, set, tuple)):
        entries = [entries]

    if entries:
        for entry in entries:
            if isinstance(entries, (int,)):
                entries = [f"{entries}.1"]
            # elif isinstance(entries, (str,)):
            #     entries = [entries.split(".")[0] + ".1"]
            # else:
            #     raise ValueError(f"Wrong type for entries: {entries}.")

            diff = set(entries) - set(scan_names)
            if diff:
                raise RuntimeError(f"Entries not found in file: {diff}.")
            scan_names = entries[:]

    # scan_names = sorted(scan_names, key=float)
    return scan_names


@contextmanager
def open_ctx(source, is_spec=False, scan_name=None):
    if is_bliss_scan(source):
        with scan_ctx(source) as obj:
            yield obj.get_data()
    if isinstance(source, h5py.Group):
        if scan_name:
            yield source[scan_name]
        else:
            yield source
    elif is_spec:
        with spec_ctx(source) as obj:
            if scan_name:
                yield obj[scan_name]
            else:
                yield obj
    else:
        with h5_ctx(source) as obj:
            if scan_name:
                yield obj[scan_name]
            else:
                yield obj


def is_bliss_scan(obj):
    """
    Returns True if the given object is a Bliss Scan instance.
    """
    return BlissScan and isinstance(obj, (BlissScan,))


def group_has_end_time(file, path):
    """Returns True when the HDF5 item is a Group with an "end_time"
    dataset. A reader can use this as an indication that the Group
    has been fully written (at least if the writer supports this).

    :param Union[h5py.Group,h5py.Dataset] h5item:
    :returns bool:
    """
    try:
        h5item = file[path]
        if isinstance(h5item, h5py.Group):
            return "end_time" in h5item #and "measurement" in h5item
    except KeyError:
        pass
    return False


def top_level_names(filename, include_only=group_has_end_time, **open_options):
    r"""Return all valid top-level HDF5 names.

    :param str filename:
    :param callable or None include_only:
    :param \**open_options: see `File.__init__`
    :returns list(str):
    """
    with File(filename, **open_options) as h5file:
        try:
            if callable(include_only):
                return [name for name in h5file["/"] if include_only(h5file, name)]
            else:
                return list(h5file["/"])
        except KeyError as ex:
            print(ex)
            raise retry_mod.RetryError


class SkipPolicy(enum.Enum):
    SKIP_ALL_MISSING = enum.auto()
    SKIP_ANY_MISSING = enum.auto()


def h5_to_ascii(source,
                output_dir,
                out_file_basename,
                scan_names=None,
                filename_suffix=None,
                filename_ext="dat",
                counters=None,
                positioners=None,
                exclude_counters=None,
                comment_char="#",
                skip_policy=SkipPolicy.SKIP_ALL_MISSING,
                extract_meta=True):
    """
    Extracts counters from Bliss hdf5 file and
    writes them to an ascii file.
    Counter names are written at the top of each column.
    TODO: print a warning if a counter or positioner is not found.
    """

    if not isinstance(skip_policy, SkipPolicy):
        try:
            skip_policy = SkipPolicy[skip_policy]
        except KeyError as ex:
            raise ValueError(f"{skip_policy} is not a valid SkipPolicy.") from ex

    if counters is not None:
        if not isinstance(counters, (list, set, tuple,)):
            counters = [counters]

    if exclude_counters is not None:
        if not isinstance(exclude_counters, (list, set, tuple,)):
            exclude_counters = [exclude_counters]
    else:
        exclude_counters = []

    if positioners is not None:
        if not isinstance(positioners, (list, set, tuple,)):
            positioners = [positioners]

    os.makedirs(os.path.abspath(os.path.dirname(output_dir)),
                exist_ok=True)

    # with open_ctx(source) as h5f:
        # scan_names = get_scan_names(h5f, scan_names)
    # _scan_names = top_level_names(source)
    # if scan_names:
    #     diff = set(scan_names) - set(_scan_names)
    #     if diff:
    #         raise RuntimeError(f"Entries not found in file: {diff}.")
    # else:
    #     scan_names = _scan_names
    if not scan_names:
        scan_names = top_level_names(source, mode="r", include_only=group_has_end_time)

    errors = []
    ignored = []
    out_fnames = []

    for scan_name in scan_names:
        print(f"Opening scan {scan_name}.")
        try:
            with open_ctx(source, scan_name=scan_name) as scan_g:
                meas_g = scan_g["measurement"]
                # inst_keys = list(scan_g["instrument"].keys())
                pos_keys = list(scan_g["instrument/positioners"].keys())
                meas_keys = list(meas_g.keys())

                # using list comprehension instead of set intersection
                # to keep order
                if positioners is None:
                    positioner_names = [name for name in meas_keys
                                        if name in pos_keys]
                else:
                    positioner_names = [name for name in positioners
                                        if name in pos_keys and name in meas_keys]

                if counters is None:
                    counter_names = [name for name in meas_g.keys()
                                    if name not in pos_keys
                                        and name not in exclude_counters]
                else:
                    counter_names = [name for name in counters
                                     if name not in pos_keys and name in meas_keys
                                         and name not in exclude_counters]
                    
                if counters or positioners:
                    if exclude_counters:
                        cnt_to_keep = set(counters) - set(exclude_counters)
                    else:
                        cnt_to_keep = set(counters)
                    missing_cnt = cnt_to_keep - set(counter_names)

                    if positioners:
                        missing_pos = set(positioners) - set(positioner_names)
                    else:
                        missing_pos = set()

                    skip = len(counter_names) == 0
                    skip |= (len(missing_cnt) > 0 or len(missing_pos) > 0) \
                                and skip_policy == SkipPolicy.SKIP_ANY_MISSING

                    if skip:
                        msg = (f"{scan_name}: "
                               f"requested counters/positioners "
                               f"missing: {missing_cnt | missing_pos}")
                        ignored.append(msg)
                        print(f"Skipping {msg}.")
                        continue

                names = [name for name in positioner_names + counter_names
                        if meas_g[name].ndim == 1]
                npts = min(meas_g[name].size for name in names)
                data = np.ndarray((len(names), npts))
                # data = {name: meas_g[name][:] for name in names}
                for i, name in enumerate(names):
                    data[i, :] = meas_g[name][:npts]
                # data = [meas_g[name][:npts] for name in names]

            out_fname = os.path.join(output_dir,
                                    f"{out_file_basename}_{scan_name}")
        except Exception as ex:
            msg = f"Failed to convert scan {scan_name}. Error is {ex}."
            print(msg)
            errors.append(msg)
            continue
            # return None
        
        if filename_suffix:
            out_fname += f"_{filename_suffix}"
        out_fname += "." + filename_ext.lstrip(".")

        write_ascii(out_fname, data, names, com_char=comment_char)

        if extract_meta:
            h5_meta_to_ascii(source,
                             output_dir,
                             out_file_basename,
                             scan_names=[scan_name])


        out_fnames.append(out_fname)

    if ignored:
        print("=========")
        print("SKIPPED (missing counters):")
        print("\n".join(ignored))
    if errors:
        print("=========")
        print("ERRORS:")
        print("\n".join(errors))
    print("=========")
    print(f"{len(ignored)} ignored, {len(errors)} error(s).")

    return out_fnames


def _yaml_seq(*l):
    s = ruamel.yaml.comments.CommentedSeq(l)
    s.fa.set_flow_style()
    return s


def h5_meta_to_ascii(source,
                     output_dir,
                     out_file_basename,
                     scan_names=None,
                     meta_groups=None,
                     exclude_groups=None,
                     filename_suffix="metadata",
                     filename_ext="txt"):
    """
    Writes a Bliss scan hdf5 metadata to an ascii file.
    Groups added are:
        positioners_start, positioners_end,
        positioners_dial_start, positioners_dial_end,
        sample, start_time, end_time, title
    Extra groups can be added with the keyword extra_groups.
    """
    os.makedirs(os.path.abspath(os.path.dirname(output_dir)),
                exist_ok=True)

    # with open_ctx(source) as h5f:
    #     scan_names = get_scan_names(h5f, scan_names)

    if not scan_names:
        scan_names = top_level_names(source)

    if meta_groups:
        if not isinstance(meta_groups, (list, tuple, set)):
            meta_groups = [meta_groups]
    # else:
    #     meta_group_names = ["title", "start_time", "end_time", "sample"]

    if exclude_groups:
        if not isinstance(exclude_groups, (list, tuple, set)):
            exclude_groups = [exclude_groups]
    else:
        exclude_groups = []

    for scan_name in scan_names:
        yaml = ruamel.yaml.YAML()
        yaml.preserve_quotes = True
        # yaml.compact_seq_seq = False

        out_fname = os.path.join(output_dir,
                                 f"{out_file_basename}_{scan_name}")
        if filename_suffix:
            out_fname += f"_{filename_suffix}"
        out_fname += "." + filename_ext.lstrip(".")

        try:
            with open_ctx(source, scan_name=scan_name) as scan_g:
                inst_g = scan_g["instrument"]
                meas_keys = list(scan_g["measurement"].keys())

                if meta_groups:
                    meta_names = [name for name in meta_groups
                                if name not in exclude_groups]
                else:
                    meta_names = [name for name in ["title", "start_time", "end_time"]
                                if name not in exclude_groups]
                    meta_names += [f"instrument/{name}" for name, value in inst_g.items()
                                if not name.startswith("positioners")
                                and name not in exclude_groups
                                and name not in meas_keys
                                and value.attrs.get("NX_class") not in ("NXinstrument",
                                                                        "NXpositioner",
                                                                        "NXdetector")]

                pos_names = list(inst_g["positioners_start"].keys())
                pos_grp_names = ["positioners_start", "positioners_end",
                                "positioners_dial_start", "positioners_dial_end"]

                pos_list = []
                # TODO: optimize (invert loops, etc...)
                for pos_name in pos_names:
                    pos_list += [[pos_name] + [_h5_conv(inst_g[grp_name][pos_name]) for grp_name in pos_grp_names]]

                pos_dict = {}
                for pos_name in pos_names:
                    pos_dict[pos_name] = _yaml_seq(*[_h5_conv(inst_g[grp_name][pos_name])
                                        for grp_name in pos_grp_names])
                meta_dict = {}
                for group_name in meta_names:
                    try:
                        split_path = group_name.split("/")
                        sub_dict = _sub_dict(meta_dict, split_path)
                        sub_dict.update({split_path[-1]:_h5todict(scan_g[group_name])})
                    except KeyError as ex:
                        print(source, ex)

                with open(out_fname, "w") as ascii_f:
                    ascii_f.write(f"Source: {source}\n")
                    ascii_f.write(f"Scan: {scan_name}\n")
                    ascii_f.write("\n")
                    yaml.dump(meta_dict, ascii_f)
                    ascii_f.write("\n")
                    ascii_f.write("# start, stop, dial_start, dial_end\n")
                    yaml.dump({"positioners": pos_dict}, ascii_f)
                print(f"Wrote {out_fname}.")
        except Exception as ex:
            print(f"Failed to extract metadata from scan {scan_name}. Error is {ex}.")
            

def _sub_dict(parent, path):
    """
    Returns a dictionary nested into the,
    given parent dictionary following the
    given path. If the parent's subdictionaries
    at the given path don't exist, they will be
    created and added to the parent.
    Not supported: if a value along the path
                   is not a dictionary.
    e.g:
        d = {}
        d = _sub_dict(d, ["path", "to", "sub"])
        # d -> {"path": {"to": {} } }
        # x -> d["path"]["to"]
    """
    if len(path) == 1:
        return parent
    if path[0] not in parent:
        parent[path[0]] = {}
    return _sub_dict(parent[path[0]], path[1:])


def _h5todict(data):
    """
    Returns a hdf5 group/file as a dictionary.
    """
    if isinstance(data, (h5py.Group, h5py.File)):
        return {key: _h5todict(value) for key, value in data.items()}
    return _h5_conv(data)


def _h5_conv(data):
    """
    Converts hdf5 data to python built-in types.
    (tested with bytes, int and float)
    """
    data = data[()]
    if isinstance(data, (bytes)):
        return data.decode()
    return data.item()


# if __name__ == "__main__"