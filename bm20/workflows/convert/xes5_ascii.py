import os
import re
from datetime import datetime
from collections import OrderedDict
from contextlib import contextmanager

import tabulate
import numpy as np
import h5py
from bliss.scanning.scan import Scan
from silx.io import spech5


# MEAS_KEYS_COMMON = ["sec", "i0", "i1", "trsamp", "ge18_fluo"]
# MEAS_KEYS_STEP = ["energy"]
# MEAS_KEYS_CONT = ["fbragg_energy", "fbragg_egy_delta"]

# CONT_KEYS_ALIASES = {"fbragg_energy":"energy",
#                      "fbragg_egy_delta":"edelta",
#                      "ge18_fluo":"fluo"}

# DET_KEYS_ALIASES = {"elapsed_time":"realt",
#                     "fractional_dead_time":"deadt"}

# detkey_re = re.compile("^.*_det([0-9]{1,2})_(.+$)")


# def get_detector_channels(keys):
#     channels = set()
#     for key in keys:
#         m = detkey_re.match(key)
#         if m:
#             chan = m.groups()[0]
#             channels.add(chan)
#     return sorted(list(channels), key=int)


def is_continuous_scan(scan_g):
    return "axis:fbragg" in scan_g.keys()


# def cont_keys_alias(key):
#     alias = CONT_KEYS_ALIASES.get(key)
#     if alias is None:
#         return det_keys_alias(key)
#     return alias


# def det_keys_alias(key):
#     m = detkey_re.match(key)
#     if m:
#         chan, counter = m.groups()
#         alias = DET_KEYS_ALIASES.get(counter, counter)
#         key = f"{alias}{chan}"
#     return key


def write_ascii(outfile, data_dict, comments=None, com_char="#"):
    """
    Data: dictionary whose keys will be used as header.
    """
    with open(outfile, 'w') as out_f:
        if comments:
            if isinstance(comments, (list, tuple,)):
                comments = f"\n{com_char} ".join(comments)
            out_f.write(f"{com_char} ")
            out_f.write(comments)
            out_f.write("\n")
        out_f.write(com_char)
        out_f.write(tabulate.tabulate(data_dict,
                                      headers=data_dict.keys(),
                                      tablefmt="plain"))


@contextmanager
def scan_ctx(obj, scan=None):
    try:
        yield obj
    except Exception as ex:
        raise


@contextmanager
def h5_ctx(obj, scan=None):
    try:
        with h5py.File(obj, 'r') as h5f:
            if scan:
                yield h5f[scan]
            else:
                yield h5f
    except Exception as ex:
        raise


@contextmanager
def spec_ctx(obj, scan=None):
    try:
        with spech5.SpecH5(obj) as spech5f:
            if scan:
                yield spech5f[scan]
            else:
                yield spech5f
    except Exception as ex:
        raise


@contextmanager
def open_ctx(source, is_spec=False, scan=None):
    if isinstance(source, (Scan,)):
        with scan_ctx(source) as obj:
            yield obj.get_data()
    elif isinstance(source, h5py.Group):
        yield source
    elif is_spec:
        with spec_ctx(source) as obj:
            if scan:
                yield obj[scan]
            else:
                yield obj
    else:
        with h5_ctx(source) as obj:
            if scan:
                yield obj[scan]
            else:
                yield obj


def xes5_to_ascii(source,
                  out_file_basename,
                  overwrite=False,
                  comments=None,
                  com_char="#",
                  is_spec=False):
    for postfix in ("ketek", "norm_i0", "full"):
        fname = f"{out_file_basename}_{postfix}.dat"
        if os.path.isfile(fname) and not overwrite:
            raise RuntimeError(f"File {fname} already exists.")
    
    os.makedirs(os.path.abspath(os.path.dirname(out_file_basename)), exist_ok=True)

    with open_ctx(source) as meas_g:
        # meas_g = source["measurement"]

        is_cont = is_continuous_scan(meas_g)

        if is_cont:
            energy = meas_g["energy_center"][:]
        else:
            energy = meas_g["energy"][:]

        ketek = meas_g["ketek_det0"][:]
        i0 = meas_g["i0"][:]
        e_delta = meas_g["energy_delta"][:]
        sec = meas_g["sec"][:]

        out_f = f"{out_file_basename}_ketek.dat"
        write_ascii(out_f,
                    OrderedDict(energy=energy, ketek=ketek),
                    com_char=com_char)
        out_f = f"{out_file_basename}_norm_i0.dat"
        write_ascii(out_f,
                    OrderedDict(energy=energy, norm=ketek/i0),
                    com_char=com_char)
        out_f = f"{out_file_basename}_full.dat"
        write_ascii(out_f,
                    OrderedDict(energy=energy,
                                ketek=ketek,
                                i0=i0,
                                e_delta=e_delta,
                                sec=sec),
                    com_char=com_char)
            

if __name__ == "__main__":
    f = "/data/bm20/inhouse/bliss_tmp/testfastxes/bm20/20230501/RAW_DATA/test1/test1_x5t/test1_x5t.h5"
    out_dir = "/data/bm20/inhouse/bliss_tmp/testfastxes/bm20/20230501/PROCESSED_DATA/"
    with open_ctx(f) as h5f:
        out_file = os.path.splitext(os.path.basename(f))[0]
        for scan in h5f.keys():
            out_file_base = os.path.join(out_dir, f"{out_file}_{scan}")
            print(out_file_base)
            xes5_to_ascii(h5f[scan], out_file_base, overwrite=1)#, _energy="Energy", is_spec=True)#, comments=comments)