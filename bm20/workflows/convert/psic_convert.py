import os
import re
from datetime import datetime
from collections import OrderedDict
from contextlib import contextmanager



from tabulate import tabulate
import numpy as np
import h5py
from bliss.scanning.scan import Scan
from silx.io import spech5


from .common import h5_to_ascii, write_ascii, get_scan_names, open_ctx, h5_meta_to_ascii




# def xes5_to_ascii(source,
#                   out_file_basename,
#                   overwrite=False,
#                   comments=None,
#                   com_char="#",
#                   is_spec=False):
#     for postfix in ("ketek", "norm_i0", "full"):
#         fname = f"{out_file_basename}_{postfix}.dat"
#         if os.path.isfile(fname) and not overwrite:
#             raise RuntimeError(f"File {fname} already exists.")
    
#     os.makedirs(os.path.abspath(os.path.dirname(out_file_basename)), exist_ok=True)

#     with open_ctx(source) as meas_g:
#         # meas_g = source["measurement"]

#         is_cont = is_continuous_scan(meas_g)

#         if is_cont:
#             energy = meas_g["energy_center"][:]
#         else:
#             energy = meas_g["energy"][:]

#         ketek = meas_g["ketek_det0"][:]
#         i0 = meas_g["i0"][:]
#         e_delta = meas_g["energy_delta"][:]
#         sec = meas_g["sec"][:]

#         out_f = f"{out_file_basename}_ketek.dat"
#         write_ascii(out_f,
#                     OrderedDict(energy=energy, ketek=ketek),
#                     com_char=com_char)
#         out_f = f"{out_file_basename}_norm_i0.dat"
#         write_ascii(out_f,
#                     OrderedDict(energy=energy, norm=ketek/i0),
#                     com_char=com_char)
#         out_f = f"{out_file_basename}_full.dat"
#         write_ascii(out_f,
#                     OrderedDict(energy=energy,
#                                 ketek=ketek,
#                                 i0=i0,
#                                 e_delta=e_delta,
#                                 sec=sec),
#                     com_char=com_char)
            

if __name__ == "__main__":
    from .common import h5_meta_to_ascii
    # f = "/data/bm20/inhouse/bliss_tmp/testpsic/bm20/20231001/RAW_DATA/albula/albula_0002/albula_0002.h5"
    # out_dir = "/data/bm20/inhouse/bliss_tmp/testpsic/bm20/20231001/PROCESSED_DATA/"
    # f = "/data/bm20/inhouse/bliss_tmp/testfexafs/bm20/20231101/RAW_DATA/sample/sample_0001/sample_0001.h5" 
    f = "/data/bm20/inhouse/bliss_tmp/testxesau/bm20/20231101/RAW_DATA/testxesau_bm20.h5"
    out_dir = "/data/bm20/inhouse/bliss_tmp/testxesau/bm20/20231101/PROCESSED_DATA/"
    # with open_ctx(f) as h5f:
    #     out_file = os.path.splitext(os.path.basename(f))[0]
    #     for scan in h5f.keys():
    out_file_base = os.path.splitext(os.path.basename(f))[0]
    # fast_xes5_counters = ["bla", "spectro5_egy", "energy", "fbragg_energy_ center", "epoch_trig", "i0", "xesm4_det0_ketek", "sec", "timer_delta", "xesm4_det0_elapsed_time", "fbragg_center"]
    exclude = ["trigct_delta", "trigct_raw"]
    h5_to_ascii(f, out_dir, out_file_basename=out_file_base, exclude_counters=exclude)#, counters=fast_xes5_counters)#, scan_names="4.1")#, counters="sim_cst_0", positioners="simot1")#, _energy="Energy", is_spec=True)#, comments=comments)
    h5_meta_to_ascii(f, out_dir, out_file_base)#, scan_names="4.1")#, exclude_groups="start_time")