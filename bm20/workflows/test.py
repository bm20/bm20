from ewokscore import Task
# from ewoks import execute_graph

from ewoksjob.client import submit

# Implement a workflow task
class SumTask(
    Task, input_names=["a"], optional_input_names=["b"], output_names=["result"]
):
    def run(self):
        result = self.inputs.a
        if self.inputs.b:
            result += self.inputs.b
        self.outputs.result = result


class XesWriter(Task,
                input_names=["scan_id"],
                output_names=["outfiles"]):
    def run(self):
        scan_id = self.inputs.scan_id
        self.outputs.outfiles = [f"__{scan_id}__"]


# Define a workflow with default inputs
# nodes = [
#     {
#         "id": "task1",
#         "task_type": "class",
#         "task_identifier": "__main__.SumTask",
#         "default_inputs": [{"name": "a", "value": 1}],
#     },
#     {
#         "id": "task2",
#         "task_type": "class",
#         "task_identifier": "__main__.SumTask",
#         "default_inputs": [{"name": "b", "value": 1}],
#     },
#     {
#         "id": "task3",
#         "task_type": "class",
#         "task_identifier": "__main__.SumTask",
#         "default_inputs": [{"name": "b", "value": 1}],
#     },
# ]

nodes = [
    {
        "id": "task1",
        "task_type": "method",
        "task_identifier": "numpy.add",
        "default_inputs": [{"name": 0, "value": 1}, {"name": 1, "value": 10}],
    },
]

nodes = [
    {
        "id": "task1",
        "task_type": "class",
        "task_identifier": "bm20.scans.workflows.test.XesWriter",
    },
]

links = [
    # {
    #     "source": "task1",
    #     "target": "task2",
    #     "data_mapping": [{"source_output": "result", "target_input": "a"}],
    # },
    # {
    #     "source": "task2",
    #     "target": "task3",
    #     "data_mapping": [{"source_output": "result", "target_input": "a"}],
    # },
]
workflow = {"graph": {"id": "testworkflow"}, "nodes": nodes, "links": links}

# Define task inputs'
inputs = [{"name":"scan_id", "value": 128}]

# Execute a workflow (use a proper Ewoks task scheduler in production)
# varinfo = {"root_uri": "/tmp/myresults"}  # optionally save all task outputs
# result = execute_graph(workflow, varinfo=varinfo, inputs=inputs)
# print(result)
# varinfo = {}
# c = submit(args=(workflow,), kwargs={"inputs":inputs})
# print(c.get())



import gevent
from blissdata.data.node import get_session_node

def session_watcher(session_name, **kw):
    session = get_session_node(session_name)
    scan_types = ("scan", "scan_group")
    scans = dict()
    print("Start data processing ...", flush=True)

    try:
        for ev in session.walk_on_new_events(exclude_children=scan_types, wait=True):
            if ev.type == ev.type.END_SCAN:
                print("=====", scans.pop(ev.node.db_name).get())
            elif ev.type == ev.type.NEW_NODE and ev.node.type=="scan":
                scans[ev.node.db_name] = gevent.spawn(scan_watcher, ev.node.db_name)
    except KeyboardInterrupt:
        for scan in scans.values():
            scan.kill()
    print("Data processing stopped", flush=True)


from pprint import pprint
from collections import Counter
from blissdata.data.node import get_node
from nexus_writer_service.utils.scan_utils import scan_uris, scan_info

def scan_watcher(scan_name):
    print("NAME", scan_name)
    scan = get_node(scan_name)
    print("\nSTART", scan.name, flush=True)
    print(scan.__class__)
    info = scan_info(scan)
    print('ST', info["state"])

    print(" URI:", scan_uris(scan))

    events = Counter()
    for ev in scan.walk_events(wait=True):
        print(ev.__class__, ev)
        # print('ST', info["state"])
        events[ev.type] += 1
        if ev.type == ev.type.END_SCAN:
            break

    info = scan_info(scan)
    print(info["state"])
    pprint(events)
    print("END", scan.name, flush=True)
    return 666


if __name__ == '__main__':
    session_watcher('simu')