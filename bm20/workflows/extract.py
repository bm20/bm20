import os
import argparse
from .convert.common import h5_to_ascii, write_ascii, get_scan_names, open_ctx, h5_meta_to_ascii
from blissoda.utils.directories import get_processed_dir


def main():
    parser = argparse.ArgumentParser(description='Convert edf to jpg.')
    parser.add_argument('input_f', metavar='input_f', type=str, nargs=1,
                        help='A scan h5 file.')
    parser.add_argument('-o',
                        dest='outdir',
                        metavar='outdir',
                        nargs=1,
                        default='.',
                        help='output directory')
    parser.add_argument('-f',
                        dest='force',
                        action='store_true',
                        default=False,
                        help='will assume yes if asked about overwriting files')

    args = parser.parse_args()

    outdir = args.outdir[0]
    input_f = args.input_f[0]
    force = args.force
    convert(input_f, outdir, force=force)


def convert_scan(scan, ):
    filename = scan.scan_info["filename"]
    scan_nb = scan.scan_info["scan_nb"]
    processed_dir = get_processed_dir(filename)
    scan_names = [f"{scan_nb}.1"]
    exclude = ["trigct_delta", "trigct_raw"]

    # output_basename = os.path.splitext( os.path.basename(filename))[0]
    if not os.path.isdir(processed_dir):
        os.makedirs(processed_dir)
    out_file_basename = os.path.splitext(os.path.basename(filename))[0]
    
    h5_to_ascii(filename,
                processed_dir,
                out_file_basename,
                scan_names=scan_names,
                exclude_counters=exclude)

    h5_meta_to_ascii(
        filename,
        processed_dir,
        out_file_basename,
        scan_names=scan_names)


def convert(input_f, out_dir, force=False):
    print(input_f)
    print('================================')
    print('================================')

    # if len(input_f) == 1:
    #     # directory or file
    #     if os.path.isdir(input_f[0]):
    #         print(f'Looking for files in directory {input_f[0]}')
    #         input_files = glob.glob(os.path.join(input_f[0], '*.edf'))
    #     else:
    #         input_files = input_f
    # else:
    #     # list of files
    #     input_files = [f for f in input_f if not os.path.isdir(f) and f.endswith('.edf')]

    # n_input_files = len(input_files)
    # print(f'Input: {n_input_files} files.')

    # if len(input_files) == 0:
    #     print("No files, exiting.")
    #     exit(0)

    out_dir = os.path.abspath(out_dir)
    if not os.path.isdir(out_dir):
        print(f'Creating path {out_dir}.')
        os.makedirs(out_dir)

    out_file_base = os.path.splitext(os.path.basename(input_f))[0]
    # fast_xes5_counters = ["bla", "spectro5_egy", "energy", "fbragg_energy_ center", "epoch_trig", "i0", "xesm4_det0_ketek", "sec", "timer_delta", "xesm4_det0_elapsed_time", "fbragg_center"]
    exclude = ["trigct_delta", "trigct_raw"]

    h5_to_ascii(input_f, out_dir, out_file_basename=out_file_base, exclude_counters=exclude)
    h5_meta_to_ascii(input_f, out_dir, out_file_base)

    

    # n_overwrite = 0
    # overwrite = []
    # for in_f in input_files:
    #     out_f = os.path.join(outdir, os.path.splitext(os.path.basename(in_f))[0]) + '.jpg'
    #     if os.path.exists(out_f):
    #         overwrite.append(out_f)
    # n_overwrite = len(overwrite)

    # if n_overwrite > 0:
    #     if force:
    #         print(f'WARNING! {n_overwrite} files will be overwritten')
    #         print('File(s) that will be overwritten:')
    #         print('- ' + '\n - '.join(overwrite))
    #         print('User decided to ignore.')
    #     else:
    #         answer = None 
    #         while answer not in ("yes", "no", "y", "n"): 
    #             print(f'WARNING! {n_overwrite} files will be overwritten. Continue anyway?')
    #             answer = input("Enter yes (y), no (n) or l to list the files: ").lower()
    #             if answer == "l": 
    #                 print('File(s) that will be overwritten:')
    #                 print(' - ' + '\n - '.join(overwrite))
    #             elif answer in ("yes", "y"): 
    #                 # Do this. 
    #                 pass
    #             elif answer in ("no", "n"): 
    #                 print("Cancelled.")
    #                 exit(0)
    #             else: 
    #                 print("Please enter yes/y or no/n.") 

    # print('================================')
    # print('================================')
    # print('Starting conversion.')

# strip_map = dict.fromkeys(map(ord, '_'), None)

# for i_in_f, in_f in enumerate(input_files):
#     base_name = os.path.splitext(os.path.basename(in_f))[0]
#     if strip:
#         base_name = base_name.translate(strip_map)
#     out_f = os.path.join(outdir, base_name) + '.jpg'
#     print(f'Converting file {i_in_f + 1}/{n_input_files}. {in_f} -> {out_f}.')
#     with silx_io.open(in_f) as edf_f:
#         keys = list(edf_f.keys())
#         if len(keys) > 1:
#             raise RuntimeError(f'Edf file {in_f} has more than one entry.')
#         image = edf_f[keys[0]]['image']['data'][:]
#     if depth != 8:
#         image = image * ((2**8-1) / (2**(depth) - 1))
#     img2 = image.astype(np.uint8)
    
#     cv2.imwrite(out_f, image.astype(np.uint8))




if __name__ == "__main__":
    main()
    # from .common import h5_meta_to_ascii
    # # f = "/data/bm20/inhouse/bliss_tmp/testpsic/bm20/20231001/RAW_DATA/albula/albula_0002/albula_0002.h5"
    # # out_dir = "/data/bm20/inhouse/bliss_tmp/testpsic/bm20/20231001/PROCESSED_DATA/"
    # # f = "/data/bm20/inhouse/bliss_tmp/testfexafs/bm20/20231101/RAW_DATA/sample/sample_0001/sample_0001.h5" 
    # f = "/data/bm20/inhouse/bliss_tmp/testxesau/bm20/20231101/RAW_DATA/testxesau_bm20.h5"
    # out_dir = "/data/bm20/inhouse/bliss_tmp/testxesau/bm20/20231101/PROCESSED_DATA/"
    # # with open_ctx(f) as h5f:
    # #     out_file = os.path.splitext(os.path.basename(f))[0]
    # #     for scan in h5f.keys():
    # out_file_base = os.path.splitext(os.path.basename(f))[0]
    # # fast_xes5_counters = ["bla", "spectro5_egy", "energy", "fbragg_energy_ center", "epoch_trig", "i0", "xesm4_det0_ketek", "sec", "timer_delta", "xesm4_det0_elapsed_time", "fbragg_center"]
    # exclude = ["trigct_delta", "trigct_raw"]
    # h5_to_ascii(f, out_dir, out_file_basename=out_file_base, exclude_counters=exclude)#, counters=fast_xes5_counters)#, scan_names="4.1")#, counters="sim_cst_0", positioners="simot1")#, _energy="Energy", is_spec=True)#, comments=comments)
    # h5_meta_to_ascii(f, out_dir, out_file_base)#, scan_names="4.1")#, exclude_groups="start_time")