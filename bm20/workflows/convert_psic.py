from typing import List, Optional
import os
# from ..resources.xrpd import RESOURCE_ROOT

# from ..xrpd.processor import XrpdProcessor

from blissoda.persistent import ParameterInfo
from blissoda.utils import directories
from ewoksjob.client import submit, get_future
from blissoda.persistent import WithPersistentParameters, ParameterInfo, autocomplete_property

from bliss.scanning import scan_meta
from bliss import current_session


WORKFLOW = "/data/bm20/inhouse/ewoks/workflows/convert.json"


class Bm20PsicProcessor:
    # (
    # WithPersistentParameters,
    # parameters=[
    #     ParameterInfo("worker", category="workflows")
    # ]):
    def __init__(self, **defaults) -> None:
        super().__init__(**defaults)
        scan_meta_obj = scan_meta.get_user_scan_meta()
        if "workflows" not in scan_meta_obj.categories_names():
            scan_meta_obj.add_categories({"workflows"})
        scan_meta_obj.workflows.timing = scan_meta.META_TIMING.START
        scan_meta_obj.workflows.set("@NX_class", {"@NX_class": "NXcollection"})
        scan_meta_obj.workflows.set("processing", self._convert)

    def get_filename(self, scan) -> str:
        filename = scan.scan_info.get("filename")
        if filename:
            return filename
        return current_session.scan_saving.filename
    
    def get_submit_arguments(self, scan, lima_name) -> dict:
        return {
            "inputs": self.get_inputs(scan, lima_name),
            "outputs": [{"all": False}],
        }

    def _convert(self, scan):
        # print("CONVERT", scan.scan_info)
        dfilename = self.get_filename(scan)
        root = directories.get_workflows_dir(dfilename)
        os.makedirs(root, exist_ok=True)

        # Trigger workflow from the current process.
        filename = self.get_filename(scan)
        scan_name = f"{scan.scan_info['scan_nb']}_{scan.name}"
        sinfo = scan.scan_info
        sav = scan.scan_saving
        # scan_name = f"{s[session_name]}:"
        # db_name = f"simu:data:bm20:inhouse:bliss_tmp:testpsic:albula:albula_0001:{scan_name}"
        try:
            db_name = [sav.session] + sav.base_path.lstrip(os.path.sep).split(os.path.sep) + \
                [sav.proposal_name, sav.collection_name,
                f"{sav.collection_name}_{sav.dataset_name}", scan_name]
            db_name = ":".join(db_name)
            # print("DB NAME", db_name)
            scan_nb = scan.scan_info.get("scan_nb")
            entry = [f"{scan_nb}.1"]
            inputs = [{"name":"filename", "value":filename},
                    {"name":"scan_name", "value":db_name},
                    {"name":"entry", "value":entry}]
            future = submit(args=(WORKFLOW,), kwargs={"inputs":inputs})#, queue=self.worker)
        except Exception as ex:
            print(ex)

        # scan_name = os.path.splitext(os.path.basename(filename))[0]
        # scan_name = f"{scan_name}: {scan_nb}.1 {scan.name}"
        # if scan.scan_info.get("save"):
        #     if len(self.lima_names) > 1:
        #         output_filename = self.save_nexus_filename(
        #             scan, lima_name=lima_name
        #         )
        #     else:
        #         output_filename = self.save_nexus_filename(scan)
        #     output_url = f"{output_filename}::/{scan_nb}.1/{lima_name}_integrate/integrated"
        # else:
        #     output_url = None
        # self.plotter.handle_workflow_result(
        #     future,
        #     lima_name,
        #     scan_name,
        #     output_url=output_url,
        #     retry_timeout=self.retry_timeout,
        #     retry_period=self.retry_period,
        # )
        #future = get_future(future.task_id)
        #return future
