from gevent import monkey; monkey.patch_all()

import serial

import os
import time
import sys
import serial
import logging
import traceback
import functools


import gevent
from tango.server import run
from tango import AttrQuality, AttrWriteType, DispLevel, DevState, DebugIt, GreenMode
import tango
from tango.server import Device, attribute, command, pipe, device_property, DeviceMeta

logger = logging.getLogger(__file__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
logger.addHandler(out_hdlr)
#logger.setLevel(logging.DEBUG)


_TIC500_N_OUTPUT = 21


_log_level = 2
def exc_catch(func):
    """
    Small decorator that catches exceptions and sends them to the logger.
    """
    functools.wraps(func)
    def wrapper(*args, **kwargs):
        global _log_level
        if logger.level == logging.DEBUG:
            t_in = time.time()
            _log_level += 1
            logger.debug(f'{"-"*_log_level}> {func.__name__}(args={args}, kwargs={kwargs})')
        try:
            result = func(*args, **kwargs)
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            exc_tb = exc_tb.tb_next
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            msg = f'{exc_type.__name__} at line {exc_tb.tb_lineno} in file {fname}: {exc_obj}'
            logger.error(msg)
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            raise
        if logger.level == logging.DEBUG:
            # logger.debug(f'<{"-"*_log_level} {func.__name__} returned {result}. Took {time.time() - t_in:.4f}s.')
            logger.debug(f'<{"-"*_log_level} {func.__name__} returned. Took {time.time() - t_in:.4f}s.')
            _log_level -= 1
        return result
    return wrapper


class CryovacTi500Ds(Device):

    baudrate = device_property(dtype=int, doc='baudrate of the serial port')
    address = device_property(dtype=str, doc='serial port address')
    poll_s = device_property(dtype=float, doc='Poll interval in seconds', default_value=3.)

    def __init__(self, *args, **kwargs):
        self._output_names = []

        super(CryovacTi500Ds, self).__init__(*args, **kwargs)

    @exc_catch
    def init_device(self):
        Device.init_device(self)
        logger.debug(f'Opening serial line {self.address} (bauderate={self.baudrate})')

        self._rlock = gevent.lock.RLock()
        self._serial = serial.Serial(self.address,
                                     baudrate=self.baudrate,
                                     bytesize=serial.EIGHTBITS,
                                     parity=serial.PARITY_NONE,
                                     stopbits=serial.STOPBITS_ONE)

        output_names = self._serial_com('"getOutput.names"')
        self._output_names = output_names.split(', ')
        self._output_values = []
        self._poll_t = 0

        self.set_state(DevState.ON)
        self._do_poll = True

        logger.debug('Spawning poller.')
        self._poll_gl = gevent.spawn(self._run_poll)
        logger.debug('Spawned poller.')

    def _run_poll(self):
        logger.debug('Entering poll method.')
        while self._do_poll:
            try:
                output_str = self._serial_com(f'"getOutput"')
                self._output_values = [float(val)
                                        for val in output_str.split(', ')]
                logger.debug(f'Polled values: {self._output_values}')
                self._poll_t = time.time()
                gevent.sleep(self.poll_s)
            except RuntimeError as ex:
                logger.warning(f'Exception in poller {ex}.')
                if time.time() - self._poll_t > 3 * self.poll_s:
                    logger.error('Failed to poll for too long...')
                    raise RuntimeError('Failed to poll for too long...')
                logger.warning('Retrying...')
                gevent.sleep(0.2)
        logger.debug('Exiting poll method.')

    @exc_catch
    def _serial_com(self, command, cast_fn=None):
        logger.debug(f'CryovacTi500Ds._serial_com({command})')

        with self._rlock:
            logger.debug('Acquired lock.')
            b_command = command.encode() + b'\n'
            n = self._serial.write(b_command)
            if n != len(b_command):
                raise RuntimeError(f'Failed to send command {b_command[:-1]}.')
            gevent.sleep(0.2)
            reply = self._serial.readline()

        logger.debug(f'Read: <{reply}>')
        reply = reply.decode().rstrip('\r\n')

        if reply.startswith('Error:'):
            raise RuntimeError(reply)

        logger.debug(f'CryovacTi500Ds._serial_com, controller replied: {reply}')

        if cast_fn:
            reply = cast_fn(reply)

        return reply
    
    @attribute(dtype=[str,], max_dim_x=_TIC500_N_OUTPUT)
    @exc_catch
    def output_names(self):
        return self._output_names

    @attribute(dtype=[float,], max_dim_x=_TIC500_N_OUTPUT)
    @exc_catch
    def output_values(self):
        return self._output_values

    @command(dtype_in=str, dtype_out=float)
    def getOutputByName(self, name):
        return self._serial_com(f'"{name}?"', cast_fn=float)

    @command(dtype_in=int, dtype_out=float)
    def getOutputByIndex(self, index):
        name = self._output_names[index]
        return self._serial_com(f'"{name}?"', cast_fn=float)


if __name__ == "__main__":
    # CryovacTi500Ds.run_server()
    # if __name__ == "__main__":
#     #logging.basicConfig(level=logging.DEBUG,
#     #                    format="%(threadName)12s %(levelname)5s %(asctime)s %(name)s:%(message)s")

    run([CryovacTi500Ds], green_mode=GreenMode.Gevent)




# #  Baud rate, Parity check, Data bits, Stop bit, Flow control:
# #  9600,      NO,           8,         1,        NONE
# #
# #  MAIN COMMANDS:
# #  TEMPC   Get the actual sensor temperature in degrees Celsius (Float)
# #  TEMPK   Get the actual sensor temperature in Kelvin (Float)
# #  VAC     Get the current AC output voltage (RMS) for both outputs (Float)
# #  IAC     Get the current AC output current (RMS) for both outputs (Float)
# #  REMOTE  Set or get remote on/off status 0 = off 1 = on (INT)


# # ~ 20ms
# # 2907.EH_NA> tango_io("id16na/serial/cp5cooler", "DevSerFlush",2) ;
# # tango_io("id16na/serial/cp5cooler", "DevSerWriteString", "TEMPC\n") ;
# # p tango_io("id16na/serial/cp5cooler", "DevSerReadLine")
# # -185.00



#         # new high level PyTango API...

# class cp5cooler(Device):
#     __metaclass__ = DeviceMeta

#     temperature = attribute(dtype=float,unit="C", format="4.1f")
#     voltage1 = attribute(dtype=float, unit="V")
#     current1 = attribute(dtype=float, unit="A")
#     power1 = attribute(dtype=float, unit="W")
#     voltage2 = attribute(dtype=float, unit="V")
#     current2 = attribute(dtype=float, unit="A")
#     power2 = attribute(dtype=float, unit="W")
#     remote = attribute(label="Remote",
#                        dtype=bool,
#                        access=AttrWriteType.READ)

#     serial_device = device_property(dtype=str)


#     def init_device(self):
#         self.debug_stream("cp5cooler : in init_device()")

#         Device.init_device(self)

#         try:
#             self.serial = serial.Serial(self.serial_device, baudrate=9600, bytesize=8, parity='N', stopbits=1 )
#         except:
#             print("\nPlease add a valid \"serial_device\" propertie\n\n\n")
#             sys.exit()

#         self._voltage1 = 0
#         self._voltage2 = 0
#         self._current1 = 0
#         self._current2 = 0

#         self.set_state(DevState.ON)
#         self.set_status("cool cool cooler")
#         self.debug_stream("cp5cooler : init_device finisehd")
        
#     def _serial_readline(self):
#         return self.serial.readline().decode()
        
#     def _serial_write(self, data):
#         self.serial.write(data.encode())

#     def delete_device(self):
#         self.serial.close()

#     def always_executed_hook(self):
#         pass

#     def read_temperature(self):
#         self.debug_stream("cp5cooler : reading temp")
#         self._serial_write("TEMPC\n")
#         _ans = float(self._serial_readline())
#         self.debug_stream("cp5cooler : temp _ans = %g" % _ans)
#         return float(_ans)

#     def read_voltage1(self):
#         self.debug_stream("cp5cooler : reading voltage1")

#         self._serial_write("VAC\n")
#         _ans1 = float(self._serial_readline())
#         _ans2 = float(self._serial_readline())
#         self.debug_stream("cp5cooler : voltage _ans1 = %g, _ans2=%g" % (_ans1,_ans2))
#         self._voltage1 = _ans1
#         self._voltage2 = _ans2
#         return _ans1


#     def read_voltage2(self):
#         self.debug_stream("cp5cooler : reading voltage2")
#         return self._voltage2

#     def read_current1(self):
#         self.debug_stream("cp5cooler : reading current1")
#         self._serial_write("IAC\n")
#         _ans1 = float(self._serial_readline())
#         _ans2 = float(self._serial_readline())
#         self.debug_stream("cp5cooler : current _ans1 = %g, _ans2=%g" % (_ans1,_ans2))

#         self._current1 = _ans1
#         self._current2 = _ans2
#         return _ans1

#     def read_current2(self):
#         self.debug_stream("cp5cooler : reading voltage2")
#         return self._current2

#     def read_power1(self):
#         self.debug_stream("cp5cooler : reading power1")
#         return self._current1 * self._voltage1

#     def read_power2(self):
#         self.debug_stream("cp5cooler : reading power2")
#         return self._current2 * self._voltage2

#     def read_remote(self):
#         self.debug_stream("cp5cooler : reading remote")
#         self.serial.DevSerFlush(2)
#         self.serial.DevSerWriteString("REMOTE\n")
#         _ans = int(self.serial.DevSerReadLine())
#         self.debug_stream("cp5cooler : remote ans = %d" % _ans)
#         if _ans == 1:
#             return True
#         else:
#             return False


# if __name__ == "__main__":
#     #logging.basicConfig(level=logging.DEBUG,
#     #                    format="%(threadName)12s %(levelname)5s %(asctime)s %(name)s:%(message)s")

#     run([cp5cooler], green_mode=GreenMode.Gevent)

