from gevent import monkey; monkey.patch_all()

import os
import time
import sys
import logging
import traceback
import functools
import datetime
from epics import caget


# import gevent
from tango.server import run
from tango import AttrQuality, AttrWriteType, DispLevel, DevState, DebugIt, GreenMode
import tango
from tango.server import Device, attribute, command, pipe, device_property, DeviceMeta

from logging.handlers import RotatingFileHandler

logger = logging.getLogger(__file__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
logger.addHandler(out_hdlr)
#logger.setLevel(logging.DEBUG)


_log_level = 2
def exc_catch(func):
    """
    Small decorator that catches exceptions and sends them to the logger.
    """
    functools.wraps(func)
    def wrapper(*args, **kwargs):
        global _log_level
        if logger.level == logging.DEBUG:
            t_in = time.time()
            _log_level += 1
            logger.debug(f'{"-"*_log_level}> {func.__name__}(args={args}, kwargs={kwargs})')
        try:
            result = func(*args, **kwargs)
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            exc_tb = exc_tb.tb_next
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            msg = f'{exc_type.__name__} at line {exc_tb.tb_lineno} in file {fname}: {exc_obj}'
            logger.error(msg)
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            raise
        if logger.level == logging.DEBUG:
            # logger.debug(f'<{"-"*_log_level} {func.__name__} returned {result}. Took {time.time() - t_in:.4f}s.')
            logger.debug(f'<{"-"*_log_level} {func.__name__} returned. Took {time.time() - t_in:.4f}s.')
            _log_level -= 1
        return result
    return wrapper


class CryocoolerXVDs(Device):
    poll_s = device_property(dtype=float, doc='Poll interval in seconds', default_value=20.)
    log_file = device_property(dtype=float, doc='Poll interval in seconds', default_value="~")

    def __init__(self, *args, **kwargs):
        self._output_names = []
        self._rlogger = None
        self._start_t = time.time()
        super().__init__(*args, **kwargs)

    # def _create_logger(self):
    #     logger = logging.getLogger("Rotating Log")
    #     logger.setLevel(logging.INFO)
    #     serv_name = self.get_name().split("/")[-1]
    #     log_file = os.path.join(
    #         os.path.expanduser(self.log_file),
    #         f"cryoxv_{serv_name}.log")
    #     self._handler = RotatingFileHandler(log_file,
    #                                   maxBytes=2000000,
    #                                   backupCount=5)
    #     logger.addHandler(self._handler)
    #     self._rlogger = logger
        
    @exc_catch
    def init_device(self):
        Device.init_device(self)
        # self._poll_t = 0

        # self._create_logger()

        self.set_state(DevState.ON)
        # self._do_poll = True

        # logger.debug('Spawning poller.')
        # #self._poll_gl = gevent.spawn(self._run_poll)
        # logger.debug('Spawned poller.')

    # def _run_poll(self):
    #     logger.debug('Entering poll method.')
    #     while self._do_poll:
    #         try:
    #             # l1_lvl = self.L1Level_MON
    #             # self._output_values = [float(val)
    #             #                         for val in output_str.split(', ')]
    #             # self._rlogger.debug(f'Polled values: {self._output_values}')
    #             date_str = datetime.datetime.now().strftime('%c')
    #             self._rlogger.info(f"{date_str}; {caget('FMBBeamlineCC:L1Level_MON.VAL'):.2f}")                
    #             self._poll_t = time.time()
    #             gevent.sleep(self.poll_s)
    #         except RuntimeError as ex:
    #             logger.warning(f'Exception in poller {ex}.')
    #             # if time.time() - self._poll_t > 3 * self.poll_s:
    #             #     logger.error('Failed to poll for too long...')
    #             #     raise RuntimeError('Failed to poll for too long...')
    #             logger.warning('Retrying...')
    #             gevent.sleep(3)
    #     logger.debug('Exiting poll method.')
    
    @attribute(dtype=float, max_dim_x=1)
    def L1Level_MON(self):
        print("READ IN", time.time() - self._start_t)
        # val = 50.
        val = caget('FMBBeamlineCC:L1Level_MON.VAL')
        print("READ OUT", val)
        return val

    @attribute(dtype=str, max_dim_x=1)
    def logfile(self):
        return self._handler.baseFilename


if __name__ == "__main__":
    run([CryocoolerXVDs])#, green_mode=GreenMode.Gevent)
