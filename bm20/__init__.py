# -*- coding: utf-8 -*-

"""Top-level package for BM20 project."""

__author__ = """BCU Team"""
__email__ = 'bliss@esrf.fr'
__version__ = '0.1.0'

# metadata stuff
try:
    from .techniques.metadata import common
except ImportError:
    print("WARNING! Bliss not found.")