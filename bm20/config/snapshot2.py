import functools
from datetime import datetime
from collections import OrderedDict
from itertools import chain, islice
from typing import Union
from tabulate import tabulate, SEPARATING_LINE
from contextlib import contextmanager
from collections import Iterable

from bliss.config.settings import ParametersWardrobe, QueueObjSetting
from bliss.common.axis import Axis
from bliss.config.static import ConfigList
from bliss.config.plugins.generic import ConfigItemContainer
from bliss.shell.getval import getval_idx_list, getval_yes_no, getval_int_range
from bliss.shell.standard import umv


class AxisSnapshotGroup:
    # TODO: initial checks
    axes = property(lambda self: self._axes)
    name = property(lambda self: self._name)
    desc = property(lambda self: self._desc)

    def __init__(self, name, config):
        self._name = name
        self._desc = config.get("desc", name)
        axes = [ax for ax in config["items"]]
        for ax in axes:
            if not isinstance(ax, (Axis,)):
                raise RuntimeError(f"Object {ax} is not an Axis.")
        self._axes = axes[:]

    def item_names(self):
        return [ax.name for ax in self.axes]

    def data(self):
        return {axis.name:{"position":axis.position} for axis in self.axes}
    
    def diff_table(self, data):
        table = []
        for ax in self.axes:
            ax_data = data[ax.name]
            if ax_data is not None:
                table += [[ax.name, ax.position, data[ax.name].get("position")]]
        # table = [[ax.name, ax.position, data[ax.name].get("position")]
        #     for ax in self.axes]
        return table
    
    def restore(self, entry):
        args = []
        for ax in self.axes:
            target = entry.get_saved_data(ax.name)
            # target = data.get(ax.name)
            if target is not None:
                args += [ax, target["position"]]
            else:
                print(f"WARNING. {self.name}: no position given for {ax.name}.")
        umv(*args)


class SnapEntry:
    """
    A object wrapping an entry in the snapshot QueueObjSetting.
    """
    comment = property(lambda self: self._data["comment"])
    """ Entry's comment. """
    epoch = property(lambda self: self._data["epoch"])
    """ Entry's epoch. """
    index = property(lambda self: self._data["index"])
    """ Entry's index (unique). """
    date = property(lambda self: self._data["date"])
    """ Entry's date as a string (%Y-%m-%dT%H:%M:%S). """
    title = property(lambda self: self._data["title"])
    """ Entry's title (index - date - comment). """
    keywords = property(lambda self: self._data["keywords"])
    """ Entry's user defined keywords. """

    data = property(lambda self: self._data)
    """ The entries complete store data, as a dictionary. """
    keys = property(lambda self: list(self._data.keys()))
    snap = property(lambda self: self._data.get("snap"))

    def __init__(self, data=None, index=-1, comment="default", snap_dict=None, **kwargs):
        epoch = kwargs.pop("_epoch", None)
        if data is None or len(data) == 0:
            if epoch is None:
                now = datetime.now()
                epoch = now.timestamp()
            else:
                now = datetime.fromtimestamp(epoch)

            date_str = now.strftime("%Y-%m-%dT%H:%M:%S")

            data = {"title": f"{index} - {date_str} - {comment}",
                    "comment": comment,
                    "index": index,
                    "epoch": epoch,
                    "date": date_str,
                    "keywords": {}}
        
        if "keywords" not in data:
            data["keywords"] = {}

        data["keywords"].update(kwargs)
            
        if snap_dict:
            data["snap"] = snap_dict
            
        self._data = data

    def get_saved_data(self, key):
        return self.snap[key]
    
    def match(self, index=None, comment=None, **kwargs):
        if index is not None and self.index != index:
            return False
        if comment is not None and self.comment.find(comment) == -1:
            return False
        keywords = self.keywords
        for key, value in kwargs.items():
            try:
                if keywords[key] != value:
                    return False
            except KeyError:
                pass
        return True


class SnapEntries:
    defaults = property(lambda self: self._queue[0])

    def __init__(self, queue: QueueObjSetting):
        if len(queue) == 0:
            queue.append(SnapEntry().data)
        self._queue = queue

    def titles(self):
        return [SnapEntry(entry).title for entry in self._queue[1:]]

    def add_default_keys(self, keys):
        defaults = self._queue[0]
        for key in keys:
            if key not in defaults:
                defaults["key"] = None
        self._queue[0] = defaults

    def entry(self, index):
        entry = self.find(index=index)
        if not entry:
            raise ValueError(f"Entry index {index} not found.")
        # shouldnt have more than one... test?
        return SnapEntry(entry[0])
    
    def add_entry(self, comment, snap_dict, **kwargs):
        _epoch = kwargs.pop("_epoch")

        # TODO: use a field in the "default" entry to ensure uniqueness.
        idx = SnapEntry(self._queue[-1]).index + 1
        entry = SnapEntry(comment=comment, index=idx, snap_dict=snap_dict,
                          _epoch=_epoch, **kwargs)
        self._queue.append(entry.data)
        return entry
    
    @property
    def queue(self):
        return islice(self._queue, 1, None)
    
    @property
    def entries(self):
        return [SnapEntry(item) for item in self.queue]
    
    def find(self, index=None, comment=None, **kwargs):
        results = []
        for entry in self.entries:
            # entry = SnapEntry(data)
            if entry.match(index=index, comment=comment, **kwargs):
                results.append(entry)
        return results
    

class Snapshot:
    """
    Class that helps saving and restoring some beamline parameters.
    Only motors at the moment.
    """
    name = property(lambda self: self._name)
    config = property(lambda self: self._config)
    entries = property(lambda self: self._snapshots.entries)
    titles = property(lambda self: self._snapshots.titles())

    def __init__(self, name, config):
        # super().__init__(config)
        self._config = config
        self._name = name
        self._axes = None
        self._groups = None
        self._save_menu_obj = None
        self._snapshot_queue = None#QueueObjSetting(f"snapshot:{self._name}")
        self._restore_menu_obj = None
        self._entries = None
        # self._snapshots_wr = None

    @property
    def n_snapshots(self):
        return len(self._snapshot_queue) - 1
    
    @property
    def _snapshots(self):
        if self._entries is None:
            queue = self._snapshots_queue
            groups = self.groups
            item_names = chain.from_iterable([grp.item_names()
                                              for grp in groups.values()])
            entries = SnapEntries(queue)
            entries.add_default_keys(item_names)
            self._entries = entries
        return self._entries

    @property
    def _snapshots_queue(self):
        """
        Returns the snapshot QueueSettings Object
        """
        if self._snapshot_queue is None:
            self._snapshot_queue = QueueObjSetting(f"snapshot:{self._name}")
        return self._snapshot_queue

    def __info__(self):
        txt = "================\n"
        txt += f"AxisSnapshotGroup: {self.name}\n"

        table = []
        for entry in self.entries:
            idx = entry.index
            date = entry.date
            comment = entry.comment
            table += [[idx, date, comment]]
        
        txt += tabulate(table, headers=["idx", "date", "comment"])
        return txt

    def save_snapshot(self, comment, **kwargs):
        _get_keywords = kwargs.pop("_get_keywords", None)
        epoch = kwargs.pop("_epoch", None)

        groups = self.groups
        snap = self._snapshots

        snap_data = {}
        for group in groups.values():
            data = group.data()
            for name, value in data.items():
                snap_data[name] = value

        # TODO: in what order should we do this?
        # let the function overwrite or not kwargs?
        if _get_keywords:
            kwargs = _get_keywords(data, kwargs)

        snap.add_entry(comment, snap_dict=snap_data, _epoch=epoch, **kwargs)

    def select(self, index):
        snapshots = self._snapshots
        entry = snapshots.find(index=index)
        if not entry:
            raise ValueError(f"{self.name}: not entry with index {index}.")
        return entry[0]

    def search(self, comment=None, **kwargs):
        return self._snapshots.find(comment=comment, **kwargs)
        
    def print_search(self, comment=None, **kwargs):
        results = self.search(comment=comment, **kwargs)
        table = [[result.index,
                  result.date,
                  result.comment] for result in results]
        print(tabulate(table, headers=["index", "date", "comment"]))
        return results

    @property
    def groups(self):
        # TODO: check uniqueness of items across all groups
        if self._groups is None:
            self._groups = {}
            for axes_cfg in self.config.get("axes"):
                label = axes_cfg["label"]
                self._groups[label] = AxisSnapshotGroup(label, axes_cfg)
        return self._groups

    def restore(self, entry: Union[int, SnapEntry], group_names=None):
        if isinstance(entry, (int,)):
            entry = self.select(entry)
        if group_names is None:
            groups = self.groups.values()
        else:
            groups = [self.groups[group_name] for group_name in group_names]
        for group in groups:
            group.restore(entry)


def is_iter(obj):
    return not isinstance(obj, str) and isinstance(obj, Iterable)


class SnapshotMenu(ConfigItemContainer):
    labels = property(lambda self: self._labels)
    keywords = property(lambda self: self._keywords)
    keywords_names = property(lambda self: list(self.keywords.keys()))
    snapshots = property(lambda self: self._snapshot)
    options = property(lambda self: self._options)
    
    def __init__(self, config):
        super().__init__(config)
        self._labels = config.get("labels", {})
        self._snapshot = config["snapshot"]
        keywords = config.get("keywords", {})

        # doing a list(value) to convert ConfigList to list.
        keywords = {key:(list(value) if is_iter(value) else [value])
                    for key, value in keywords.items()}
        self._keywords = keywords
        self._save = None
        self._restore = None

        options = config.get("options")

        if options:
            self._options = OrderedDict({"all": {"desc":"Restore all",
                                                 "groups": None}})
            # check for uniqueness of keys
            self._options.update(options)
        else:
            self._options = {}

    def _create_subitem_from_config(
        self, name, cfg, parent_key, item_class, item_obj=None
    ):
        if parent_key == "save":
            save = item_class(self, name, cfg)
            self._save = save
            return save
        if parent_key == "restore":
            restore = item_class(self, name, cfg)
            self._restore = restore
            return restore
        
    def _get_subitem_default_class_name(self, cfg, parent_key):
        if parent_key == "save":
            return "SnapshotSaveMenu"
        if parent_key == "restore":
            return "SnapshotRestoreMenu"
        raise RuntimeError(f"Keyword not supported: {parent_key}.")
    
    def _get_subitem_default_module(self, class_name, cfg, parent_key):
        package = cfg.get("package")
        if package:
            return package
        return super()._get_subitem_default_module(class_name, cfg, parent_key)
    
    def keyword_allowed_values(self, keyword):
        return self._keywords.get(keyword, [])
    
    def save(self, *args, **kwargs):
        if self._save:
            _get_keywords = self._save.keywords_values
        else:
            _get_keywords = None
        self.snapshots.save_snapshot(*args,
                                     _get_keywords=_get_keywords,
                                     **kwargs)
        
    def selection_menu(self, comment=None, **kwargs):
        while True:
            selection = _selection_menu(self, comment=comment, **kwargs)
            if selection:
                if self.restore_entry_menu(selection):
                    return
            else:
                return

    def restore_entry_menu(self, entry: Union[int, SnapEntry]):
        if isinstance(entry, (int,)):
            entry = self.snapshots.select(entry)
        return _restore_entry_menu(self, entry)


class SnapshotSaveMenu:
    name = property(lambda self: self._name)
    config = property(lambda self: self._config)
    snapshotmenu = property(lambda self: self._snapshotmenu)

    def __init__(self, snapshotmenu, name, config):
        self._name = name
        self._config = config
        self._snapshotmenu = snapshotmenu
        for label, keywords in self.snapshotmenu.labels.items():
            self._add_label_method(label, **keywords)

    def _add_label_method(self, label, **kwargs):
        """Add a method named after the position label to move to the
        corresponding position.
        """
        if label.isidentifier():
            setattr(
                self,
                label,
                functools.partial(self, **kwargs),
            )
        else:
            raise RuntimeError(f"'{label}' is not a valid python identifier.")
        
    def __call__(self, comment: str, **kwargs):
        self.snapshotmenu.save(comment=comment, **kwargs)

    def keywords_values(self, entry, keywords):
        keywords = self._keywords_values(entry, keywords)
        self._validate_keywords(keywords)
        keys = list(keywords.keys())
        for key in self.snapshotmenu.keywords.keys():
            if key not in keys:
                keywords[key] = None
        return keywords
        
    def _keywords_values(self, entry: SnapEntry, keywords):
        return keywords
    
    def _validate_keywords(self, keywords):
        cfg_keywords = self.snapshotmenu.keywords
        if not keywords:
            return True
        for key, value in keywords.items():
            if key not in cfg_keywords:
                raise ValueError(f"{self._name}: unknown keyword: {key}.")
            kwd_values = cfg_keywords.get(key, [value])
            if value not in kwd_values:
                raise ValueError(f"{self._name}: invalid value for "
                                 f"keyword {key}: {value}."
                                 f"Allowed values: {kwd_values}")
        return True


class SnapshotRestoreMenu:
    name = property(lambda self: self._name)
    config = property(lambda self: self._config)
    snapshotmenu = property(lambda self: self._snapshotmenu)

    def __init__(self, snapshotmenu, name, config):
        self._name = name
        self._config = config
        self._snapshotmenu = snapshotmenu
        for label, keywords in self.snapshotmenu.labels.items():
            self._add_label_method(label, **keywords)

    def _add_label_method(self, label, **kwargs):
        """Add a method named after the position label to move to the
        corresponding position.
        """
        if label.isidentifier():
            setattr(
                self,
                label,
                functools.partial(self, **kwargs),
            )
        else:
            raise RuntimeError(f"'{label}' is not a valid python identifier.")
        
    def __call__(self, comment=None, **kwargs):
        self.snapshotmenu.selection_menu(comment=comment, **kwargs)

    def __info__(self):
        txt = "================\n"
        txt += f"AxisSnapshotGroup: {self.name}\n"

        kwrds = list(self.snapshotmenu.keywords_names)

        table = []
        for entry in self.snapshotmenu.snapshots.entries:
            idx = entry.index
            date = entry.date
            comment = entry.comment
            keywords = entry.keywords
            user_keys = [keywords.get(key) for key in kwrds]
            table += [[idx, date, comment] + user_keys]

        # with wardrobe_iter(self.snapshotmenu.snapshots._snapshots) as wr_iter:
        #     for instance in wr_iter:
        #         if instance.current_instance != "default":
        #             info = instance.snapshot_info_
        #             user = instance.snapshot_user_
        #             idx = info["index"]
        #             date = info["date"]
        #             comment = info["comment"]
        #             user_keys = [user.get(key) for key in kwrds]
        #             table += [[idx, date, comment] + user_keys]
        
        txt += tabulate(table, headers=["idx", "date", "comment"] + kwrds)
        return txt


def _selection_menu(snapshotsmenu, comment=None, **kwargs):
    snapshots = snapshotsmenu.snapshots
    filters = kwargs.copy()
    filters["comment"] = comment
    while True:
        print("=== Restore menu ===")
        print(f" Snapshot [{snapshots.name}]")
        results = snapshots.search(**filters)

        table = []
        kwrds = snapshotsmenu.keywords_names

        for entry in results:
            idx = entry.index
            date = entry.date
            comment = entry.comment
            keywords = entry.keywords
            user_keys = [keywords.get(key) for key in kwrds]
            table += [[idx, date, comment] + user_keys]

        # for result in results.values():
        #     info = result["snapshot_info_"]
        #     user = result["snapshot_user_"]
        #     idx = info["index"]
        #     date = info["date"]
        #     comment = info["comment"]
        #     user_keys = [user.get(key) for key in kwrds]
        #     table += [[idx, date, comment] + user_keys]
        
        print(tabulate(table, headers=["idx", "date", "comment"] + kwrds))
    
        menu_kwds = ["comment"] + snapshotsmenu.keywords_names

        # applied = f"[comment=[{comment}]; "
        applied = "[" + "; ".join([f"{kwd}=[{filters.get(kwd, None)}]" for kwd in menu_kwds])
        applied += "]"
        print("-------")
        print("> Applied filters:")
        print("> " + applied)
        print("-------")

        choices = ["select an index to restore",
                   "edit filters",
                   "quit"]
        ans = getval_idx_list(choices, "Command:", default=3)[0]
        if ans == 3:
            return
        if ans == 2:
            filters = _filters_submenu(snapshotsmenu, **filters)
            continue
        if ans == 1:
            ans = getval_int_range("Index of entry to restore (-1 to cancel)",
                                    minimum=0,
                                    default=-1,
                                    maximum=snapshots.n_snapshots - 1)
            if ans == -1:
                continue
            entry = snapshots.select(ans)
            return entry


def _filters_submenu(snapshotsmenu, **kwargs):
    menu_kwds = ["comment"] + snapshotsmenu.keywords_names
    filters = kwargs.copy()

    while True:
        table = []
        for ikwd, kwd in enumerate(menu_kwds):
            table += [[ikwd, kwd, filters.get(kwd, "*")]]
        print(tabulate(table, headers=["Id", "Filter", "Current"]))

        choices = [kw for kw in menu_kwds] + ["> Apply"]

        ans = getval_idx_list(choices, "Select a filter to edit:",
                              default=len(choices))[0]

        if ans == len(choices):
            break

        kw = list(menu_kwds)[ans - 1]
        allowed = snapshotsmenu.keyword_allowed_values(kw)
        if allowed:
            choices = allowed + ["> Clear", "> Cancel"]
            ans = getval_idx_list(choices, "Value:",
                                  default=len(choices))[0]
            if ans == len(choices):
                continue
            if ans == len(choices) - 1:
                filters.pop(kw)
                continue
            filters[kw] = choices[ans - 1]
        else:
            ans = input("Enter value:")
            if not ans:
                filters.pop(kw)
            else:
                filters[kw] = ans

    return filters


def _restore_entry_menu(snapshotsmenu, entry):
    options = snapshotsmenu.options
    while True:
        groups = None
        if options:
            choices = [option["desc"] for option in options.values()]
            choices += ["Cancel"]
            ans = getval_idx_list(choices, "Select an option:",
                                  default=len(choices))[0]
            if ans == len(choices):
                return False
            
            choice = list(options.values())[ans - 1]
            groups = choice.get("groups")
            
        if not groups:
            # restoring all
            groups = snapshotsmenu.snapshots.groups

        print("This will be applied:")
        entry_dict = entry.snap
        table = []
        for group in groups.values():
            diff_table = group.diff_table({key:entry_dict[key] for key in group.item_names()})
            grp_table = [[group.name] + diff_table[0]]
            grp_table += [[""] + diff for diff in diff_table[1:]]
            if table:
                table += SEPARATING_LINE
            table += grp_table
        print(tabulate(table, headers=["group", "item", "current", "snap"], tablefmt="simple"))

        if not getval_yes_no("Continue?"):
            continue

        snapshotsmenu.snapshots.restore(entry, group_names=list(groups.keys()))
        return True


if __name__ == "__main__":
    from time import monotonic
    from bliss.config.static import get_config
    bconfig = get_config()
    sn = bconfig.get("simu_bm20_snap")
    # for i in range(100):
    #     sn.save_snapshot(f"toto{i}", bla=2)
    t0 = monotonic()
    sn.print_search(comment="to5")
    print(monotonic() - t0)