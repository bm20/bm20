from bm20.config.snapshot2 import SnapshotSaveMenu


class Bm20SnapSaveMenu(SnapshotSaveMenu):
    def _keywords_values(self, snap_data, keywords):
        # snap_data = entry.snap
        config = self.config
        crystal = snap_data.get("monxtal")
        if crystal:
            keywords["crystal"] = int(crystal["position"])
        mir1surface = config["m1surface"]
        mir2surface = config["m2surface"]
        keywords["mir1"] = mir1surface.position
        keywords["mir2"] = mir2surface.position
        return keywords