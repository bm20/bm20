import functools
from datetime import datetime
from collections import OrderedDict
from itertools import chain
from typing import Any
from tabulate import tabulate, SEPARATING_LINE
from contextlib import contextmanager

from bliss.config.settings import ParametersWardrobe
from bliss.common.axis import Axis
from bliss.config.plugins.generic import ConfigItemContainer
from bliss.shell.getval import getval_idx_list, getval_yes_no, getval_int_range
from bliss.shell.standard import umv


class AxisSnapshotGroup:
    # TODO: initial checks
    axes = property(lambda self: self._axes)
    name = property(lambda self: self._name)
    desc = property(lambda self: self._desc)

    def __init__(self, name, config):
        self._name = name
        self._desc = config.get("desc", name)
        axes = [ax for ax in config["items"]]
        for ax in axes:
            if not isinstance(ax, (Axis,)):
                raise RuntimeError(f"Object {ax} is not an Axis.")
        self._axes = axes[:]

    def item_names(self):
        return [ax.name for ax in self.axes]

    def data(self):
        return {axis.name:{"position":axis.position} for axis in self.axes}
    
    def diff_table(self, data):
        table = []
        for ax in self.axes:
            ax_data = data[ax.name]
            if ax_data is not None:
                table += [[ax.name, ax.position, data[ax.name].get("position")]]
        # table = [[ax.name, ax.position, data[ax.name].get("position")]
        #     for ax in self.axes]
        return table
    
    def restore(self, data):
        args = []
        for ax in self.axes:
            target = data.get(ax.name)
            if target is not None:
                args += [ax, target["position"]]
            else:
                print(f"WARNING. {self.name}: no position given for {ax.name}.")
        umv(*args)
    

@contextmanager
def wardrobe_select(wardrobe, instance="default"):
    """
    Context that selects a wardrobe, then
    reset it to the one that was selected before.
    """
    current = wardrobe.current_instance
    try:
        yield wardrobe.switch(instance)
    finally:
        wardrobe.switch(current)
    

@contextmanager
def wardrobe_iter(wardrobe, sort=True):
    """
    Context that returns an iterator over the given wardrobe instances.
    If sort=True the instance names will be sorted.
    """
    current = wardrobe.current_instance
    try:
        yield _wardrobe_iter(wardrobe, sort=sort)
    finally:
        wardrobe.switch(current)


def _wardrobe_iter(wardrobe, sort=True):
    if sort:
        instances = sorted(wardrobe.instances)
    else:
        instances = wardrobe.instances
    for instance in instances:
        if instance == "default":
            continue
        wardrobe.switch(instance)
        yield wardrobe


def match_snapshot(wardrobe, index=None, comment=None, **kwargs):
    match = True
    info = wardrobe.snapshot_info_
    user = wardrobe.snapshot_user_
    if index is not None:
        match &= info["index"] == index

    # TODO: regex
    if match and comment:
        match &= info["comment"].find(comment) != -1

    if match and kwargs:
        for key, value in kwargs.items():
            try:
                if user[key] != value:
                    match = False
                    break
            except KeyError:
                # TODO: should we raise if keyword absent?
                print(f"In snapshot {wardrobe.current_instance}, user key {key} not found. Ignoring.")
                pass
    return match


class Snapshot(ConfigItemContainer):
    """
    Class that helps saving and restoring some beamline parameters.
    Only motors at the moment.
    """
    entries = property(lambda self: [e for e in self._snapshots.instances
                                     if e != "default"])

    def __init__(self, config):
        super().__init__(config)
        self._axes = None
        self._groups = {}
        self._save_menu_obj = None
        self._restore_menu_obj = None
        self._snapshots_wr = None

    @property
    def n_snapshots(self):
        with wardrobe_select(self._snapshots):
            return self._snapshots.snapshot_info_["index"] + 1

    def _create_subitem_from_config(
        self, name, cfg, parent_key, item_class, item_obj=None
    ):
        if parent_key == "axes":
            group = item_class(name, cfg)
            self._groups[name] = group
            return group
        
    def _get_subitem_default_class_name(self, cfg, parent_key):
        if parent_key == "axes":
            return "AxisSnapshotGroup"
        raise RuntimeError(f"Keyword not supported: {parent_key}.")

    @property
    def _snapshots(self):
        if self._snapshots_wr is None:
            snapshots = ParametersWardrobe(f'snapshots_{self.name}')
            groups = self.groups

            snap_keys = set(snapshots.to_dict().keys())
            item_names = set(chain.from_iterable([grp.item_names()
                                                for grp in groups.values()]))

            if "snapshot_info_" not in snap_keys:
                snapshots.add("snapshot_info_", {"index": -1})

            if "snapshot_user_" not in snap_keys:
                snapshots.add("snapshot_user_", {})

            for name in item_names - snap_keys:
                snapshots.add(name, None)

            self._snapshots_wr = snapshots
        return self._snapshots_wr
    
    def _add_keywords(self, keywords):
        snapshots = self.snapshots
        for key in keywords:
            snapshots.add(key)

    def __info__(self):
        txt = "================\n"
        txt += f"AxisSnapshotGroup: {self.name}\n"

        table = []
        kwrds = list(self.save.keywords.keys())

        with wardrobe_iter(self._snapshots) as wr_iter:
            for instance in wr_iter:
                if instance.current_instance != "default":
                    info = instance.snapshot_info_
                    user = instance.snapshot_user_
                    idx = info["index"]
                    date = info["date"]
                    comment = info["comment"]
                    user_keys = [user.get(key) for key in kwrds]
                    table += [[idx, date, comment] + user_keys]
        
        txt += tabulate(table, headers=["idx", "date", "comment"] + kwrds)
        return txt

    def save_snapshot(self, comment, **kwargs):
        groups = self.groups
        snap = self._snapshots
        snap.switch("default")
        snapshot_info = snap.snapshot_info_
        last_index = snapshot_info["index"] + 1

        now = datetime.now()
        epoch = datetime.now().timestamp()
        date_str = now.strftime("%Y-%m-%dT%H:%M:%S")

        snapshot_title = f"{last_index} - {date_str} - {comment}"
        snap.switch(snapshot_title)

        try:
            this_info = snap.snapshot_info_
            this_user = snap.snapshot_user_
            this_info["index"] = last_index
            this_info["epoch"] = epoch
            this_info["comment"] = comment
            this_info["date"] = date_str

            for group in groups.values():
                data = group.data()
                for name, value in data.items():
                    setattr(snap, name, value)

            this_user.update(kwargs)
            save_menu = self.save
            this_user = save_menu.save_keywords(snap.to_dict(), this_user)

            snap.snapshot_user_ = this_user
            snap.snapshot_info_ = this_info
        except:
            snap.switch("default")
            snap.remove(snapshot_title)
            raise
        else:
            snap.switch("default")
            snapshot_info["index"] = last_index
            snap.snapshot_info_ = snapshot_info

    def select(self, index):
        snapshots = self._snapshots

        current = snapshots.current_instance
        snapshots.switch("default")
        n_entries = snapshots.snapshot_info_["index"]
        snapshots.switch(current)

        if index < 0 or index > n_entries:
            return {}

        with wardrobe_iter(snapshots) as wr_iter:
            for instance in wr_iter:
                if (instance.snapshot_info_["index"] == index
                    and instance.current_instance != "default"):
                    return self._snapshots.to_dict()
        raise RuntimeError(f"{self.name}: entry #{index} not found.")

    def search(self, comment=None, **kwargs):
        with wardrobe_iter(self._snapshots) as wr_iter:
            return OrderedDict([(instance.current_instance, instance.to_dict())
                    for instance in wr_iter
                    if match_snapshot(instance, comment=comment, **kwargs)])
        
    def print_search(self, comment=None, **kwargs):
        results = self.search(comment=comment, **kwargs)
        table = [[value["snapshot_info_"]["index"],
                  value["snapshot_info_"]["date"],
                  value["snapshot_info_"]["comment"]] for value in results.values()]
        print(tabulate(table, headers=["index", "date", "comment"]))

    @property
    def groups(self):
        # TODO: check uniqueness of items across all groups
        names = set(self._groups.keys())
        cfg_names = set(self._subitems_config.keys())
        for name in cfg_names - names:
            self._get_subitem(name)
        return self._groups

    # ~ @property
    # ~ def _menus(self):
        # ~ if self._menus_dict is None:
            # ~ menus_cfg = self.config.get("menu")
            # ~ menus = OrderedDict({"all": {"desc": "restore all", "items": []}})
            # ~ if menus_cfg:
                # ~ menus = OrderedDict([(menu["label"], {"desc": menu["desc"], "items":[group for group in menu["items"]]})
                                     # ~ for menu in menus_cfg])
            # ~ self._menus_dict = menus
        # ~ return self._menus_dict
    
    @property
    def save(self):
        if self._save_menu_obj is None:
            menu_cfg = self.config.get("menu")
            self._save_menu_obj = SnapshotSaveMenu(f"{self.name}:save", menu_cfg, self.save_snapshot)
        return self._save_menu_obj
    
    @property
    def restore(self):
        if self._restore_menu_obj is None:
            menu_cfg = self.config.get("menu")
            self._restore_menu_obj = SnapshotRestoreMenu(f"{self.name}:restore", menu_cfg, self.selection_menu)
        return self._restore_menu_obj
        
    def selection_menu(self, comment=None, **kwargs):
        while True:
            selection = _selection_menu(self, comment=comment, **kwargs)
            if selection:
                if self.restore_entry_menu(selection):
                    return
            else:
                return

    def restore_entry_menu(self, entry):
        if isinstance(entry, (int,)):
            entry = self.select(entry)
        if not isinstance(entry, (dict,)):
            raise RuntimeError(f"{self.name}: expected a dictionary.")
        return _restore_entry_menu(self, entry)


class SnapshotSaveMenu:
    config = property(lambda self: self._config)
    keywords = property(lambda self: self._keywords)
    keywords_names = property(lambda self: list(self._keywords.keys()))
    save = property(lambda self: self._save_meth)

    def __init__(self, name, config, save_meth):
        self._name = name
        self._config = config
        self._save_meth = save_meth
        self._keywords = {kw["key"]: kw.get("values")
                          for kw in config.get("keywords")}
        save_list = config.get("save", [])
        for save_cfg in save_list:
            label = save_cfg["label"]
            kwds = save_cfg.get("keywords")
            self._add_label_method(label, **kwds)

    def save_keywords(self, snap_dict, keywords):
        keywords = self._save_keywords(snap_dict, keywords)
        self._validate_keywords(keywords)
        return keywords
    
    def keyword_allowed_values(self, keyword):
        return self._keywords.get(keyword, [])
        
    def _save_keywords(self, snap_data, keywords):
        config = self.config
        crystal = snap_data.get("monxtal")
        if crystal:
            keywords["crystal"] = int(crystal["position"])
        mir1surface = config["m1surface"]
        mir2surface = config["m2surface"]
        keywords["mir1"] = mir1surface.position
        keywords["mir2"] = mir2surface.position
        return keywords
    
    def _validate_keywords(self, keywords):
        cfg_kwds = {kw["key"]: kw.get("values")
                    for kw in self.config.get("keywords", {})}
        if not cfg_kwds:
            return True
        for key, value in keywords.items():
            if key not in cfg_kwds:
                raise ValueError(f"{self._name}: unknown keyword: {key}.")
            kwd_values = cfg_kwds.get(key, [value])
            if value not in kwd_values:
                raise ValueError(f"{self._name}: invalid value for "
                                 f"keyword {key}: {value}."
                                 f"Allowed values: {kwd_values}")
        return True
    
    def __call__(self, comment, **kwargs):
        self.save(comment, **kwargs)

    def _add_label_method(self, label, **kwargs):
        """Add a method named after the position label to move to the
        corresponding position.
        """
        if label.isidentifier():
            setattr(
                self,
                label,
                functools.partial(self, **kwargs),
            )
        else:
            raise RuntimeError(f"'{label}' is not a valid python identifier.")


class SnapshotRestoreMenu:
    config = property(lambda self: self._config)
    keywords = property(lambda self: self._keywords)
    keywords_names = property(lambda self: list(self._keywords.keys()))
    select = property(lambda self: self._select_meth)
    options = property(lambda self: self._options)

    def __init__(self, name, config, select_meth):
        self._config = config
        self._select_meth = select_meth
        self._keywords = {kw["key"]: kw.get("values")
                          for kw in config.get("keywords")}
        save_list = config.get("save", [])
        for save_cfg in save_list:
            label = save_cfg["label"]
            kwds = save_cfg.get("keywords")
            self._add_label_method(label, **kwds)

        options = config.get("restore")

        if options:
            self._options = OrderedDict({"all": {"desc":"Restore all",
                                                "groups": None}})
            # check for uniqueness of keys
            self._options.update(OrderedDict(
                (item["label"],
                {"desc": item["desc"],
                "groups":[obj for obj in item["items"]]})
                for item in options
            ))
        else:
            self._options = {}

    def keyword_allowed_values(self, keyword):
        return self._keywords.get(keyword, [])

    def _add_label_method(self, label, **kwargs):
        """Add a method named after the position label to move to the
        corresponding position.
        """
        if label.isidentifier():
            setattr(
                self,
                label,
                functools.partial(self, **kwargs),
            )
        else:
            raise RuntimeError(f"'{label}' is not a valid python identifier.")
        
    def __call__(self, comment=None, **kwargs):
        self.select(comment=comment, **kwargs)


def _selection_menu(snapshots, comment=None, **kwargs):
    filters = kwargs.copy()
    filters["comment"] = comment
    while True:
        print("=== Restore menu ===")
        print(f" Snapshot [{snapshots.name}]")
        results = snapshots.search(**filters)

        table = []
        kwrds = snapshots.restore.keywords_names

        for result in results.values():
            info = result["snapshot_info_"]
            user = result["snapshot_user_"]
            idx = info["index"]
            date = info["date"]
            comment = info["comment"]
            user_keys = [user.get(key) for key in kwrds]
            table += [[idx, date, comment] + user_keys]
        
        print(tabulate(table, headers=["idx", "date", "comment"] + kwrds))
    
        menu_kwds = ["comment"] + snapshots.restore.keywords_names

        # applied = f"[comment=[{comment}]; "
        applied = "[" + "; ".join([f"{kwd}=[{filters.get(kwd, None)}]" for kwd in menu_kwds])
        applied += "]"
        print("-------")
        print("> Applied filters:")
        print("> " + applied)
        print("-------")

        choices = ["select an index to restore",
                   "edit filters",
                   "quit"]
        ans = getval_idx_list(choices, "Command:", default=3)[0]
        if ans == 3:
            return
        if ans == 2:
            filters = _filters_submenu(snapshots, **filters)
            continue
        if ans == 1:
            ans = getval_int_range("Index of entry to restore (-1 to cancel)",
                                    minimum=0,
                                    default=-1,
                                    maximum=snapshots.n_snapshots - 1)
            if ans == -1:
                continue
            entry = snapshots.select(ans)
            return entry
        

def _filters_submenu(snapshots, **kwargs):
    menu_kwds = ["comment"] + snapshots.restore.keywords_names
    filters = kwargs.copy()

    while True:
        table = []
        for ikwd, kwd in enumerate(menu_kwds):
            table += [[ikwd, kwd, filters.get(kwd, "*")]]
        print(tabulate(table, headers=["Id", "Filter", "Current"]))

        choices = [kw for kw in menu_kwds] + ["> Apply"]

        ans = getval_idx_list(choices, "Select a filter to edit:",
                              default=len(choices))[0]

        if ans == len(choices):
            break

        kw = list(menu_kwds)[ans - 1]
        allowed = snapshots.restore.keyword_allowed_values(kw)
        if allowed:
            choices = [value for value in allowed] + ["> Cancel"]
            ans = getval_idx_list(choices, "Value:",
                                  default=len(choices))[0]
            if ans == len(choices):
                continue
            filters[kw] = choices[ans - 1]
        else:
            filters[kw] = input("Enter value:")

    return filters


def _restore_entry_menu(snapshots, entry_dict):
    options = snapshots.restore.options
    while True:
        groups = None
        if options:
            choices = [option["desc"] for option in options.values()]
            choices += ["Cancel"]
            ans = getval_idx_list(choices, "Select an option:",
                                  default=len(choices))[0]
            if ans == len(choices):
                return False
            
            choice = list(options.values())[ans - 1]
            groups = choice.get("groups")
            
        if not groups:
            # restoring all
            groups = snapshots.groups.values()

        print("This will be applied:")
        table = []
        for group in groups:
            diff_table = group.diff_table({key:entry_dict[key] for key in group.item_names()})
            grp_table = [[group.name] + diff_table[0]]
            grp_table += [[""] + diff for diff in diff_table[1:]]
            if table:
                table += SEPARATING_LINE
            table += grp_table
        print(tabulate(table, headers=["group", "item", "current", "snap"], tablefmt="simple"))

        if not getval_yes_no("Continue?"):
            continue

        _restore(snapshots, entry_dict, groups=groups)
        return True


def _restore(snapshots, entry_dict, groups=None):
    print("RESTORING")
    if groups:
        for group in groups:
            group.restore(entry_dict)