import sys
from bliss.config.static import get_config

bm20names = get_config().get("bm20_names")

def __getattr__(name: str):
    return getattr(bm20names, name)




# SPEC_NAMES_TO_BLISS = {"mono": "bragg",
#                        "monp": "xl2perp",
#                        "mon_y": "ml2perp",
#                        "mon_z": "ml2long"}

# MUSST = 'musstoh'
# OPIOM_EH1 = 'eh1_mpx'

# MONO = 'd20pmac'
# MIR_SURF_1 = 'm1surface'
# MIR_SURF_2 = 'm2surface'
# ATTEN1 = 'att1'
# ATTEN2 = 'att2'
# ENERGY = 'energy'
# CRYSTAL = 'crystal'
# MONXTAL = 'monxtal'
# EGY_FSCAN = 'd20fscan'
# BRAGG = "bragg"

# LATERAL = 'monolat'
# YAW = 'monyaw'

# MONOJACK1 = 'dj1'
# MONOJACK2 = 'dj2'
# MONOJACK3 = 'dj3'

# BRAGG = 'bragg'  # the "virtual" bragg axis defined in the PMAC coordinate system
# FBRAGG = 'fbragg'  # the "virtual" bragg axis defined in the PMAC coordinate system
# XTAL1_ROLL = 'xl1roll'
# XTAL2_PERP = 'xl2perp'  # monp in SPEC
# XTAL2_PITCH = 'xl2ptch'
# XTAL2_ROLL = 'xl2roll'


# ML2_LONG = 'ml2long'  # mon_z in SPEC
# ML2_PERP = 'ml2perp'  # mon_y in SPEC
# ML2_PITCH = 'ml2ptch'
# ML2_ROLL = 'ml2roll'


# OH_SLITS = ('s1', 's2', 's3',)

# M1_JACKS = ('m1jl', 'm1jr', 'm1jf')
# M1_LAT = ('m1latf', 'm1latb')  # keep that order!
# M1_BEND = 'm1bends'
# M2_JACKS = ('m2jl', 'm2jr', 'm2jf')
# M2_LAT = ('m2latf', 'm2latb')  # keep that order!
# M2_BEND = 'm2bends'
