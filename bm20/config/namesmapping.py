class NamesMapping:
    name = property(lambda self: self._name)
    config = property(lambda self: self._config)

    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self._map = {}

        for key in self._config.get("mapping", {}).keys():
            kup = key.upper()
            self._map[kup] = key
            setattr(self, kup, None)

    def __getattribute__(self, name):
        try:
            bliss_name = super().__getattribute__("_map")[name]
            return self._config["mapping"][bliss_name]
        except KeyError:
            return super().__getattribute__(name)