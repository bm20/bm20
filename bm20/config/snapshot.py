import functools
from datetime import datetime
from collections import OrderedDict
from itertools import chain
from tabulate import tabulate, SEPARATING_LINE
from contextlib import contextmanager

from bliss.config.settings import ParametersWardrobe, QueueSetting
from bliss.common.axis import Axis
from bliss.config.plugins.generic import ConfigItemContainer
from bliss.shell.getval import getval_idx_list, getval_yes_no, getval_int_range
from bliss.shell.standard import umv


class AxisSnapshotGroup:
    # TODO: initial checks
    axes = property(lambda self: self._axes)
    name = property(lambda self: self._name)
    desc = property(lambda self: self._desc)

    def __init__(self, name, config):
        self._name = name
        self._desc = config.get("desc", name)
        axes = [ax for ax in config["items"]]
        for ax in axes:
            if not isinstance(ax, (Axis,)):
                raise RuntimeError(f"Object {ax} is not an Axis.")
        self._axes = axes[:]

    def item_names(self):
        return [ax.name for ax in self.axes]

    def data(self):
        return {axis.name:{"position":axis.position} for axis in self.axes}
    
    def diff_table(self, data):
        table = []
        for ax in self.axes:
            ax_data = data[ax.name]
            if ax_data is not None:
                table += [[ax.name, ax.position, data[ax.name].get("position")]]
        # table = [[ax.name, ax.position, data[ax.name].get("position")]
        #     for ax in self.axes]
        return table
    
    def restore(self, data):
        args = []
        for ax in self.axes:
            target = data.get(ax.name)
            if target is not None:
                args += [ax, target["position"]]
            else:
                print(f"WARNING. {self.name}: no position given for {ax.name}.")
        umv(*args)
    

@contextmanager
def wardrobe_select(wardrobe, instance="default"):
    """
    Context that selects a wardrobe, then
    reset it to the one that was selected before.
    """
    current = wardrobe.current_instance
    try:
        yield wardrobe.switch(instance)
    finally:
        wardrobe.switch(current)
    

@contextmanager
def wardrobe_iter(wardrobe, sort=True):
    """
    Context that returns an iterator over the given wardrobe instances.
    If sort=True the instance names will be sorted.
    """
    current = wardrobe.current_instance
    try:
        yield _wardrobe_iter(wardrobe, sort=sort)
    finally:
        wardrobe.switch(current)


def _wardrobe_iter(wardrobe, sort=True):
    if sort:
        instances = sorted(wardrobe.instances,
                           key=lambda entry: int(entry.split(" ", 1)[0])
                                             if entry != "default" else -1)
    else:
        instances = wardrobe.instances
    for instance in instances:
        if instance == "default":
            continue
        wardrobe.switch(instance)
        yield wardrobe


def match_snapshot(wardrobe, index=None, comment=None, **kwargs):
    match = True
    info = wardrobe.snapshot_info_
    user = wardrobe.snapshot_user_
    if index is not None:
        match &= info["index"] == index

    # TODO: regex
    if match and comment:
        match &= info["comment"].find(comment) != -1

    if match and kwargs:
        for key, value in kwargs.items():
            try:
                if user[key] != value:
                    match = False
                    break
            except KeyError:
                # TODO: should we raise if keyword absent?
                print(f"In snapshot {wardrobe.current_instance}, user key {key} not found. Ignoring.")
                pass
    return match


class Snapshot:
    """
    Class that helps saving and restoring some beamline parameters.
    Only motors at the moment.
    """
    name = property(lambda self: self._name)
    config = property(lambda self: self._config)
    entries = property(lambda self: [e for e in self._snapshots.instances
                                     if e != "default"])

    def __init__(self, name, config):
        # super().__init__(config)
        self._config = config
        self._name = name
        self._axes = None
        self._groups = None
        self._save_menu_obj = None
        self._restore_menu_obj = None
        self._snapshots_wr = None

    @property
    def n_snapshots(self):
        with wardrobe_select(self._snapshots):
            return self._snapshots.snapshot_info_["index"] + 1

    # def _create_subitem_from_config(
    #     self, name, cfg, parent_key, item_class, item_obj=None
    # ):
    #     if parent_key == "axes":
    #         group = item_class(name, cfg)
    #         self._groups[name] = group
    #         return group
        
    # def _get_subitem_default_class_name(self, cfg, parent_key):
    #     if parent_key == "axes":
    #         return "AxisSnapshotGroup"
    #     raise RuntimeError(f"Keyword not supported: {parent_key}.")

    @property
    def _snapshots(self):
        if self._snapshots_wr is None:
            snapshots = ParametersWardrobe(f'snapshots_{self.name}')
            groups = self.groups

            snap_keys = set(snapshots.to_dict().keys())
            item_names = set(chain.from_iterable([grp.item_names()
                                                for grp in groups.values()]))

            if "snapshot_info_" not in snap_keys:
                snapshots.add("snapshot_info_", {"index": -1})

            if "snapshot_user_" not in snap_keys:
                snapshots.add("snapshot_user_", {})

            for name in item_names - snap_keys:
                snapshots.add(name, None)

            self._snapshots_wr = snapshots
        return self._snapshots_wr
    
    def _add_keywords(self, keywords):
        snapshots = self.snapshots
        for key in keywords:
            snapshots.add(key)

    def __info__(self):
        txt = "================\n"
        txt += f"AxisSnapshotGroup: {self.name}\n"

        table = []
        with wardrobe_iter(self._snapshots) as wr_iter:
            for instance in wr_iter:
                if instance.current_instance != "default":
                    info = instance.snapshot_info_
                    idx = info["index"]
                    date = info["date"]
                    comment = info["comment"]
                    table += [[idx, date, comment]]
        
        txt += tabulate(table, headers=["idx", "date", "comment"])
        return txt

    def save_snapshot(self, comment, **kwargs):
        _get_keywords = kwargs.pop("_get_keywords", None)
        date_str = kwargs.pop("date_str")

        groups = self.groups
        snap = self._snapshots
        snap.switch("default")
        snapshot_info = snap.snapshot_info_
        last_index = snapshot_info["index"] + 1

        if not date_str:
            now = datetime.now()
            epoch = datetime.now().timestamp()
            date_str = now.strftime("%Y-%m-%dT%H:%M:%S")

        snapshot_title = f"{last_index} - {date_str} - {comment}"
        snap.switch(snapshot_title)

        try:
            this_info = snap.snapshot_info_
            this_user = snap.snapshot_user_
            this_info["index"] = last_index
            this_info["epoch"] = epoch
            this_info["comment"] = comment
            this_info["date"] = date_str

            for group in groups.values():
                data = group.data()
                for name, value in data.items():
                    setattr(snap, name, value)

            # TODO: in what order should we do this?
            # let the function overwrite or not kwargs?
            if _get_keywords:
                kwargs = _get_keywords(data, kwargs)
            this_user.update(kwargs)

            snap.snapshot_user_ = this_user
            snap.snapshot_info_ = this_info
        except:
            snap.switch("default")
            snap.remove(snapshot_title)
            raise
        else:
            snap.switch("default")
            snapshot_info["index"] = last_index
            snap.snapshot_info_ = snapshot_info

    def select(self, index):
        snapshots = self._snapshots

        current = snapshots.current_instance
        snapshots.switch("default")
        n_entries = snapshots.snapshot_info_["index"]
        snapshots.switch(current)

        if index < 0 or index > n_entries:
            return {}

        with wardrobe_iter(snapshots) as wr_iter:
            for instance in wr_iter:
                if (instance.snapshot_info_["index"] == index
                    and instance.current_instance != "default"):
                    return self._snapshots.to_dict()
        raise RuntimeError(f"{self.name}: entry #{index} not found.")

    def search(self, comment=None, **kwargs):
        with wardrobe_iter(self._snapshots) as wr_iter:
            return OrderedDict([(instance.current_instance, instance.to_dict())
                    for instance in wr_iter
                    if match_snapshot(instance, comment=comment, **kwargs)])
        
    def print_search(self, comment=None, **kwargs):
        results = self.search(comment=comment, **kwargs)
        table = [[value["snapshot_info_"]["index"],
                  value["snapshot_info_"]["date"],
                  value["snapshot_info_"]["comment"]] for value in results.values()]
        print(tabulate(table, headers=["index", "date", "comment"]))

    @property
    def groups(self):
        # TODO: check uniqueness of items across all groups
        if self._groups is None:
            self._groups = {}
            for axes_cfg in self.config.get("axes"):
                label = axes_cfg["label"]
                self._groups[label] = AxisSnapshotGroup(label, axes_cfg)
        return self._groups

    def restore(self, entry, group_names=None):
        if isinstance(entry, (int,)):
            entry = self.select(entry)
        if not isinstance(entry, (dict,)):
            raise RuntimeError(f"{self.name}: expected a dictionary.")
        if group_names is None:
            groups = self.groups.values()
        else:
            groups = [self.groups[group_name] for group_name in group_names]
        for group in groups:
            group.restore(entry)


class SnapshotMenu(ConfigItemContainer):
    labels = property(lambda self: self._labels)
    keywords = property(lambda self: self._keywords)
    keywords_names = property(lambda self: list(self.keywords.keys()))
    snapshots = property(lambda self: self._snapshot)
    options = property(lambda self: self._options)
    
    def __init__(self, config):
        super().__init__(config)
        self._labels = config.get("labels", {})
        self._snapshot = config["snapshot"]
        self._keywords = config["keywords"]

        options = config.get("options")

        if options:
            self._options = OrderedDict({"all": {"desc":"Restore all",
                                                 "groups": None}})
            # check for uniqueness of keys
            self._options.update(options)
        else:
            self._options = {}

    def _create_subitem_from_config(
        self, name, cfg, parent_key, item_class, item_obj=None
    ):
        if parent_key == "save":
            save = item_class(self, name, cfg)
            self._save = save
            return save
        if parent_key == "restore":
            restore = item_class(self, name, cfg)
            self._restore = restore
            return restore
        
    def _get_subitem_default_class_name(self, cfg, parent_key):
        if parent_key == "save":
            return "SnapshotSaveMenu"
        if parent_key == "restore":
            return "SnapshotRestoreMenu"
        raise RuntimeError(f"Keyword not supported: {parent_key}.")
    
    def _get_subitem_default_module(self, class_name, cfg, parent_key):
        package = cfg.get("package")
        if package:
            return package
        return super()._get_subitem_default_module(self, class_name, cfg, parent_key)
    
    def keyword_allowed_values(self, keyword):
        return self._keywords.get(keyword, [])
    
    def save(self, *args, **kwargs):
        if self._save:
            _get_keywords = self._save.keywords_values
        else:
            _get_keywords = None
        self.snapshots.save_snapshot(*args,
                                     _get_keywords=_get_keywords,
                                     **kwargs)
        
    def selection_menu(self, comment=None, **kwargs):
        while True:
            selection = _selection_menu(self, comment=comment, **kwargs)
            if selection:
                if self.restore_entry_menu(selection):
                    return
            else:
                return

    def restore_entry_menu(self, entry):
        if isinstance(entry, (int,)):
            entry = self.snapshot.select(entry)
        if not isinstance(entry, (dict,)):
            raise RuntimeError(f"{self.name}: expected a dictionary.")
        return _restore_entry_menu(self, entry)


class SnapshotSaveMenu:
    name = property(lambda self: self._name)
    config = property(lambda self: self._config)
    snapshotmenu = property(lambda self: self._snapshotmenu)

    def __init__(self, snapshotmenu, name, config):
        self._name = name
        self._config = config
        self._snapshotmenu = snapshotmenu
        for label, keywords in self.snapshotmenu.labels.items():
            self._add_label_method(label, **keywords)

    def _add_label_method(self, label, **kwargs):
        """Add a method named after the position label to move to the
        corresponding position.
        """
        if label.isidentifier():
            setattr(
                self,
                label,
                functools.partial(self, **kwargs),
            )
        else:
            raise RuntimeError(f"'{label}' is not a valid python identifier.")
        
    def __call__(self, comment=None, **kwargs):
        self.snapshotmenu.save(comment=comment, **kwargs)

    def keywords_values(self, snap_dict, keywords):
        keywords = self._keywords_values(snap_dict, keywords)
        self._validate_keywords(keywords)
        keys = list(keywords.keys())
        for key in self.snapshotmenu.keywords.keys():
            if key not in keys:
                keywords[key] = None
        return keywords
        
    def _keywords_values(self, snap_data, keywords):
        return keywords
    
    def _validate_keywords(self, keywords):
        cfg_keywords = self.snapshotmenu.keywords
        if not keywords:
            return True
        for key, value in keywords.items():
            if key not in cfg_keywords:
                raise ValueError(f"{self._name}: unknown keyword: {key}.")
            kwd_values = cfg_keywords.get(key, [value])
            if value not in kwd_values:
                raise ValueError(f"{self._name}: invalid value for "
                                 f"keyword {key}: {value}."
                                 f"Allowed values: {kwd_values}")
        return True


class SnapshotRestoreMenu:
    name = property(lambda self: self._name)
    config = property(lambda self: self._config)
    snapshotmenu = property(lambda self: self._snapshotmenu)

    def __init__(self, snapshotmenu, name, config):
        self._name = name
        self._config = config
        self._snapshotmenu = snapshotmenu
        for label, keywords in self.snapshotmenu.labels.items():
            self._add_label_method(label, **keywords)

    def _add_label_method(self, label, **kwargs):
        """Add a method named after the position label to move to the
        corresponding position.
        """
        if label.isidentifier():
            setattr(
                self,
                label,
                functools.partial(self, **kwargs),
            )
        else:
            raise RuntimeError(f"'{label}' is not a valid python identifier.")
        
    def __call__(self, comment=None, **kwargs):
        self.snapshotmenu.selection_menu(comment=comment, **kwargs)

    def __info__(self):
        txt = "================\n"
        txt += f"AxisSnapshotGroup: {self.name}\n"

        kwrds = list(self.snapshotmenu.keywords_names)

        table = []
        with wardrobe_iter(self.snapshotmenu.snapshots._snapshots) as wr_iter:
            for instance in wr_iter:
                if instance.current_instance != "default":
                    info = instance.snapshot_info_
                    user = instance.snapshot_user_
                    idx = info["index"]
                    date = info["date"]
                    comment = info["comment"]
                    user_keys = [user.get(key) for key in kwrds]
                    table += [[idx, date, comment] + user_keys]
        
        txt += tabulate(table, headers=["idx", "date", "comment"] + kwrds)
        return txt


def _selection_menu(snapshotsmenu, comment=None, **kwargs):
    snapshots = snapshotsmenu.snapshots
    filters = kwargs.copy()
    filters["comment"] = comment
    while True:
        print("=== Restore menu ===")
        print(f" Snapshot [{snapshots.name}]")
        results = snapshots.search(**filters)

        table = []
        kwrds = snapshotsmenu.keywords_names

        for result in results.values():
            info = result["snapshot_info_"]
            user = result["snapshot_user_"]
            idx = info["index"]
            date = info["date"]
            comment = info["comment"]
            user_keys = [user.get(key) for key in kwrds]
            table += [[idx, date, comment] + user_keys]
        
        print(tabulate(table, headers=["idx", "date", "comment"] + kwrds))
    
        menu_kwds = ["comment"] + snapshotsmenu.keywords_names

        # applied = f"[comment=[{comment}]; "
        applied = "[" + "; ".join([f"{kwd}=[{filters.get(kwd, None)}]" for kwd in menu_kwds])
        applied += "]"
        print("-------")
        print("> Applied filters:")
        print("> " + applied)
        print("-------")

        choices = ["select an index to restore",
                   "edit filters",
                   "quit"]
        ans = getval_idx_list(choices, "Command:", default=3)[0]
        if ans == 3:
            return
        if ans == 2:
            filters = _filters_submenu(snapshotsmenu, **filters)
            continue
        if ans == 1:
            ans = getval_int_range("Index of entry to restore (-1 to cancel)",
                                    minimum=0,
                                    default=-1,
                                    maximum=snapshots.n_snapshots - 1)
            if ans == -1:
                continue
            entry = snapshots.select(ans)
            return entry
        

def _filters_submenu(snapshotsmenu, **kwargs):
    menu_kwds = ["comment"] + snapshotsmenu.keywords_names
    filters = kwargs.copy()

    while True:
        table = []
        for ikwd, kwd in enumerate(menu_kwds):
            table += [[ikwd, kwd, filters.get(kwd, "*")]]
        print(tabulate(table, headers=["Id", "Filter", "Current"]))

        choices = [kw for kw in menu_kwds] + ["> Apply"]

        ans = getval_idx_list(choices, "Select a filter to edit:",
                              default=len(choices))[0]

        if ans == len(choices):
            break

        kw = list(menu_kwds)[ans - 1]
        allowed = snapshotsmenu.keyword_allowed_values(kw)
        if allowed:
            choices = [value for value in allowed] + ["> Clear", "> Cancel"]
            ans = getval_idx_list(choices, "Value:",
                                  default=len(choices))[0]
            if ans == len(choices):
                continue
            if ans == len(choices) - 1:
                filters.pop(kw)
                continue
            filters[kw] = choices[ans - 1]
        else:
            ans = input("Enter value:")
            if not ans:
                filters.pop(kw)
            else:
                filters[kw] = ans

    return filters


def _restore_entry_menu(snapshotsmenu, entry_dict):
    options = snapshotsmenu.options
    while True:
        groups = None
        if options:
            choices = [option["desc"] for option in options.values()]
            choices += ["Cancel"]
            ans = getval_idx_list(choices, "Select an option:",
                                  default=len(choices))[0]
            if ans == len(choices):
                return False
            
            choice = list(options.values())[ans - 1]
            groups = choice.get("groups")
            
        if not groups:
            # restoring all
            groups = snapshotsmenu.snapshots.groups

        print("This will be applied:")
        table = []
        for group in groups.values():
            diff_table = group.diff_table({key:entry_dict[key] for key in group.item_names()})
            grp_table = [[group.name] + diff_table[0]]
            grp_table += [[""] + diff for diff in diff_table[1:]]
            if table:
                table += SEPARATING_LINE
            table += grp_table
        print(tabulate(table, headers=["group", "item", "current", "snap"], tablefmt="simple"))

        if not getval_yes_no("Continue?"):
            continue

        snapshotsmenu.snapshots.restore(entry_dict, group_names=list(groups.keys()))
        return True
