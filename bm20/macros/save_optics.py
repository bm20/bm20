from bm20.config.names import bm20names

def _save_optics(hutch, comment, date=None):
    save = bm20names.snapsave

    if hutch == 1:
        bm20save.eh1(comment, date=date)
    else:
        bm20save.eh2(comment, date=date)


def save_eh1(comment):
    """
    Saves the beamline state.
    Tag: EH1.
    """
    save = bm20names.snapsave
    save.eh1(comment)


def save_eh2(comment):
    """
    Saves the beamline state.
    Tag: EH2.
    """
    save = bm20names.snapsave
    save.eh2(comment)


MIRRNAMES = {
    0: None,
    1: "Rh",
    2: "Si",
    3: "Pt"
}

# CRYSTALS = {
#     0: None,
#     1: "Si(311)",
#     2: "Si(111)/0deg",
#     3: "Si(111)/30deg",
#     4: "ML(24.8A)",
#     5: "ML(36.8A)",
# }


def restore_optics(num=None, comment=None):
    """
    Entry macro for the restoration of optics configuration.
    Port of the old restore_optics SPEC macro.
    * num is a 4 digits number:
        - hutch [0, 1, 2]
        - mirror 1 [0, 1, 2]  (na, Rh, Si, Pt)
        - Mono crystal [0, 1, 2, 3, 4, 5]  (na, DCM1, DCM2, DCM3, DMM1, DMM2) 
        - mirror 2 [0, 1, 2, 3]  (na, Rh, Si, Pt)
    * comment is a string to search in the comment field of the snapshots
    """
    hutch, mir1, xtal, mir2 = [int(i) for i in f"{num}"]
    mir_names = list(MIRRNAMES.keys())
    if not hutch in (0, 1, 2):
        raise ValueError(f"Invalid hutch number {hutch}. Allowed: [0, 1, 2].")

    if not mir1 in mir_names:
        raise ValueError(f"Invalid mirror 1 number {mir1}. Allowed: {mir_names}.")
    
    if not mir2 in mir_names:
        raise ValueError(f"Invalid mirror 2 number {mir2}. Allowed: {mir_names}.")

    restore = bm20names.snaprestore
    restore(comment=comment, hutch=hutch, mir1=mir1, crystal=crystal, mir2=mir2)