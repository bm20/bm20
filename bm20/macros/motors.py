"""
Misc helpers for motors.
"""

from time import monotonic
from itertools import chain
from tabulate import tabulate

import gevent

import numpy as np

from bliss.config.static import get_config
from bliss.shell.standard import umvr, umv, mv
from bliss.common.cleanup import error_cleanup
from bliss.controllers.motors.icepap import Icepap
from bliss.common.axis import Axis


from bliss.common.logtools import (disable_user_output,
                                   user_info)


def get_config_object(mne):
    return get_config().get(mne)


def axes_to_names(*axes):
    """
    Returns a list of axes names objects from their Axis instance.
    Elements can already be a str they're just returned.
    """
    return [name.name if isinstance(name, (Axis,)) else name
            for name in axes]


def names_to_axes(*axes_names):
    """
    Returns a list of Axis objects from their name.
    Elements can already be Axis objects, in that case
    they're just returned.
    """
    return [get_config_object(name) if not isinstance(name, (Axis,)) else name
            for name in axes_names]


class AxisMoveTimeout:
    """
    inspired from bliss standard.py
    """
    def __init__(self, *axes, timeout_s=5.):
        self._axes = axes[:]
        self._prev_t = None
        self._positions = np.array([a.position for a in axes])
        self._tol = np.array([a.tolerance for a in axes])
        self._timeout_s = timeout_s
        
    def check_move(self, sync_hard=False):
        def _pos(axis):
            if sync_hard:
                axis.sync_hard()
            return axis.position
        positions = np.array([_pos(a) for a in self._axes])
        cur_t = monotonic()
        still = abs(positions - self._positions) <= self._tol
        if self._prev_t is not None and np.all(still):
            if cur_t >= self._prev_t + self._timeout_s:
                names = [a.name for a in self._axes]
                raise RuntimeError("Timeout: some of the following axes have not "
                                   f"been moving for {self._timeout_s}: {names}")
        else:
            self._prev_t = cur_t
        self._positions = positions


class AxisPositionPrinter:
    """
    inspired from bliss standard.py
    """
    def __init__(self, *motors):
        self._motors = motors
        motor_names = [motor.name for motor in motors]
        self._motor_names = motor_names
        self._col_len = max(max(map(len, motor_names)), 8)
        self._hfmt = f'^{self._col_len}'
        self._rfmt = f'>{self._col_len}.03f'

    def print_names(self):
        rows = '  '.join([format(col, self._hfmt) for col in self._motor_names])
        print(rows)

    def print_positions(self, fmt_fn=None, sync_hard=False, state=False):
        # if fmt_fn is None:
        #     fmt_fn = self._rfmt
        def _pos(axis):
            if sync_hard:
                axis.sync_hard()
            return axis.position
        positions = [_pos(motor) for motor in self._motors]
        rows = [format(col, self._rfmt) for col in positions]
        if fmt_fn:
            rows = [fmt_fn(axis, row, self._col_len)
                    for axis, row in zip(self._motors, rows)]
        row_txt =  '  '.join(rows)
        print("\r" + row_txt, end="", flush=True)


class SingleAxisStatusPrinter:
    """
    inspired from bliss standard.py
    """
    def __init__(self, motor, elapsed=True, col_width=12):
        self._motor = motor
        self._motor_name = motor.name
        self._col_width = col_width
        self._hfmt = f'^{self._col_width}'
        self._rfmt = f'>{self._col_width}.03f'
        self._t0 = monotonic()
        self._last_state_names = None

    def print_name(self):
        print(f'{self._motor_name:{self._hfmt}}')

    def print_status(self, sync_hard=False):
        if sync_hard:
            self._motor.sync_hard()
        states = ', '.join(self._motor.state.current_states_names)
        if self._last_state_names is not None and states != self._last_state_names:
            print('')
        self._last_state_names = states
        print(f'\r{self._motor.position:{self._rfmt}} - [{self._last_state_names}]', end="", flush=True)


def is_homed(axis):
    """
    Returns True if the given axis is homed.
    Supported axis:
    - IcePap
    - TurboPmac
    Icepap: tests the availablitiy of "home_found_dial"
    TurboPmac: checks the state for "HOMED"

    """
    # from ..controllers.turbopmac import TurboPmac
    from bliss.controllers.motors.turbopmac import TurboPmac

    if isinstance(axis.controller, (Icepap,)):
        try:
            axis.home_found_dial()
        except RuntimeError:
            return False
        return True
    if isinstance(axis.controller, (TurboPmac,)):
        return 'HOMED' in axis.state
    raise ValueError(f'Unknown controller type: {axis.controller.__class__}.')


def group_umvr(distance, *axes):
    umvr(*chain(*zip(axes, [distance] * len(axes))))


def group_umv(position, *axes):
    umv(*chain(*zip(axes, [position] * len(axes))))


def parallel_group_mvr(distance, *axes, silent=True):
    parallel_mvr(*chain(*zip(axes, [distance] * len(axes))), silent=True)


def parallel_group_mv(distance, *axes, silent=True):
    parallel_mv(*chain(*zip(axes, [distance] * len(axes))), silent=True)


def parallel_mvr(*args, **kwargs):
    """
    Calls mvr in parallel on n axis.

    Args:
    args: axis_0, distance_0, axis_1, distance_1, ...
    """
    # TODO : check args
    silent = kwargs.get('silent', True)
    motor_list = list(zip(*[iter(args)] * 2))
    axes_list = [elem[0] for elem in motor_list]

    pos_printer = AxisPositionPrinter(*axes_list)

    def _mvr(ax, dist):
        with disable_user_output():
            mv(ax, dist)

    with error_cleanup(*axes_list):
        gl = [gevent.spawn(_mvr, axis, distance)
              for axis, distance in motor_list]

        gevent.sleep(0.5)
        pos_printer.print_names()

        position_reached = False
        while not position_reached:
            position_reached = len([g for g in gl if g]) == 0
            pos_printer.print_positions()
            gevent.sleep(0.2)

    pos_printer.print_positions()
    #_print_elapsed(print, t0, axes_list)
    print('')


def parallel_mv(*args, **kwargs):
    """
    Calls mv in parallel on n axis.

    Args:
    args: axis_0, distance_0, axis_1, distance_1, ..., silent=True
    """
    # motor_list = list(zip(*[iter(args)] * 2))
    # axes_list = [elem[0] for elem in motor_list]
    

    axes, positions = [], []
    for i, x in enumerate(args):
        (axes, positions)[i % 2].append(x)
    axes = names_to_axes(*axes)
    mv_args = chain(*zip(axes, positions))
    umv(*mv_args)

#     # TODO : check args
#     silent = kwargs.get('silent', True)
#     motor_list = list(zip(*[iter(args)] * 2))
#     axes_list = [elem[0] for elem in motor_list]
#     axes_list = names_to_axes(*axes_list)

#     pos_printer = AxisPositionPrinter(*axes_list)

#     def _mv(ax, dist):
#         with disable_user_output():
#             mv(ax, dist)

#     with error_cleanup(*axes_list):
#         # gl = [gevent.spawn(_mv, axis, distance)
#         #       for axis, distance in motor_list]

#         # gevent.sleep(0.5)
#         pos_printer.print_names()

#         position_reached = False
#         while not position_reached:
#             position_reached = len([g for g in gl if g]) == 0
#             pos_printer.print_positions()
#             gevent.sleep(0.2)

#     pos_printer.print_positions()
#     #_print_elapsed(print, t0, axes_list)
#     print('')


def move_to_limit(limit, *axes, silent=True):
    """
    Moves a list of axis to their limit, 
    TODO: timeout

    Args:
    limit (int): -1 or 1, direction of the limit search
    axes: all the axes that need to be moved
    """
    assert limit in (-1, 1)

    axes_list = axes
    axes_names = [axis.name for axis in axes_list]

    if limit > 0:
        lim_str = 'lim+'
        lim_state = 'LIMPOS'
    else:
        lim_str = 'lim-'
        lim_state = 'LIMNEG'

    with error_cleanup(*axes_list):
        pos_printer = AxisPositionPrinter(*axes_list)
        pos_timeout = AxisMoveTimeout(*axes_list)

        print(f'Moving {axes_names} to {lim_str}.')

        # with disable_lprint(silent):
        for axis in axes_list:
            axis.hw_limit(limit, wait=False)

        gevent.sleep(1)

        print(f'============ {lim_str} ============')
        pos_printer.print_names()

        fmt_fn = lambda ax, s, w: f'{lim_str:^{w}}' if lim_state in ax.state else s

        limit_reached = False
        while not limit_reached:
            lim_reached = [axis_limit_reached(limit, axis) for axis in axes_list]
            pos_printer.print_positions(fmt_fn=fmt_fn, sync_hard=True)
            limit_reached = all(lim_reached)
            pos_timeout.check_move()

        print('')

    for axis in axes_list:
        axis.sync_hard()


def axis_limit_reached(limit, axis):
    assert limit in (-1, 1)
    state = axis.state
    if limit == -1:
        return 'LIMNEG' in state or 'SCLINNEG' in state
    return 'LIMPOS' in state or 'SCLIMPOS' in state
    

def home_search(direction, *axes, home_state=None, silent=True):
    """
    # warning : test for success is either:
      - HOME in axis state
      - home_found_dial doesnt raise a RuntimeException
    Start home search on the given axes.
    TODO: timeout

    Args:
    direction (int): -1 or 1, direction of the home search
    axes: all the axes that need to be moved
    """
    assert direction in (-1, 1)

    axes_list = axes

    if direction > 0:
        home_str = 'home+   '
    else:
        home_str = 'home-   '

    print(f'Looking for {home_str}')
    if home_state is not None:
        print(f'Expected axis state: {home_state}.')

    def _home(ax):
        with error_cleanup(ax):
            # with disable_lprint(silent):
            with disable_user_output():
                ax.home(direction, wait=True)

    # for ax in axes:
    #     print(f'State: {ax.name}, {ax.state}')

    pos_printer = AxisPositionPrinter(*axes_list)

    print(f'Moving to {home_str}.')

    with error_cleanup(*axes_list):
        for axis in axes_list:
            # gevent.spawn(_home, axis)
            # with disable_lprint(silent):
            with disable_user_output():
                axis.home(direction, wait=False)

        gevent.sleep(1)

        print(f'============ {home_str} ============')
        pos_printer.print_names()

        def _homed(ax):
            state = ax.state
            if 'MOVING' in state:
                return False
            if home_state is not None and home_state in state:
                return True
            elif isinstance(ax.controller, (Icepap,)):
                # for IcePAP
                try:
                    # need to find how to check on the "homed" state
                    # when the motor is running.
                    _ = ax.home_found_dial()
                    return True
                except RuntimeError:
                    pass
            return None
            # raise RuntimeError(f'Error while checking home state for {ax.name}:'
            #                     ' unsupported controller '
            #                     'type: {ax.controller.__class__.}')

        def fmt_fn(ax, s, w):
            h = _homed(ax)
            if h is None:
                s = 'ERROR'
                return f'{s:^{w}}'
            if h is True:
                return f'{home_str:^{w}}'
            return s

        pos_timeout = AxisMoveTimeout(*axes_list)
        home_reached = False
        while not home_reached:
            homed = [_homed(axis) for axis in axes_list]
            stopped = ['MOVING' not in axis.state for axis in axes_list]

            try:
                pos_printer.print_positions(fmt_fn=fmt_fn, sync_hard=True)
            except Exception as ex:
                print('#######')
                print(ex)
            pos_timeout.check_move()
            home_reached = all(homed) or all(stopped)

    # keep this sleep until we implement a wait on more than one state
    # e.g: HOMED + READY on the pmac, otherwise sometimes the motor
    # is HOMED but not READY yet.
    gevent.sleep(1)

    for axis in axes_list:
        axis.sync_hard()

    print('')


def set_home_position(axes, home_positions=None):
    axes = names_to_axes(*axes)

    if not home_positions:
        home_positions = [axis.config.get("home_position", float, None)
                          for axis in axes]
    if len(home_positions) != len(axes):
        raise RuntimeError("Number of axes and home positions "
                            "should be the same")
    positions = [[axis.name, axis.position, pos]
                  for axis, pos in zip(axes, home_positions)]
    print('Settings motors dial to the home positions defined in the config.')
    tabu = tabulate(positions, headers=['name', 'current', 'home_pos'])
    print(tabu)

    for idx, axis in enumerate(axes):
        home_position = positions[idx][2]
        if home_position is not None:
            axis.dial = home_position
            axis.offset = 0


def home_from_limit(limit,
                    axes_names,
                    set_home_pos_on_error=False,
                    home_state=None):
    """
    Home search from the hard limit.
    The home position for each axis will be set to the
    "home_position" value found in the yaml (position left as is
    set if that value is not found).
    """
    assert limit in (-1, 1)

    home_direction = -1 * limit

    axes = names_to_axes(*axes_names)
    axes_names = axes_to_names(*axes)

    print('##########################################')
    print('##########################################')
    user_info(f'Homing {axes_names}.')
    print('##########################################')
    move_to_limit(limit, *axes)
    gevent.sleep(2)
    print('##########################################')
    wait_move(*axes)
    home_search(home_direction, *axes, home_state=home_state)
    gevent.sleep(2)
    #wait_sync(*axes)
    print('##########################################')
    homed_axes = [axis for axis in axes if is_homed(axis)]
    diff_axes = set(axes) - set(homed_axes)
    print('##########################################')
    if set_home_pos_on_error:
        homed_axes = axes[:]
    home_positions = [axis.config.get("home_position", float, None)
                      for axis in homed_axes]
    if home_positions:
        set_home_position(homed_axes, home_positions)
    gevent.sleep(2)
    print('##########################################')

    if diff_axes:
        user_info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        user_info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('ERROR, some axes could not be homed:\n - '
              + '\n - '.join([f'{axis.name}' for axis in diff_axes]))
        if(set_home_pos_on_error):
            print("But user asked to set their dial anyway.")
        user_info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        user_info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')


def wait_sync(*axes):
    gevent.sleep(1)
    for axis in axes:
        axis.sync_hard()

def wait_move(*axes, timeout_s=5):
    # TODO: timeout
    for ax in axes:
        ax.wait_move()


# =========================================
# =========================================
# Misc.
# =========================================
# =========================================


def _bd(ax):
        # print(ax.acceleration, ax.velocity, ax.steps_per_unit, ax.sign)
        return (ax.acctime * ax.velocity) * np.sign(ax.steps_per_unit * ax.sign)


def find_limits(*axes):
    """
    Moves given axes into their limits and sets the limit as two times
    the break distance (once the break distance itself (i.e. the tigger point)
    and once as a safety margin) before the current position as software limit.

    Calculation of break distance (br):<br>
    s = 1/2 a*t^2;
    a = v*t;
    br = 2*s = 2*1/2*v*t = v*t
    """

    for axis in axes:
        axis.limits = None, None

    move_to_limit(1, *axes)
    plimits = [axis._hw_position - _bd(axis) for axis in axes]

    move_to_limit(-1, *axes)
    nlimits = [axis._hw_position + _bd(axis) for axis in axes]

    for i, axis in enumerate(axes):
        axis.limits = nlimits[i], plimits[i]