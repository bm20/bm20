from bliss.shell.standard import dscan

# from bm20.config import names as bm20_names


from bm20.config.names import bm20names

from bm20.oh.attenuators import show_attenuators
from bm20.oh.mirrors import show_mirrors


def show_optics():
    mir1 = bm20names.MIR_SURF_1
    mir2 = bm20names.MIR_SURF_2
    crystal = bm20names.CRYSTAL.crystal
    print("========")
    print("Current configuration:\n"
          f"  M1  : {mir1.position}\n"
          f"  DCM : {crystal.name} (d={crystal.d_spacing} A)\n"
          f"  M2  : {mir2.position}")
    print("========")
    # print "\nType \"help showoptics\" to get help on the different configurations."


def rocking_curve(axis, d_start, d_end, intervals, counter, count_time=0.1):
    """
    Starts a rocking curve on the given axis, 
    """
    print(f"Rocking curve: dscan({axis.name}, {d_start}, {d_end}, {intervals}, {count_time}, {counter.name})")
    s = dscan(axis, d_start, d_end, intervals, count_time, counter)
    print(f"Moving {axis.name} to {s.cen(counter)}")
    s.goto_cen(counter, axis=axis)


def rc_xl2pitch_i0():
    """
    Rocking curve on XTAL2_PITCH using I0: -0.006, 0.006, 80, 0.1
    """
    rocking_curve(bm20names.XTAL2_PITCH, -0.006, 0.006, 80, bm20names.I0, count_time=0.1)


def rc2():
    """
    Rocking curve on XTAL2_PITCH using I0: -0.004, 0.004, 30, 0.1
    """
    rocking_curve(bm20names.XTAL2_PITCH, -0.004, 0.004, 30, bm20names.I0, count_time=0.1)


def rcx1():
    """
    Rocking curve on XTAL2_PITCH using IC0: -0.002, 0.002, 60, 0.1
    """
    rocking_curve(bm20names.XTAL2_PITCH, -0.002, 0.002, 60, bm20names.IC0, count_time=0.1)


def rcx2():
    """
    Rocking curve on XTAL2_PITCH using IC2: -0.003, 0.003, 60, 0.1
    """
    rocking_curve(bm20names.XTAL2_PITCH, -0.002, 0.002, 60, bm20names.IC2, count_time=0.1)


def rci():
    """
    Rocking curve on XTAL2_PITCH using IC0: -0.006, 0.006, 60, 0.2
    """
    rocking_curve(bm20names.XTAL2_PITCH, -0.006, 0.006, 60, bm20names.IC0, count_time=0.2)


def m2p():
    """
    Rocking curve on M2_PITCH using IC2: -0.004, 0.004, 40, 0.2
    """
    rocking_curve(bm20names.M2_PITCH, -0.04, 0.04, 40, bm20names.IC2, count_time=0.2)