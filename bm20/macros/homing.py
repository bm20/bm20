"""
Homing functions for optics components.
"""

__all__ = ["home_mono_latyaw",
           "home_mono_dcm",
           "home_mono_dmm",
           "home_m1_tab",
           "home_m2_tab",
           "home_m1_lat",
           "home_m2_lat",
           "home_m1_bend",
           "home_m2_bend",
        #    "home_slits",
           "home_oh_slits",
           "home_eh1_slits",
           "home_eh2_slits",
           "home_slit",
           "home_attenuators",
           "is_lateral_homed",
           "is_yaw_homed"]


import gevent
from enum import Enum

from bliss.common.cleanup import error_cleanup
from bliss.shell.standard import umv
from bliss.config.static import get_config
from bliss.common.logtools import (log_warning,
                                   user_warning,
                                   user_info,
                                   disable_user_output)


from bm20.config.names import bm20names
from .motors import (move_to_limit,
                     home_from_limit,
                     group_umv,
                     names_to_axes,
                     axis_limit_reached,
                     parallel_group_mvr,
                     set_home_position,
                     parallel_group_mv,
                     parallel_mv)
from .motors import SingleAxisStatusPrinter, AxisPositionPrinter


from .helpers import prompt_user_yes_no


# lateral and yaw home flags. Set to one by their respective PLC
# after homing.
LATERAL_HOMED_REG = 'M745'
YAW_HOMED_REG = 'M845'


class MonoHomeOffsetRegisters(Enum):
    LATERAL = 'P217'
    YAW = 'P218'


def is_lateral_homed():
    config = get_config()
    pmac = config.get(bm20names.MONO)
    return int(pmac.raw_write_read(LATERAL_HOMED_REG)) == 1


def is_yaw_homed():
    config = get_config()
    pmac = config.get(bm20names.MONO)
    return int(pmac.raw_write_read(LATERAL_HOMED_REG)) == 1


def home_mono_latyaw(force=False):
    """
    Homing of lateral and yaw axes on the DCM.
    This is done by writing to a wago channel, then waiting for
    some registers to be set on the PMAC.
    """
    config = get_config()
    pmac = config.get(bm20names.MONO)
    lateral = config.get(bm20names.LATERAL)
    yaw = config.get(bm20names.YAW)
    wago = pmac.config.get('wago')

    lat_homed = is_lateral_homed()
    yaw_homed = is_yaw_homed()

    if lat_homed:
        print("LATERAL is already homed.")
    else:
        print("LATERAL is not homed  yet.")
    if yaw_homed:
        print("YAW is already homed.")
    else:
        print("YAW is not homed  yet.")

    if not force and lat_homed or yaw_homed:
        ans = prompt_user_yes_no('Some motors already homed. Continue anyway?')
        if ans != 'y':
            print('Mono homing cancelled.')
            return
        
    home_offset = int(pmac.raw_write_read(MonoHomeOffsetRegisters.LATERAL.value))
    _home_vessel(pmac,
                 lateral,
                 wago.start_home_lat,
                 is_lateral_homed,
                 home_offset,
                 force=True)
    home_offset = int(pmac.raw_write_read(MonoHomeOffsetRegisters.YAW.value))
    _home_vessel(pmac,
                 yaw,
                 wago.start_home_yaw,
                 is_yaw_homed,
                 home_offset,
                 force=True)

    print("TODO: select default crystal.")
    # user_info(f'Selecting default crystal {D20Crystals.DCM2.name} ({D20Crystals.DCM2.value.description}).')
    # pmac.select_crystal(D20Crystals.DCM2)
    # sync()
    lateral.sync_hard()
    yaw.sync_hard()
    print('Vessel homing done.')


def _home_vessel(pmac, 
                 axis,
                 start_home_fn,
                 is_homed_fn,
                 home_offset,
                 force=False):
    """
    LATERAL and YAW common homing procedure.
    Will not do anything if the axis is already homed, unless force=True

    1. checks error wago channel
    2. clears all Mono wago channels
    3. write to the corresponding wago channel to start to homing
    4. loop: check HOMED register, display position
    5. TODO: timeout on position
    """

    wago = pmac.wago

    # checking the register.
    if is_homed_fn() and not force:
        msg = (f'It seems that the HOME register for the {axis.name} '
                f'axis is already set. Use the '
                f'"force" keyword to force the homing.')
        log_warning(pmac, msg)
        raise RuntimeError(msg)

    if wago.is_error(raise_ex=False):
        user_warning(f'Warning: the {wago.chan_error} '
                    f'channel on {wago.name} is set. Clearing it...')
        # this will raise an exception if the error reset fails.
        wago.clear_error()

    axis.sync_hard()

    print('\n====================')
    print(f'Starting homing of {axis.name} axis.')
    user_info(f'Axis will first move to neg limit, then move back {home_offset} steps.')

    # clearing all other channels
    wago.clear()

    with error_cleanup(pmac.abort_mono, verbose=True):
        try:
            start_home_fn(force=True)
            pos_printer = SingleAxisStatusPrinter(axis)
            pos_printer.print_name()
            pos_printer.print_status(sync_hard=True)

            gevent.sleep(2)

            wago.is_error()

            while not is_homed_fn():
                pos_printer.print_status(sync_hard=True)
                gevent.sleep(0.5)
        except Exception as ex:
            print('EX', ex)
            raise
    gevent.sleep(1.)
    pos_printer.print_status(sync_hard=True)
    print('')
    if is_homed_fn():
        user_info(f'INFO: {axis.name} homing completed ({axis.position}, '
                  f'home register=True)')
    else:
        user_info(f'INFO: {axis.name} homing completed ({axis.position}, '
                  f'home register=False)')
    print('====================\n')


def home_mono_dcm():
    """
    Homing of the DCM axes, driven by the PMAC:
        (NOT the ML axes, those are on ICEPAPs)
    - bragg (pmac #1)
    - xtal2perp
    - xtal1roll
    - xta2pitch
    - xtal2roll

    First moves xtal2perp out of the way (lim+) to avoid
    contact between a shield and a braid (happening after
    Oxford's intervention during summer 2019).
    """
    config = get_config()
    pmac = config.get(bm20names.MONO)

    axis_list = [bm20names.BRAGG, bm20names.XTAL1_ROLL, bm20names.XTAL2_PITCH, bm20names.XTAL2_ROLL]
    xtal2perp = config.get(bm20names.XTAL2_PERP)

    try:
        # moving x2perp to lim+
        move_to_limit(1, xtal2perp)
        # moving all other axes to lim-
        home_from_limit(-1,
                        axis_list,
                        home_state='HOMED')
        print('Moving axes to 0.')
        axis_list =  names_to_axes(*[bm20names.XTAL1_ROLL, bm20names.XTAL2_PITCH, bm20names.XTAL2_ROLL])
        group_umv(0, *axis_list)
        # wait_move(*axis_list)
        home_from_limit(-1,
                        [xtal2perp],
                        home_state='HOMED')
        
    except Exception as ex:
        print(ex)
        print('====>', ex)
        pmac.abort_mono()
        raise


def home_mono_dmm():
    """
    Homing of the DMM axes, driven by the ICEPAP:
        (NOT the DCM axes, those are on PMAC)
    """
    name_list = [bm20names.ML2_LONG, bm20names.ML2_PITCH, bm20names.ML2_PERP]
    home_from_limit(-1, name_list)
    home_from_limit(1, [bm20names.ML2_ROLL])
    parallel_mv(bm20names.ML2_ROLL, -0.425, bm20names.ML2_PITCH, 0,
                bm20names.ML2_PERP, 30, bm20names.ML2_LONG, 390)
    

# =========================================
# =========================================
# Mirrors.
# =========================================
# =========================================

def home_m1_tab():
    """
    Homing procedure for the m1 mirror table.
    """
    user_info('Homing of m1 table started...')
    axes_names = bm20names.M1_JACKS
    return home_from_limit(-1, axes_names)


def home_m2_tab():
    """
    Homing procedure for the m1 mirror table.
    """
    user_info('Homing of m1 table started...')
    axes_names = bm20names.M2_JACKS
    return home_from_limit(-1, axes_names)


def home_m1_lat(silent=True):
    """
    Homing procedure for the m1 mirror lateral axes.
    """
    user_info('Homing of m1 lateral started...')
    config = get_config()
    lat_front, lat_back = [config.get(axis) for axis in bm20names.M1_LAT]
    return _home_mirror_lat(lat_front, lat_back)


def home_m2_lat(silent=True):
    """
    Homing procedure for the m2 mirror lateral axes.
    """
    user_info('Homing of m2 lateral started...')
    config = get_config()
    lat_front, lat_back = [config.get(axis) for axis in bm20names.M2_LAT]
    return _home_mirror_lat(lat_front, lat_back)


def _home_mirror_lat(lat_front, lat_back):
    """
    Homing procedure for the lateral motors of mirrors
    This one is a bit different than the others.
    Limits are search clockwise, then counter clockwise:
    First:
        lat_f -> lim- and lat_b -> lim+
    then:
        lat_f -> lim+ and lat_b -> lim-
    Motors are then moved to the middle, then homed.
    """

    with error_cleanup(lat_front, lat_back):
        pos_printer = AxisPositionPrinter(lat_front, lat_back)

        print(f'Searching for limits clockwise:')
        print(f'[lat_f->lim-] and [lat_b->lim+]')

        def fmt_fn(ax, s, w):
            if ax is lat_front:
                lim_state = 'LIMNEG'
            else:
                lim_state = 'LIMPOS'
            return f'{lim_state:^{w}}' if lim_state in ax.state else s

        with disable_user_output():
            lat_front.hw_limit(-1, wait=False)
            lat_back.hw_limit(1, wait=False)
            gevent.sleep(1)

            print(f'============ Moving ============')
            pos_printer.print_names()

            limit_reached = False
            while not limit_reached:
                limit_reached = (axis_limit_reached(-1, lat_front)
                                 and axis_limit_reached(1, lat_back))
                pos_printer.print_positions(fmt_fn=fmt_fn, sync_hard=True)
        print('')

        lat_front.sync_hard()
        lat_back.sync_hard()
        f1 = lat_front.position
        b1 = lat_back.position

        print(f'Searching for limits counter-clockwise:')
        print(f'[lat_f->lim+] and [lat_b->lim-]')

        with disable_user_output():
            lat_front.hw_limit(1, wait=False)
            lat_back.hw_limit(-1, wait=False)
            gevent.sleep(1)

            print(f'============ Moving ============')
            pos_printer.print_names()

            limit_reached = False
            while not limit_reached:
                limit_reached = (axis_limit_reached(1, lat_front)
                                 and axis_limit_reached(-1, lat_back))
                pos_printer.print_positions(fmt_fn=fmt_fn, sync_hard=True)

        print('')

        lat_front.sync_hard()
        lat_back.sync_hard()
        f2 = lat_front.position
        b2 = lat_back.position

        home_f = (f1 + f2) / 2.
        home_b = (b1 + b2) / 2.

        print('Moving to middle position.')
        print(f'{lat_front.name}: {f1} + {f2} / 2. = {home_f}')
        print(f'{lat_back.name}: {b1} + {b2} / 2. = {home_b}')

        umv(lat_front, home_f, lat_back, home_b)

        return home_from_limit(-1, [lat_front, lat_back])
                           

def home_m1_bend():
    """
    Homing procedure for the m1 mirror bending motor.
    """
    print('Homing of m1 bending started...')

    axes_names = [bm20names.M1_BEND]
    return home_from_limit(-1, axes_names)


def home_m2_bend():
    """
    Homing procedure for the m2 mirror bending motor.
    """
    print('Homing of m2 bending started...')

    axes_names = [bm20names.M2_BEND]
    return home_from_limit(-1, axes_names)


# =========================================
# =========================================
# Mirrors.
# =========================================
# =========================================


def _home_slits(*slit_names):
    config = get_config()
    blades = [config.get(f"{slit_name}{side}")
              for side in ["l", "r", "t", "b"]
              for slit_name in slit_names]

    # move to lim-
    # search for home+ switch
    # set user/dial/offset
    # move to 0

    # some blades cant find their home position
    # but it didnt bother SPEC, who set the home_position anyway
    # (at the lim+)
    # so we're keeping the same behaviour with set_home_pos_on_error=True
    home_from_limit(-1, blades, set_home_pos_on_error=True)


def home_slit(slit_name):
    _home_slits(*[slit_name])


def home_oh_slits():
    _home_slits(*["s1", "s2", "s3"])


def home_eh1_slits():
    _home_slits(*["s5"])


def home_eh2_slits():
    _home_slits(*["s6"])


# def home_slits():
#     _home_slits(*["s1", "s2", "s3", "s5", "s6"])


# =========================================
# =========================================
# Attenuators.
# =========================================
# =========================================


def home_attenuators():
    """
    Homing procedure for the attenuators.
    """
    print('Homing of attenuators started...')
    config = get_config()

    axes_names = ['atta', 'attb']
    axes = [config.get(name)
            for name in axes_names]

    print('Moving away from the limit.')
    parallel_group_mvr(0.2, *axes)
    gevent.sleep(1)
    move_to_limit(1, *axes)
    gevent.sleep(1)
    set_home_position(axes)
    gevent.sleep(1)
    parallel_group_mvr(0.2, *axes)
    parallel_group_mv(0, *axes)
