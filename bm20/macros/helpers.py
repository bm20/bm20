def prompt_user_yes_no(text):
    """
    A simple function that asks the user a question then
    expects a y/n/c answer.
    """
    print(text)
    ans = ''
    while ans not in ('y', 'n', 'c'):
        ans = input("Please reply [y/n/c]: ").lower()
    return ans


def _pprint(dd, title=None, keys=None, indent=2, prepend=4, symbols=["* ", "- ", "- "], ignore_none_value=True, ignore_empty_dict=True):
    if not dd:
        return ""
    if title is None:
        txt = ""
    else:
        txt = f"{title}\n"

    symbols += ["- "] * (2 - len(symbols))
    if keys is None:
        keys = dd.keys()
    else:
        keys = [k for k in keys if k in dd]
    for key in keys:
        value = dd[key]
        if isinstance(value, (dict,)):
            if not value and ignore_empty_dict:
                continue
            txt += f"{' ' * prepend}{symbols[0]}{key}:\n"
            txt += _pprint(value, indent=indent, prepend=prepend+indent, symbols=symbols[1:])
        else:
            if value is None and ignore_none_value:
                continue
            txt += f"{' ' * prepend}{symbols[0]}{key} = {value}\n"
    return txt