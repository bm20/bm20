from tabulate import tabulate
from collections import OrderedDict

from bliss.config.static import get_config
from bliss.comm.spec.connection import SpecConnection, SpecClientNotConnectedError
from bliss.shell.getval import getval_yes_no


class SpecBliss:
    """
    Object that helps synchronizing axis offsets between SPEC and BLISS.


    - plugin: generic
      package: bm20.macros.spec
      class: SpecBliss
      name: sb_optics
      host: nolde  # host running SPEC
      session: dn  # name of the SPEC session
      axis:
      - m1: simot1  # SPEC mnemonic: BLISS mnemonic
      - simot2      # here the mnemonic is the same in SPEC and BLISS
      - s2t: slit2t


    
    """
    name = property(lambda self: self._name)

    def __init__(self, config):
        self._config = config
        self._name = config["name"]
        self._host = config["host"]
        self._session = config["session"]

        self._spec2bliss = OrderedDict()

        for axis in config.get('axis'):
            if isinstance(axis, (str,)):
                spec_mne = axis
                bliss_mne = spec_mne
            else:
                try:
                    axis = axis.to_dict()
                except AttributeError:
                    raise RuntimeError(f"{self.name}: wrong axis type "
                                        "(expected mapping or str).")
                if len(axis) != 1:
                    raise RuntimeError(f"TODO: invalid syntax in yaml: {axis}.")
                spec_mne, bliss_mne = list(axis.items())[0]
            
            if spec_mne in self._spec2bliss:
                raise RuntimeError(f"{self._name}: multiple occurences of SPEC mnemonic {spec_mne}.")
            if bliss_mne in self._spec2bliss.values():
                raise RuntimeError(f"{self._name}: multiple occurences of BLISS mnemonic {bliss_mne}.")

            self._spec2bliss[spec_mne] = bliss_mne

        address = f"{self._host}:{self._session}"
        self._connection = SpecConnection(address)

    def __info__(self):
        txt = "===============\n"
        txt += f"SpecBliss: {self.name}\n"
        txt += f"Session:   {self._host}:{self._session}\n"
        txt += "---------------\n\n"
        tab_data = []
        for spec_mne, bliss_mne in self._spec2bliss.items():
            tab_data += [[spec_mne, bliss_mne]]
        txt += tabulate(tab_data,
                        headers=["spec", "bliss"])
        txt += "\n===============\n"
        return txt
    
    def state(self):
        config = get_config()
        conn = self._connection
        txt = "===============\n"
        txt += f"SpecBliss: {self.name}\n"
        txt += f"Session:   {self._host}:{self._session}\n"
        txt += "---------------\n\n"
        tab_data = []
        try:
            for spec_mne, bliss_mne in self._spec2bliss.items():
                b_ofst = config.get(bliss_mne).offset
                s_ofst = conn.getChannel(f'motor/{spec_mne}/offset').read()
                tab_data += [[spec_mne, bliss_mne,
                            s_ofst,
                            b_ofst,
                            s_ofst - b_ofst]]
        except SpecClientNotConnectedError:
            raise RuntimeError(f"Can't connect to the SPEC session "
                               "{self._host}:{self._session}, is it running?")
        txt += "Current axis state:\n"
        txt += tabulate(tab_data,
                    #    showindex=self._spec2bliss.keys(),
                        headers=["spec", "bliss", "spec_ofst", "bliss_ofst", "diff"],
                        floatfmt=".6f")
        txt += "\n===============\n"
        print(txt)
        

    def spec_ofst_to_bliss(self):
        config = get_config()
        conn = self._connection

        try:
            tab_data = []
            for spec_mne, bliss_mne in self._spec2bliss.items():
                b_ofst = config.get(bliss_mne).offset
                s_ofst = conn.getChannel(f'motor/{spec_mne}/offset').read()
                tab_data += [[spec_mne, bliss_mne,
                            s_ofst,
                            b_ofst,
                            s_ofst - b_ofst]]
        except SpecClientNotConnectedError:
            raise RuntimeError(f"Can't connect to the SPEC session "
                               "{self._host}:{self._session}, is it running?")
        print("The following offset(s) will be applied (SPEC to BLISS):")
        print(tabulate(tab_data,
                    #    showindex=self._spec2bliss.keys(),
                       headers=["spec", "bliss", "spec_ofst", "bliss_ofst", "diff"],
                       floatfmt=".6f"))
        
        if not getval_yes_no("Please confirm", default='n'):
            print("Canceled.")
            return
        print("Ok, setting offset(s)")

        # TODO: maybe check if the ofst has changed in the meantime?
        # like, use fell asleep before accepting...
        for spec_mne, bliss_mne, spec_ofst, bliss_ofst, _ in tab_data:
            bliss_axis = config.get(bliss_mne)
            bliss_axis.offset = spec_ofst
            print(f"OK, {bliss_mne} offset set to {bliss_axis.offset}... "
                  f"(from {spec_mne}, was {bliss_ofst}).")

    def bliss_ofst_to_spec(self):
        config = get_config()
        conn = self._connection
        
        try:
            tab_data = []
            for spec_mne, bliss_mne in self._spec2bliss.items():
                b_ofst = config.get(bliss_mne).offset
                s_ofst = conn.getChannel(f'motor/{spec_mne}/offset').read()
                tab_data += [[spec_mne, bliss_mne,
                            b_ofst,
                            s_ofst,
                            b_ofst - s_ofst]]
        except SpecClientNotConnectedError:
            raise RuntimeError(f"Can't connect to the SPEC session "
                               "{self._host}:{self._session}, is it running?")
        print("The following offset(s) will be applied (BLISS to SPEC):")
        print(tabulate(tab_data,
                    #    showindex=self._spec2bliss.keys(),
                       headers=["spec", "bliss", "bliss_ofst", "spec_ofst", "diff"],
                       floatfmt=".6f"))
        
        if not getval_yes_no("Please confirm", default='n'):
            print("Canceled.")
            return
        print("Ok, setting offset(s)")

        # TODO: maybe check if the ofst has changed in the meantime?
        # like, use fell asleep before accepting...
        for spec_mne, bliss_mne, bliss_ofst, spec_ofst, _ in tab_data:
            conn.getChannel(f'motor/{spec_mne}/offset').write(bliss_ofst)
            # TODO: check
            new_ofst = conn.getChannel(f'motor/{spec_mne}/offset').read()
            print(f"OK, {spec_mne} offset set to {new_ofst}... "
                  f"(from {bliss_mne}, was {spec_ofst}).")


        




        # crystals = {crystal['index']:CrystalDef(**crystal)
                    
#         crystals[-1] = NoCrystal
#         self._crystals = crystals

#     # def index(self, index):
#     #     return self._crystals[index]
    
#     def from_index(self, index):
#         return self._crystals[index]
    
#     def d_spacing(self, index):
#         return self.from_index(index).d_spacing
    
#     def label(self, index):
#         return self.from_index(index).label
    
#     def register(self, index):
#         return self.from_index(index).register
    
#     def out_chan(self, index):
#         return self.from_index(index).chan_out

#     def in_chan(self, index):
#         return self.from_index(index).chan_in
    
#     def valid_indices(self):
#         return [xtal.index for xtal in self._crystals.values() if xtal.index >= 0]
    
#     def default(self):
#         return self.config["default"]
    


# def spec_to_bliss(axes_dict):
#     print("WARNING! This will read the offset from the following motors"
#           " and apply it to the corresponding axis in BLISS.")
    

# def axes = {"mono": "bragg"}
    
    
