import os
import csv
import numpy as np
from silx.gui import qt as Qt
from silx.gui.plot import Plot1D


def plot():
    base_dir = "/data/bm20/inhouse/bliss_tmp/testbm20exafs/bm20/20230401/ascii"
    # files = ["MoSPEC_000_000_1_fluo.xy",
    #         "sample_0001_5_ge18_fluo.xy",
    #         "first_0001_4_ge18_fluo.xy",
    #         "first_0001_9_ge18_fluo.xy",
    #         "first_0001_10_ge18_fluo.xy",]
    files = ["MoSPEC_000_000_1_trsample.xy",
            "sample_0001_5_trsamp.xy",
            "first_0001_4_trsamp.xy",
            "first_0001_9_trsamp.xy",
            "first_0001_10_trsamp.xy",]

    app = Qt.QApplication([])
    plot = Plot1D()
    plot.getLegendsDockWidget().setVisible(True)

    for fn in files:
        with open(os.path.join(base_dir, fn)) as fd:
            data = csv.reader(fd,
                              delimiter=' ',
                              skipinitialspace=True,
                              quoting=csv.QUOTE_NONNUMERIC)
            data = np.array(list(data))
        plot.addCurve(data[:, 0], data[:, 1], legend=os.path.splitext(fn)[0])

    plot.show()
    app.exec()

if __name__ == '__main__':
    plot()