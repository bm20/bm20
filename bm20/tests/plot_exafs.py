import os
import csv
import numpy as np
from silx.gui import qt as Qt
from silx.gui.plot import Plot1D
from silx.io.spech5 import SpecH5

from bliss.config.static import get_config


def plot():

    simu_exafs = get_config().get('simu_exafs')
    e = simu_exafs(element="Re", edge="L3", ref_element="Ta", ref_edge="L3")
    e._k_scan.ctime_max = 1.5
    e._k_scan.k_end = 7

    base_dir = "/data/bm20/inhouse/EXAFS/EV-522"
    # files = ["MoSPEC_000_000_1_fluo.xy",
    #         "sample_0001_5_ge18_fluo.xy",
    #         "first_0001_4_ge18_fluo.xy",
    #         "first_0001_9_ge18_fluo.xy",
    #         "first_0001_10_ge18_fluo.xy",]
    h5name = "thio5_ph8_3w_15K_ReL3_000_000.dat"

    app = Qt.QApplication([])
    plot = Plot1D()
    plot.getLegendsDockWidget().setVisible(True)

    specf = os.path.join(base_dir, h5name)
    print(specf)

    with SpecH5(specf) as h5f:
        pos = h5f['1.1/measurement/Energy']
        ctime = h5f['1.1/measurement/Seconds']
        plot.addCurve(pos, ctime, legend="SPEC")

        p, t = e.positions()
        plot.addCurve(p, t, legend="BLISS")

        print(sum(t), sum(ctime))
        print(len(t), len(ctime))

    plot.show()
    app.exec()


if __name__ == '__main__':
    plot()