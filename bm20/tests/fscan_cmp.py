import os
from os import makedirs
from os.path import splitext, basename
from silx.io import spech5

import numpy as np
from tabulate import tabulate

from tests.legacy.params import *
import h5py

from silx.gui import qt as Qt
from silx.gui.plot import Plot1D

from tests.legacy.params import e_to_k

# params = ExafsParameters("Mo", "K")

# s = params._calc_cont()
# p, t = s._calc_cont()

# print(s)
# print(np.sum(t))


def get_trref(i1, i2):
    div = np.divide(i2, i1, out=np.zeros_like(i1), where=i1>0)
    return -1 * np.log(div, where=div>0)


app = Qt.QApplication([])



fname = "/data/bm20/inhouse/bliss_tmp/testfsmok/bm20/20230401/RAW_DATA/MoK/MoK_0001/MoK_0001.h5"
specf = "/data/bm20/inhouse/bliss_tmp/testfsmok/bm20/20230401/RAW_DATA/spec/MoO3_RT_MoK_000_000.dat"
out_dir = "/data/bm20/inhouse/bliss_tmp/testfsmok/bm20/20230401/csv"

makedirs(out_dir, exist_ok=True)

ignore_sca = {'1.1':[7],
              '2.1':[7],
              '3.1':[7]}


mainwin = Qt.QWidget()
layout = Qt.QGridLayout(mainwin)

ptrsamp = Plot1D()
ptrref = Plot1D()
pfluo = Plot1D()
pk3 = Plot1D()
ptrsamp.setGraphTitle("Trsample")
ptrref.setGraphTitle("Trref")
pfluo.setGraphTitle("fluo")
pk3.setGraphTitle("fluo")

layout.addWidget(ptrsamp, 0, 0)
layout.addWidget(ptrref, 0, 1)
layout.addWidget(pfluo, 1, 0)
layout.addWidget(pk3, 1, 1)


with h5py.File(fname, "r") as f5:
    scans = list(f5.keys())
    print(scans)

    for scan in scans[3:]:
        meas = f5[scan]["measurement"]
        egy = meas["fbragg_energy"][:]

        k_idx = np.where(egy >= 20000)

        fbragg = meas["fbragg_center"][:]
        trsamp = meas["trsamp"][:]
        i0 = meas["i0"][:]
        i1 = meas["i1"][:]
        i2 = meas["i2"][:]

        ignore = ignore_sca.get(scan)
        if ignore:
            sca_n = set(range(18)) - set(ignore + [6])
            print(scan, sca_n)
            scas_sum = np.sum([meas[f"fx_ge18_det{n}_sca"][:] for n in sca_n], axis=0)
            print(scas_sum)
            fluo = np.divide(scas_sum / len(sca_n), i0, where=i0!=0)
        else:
            fluo = meas["ge18_fluo"][:]

        trref = get_trref(i1, i2)

        # kval = e_to_k(egy - 20000)

        ptrsamp.addCurve(egy, trsamp, legend=f"bliss_{scan}")
        ptrref.addCurve(egy, trref, legend=f"bliss_{scan}")
        pfluo.addCurve(egy, fluo, legend=f"bliss_{scan}")

        out_file = splitext(basename(fname))[0] + f"_{scan}.csv"
        out_file = os.path.join(out_dir, out_file)

        w_data = np.ndarray((7, egy.shape[0]))
        for i, arr in enumerate([egy, i0, i1, i2, trsamp, fluo, trref]):
            w_data[i] = arr
        headers = ["egy", "i0", "i1", "i2", "trsamp", "fluo", "trref"]
        with open(out_file, 'w') as out_f:
            out_f.write(tabulate(w_data,
                                 headers=headers,
                                 tablefmt="plain"))


with spech5.SpecH5(specf) as f5:
    scans = f5.keys()

    for scan in scans:
        meas = f5[scan]["measurement"]
        egy = meas["Energy"]
        trsamp = meas["trsample"]
        i0 = meas["i0"][:]
        i1 = meas["i1"][:]
        i2 = meas["i2"][:]
        fluo = meas["fluo"][:]
        trref = meas["trref"][:]

        ptrsamp.addCurve(egy, trsamp, legend=f"spec_{scan}", yaxis="right")
        ptrref.addCurve(egy, trref, legend=f"spec_{scan}", yaxis="right")
        pfluo.addCurve(egy, fluo, legend=f"spec_{scan}", yaxis="right")


        # k_idx = np.where(egy >= 20000)
        # kval = e_to_k(egy[k_idx] - 20000)
        # trsamp_k3 = trsamp[k_idx] * kval**3
        # pk3.addCurve(kval, trsamp_k3, legend=f"spec_{scan}")

mainwin.show()
app.exec()