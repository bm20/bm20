import os

import numpy as np
from silx.io import open as silx_open
import pandas as pd

h5fname = "/data/bm20/inhouse/bliss_tmp/testfexafs/bm20/20231101/RAW_DATA/sample/testfexafs_sample.h5"
out_dir = "/data/bm20/inhouse/bliss_tmp/testfexafs/bm20/20231101/PROCESSED_DATA"
entry = "sample_0001_4.1"
out_fbase = os.path.join(out_dir, os.path.basename(os.path.splitext(h5fname)[0]) + f"_{entry}")

with silx_open(h5fname) as h5f:
    entry_g = h5f[entry]
    meas_g = entry_g["measurement"]
    # print(list(meas_g.keys()))

    fluo = meas_g["fluo"][:]
    energy_center = meas_g["energy_center"][:]
    energy_delta = meas_g["energy_delta"][:]
    scas = np.array([meas_g[f"fx_ge18_det{i}_sca"][:] for i in range(18) if i not in (6, 8)])
    i0 = meas_g["i0"][:]

sca_sum = np.sum(scas, axis=0) / scas.shape[0]
print(sca_sum.shape, i0.shape)

out_f = f"{out_fbase}_nobin.csv"
fluo = sca_sum/i0
# data = np.stack([energy_center, energy_delta, fluo], axis=1)
data = np.stack([energy_center, energy_delta, fluo], axis=1)
pd.DataFrame(data).to_csv(out_f, header=["energy_center", "energy_delta", "fluo"])
print("wrote", out_f)

# bin2 = data[1:] + data[0:-1]
for n_bin in [2, 5, 10, 15]:
    out_f = f"{out_fbase}_bin_{n_bin}.csv"
    max_len = fluo.shape[0] - fluo.shape[0] % n_bin
    # binned = np.reshape(fluo[:max_len], (-1, n_bin)).sum(axis=1) / n_bin
    binned = np.reshape(sca_sum[:max_len], (-1, n_bin)).sum(axis=1) / np.reshape(i0[:max_len], (-1, n_bin)).sum(axis=1)
    egy_center = (energy_center[0:max_len:n_bin] + energy_center[n_bin-1:max_len:n_bin]) / 2.
    # egy_delta = (energy_delta[0:max_len:n_bin] + energy_delta[n_bin-1:max_len:n_bin])
    egy_delta = np.reshape(energy_delta[:max_len], (-1, n_bin)).sum(axis=1)
    data = np.stack([egy_center, egy_delta, binned], axis=1)
    pd.DataFrame(data).to_csv(out_f, header=["energy_center", "energy_delta", "fluo"])
    print("wrote", out_f)

# print(bin2[50:52], fluo[100] + fluo[101], fluo[102] + fluo[103])

# np.savetxt(out_f, data, header=["energy_center", "energy_delta", "fluo"])
# print("wrote", out_f)