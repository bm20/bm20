from bliss.config.static import get_config

from bliss.scanning.scan import Scan
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.acquisition.timer import SoftwareTimerMaster

from bliss.controllers.mca import BaseMCA
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
import numpy as np


def chain():
    config = get_config()
    chain = AcquisitionChain()
    fx_ge18 = config.get('fx_ge18')
    energy = config.get('energy')
    builder = ChainBuilder([fx_ge18.counters.realtime_det0])
    acq_params={"npoints":10, "trigger_mode":"GATE", "block_size":1}
    # ctrl_params={"preset_mode":"REALTIME", "preset_value":1.2}

    master = VariableStepTriggerMaster(energy, np.linspace(12000, 12500, 10))
    # master = SoftwareTimerMaster(1, )

    for mca in builder.get_nodes_by_controller_type(BaseMCA):
        mca.set_parameters(acq_params=acq_params)#, ctrl_params=ctrl_params)
        chain.add(master, mca)

    print(chain._tree)

    scan = Scan(chain)
    scan.run()
    return scan