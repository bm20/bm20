import os

from silx.gui import qt as Qt
from silx.gui.plot import Plot1D
import numpy as np

import h5py
from bm20.oh.mono.energy import bragg2energy


app = Qt.QApplication([])

plot = Plot1D()

d_spacing = 3.13542
#fname = "fastx_loop_0001.h5"
# fname = "fastx_loop_4k_0001.h5"
# fname = "fastx_loop_25k_0001.h5"
# fname = "fastx_loop_long_0001.h5"
# fname = "/data/bm20/inhouse/bm202303/bm20/20230301/raw/fastx_loop_1k/fastx_loop_1k_0001/fastx_loop_1k_0001.h5"
# fname = "/data/bm20/inhouse/bm202303/bm20/20230301/raw/fastx_loop_5k_6k/fastx_loop_5k_6k_0001/fastx_loop_5k_6k_0001.h5"
# fname = "/data/bm20/inhouse/bm202303/bm20/20230301/raw/fastx_loop_5k_6k_0.5/fastx_loop_5k_6k_0.5_0001/fastx_loop_5k_6k_0.5_0001.h5"
# fname = "/data/bm20/inhouse/bm202303/bm20/20230301/raw/fastx_loop_5k3_6k3_0.5/fastx_loop_5k3_6k3_0.5_0001/fastx_loop_5k3_6k3_0.5_0001.h5"
# fname = "/data/bm20/inhouse/bm202303/bm20/20230301/raw/fastx_loop_5k_6k_2/fastx_loop_5k_6k_2_0001/fastx_loop_5k_6k_2_0001.h5"
# fname = "/data/bm20/inhouse/bm202303/bm20/20230301/raw/fastx_loop_5k_6k_4/fastx_loop_5k_6k_4_0001/fastx_loop_5k_6k_4_0001.h5"
#fname = "/data/bm20/inhouse/bm202303/bm20/20230301/raw/fastx_loop_5k_6k_2_1ev_1s/fastx_loop_5k_6k_2_1ev_1s_0001/fastx_loop_5k_6k_2_1ev_1s_0001.h5"
fname = "/data/bm20/inhouse/bm202303/bm20/20230301/raw/test_stab/test_stab_0001/test_stab_0001.h5"

meas_k = "fbragg"

title = os.path.splitext(os.path.basename(fname))[0]

plot.setGraphTitle(title)

b0 = np.linspace(29.52439, 3.81377, 600)
e0 = bragg2energy(b0, d_spacing)

# v_egy = 5

# "fscan fbragg 23.2929 -0.0351245 115 0.1 0.105"

# print(b0)
# print(e0)

with h5py.File(fname) as f5:
    scan_names = sorted(f5.keys())
    for scan in scan_names[-1:]:
        try:
            meas = f5[scan]["measurement"]
            fb_del = meas["fbragg_delta"][:]
            fb_cen = meas["fbragg_center"][:]
            fb_raw = meas["fbragg_raw"][:]
            t_raw = meas["timer_raw"][:]
            if "fbragg_energy" in meas:
                fb_egy = meas["fbragg_energy"][:]
                fb_egy_delta = meas["fbragg_egy_delta"][:]
            else:
                fb_egy = None
                fb_egy_delta = None

            xes5 = f5[scan]["instrument"]["xes"]
            e0 = xes5["e0"][()]
            e1 = xes5["e1"][()]
            step_ev = xes5["step_ev"][()]
            step_s = xes5["step_S"][()]

            v_egy = step_ev / step_s

            print(scan, e0, e1, step_ev, step_s)

            # egy_raw = bragg2energy(fb_raw/200000, d_spacing)
            # egy_delta = egy_raw[1::2] - egy_raw[0:-1:2]
            # egy_cen = bragg2energy(fb_cen, d_spacing)
            
            epoch_trig = meas["epoch_trig"][:]
            fbragg = meas["fbragg"][:]

            # plot.addCurve(fb_egy, fb_egy_delta, legend=f"{scan}_egy_delta")

            # x = np.arange(fb_cen.shape[0])
            # x2 = np.arange(fb_raw.shape[0])
            # b_raw2 = np.zeros(fb_raw.shape)
            # print(fb_raw.shape, fb_cen.shape, fb_del.shape)
            # b_raw2[0:-2:2] = fb_cen - fb_del / 2
            # b_raw2[1:-1:2] = fb_cen + fb_del / 2
            # plot.addCurve(x2[:-1], b_raw2[:-1] - fb_raw[:-1]/200000, legend=f"{scan}_raw2")


            # egy2 = bragg2energy(fb_cen, d_spacing)
            # x = np.arange(fb_egy.shape[0])
            # plot.addCurve(x, fb_egy - egy2, legend=f"{scan}_egy")

            # x = np.arange(fb_del.shape[0])
            # del2 = (fb_raw[1:-1:2] - fb_raw[0:-2:2])/200000
            # plot.addCurve(x, del2 - fb_del, legend=f"{scan}_del2")
            # plot.addCurve(x, fb_del, legend=f"{scan}_del")

            # "fscan fbragg 23.2929 -0.0351245 115 0.1 0.105"
            # xes_fast_fbragg("test_stab", 5000, 6000, 8.6666, 0.1, run=1)
            # real_egy = np.
            egy_raw = bragg2energy(fb_raw/200000, d_spacing)
            egy_delta = egy_raw[1::2] - egy_raw[0:-1:2]
            egy_cen = bragg2energy(fb_cen, d_spacing)

            e0_raw = egy_raw[0]
            t0_raw = t_raw[0]
            # tim = np.arange(egy_cen.shape[0]) * 1#0.105
            e_exp = e0 + (t_raw - t0_raw) * v_egy / 10000

            # plot.addCurve(t_raw, egy_raw, legend=f"{scan}_e_raw")
            # plot.addCurve(t_raw, e_exp, legend=f"{scan}_e_exp")
            # plot.addCurve(t_raw, egy_raw - e_exp, legend=f"{scan}_e_diff", yaxis="right")

            # plot.addCurve(t_raw[0:-1], np.diff(egy_raw), legend="raw_diff")
            # plot.addCurve(t_raw[0:-1], np.diff(e_exp), legend="raw_diff")
            plot.addCurve(t_raw[0:-1], np.diff(egy_raw), legend="raw_diff")

            # egy = bragg2energy(fbragg, d_spacing)
            # plot.addCurve(egy[0:-1], np.diff(egy), legend=scan)
        except KeyError as ex:
            print(f"ERR {scan}", ex)
            continue
plot.show()
app.exec()