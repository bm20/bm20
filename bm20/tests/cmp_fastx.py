import numpy as np

import h5py
from bm20.oh.mono.energy import bragg2energy


# <KeysViewHDF5 ['epoch_trig', 'fbragg', 'fbragg_center',
#                'fbragg_delta', 'fbragg_period', 'fbragg_raw',
#                'fbragg_trig', 'i0', 'timer_delta', 'timer_period',
#                'timer_raw', 'timer_trig', 'trigct_delta',
#                'trigct_raw', 'xesm4_det0', 'xesm4_det0_elapsed_time',
#                'xesm4_det0_event_count_rate', 'xesm4_det0_events', 
#                'xesm4_det0_fractional_dead_time', 'xesm4_det0_live_time',
#                'xesm4_det0_roi', 'xesm4_det0_trigger_count_rate',
#                'xesm4_det0_trigger_live_time', 'xesm4_det0_triggers']>

d_spacing = 3.13542
# fname = "fastx_loop_0001.h5"
fname = "fastx_loop_4k_0001.h5"
# keys = ["fbragg_center",
#         "fbragg",
#         "fbragg_raw",
#         "fbragg_delta"]

# meas_min = {}
# meas_max = {}

meas_k = "fbragg_raw"

with h5py.File(fname) as f5:
    scan_names = sorted(f5.keys())
    k0 = scan_names[0]
    meas = f5[k0]["measurement"]

    bcen = meas[meas_k][:]
    egy = bragg2energy(bcen/200000, d_spacing)
    min_egy = egy
    max_egy = egy[:]
#     for k in keys:
#         meas_min[k] = meas[k][:]
#         meas_max[k] = meas[k][:]
    
    for scan in scan_names[1:]:
        meas = f5[scan]["measurement"]
        bcen = meas[meas_k][:]
        egy = bragg2energy(bcen/200000, d_spacing)
        min_egy = np.minimum(min_egy, egy)
        max_egy = np.maximum(max_egy, egy)
#         for k in keys:
#             meas_min[k] = np.minimum(meas_min[k], meas[k])
#             meas_max[k] = np.maximum(meas_max[k], meas[k])
    
# print(meas_min["fbragg_delta"])
# print(meas_max["fbragg_delta"])

print(min_egy)
print(max_egy)
print(np.max(max_egy - min_egy))
print(np.min(max_egy - min_egy))

print(np.where((max_egy - min_egy) > 0.05))

