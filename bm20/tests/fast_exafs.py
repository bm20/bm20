import numpy as np
from tests.legacy.params import *

params = ExafsParameters("Re", "L3")

# s = params.scans_params[-1]
# p, t = s._calc_cont()
# print(s.scan_pos[-1])
# # print(p[0:10])
# exit(0)



# s0 = params.scans_params[0]
# s1 = params.scans_params[1]

# print(s0.scan_pos[0], s0.scan_pos[-1], s0.edge_ev)
# print(s1.scan_pos[0], s1.scan_pos[-1], s1.edge_ev)


# print(s0.scan_pos.shape)
# p, t = s0._calc_cont()
# print(p.shape, t.shape)

print('###########################')
start_ev = None
for i, s in enumerate(params.scans_params[:-1]):
    print('--', i)
    s0 = s
    s1 = params.scans_params[i+1]

    p0, t0 = s0._calc_cont(ev_start=start_ev)
    p1, t1 = s1._calc_cont(ev_start=p0[-1])

    dps = p1[1] - p0[-1]
    print("## p0[0], p0[-1]", p0[0], p0[-1], t0[0], t0[-1])
    # print("## dps", dps)
    # print("## t1[0] t1[-1]", t1[0], t1[-1])#, dps/t1[0], s1.scan_t[0], s1.scan_t[-1])
    start_ev = p0[-1]
    # dpe = np.diff(p1[0] - p0[-1])
    # v = dp/t[:-1]
    # print(v)
print('--', i)
s0 = params.scans_params[-1]
# p0, k0, v0, t0 = s0._calc_cont(ev_start=start_ev)
p0, t0 = s0._calc_cont(ev_start=start_ev)
print("## p0[0], p0[-1]", p0[0:3], p0[-1], t0[0:3], t0[-1], s0.scan_pos[0], s0.edge_ev)
print(p0.shape)


x = np.arange(p0.shape[0])
# v = np.diff(p0) / t0[1:]
# print(v[0:10])
# print(np.diff(t0))
from silx.gui import qt as Qt
from silx.gui.plot import Plot1D
app = Qt.QApplication([])
plot = Plot1D()
plot.addCurve(p0[1:], np.diff(p0)/2, legend="p0")
# x = np.arange(s0.scan_pos.shape[0]-1)
# plot.addCurve(s0.scan_pos, s0.scan_t, legend="p0")

#from bm20.scans.exafs.params import *
#kmax = 14.5
#vmin = 1
#vmax = 5

#k0 = e_to_k(0)
#k1 = e_to_k(vmax)
#kvmax = k1 - k0

#e1 = k_to_e(kmax)
#k0 = e_to_k(e1)
#k1 = e_to_k(e1 - vmin)
#kvmin = k0 - k1

#print("KV", kvmin, kvmax)

#e0 = params.scans_params[0].edge_ev + 30
#e1 = k_to_e(kmax) + e0
#e = np.linspace(e0, e1, 200)
#k = e_to_k(e - params.scans_params[0].edge_ev)
#v = k_to_v(2, kvmin, kvmax, k)

#kv = []
#ks = []
#eg2 = []
#k0 = k[0]
#while k0 < k[-1]:
    #v = k_to_v(2, kvmin, kvmax, k0, k_max=k[-1])
    ## print(k0, v)
    #k0 += v
    #ks.append(k0)
    #kv.append(v)
    #eg2.append(k_to_e(k0))

#ks = np.array(ks)
#kv = np.array(kv)
#eg2 = np.array(eg2)
#eg = k_to_e(ks) + e0
#plot.addCurve(ks[1:], np.diff(eg))
## plot.addCurve(np.arange(k.shape[0]), k)
#print(ks[0])
## print(eg)
## plot.addCurve(eg[0:-1], np.diff(eg))
plot.show()
app.exec()

# ksp = p.k_scans[0]

# # print(ksp.scan_pos[-5:])
# scan_k, scan_e, scan_t, e_cont, tm = ksp._calc_cont()

# t_acc = np.add.accumulate(scan_t)

# # print(t_acc)

# step_s = 2

# i = 0
# next_t = step_s
# b_arr = []
# while i < t_acc.shape[0]:
# # for x in range(4):
#     i_next = np.searchsorted(t_acc[i:], next_t)
#     # if i > scan_e.shape[0]:
#     #     b_arr.append(scan_e[-1])
#     # else
#     i += i_next
#     if i < t_acc.shape[0]:
#         b_arr.append(scan_e[i])
#         next_t += step_s
#         # print(next_t, i, t_acc[i])
#         i += 1
#     else:
#         b_arr.append(scan_e[-1])

# print('I', t_acc)
# print('==', scan_e[0])
# b_arr = np.array(b_arr)
# print(b_arr.shape)
# print(b_arr[-1])
# print(e_cont[-1])

# print(min(tm), max(tm))
# tmd = np.diff(tm)
# print(min(tmd), max(tmd))
# print(tmd.shape)
# print(np.count_nonzero(np.where(tmd > 0.05)))

# # i = np.searchsorted(t_acc[i:], next_t)
# # print(i, t_acc[i])
# # print(t_acc.shape)

# # k_m = (self.t_max_s - self.t_min_s) / pow(self.k_end, self.t_pwr)
# # k_start = e_to_k(self.e_start)
# # if (self.k_end - k_start) % self.k_inc != 0.:
# #     k_end = self.k_end + self.k_inc
# # else:
# #     k_end = self.k_end
# # k_pos = np.arange(k_start, k_end, self.k_inc)
# # scan_pos = k_to_e(k_pos) + self._edge_ev
# # scan_t = self.t_min_s + k_m * np.power(k_pos, self.t_pwr)

# # self._scan_pos = scan_pos
# # self._scan_t = scan_t

