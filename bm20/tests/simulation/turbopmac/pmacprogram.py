import time

from bliss.shell.standard import umvr, umv, mvr, mv
from bliss.common.axis import AxisOnLimitError
from bliss.common.logtools import log_info, log_debug, log_critical

import gevent
from gevent import pool, Greenlet


class PmacProgram:
    num = property(lambda self: self._num)
    opened = property(lambda self: self._opened)
    buffer = property(lambda self: "\r".join([f"{cmd}:{value}" if value else f"{cmd}" for cmd, value in self._sequence]))
    
    def __init__(self, pmac, num):
        self._num = num
        # self._code = []
        self._opened = False
        self._pmac = pmac
        self._absolute = True
        self._sequence = []
        # self._cs_id = None
        self._pool = pool.Pool()
        self._running = False
        
    def open(self):
        self._opened = True
        
    def close_buffer(self):
        self._opened = False
        
    def clear_buffer(self):
        self._sequence.clear()
        
    def abs(self):
        self._sequence.append(('ABS', None))
    
    def inc(self):
        self._sequence.append(('INC', None))

    def linear(self):
        self._sequence.append(('LINEAR', None))

    def abs(self):
        self._sequence.append(('ABS', None))

    def feedrate(self, feedrate):
        self._sequence.append(('F', feedrate))

    def tm(self, tm):
        self._sequence.append(("TM", tm))

    def tooltip_target(self, name, target):
        self._sequence.append((name, target))

    def run_motion_program(self, cs, X=None, Y=None, Z=None):
        # in_pos = f"M{cs._cs_sx}87"
        # prog_running = f"M{cs._cs_sx}80"
        # self._pmac.pmac_set_m_value(in_pos, 0)
        # self._pmac.pmac_set_m_value(prog_running, 1)
        cs.set_running(True)
        self._run_in_thread(self._run_motion_program, cs=cs, X=X, Y=Y, Z=Z)
        
    def _run_motion_program(self, cs=None, X=None, Y=None, Z=None):
        absol = 1
        lin = 1
        feedrate = None
        tm = None
        velocity = None
        try:
            # in_pos = f"M{cs._cs_sx}87"
            # prog_running = f"M{cs._cs_sx}80"
            # self._pmac.pmac_set_m_value(in_pos, 0)
            # self._pmac.pmac_set_m_value(prog_running, 1)
            for (cmd, value) in self._sequence:
                print("COMMAND:", cmd, value)
                if cmd == 'ABS':
                    absol=1
                elif cmd == 'INC':
                    absol = 0
                elif cmd == 'LINEAR':
                    pass
                elif cmd == 'F':
                    feedrate = value
                    tm = None
                    velocity = None
                elif cmd == "TM":
                    tm = value
                    feedrate = None
                    velocity = None
                elif cmd in ('X',):
                    distance = abs(X.position - value)
                    # print('==================================MOVING ', cs, X.name, X.position, feedrate, tm, value, distance)
                    if velocity is None:
                        if tm is not None:
                            velocity = 1000 * distance / tm
                        elif feedrate is not None:
                            velocity = feedrate
                        if velocity is not None:
                            if cs is not None:
                                cs.set_tooltip_velocity(velocity)
                            # X.velocity = velocity
                    t0 = time.monotonic()
                    if absol:
                        mv(X, value)
                    else:
                        mvr(X, value)
                    print("MOVED", time.monotonic() - t0)
                else:
                    print("UKNOWN CMD", cmd, value)
                    # self._running = False
                    raise RuntimeError(f'Unknown command {cmd}.')
        except Exception as ex:
            print(ex)
        finally:
            print("FINALLY")
            # self._pmac.pmac_set_m_value(in_pos, 1)
            # self._pmac.pmac_set_m_value(prog_running, 0)
            cs.set_running(False)
            self._running = False
    
    def  _run_in_thread(self,
                        func,
                        *args,
                        on_success=None,
                        on_failure=None,
                        on_limit=None,
                        updt_ival_s=1,
                        **kwargs):
        def _loop():
            self._running = True
            t0 = 0
            mvth = gevent.spawn(func, *args, **kwargs)
            # axis = self.axis
            # address = self.address
            log_info(self, f'Started program')
            gevent.sleep(0.1)
            while self._running:
                t1 = time.time()
                if t1 - t0 >= updt_ival_s:
                    t0 = t1
                    print(f'CS still moving.')
                gevent.sleep(0.5)
            print("JOINING")
            mvth.join()
            print("JOINED")
            # try:
            #     results = mvth.get()
            # except AxisOnLimitError as ex:
            #     log_info(self, f'Axis {axis.name} hit the limit: {ex}.')
            #     if on_limit:
            #         on_limit(self, address, *args, **kwargs)
            # except Exception as ex:
            #     log_critical(self, f'ERROR in {func}({address}, {args}, {kwargs})')
            #     log_critical(self, f'{ex}')
            #     if on_failure:
            #         on_failure(self, address, *args, **kwargs)
            # else:
            #     log_info(self, f'Success (axis={axis.name}) {func}.')
            #     if on_success:
            #         on_success(self, address, *args, **kwargs)
            # log_info(self, f'DONE threadloop {address}, {func}')

        gl = Greenlet(_loop)
        self._pool.start(gl)
        gevent.sleep(0.1)
        print("OUT")
