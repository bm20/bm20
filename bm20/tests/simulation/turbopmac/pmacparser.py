"""
Parser 
"""
import os
import sys
import traceback
from collections import deque
from lark import Lark, Transformer
from lark.exceptions import VisitError
from .pmacvariables import PmacAddrMap


pmac_grammar = r"""
    ?value: (command | CR)+
    
    ADDR.1: "->" (("X" | "Y" | "D") ":" "$" ~0..1 HEXDIGIT+ ("," INT ("," INT ("," ("U" |"S" |"D" |"C")) ~0..1) ~0..1) ~0..1) ~0..1

    mot_addr.1: "#" INT

    abort_program: "A"

    point_program: "B" INT

    mot_pos.1: "P"

    mot_status.1: "?"

    m_read.1: "M" INT

    m_write.1: "M" INT (("=" SIGNED_NUMBER) | ADDR)

    i_read.1: "I" INT

    i_write.1: "I" INT "=" SIGNED_NUMBER
    
    p_read.1: "P" INT

    p_write.1: "P" INT "=" SIGNED_NUMBER

    q_read.1: "Q" INT

    q_write.1: "Q" INT "=" SIGNED_NUMBER

    !list_prog.1: "LIST" (("PROG" INT) | "INVERSE" | "FORWARD")

    jog_cmd.1: "J/"              -> jog_stop
              | "J=" SIGNED_NUMBER  -> jog_abs
              | "J:" SIGNED_NUMBER  -> jog_rel
              | "J" ("+"|"-")        -> jog_inf
           
    PROG_NUM.1: ("0".."9")+
    !open_buffer.1: "OPEN" ("PROG" PROG_NUM | "INVERSE" | "FORWARD")

    run_program: "R"
    
    motor_inverse.1: "#" INT "->I"

    !close_buffer: "CLOSE"

    //any_str.-100: /.+/

    command: mot_addr
           | mot_pos
           | mot_status
           | m_read
           | m_write
           | i_read
           | i_write
           | p_read
           | p_write
           | q_read
           | q_write
           | list_prog
           | jog_cmd
           | abort_program
           | point_program
           | run_program
           | "K"        -> kill_cmd
           | "VERSION"   -> pmac_version
           | "DATE"   -> pmac_date
           | "HOME"   -> pmac_home
           | "TYPE"   -> pmac_type
           | open_buffer
           | close_buffer
           | "&" INT -> select_cs
           | motor_inverse
          // | any_str

    %import common.ESCAPED_STRING
    %import common.SIGNED_NUMBER
    %import common.INT
    %import common.DIGIT
    %import common.SIGNED_INT
    %import common._STRING_INNER
    %import common.WS
    %import common.HEXDIGIT
    %import common.CR
    %ignore WS
"""


pmac_prog_grammar = r"""
    ?value: (command | CR)*

    any_str.-100: /.+/

    !close_buffer: "CLOSE"
    clear_buffer: "CLEAR"
    abs: "ABS"
    linear: "LINEAR"
    feedrate: "F" NUMBER
    tmove: "TM" NUMBER
    !tooltip_target: "X" SIGNED_NUMBER
    inc: "INC"

    command: close_buffer
           | clear_buffer
           | abs
           | linear
           | feedrate
           | tmove
           | tooltip_target
           | inc
           | any_str

    %import common.WS
    %import common.NUMBER
    %import common.SIGNED_NUMBER
    %import common.CR
    %ignore WS
"""


pmac_cs_grammar = r"""
    ?value: command command*

    any_str.-100: /.+/

    !close_buffer: "CLOSE"
    clear_buffer: "CLEAR"

    command: close_buffer
           | clear_buffer
           | any_str

    %import common.WS
    %import common.NUMBER
    %import common.SIGNED_NUMBER
    %import common.CR
    %ignore WS
"""


# TODO find a better way to do this.
class OpenProgException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data_lineno = -1


# TODO find a better way to do this.
class OpenCsException(OpenProgException):
    pass


# TODO find a better way to do this.
class CloseBufferException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data_lineno = -1


def treedecorator(func):    
    def inner(self, value):
        try:
            reply = func(self, value)
        except OpenProgException:
            raise
        except CloseBufferException:
            raise
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            exc_tb = exc_tb.tb_next
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            msg = f'{exc_type.__name__} at line {exc_tb.tb_lineno} in file {fname}: {exc_obj}'
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            raise
        self.append_reply(reply)
        return value
    return inner


class PmacTransformer(Transformer):
    # DIGIT = int
    INT = int
    SIGNED_INT = int
    SIGNED_NUMBER = float
    
    # just for code completion
    # to be removed when the grammar is fixed
    try:
        from .pmacsimu import PmacMockup
    except ImportError:
        class PmacMockup:
            pass
    
    def __init__(self, pmac: PmacMockup, sep='\x0D'):
        super().__init__()
        self._pmac = pmac
        self._replies = ''
        self._sep = sep
        
    def append_reply(self, reply):
        if reply is not None:
            self._replies += f'{reply}' + self._sep
    
    def replies(self, clear=False):
        replies = self._replies
        if clear:
            self.clear()
        return replies
    
    def clear(self):
        self._replies = ''
        
    def ADDR(self, value):
        # addr=None, memtype=None, offset=None, width=None, format=None
        if value == '->':
            return '->'
        else:
            memtype = value[2:3]
            addr = value[5:].split(',')
            addr_int = int(addr[0], 16)
            offset = None
            width = None
            format=None
            try:
                offset = int(addr[1])
                width = int(addr[2])
                format = addr[3]
            except IndexError:
                pass

        return PmacAddrMap(addr=addr_int, memtype=memtype, offset=offset, width=width, format=format)

    @treedecorator
    def mot_addr(self, value):
        self._pmac.motor_addr = value[0]

    @treedecorator
    def mot_pos(self, value):
        return self._pmac.pmac_position(self._pmac.motor_addr)

    @treedecorator
    def mot_status(self, value):
        status = self._pmac.pmac_motor_status(self._pmac.motor_addr)
        return '{:012x}'.format(status)
    
    @treedecorator
    def m_read(self, value):
        return self._pmac.pmac_get_m_value(value[0])

    @treedecorator
    def m_write(self, value):
        # TODO : get mapping addr
        if isinstance(value[1], (PmacAddrMap,)):
            self._pmac.pmac_set_m_value(value[0], value[1])
        elif isinstance(value[1], (float,)):
            return self._pmac.pmac_set_m_value(value[0], value[1])
        else:
            self._pmac.pmac_get_m_value(value[0])

    @treedecorator
    def i_read(self, value):
        return self._pmac.pmac_get_i_value(value[0])

    @treedecorator
    def i_write(self, value):
        self._pmac.pmac_set_i_value(*value)
        
    @treedecorator
    def p_read(self, value):
        return self._pmac.pmac_get_p_value(value[0])
    
    @treedecorator
    def p_write(self, value):
        self._pmac.pmac_set_p_value(*value)

    @treedecorator
    def q_read(self, value):
        raise NotImplementedError()
        # return self._pmac.pmac_get_q_value(value[0])

    @treedecorator
    def q_write(self, value):
        pass

    @treedecorator
    def list_prog(self, value):
        if value[1].value == "PROG":
            return self._pmac.get_program(int(value[2].value)).buffer
        elif value[1].value == "INVERSE":
            return self._pmac.get_coord_sys().inverse
        else:
            return self._pmac.get_coord_sys().forward

    @treedecorator
    def jog_stop(self, value):
        self._pmac.pmac_stop()

    @treedecorator
    def jog_abs(self, value):
        self._pmac.pmac_jog_to(*value)

    @treedecorator
    def jog_rel(self, value):
        self._pmac.pmac_jog_relative(*value)

    @treedecorator
    def jog_inf(self, value):
        if value[1].value == '-':
            self._pmac.pmac_jog(-1)
        elif value[1].value == '+':
            self._pmac.pmac_jog(1)
        else:
            raise RuntimeError(f"Unknown jog command: {value}")
        
    @treedecorator
    def kill_cmd(self, value):
        self._pmac.pmac_kill_motor()

    @treedecorator
    def abort_program(self, value):
        self._pmac.abort_program()

    @treedecorator
    def point_program(self, value):
        self._pmac.point_program(value[0])
    
    @treedecorator
    def run_program(self, value):
        self._pmac.run_program()

    @treedecorator
    def pmac_home(self, value):
        return self._pmac.pmac_home_axis(self._pmac.motor_addr)
    
    @treedecorator
    def pmac_version(self, value):
        return self._pmac.pmac_version()

    @treedecorator
    def pmac_date(self, value):
        return self._pmac.pmac_date()
    
    @treedecorator
    def pmac_type(self, value):
        return self._pmac.pmac_type
    
    @treedecorator
    def prog_num(self, value):
        print('prog_num', value)
    
    @treedecorator
    def open_buffer(self, value):
        buffer_type = value[1].value
        if buffer_type == 'PROG':
            prog_num = int(value[2].value)
            self._pmac.open_program(prog_num)
            lineno = value[2].end_pos
            ex = OpenProgException()
        elif buffer_type == 'INVERSE':
            self._pmac.open_inverse()
            lineno = value[1].end_pos
            ex = OpenCsException()
        else:
            self._pmac.open_forward()
            lineno = value[1].end_pos
            ex = OpenCsException()
        
        # value = value[2]
        # print(value.start_pos, value.column, value.end_line, value.end_column, value.end_pos, value.line)
        # this aint pretty
        ex.data_lineno = lineno
        raise ex
        # print(dir(self))
        # self._prog_buffer.open(*value)
        
    @treedecorator
    def close_buffer(self, value):
        self._pmac.close_opened_buffer()
        lineno = value[0].end_pos
        ex = CloseBufferException()
        ex.data_lineno = lineno
        raise ex
        
    @treedecorator
    def select_cs(self, value):
        self._pmac.set_selected_cs(value[0])

    @treedecorator
    def motor_inverse(self, value):
        print('motor_inverse', value)

    # @treedecorator
    # def any_str(self, value):
    #     print('UNKNOWN', value)
        # return value
    
    
class PmacProgTransformer(Transformer):
    DIGIT = int
    INT = int
    SIGNED_INT = int
    SIGNED_NUMBER = float
    NUMBER = float
    
    def __init__(self, pmac, sep='\x0D'):
        super().__init__()
        self._pmac = pmac
        self._replies = ''
        self._sep = sep
        
    def append_reply(self, reply):
        if reply:
            self._replies += f'{reply}' + self._sep
    
    def replies(self, clear=False):
        replies = self._replies
        if clear:
            self.clear()
        return replies
    
    def clear(self):
        self._replies = ''
    
    @treedecorator
    def close_buffer(self, value):
        self._pmac.close_opened_buffer()
        lineno = value[0].end_pos
        
        # value = value[2]
        # print(value.start_pos, value.column, value.end_line, value.end_column, value.end_pos, value.line)
        # this aint pretty
        ex = CloseBufferException()
        ex.data_lineno = lineno
        raise ex

    @treedecorator
    def clear_buffer(self, value):
        self._pmac.clear_opened_buffer()
        
    @treedecorator
    def any_str(self, value):
        # self._pmac.add_code(value[0])
        raise RuntimeError(f'Uknown command :{value[0].value}')
    
    @treedecorator
    def abs(self, value):
        self._pmac.get_opened_buffer().abs()

    @treedecorator
    def inc(self, value):
        self._pmac.get_opened_buffer().inc()

    @treedecorator
    def linear(self, value):
        self._pmac.get_opened_buffer().linear()

    @treedecorator
    def feedrate(self, value):
        self._pmac.get_opened_buffer().feedrate(value[0])

    @treedecorator
    def tmove(self, value):
        self._pmac.get_opened_buffer().tm(value[0])

    def tooltip_target(self, value):
        self._pmac.get_opened_buffer().tooltip_target(value[0].value, value[1])

        #     command: close_buffer
        #    | clear_buffer
        #    | abs
        #    | linear
        #    | feedrate
        #    | tooltip
        #    | any_str



class PmacCsTransformer(Transformer):
    DIGIT = int
    INT = int
    SIGNED_INT = int
    SIGNED_NUMBER = float
    NUMBER = float
    
    def __init__(self, pmac, sep='\x0D'):
        super().__init__()
        self._pmac = pmac
        self._replies = ''
        self._sep = sep
        
    def append_reply(self, reply):
        if reply:
            self._replies += f'{reply}' + self._sep
    
    def replies(self, clear=False):
        replies = self._replies
        if clear:
            self.clear()
        return replies
    
    def clear(self):
        self._replies = ''

    @treedecorator
    def clear_buffer(self, value):
        self._pmac.clear_opened_buffer()

    @treedecorator
    def close_buffer(self, value):
        self._pmac.close_opened_buffer()
        lineno = value[0].end_pos
        
        # value = value[2]
        # print(value.start_pos, value.column, value.end_line, value.end_column, value.end_pos, value.line)
        # this aint pretty
        ex = CloseBufferException()
        ex.data_lineno = lineno
        raise ex
        
    @treedecorator
    def any_str(self, value):
        self._pmac.add_code(value[0])
        # raise RuntimeError(f'Uknown command :{value[0].value}')


class PmacParser:
    def __init__(self, pmac, sep='\x0D'):
        self._pmac = pmac
        self._lark = Lark(pmac_grammar, start='value', propagate_positions=True)
        self._transformer = PmacTransformer(self._pmac, sep=sep)
        self._data = ''
        
    def process_data(self, data=None):
        print('PARSE', repr(data))
        if data is not None:
            self._data += data
        if not self._data:
            return
        try:
            idx = 0
            for line in self._data.split('\r'):
                # print('lINE', line, idx)
                if line:
                    tree = self._lark.parse(line)
                    self._transformer.transform(tree)
                idx += 1 + len(line)
        except VisitError as ex:
            if isinstance(ex.orig_exc, (OpenCsException,)):
                if len(self._data) > ex.orig_exc.data_lineno:
                    self._data = self._data[idx + ex.orig_exc.data_lineno:]
                else:
                    self._data = ''
                self._on_open(cs=True)
            elif isinstance(ex.orig_exc, (OpenProgException,)):
                if len(self._data) > ex.orig_exc.data_lineno:
                    self._data = self._data[idx + ex.orig_exc.data_lineno:]
                else:
                    self._data = ''
                self._on_open(cs=False)
            elif isinstance(ex.orig_exc, (CloseBufferException,)):
                if len(self._data) > ex.orig_exc.data_lineno:
                    self._data = self._data[idx + ex.orig_exc.data_lineno:]
                else:
                    self._data = ''
                self._on_close()
            else:
                print("VISITERR", ex)
                self._data = ''
                raise
        except Exception as ex:
            print("PARSER EXC", ex)
            self._data = ''
            raise
        # TODO: should we support commands splitted
        # across different packets?
        self._data = ''
        
    def _on_open(self, cs=False):
        print( "ON OPEN", cs)
        replies = self._transformer.replies()
        if cs:
            self._lark = Lark(pmac_cs_grammar, start='value', propagate_positions=True)
            self._transformer = PmacCsTransformer(self._pmac, sep=self._transformer._sep)
        else:
            self._lark = Lark(pmac_prog_grammar, start='value', propagate_positions=True)
            self._transformer = PmacProgTransformer(self._pmac, sep=self._transformer._sep)

        self._transformer._replies = replies
        self.process_data()
        
    def _on_close(self):
        replies = self._transformer.replies()
        self._lark = Lark(pmac_grammar, start='value', propagate_positions=True)
        self._transformer = PmacTransformer(self._pmac, sep=self._transformer._sep)
        self._transformer._replies = replies
        self.process_data()
        
    def replies(self, clear=False):
        replies = self._transformer.replies(clear=clear)
        if replies is None or len(replies) == 0:
            return None
        return replies


if __name__ == '__main__':
    # from bm20.simulation.turbopmac.pmacsimu import PmacMockup
    # config = """"
    # """"

    # pmac = PmacMockup()
    text = "OPEN PROG 666"#"M145->Y:$0000C0,10,1,D"#"#3->I"#"OPENFORWARDM161->D:125"#'#M161#1?TYPEI123=29834J:5P'
    lark = Lark(pmac_grammar, start='value', propagate_positions=True)
    tree = lark.parse(text)
    print(tree.pretty())
    try:
        tr = PmacTransformer(None).transform(tree)
    except VisitError as ex:
            print('===============\n\n\n========', ex)
            if isinstance(ex.orig_exc, (OpenBufferException,)):
                print('@%#$@&%&#76&&&$%^&%^OPENM')
            else:
                raise
    # print(tr)
