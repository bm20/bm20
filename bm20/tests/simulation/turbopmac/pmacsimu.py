import enum
import datetime
from collections import deque
import traceback

import gevent

from gevent import server, pool, socket, select

import struct
from collections import deque

from bliss.common import session
from bliss.common.axis import Axis, AxisOnLimitError
from bliss import global_map
from bliss.common.logtools import log_info, log_debug, log_critical
from bliss.config.static import get_config
from bliss.config.plugins.emotion import create_objects_from_config_node

from .d20mockup import Mockup
from .pmacmotor import PmacMotor
from .pmacprogram import PmacProgram
from .pmaccoordsys import PmacCoordinateSystem
from .pmacvariables import PmacVariables, PmacMVariables, PmacIVariables
from .pmacserver import PmacServer
        

class PmacMockup(Mockup):
    def __init__(self, *args, reset=False, **kwargs):
        super().__init__(*args, **kwargs)
        self._pmac_motors = {}
        # self._config = config
        
        # self._motor_addr = 1
        self._current_motor = None

        self._i_vars = PmacIVariables(self)
        self._p_vars = PmacVariables(self)
        self._m_vars = PmacMVariables(self)
        self._pmac_state_flags = {}
        self._pmac_axis_status_override = {}
        
        self._programs = {}
        self._selected_cs = -1
        self._coord_sys = {}
        self._current_opened = None

        self._plcs = {}
        
        self._reset_on_init = reset

        # self.axis_settings.add("pmac_address", int, config=True, persistent=False)
        # self.axis_settings.config_setting["pmac_address"] = False

        # axes = config.get('axes')
        # for ax in axes:
        #     self._init_pmac_axis_map(ax['ref'], ax['address'], reset=reset)

    ###################################
    # Server
    ###################################
    def run_server(self, *args, **kwargs):
        self._server = PmacServer(self)
        self._server.run_server(*args, **kwargs)

    def initialize(self, *args, **kwargs):
        super().initialize(*args, **kwargs)

        cs_list = self.config.get('coordinate_systems')
        for cs in cs_list:
            address = cs.address
            if address in self._coord_sys:
                raise RuntimeError(f"Already a CS with that address [{address}].")
            self._coord_sys[address] = cs
            self._coord_sys[address]._set_pmac(self)
            self._m_vars.init_pmac_cs_variables(address)

    def initialize_axis(self, axis):
        # TODO: should we put some of this in initialize_hardware_axis?
        super().initialize_axis(axis)
        address = axis.config.get('pmac_address')
        motor = PmacMotor(self, address, axis)
        self._pmac_motors[address] = motor
        if self._current_motor is None:
            self._current_motor = motor
            
        self._pmac_state_flags[address] = 0
        setattr(axis, 'pmac_address', address)
        # axis.apply_config(reload=True)
        # axis.sync_hard()
        if self._reset_on_init:
            home_pos = axis.config.get('home_pos', 0)
            axis.offset = 0
            axis.controller.set_hw_position(axis, home_pos)
        home_pos = axis.config.config_dict.get('home_pos')
        if home_pos is None:
            print(f'Axis {axis.name}: home_pos not found in config, '
                   'home will be at position 0.')
        else:
            print(f'Axis {axis.name}: home_pos found in config: {home_pos}.')
            
        print(f'Axis {axis.name}: hw_limits set to '
              f'[{axis.limits[0]+1},  {axis.limits[1]-1}]')
        axis.controller.set_hw_limits(axis, axis.limits[0]+1, axis.limits[1]-1)
        # axis.sync_hard()
        self._i_vars.init_pmac_motor_variables(address)
        self._m_vars.init_pmac_motor_variables(address)
        print('INITIALIZED', axis.name, address)

    def reset_axes(self):
        for motor in self._pmac_motors.values():
            axis = motor.axis
            home_pos = axis.config.get('home_pos', 0)
            axis.offset = 0
            axis.controller.set_hw_position(axis, home_pos)
            home_pos = axis.config.config_dict.get('home_pos')
            if home_pos is None:
                print(f'Axis {axis.name}: home_pos not found in config, '
                    'home will be at position 0.')
            else:
                print(f'Axis {axis.name}: home_pos found in config: {home_pos}.')
                
            print(f'Axis {axis.name}: hw_limits set to '
                f'[{axis.limits[0]+1},  {axis.limits[1]-1}]')
            axis.controller.set_hw_limits(axis, axis.limits[0]+1, axis.limits[1]-1)
            axis.sync_hard()
    
    ###################################
    # Misc. PMAC commands
    ###################################

    def pmac_get_i_value(self, address):
        """
        getting a I variable
        """
        return self._i_vars.read(address)

    def pmac_set_i_value(self, address, value, **kwargs):
        """
        setting a I register
        """
        self._i_vars.write(address, value, **kwargs)
        
    def pmac_get_p_value(self, address, **kwargs):
        """
        getting a P variable
        """
        return self._p_vars.read(address, **kwargs)

    def pmac_set_p_value(self, address, value, **kwargs):
        """
        setting a P variable
        """
        self._p_vars.write(address, value, **kwargs)

    def pmac_get_m_value(self, address, **kwargs):
        """
        getting a M variable
        """
        return self._m_vars.read(address, **kwargs)

    def pmac_set_m_value(self, address, value, **kwargs):
        """
        setting a M variable
        """
        self._m_vars.write(address, value, **kwargs)
        
    # def pmac_set_m_mapping(self, address, mem_type, mapping, **kwargs):
    #     """
    #     Assigning a mapping to a M variable
    #     """
    #     self._m_vars.set_mapping(address, mem_type, mapping, **kwargs)
        
    def pmac_version(self):
        # WARNING! Leave SIMU as the first work as is it used by other simu
        # classes to make sure they're talking to the right pmac
        return 'SIMU 1.0'
        
    def pmac_type(self):
        # WARNING! Leave SIMU as the first work as is it used by other simu
        # classes to make sure they're talking to the right pmac
        return 'SIMU PMAC'

    def pmac_time(self):
        return datetime.datetime.now().strftime('%I:%m:%S')

    def pmac_date(self):
        return datetime.datetime.now().strftime('%m/%d/%Y')
        
    ###################################
    # Motors
    ###################################
    
    def get_pmac_motor(self, address=None):
        if address is None:
            return self.current_motor
        return self._pmac_motors[address]
    
    @property
    def current_motor(self):
        return self._current_motor
    
    @property
    def motor_addr(self):
        return self._current_motor.address
    
    @motor_addr.setter 
    def motor_addr(self, addr):
        self._current_motor = self._pmac_motors[addr]
    
    def pmac_position(self, address=None):
        motor = self.get_pmac_motor(address)
        return motor.position
        
    def pmac_motor_status(self, address=None):
        #TODO : homing flag (bit 10 of first word)
        log_debug(self, 'motor_status : in')
        # axis = motor.axis
        # state = axis.state

        motor = self.get_pmac_motor(address)
        # log_debug(self, f'Motor #{address} state: {axis.state}')

        # plc = self._get_running_plc()
        # if plc:
        #     status = plc.status(axis)
        #     if status is not None:
        #         return status

        status = self._pmac_axis_status_override.get(motor.address)

        if status is None:
            status = motor.status
            # status = 0
            # # activated
            # status |= (not state.OFF) << (23 + 24)
            # # lim_pos
            # status |= state.LIMPOS << (21 + 24)
            # # lim_neg
            # status |= state.LIMNEG << (22 + 24)
            # # moving
            # status |= state.MOVING << (17 + 24)
            # status |= state.MOVING << (19 + 24)
            # # ? open_loop
            # status |= 1 << (18 + 24)
            # # ? integration_mode
            # status |= 1 << (16 + 24)
            # # coordinates
            # status |= 7 << 16

            # if not state.MOVING and not state.OFF and not state.FAULT:
            #     status |= 1

        status |= self.pmac_user_state_flags(motor.address)

        log_debug(self, 'motor_status : returning status %x.' % status)
        return status

    def pmac_stop(self, address=None):
        motor = self.get_pmac_motor(address)
        motor.stop()

    def pmac_jog_to(self, position, address=None):
        motor = self.get_pmac_motor(address)
        motor.jog_to(position)

    def pmac_jog_relative(self, distance, address=None):
        motor = self.get_pmac_motor(address)
        motor.jog_relative(distance)

    def pmac_jog(self, direction, address=None):
        motor = self.get_pmac_motor(address)
        if direction >= 0:
            motor.jog_positive()
        else:
            motor.jog_negative()

    def pmac_kill_motor(self, address=None):
        motor = self.get_pmac_motor(address)
        motor.stop()

    def set_axes_homed_flags(self):
        for motor in self._pmac_motors.values():
            log_info(self, f'Setting home flag for axis {motor.axis.name}.')
            self.pmac_set_user_state_flags(motor.address, 1 << 10)

    def clear_axes_homed_flags(self):
        for motor in self._pmac_motors.values():
            log_info(self, f'Clearing home flag for axis {motor.axis.name}.')
            self.pmac_clear_user_state_flags(motor.address, 1 << 10)

    ###################################
    # CS and PROG
    ###################################

    def has_opened_buffer(self, raise_if_opened=False):
        opened = self._current_opened is not None
        if raise_if_opened and opened:
            raise RuntimeError("A buffer is already opened.")

    def set_opened_buffer(self, buffer):
        self._current_opened = buffer

    def get_opened_buffer(self, raise_if_opened=False):
        """
        Returns the currently opened buffer.
        Raise an error if no buffer is opened.
        """
        current = self._current_opened
        if current is not None and raise_if_opened:
            raise RuntimeError(f'There is already a prog/cs opened.')
        return current

    def close_opened_buffer(self):
        current = self.get_opened_buffer()
        if current:
            current.close_buffer()
            self.set_opened_buffer(None)

    def clear_opened_buffer(self):
        current = self.get_opened_buffer()
        current.clear_buffer()

    def set_selected_cs(self, address):
        """
        """
        self._selected_cs = address
    
    def get_selected_cs(self):
        """
        """
        current = self._selected_cs
        if current < 1:
            raise RuntimeError('No coordinate system selected yet.')
        return self.get_coord_sys(cs_id=current)

    def get_coord_sys(self, cs_id=None):
        """
        Returns the coordinate system.
        Currently selected if cs_id is None.
        """
        if cs_id is None:
            return self.get_selected_cs()
        if cs_id < 1 or cs_id > 16:
            raise RuntimeError(f'Invalid coordinate system: {cs_id}.')
        cs = self._coord_sys.get(cs_id)
        if not cs:
            raise RuntimeError(f'Undefined coordinate system {cs_id} (should be defined in config).')
        #     cs = PmacCoordinateSystem(cs_id)
        #     self._coord_sys[cs_id] = cs
        return cs
    
    def open_cs(self, cs_id=None, inverse=True):
        self.has_opened_buffer(True)
        cs = self.get_coord_sys(cs_id)
        if inverse:
            cs.open_inverse()
        else:
            cs.open_forward()
        self.set_opened_buffer(cs)

    def open_inverse(self):
        self.open_cs(inverse=True)
        
    def open_forward(self):
        self.open_cs(inverse=False)
        
    def close_cs(self, num):
        cs = self.get_coord_sys(num)
        cs.close()
        self.set_current_opened(None)
    
    def get_program(self, num):
        prog = self._programs.get(num)
        if not prog:
            prog = PmacProgram(self, num)
            self._programs[num] = prog
        return prog
    
    def open_program(self, num):
        self.has_opened_buffer(True)
        prog = self.get_program(num)
        prog.open()
        self.set_opened_buffer(prog)
        
    def add_code(self, code):
        current = self.get_opened_buffer()
        if current is None:
            raise RuntimeError('No prog/cs opened.')
        current.add_code(code)

    def abort_program(self):
        print("TODO: ABOOOOOOOOOOOOOOOOOOORT")

    def point_program(self, value):
        cs = self.get_selected_cs()
        cs.set_program_pointer(value)

    def run_program(self):
        cs = self.get_selected_cs()
        cs.run_program()

    ###################################
    # PLC
    ###################################

    def _add_plc(self, plc):
        if plc.name in self._plcs:
            raise ValueError(f'There is already a PLC with name {plc.name}.')
        self._plcs[plc.name] = plc
        
    def _override_pmac_axis_status(self, address, status):
        self._pmac_axis_status_override[address] = status

    def _run_plc(self, name):
        plc = self._plcs.get(name)
        if not plc:
            raise ValueError(f'PLC {name} not found.')
        if self._get_running_plc(): 
            print(f'A PLC is already running.')
            return
        plc.start()

    def _get_running_plc(self):
        for plc in self._plcs.values():
            if not plc.ready():
                return plc
        return None

    def _kill_plc(self, name):
        plc = self._plcs.get(name)
        if plc:
            plc.kill()

    def _kill_plcs(self):
        for name, plc in self._plcs.items():
            print(f'Killing {name}.')
            plc.kill()
            print(f'Killed {name}.')

    def pmac_user_state_flags(self, address):
        return self._pmac_state_flags.get(address, 0)

    def pmac_set_user_state_flags(self, address, flags):
        current = self.pmac_user_state_flags(address)
        self._pmac_state_flags[address] |= flags

    def pmac_clear_user_state_flags(self, address, flags=None, clear_all=False):
        if clear_all:
            self._pmac_state_flags[address] = 0
        if flags is None:
            return
        # state is a 12 hex digits number
        mask = 0xFFFFFFFFFFFF ^ flags
        self._pmac_state_flags[address] &= mask


def main(name, klass=None):
    # if klass is None:
    #     klass = PmacSimu
    # pmac = klass(name, {'controller': name})
    pmac = PmacMockup(name, {'controller': name})
    pmac.run_server()


if __name__ == '__main__':
    main('mockup_pmac')
