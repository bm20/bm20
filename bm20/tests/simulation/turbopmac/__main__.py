
def run_server(name, *args, **kwargs):
    from bliss.config.static import get_config
    config = get_config()
    pmacsimu = config.get(name)

    # force initialization
    for axis in pmacsimu.config.get('axes'):
        a = config.get(axis.get('name'))
        a.state
    pmacsimu.run_server(*args,  **kwargs)


if __name__ == '__main__':
    name = 'd20pmacmockup'
    run_server(name, reset_axes=1, home_axes=1)