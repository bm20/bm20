"""

"""

def cs_to_sx(cs_num):
    # see pmac turbo srm page 216:
    # In the generic description of these I-variables, the thousands digit is represented by the letter s, and the
    # hundreds digit by the letter x; for example, Isx11. s and x can take the following values:
    # s is equal to 5 for Coordinate Systems 1 – 9;
    # s is equal to 6 for Coordinate Systems 10 – 16;
    # x is equal to the coordinate system number for Coordinate Systems 1 – 9;
    # x is equal to the (coordinate system number minus 10) for Coordinate Systems 10 – 16.
    return ((cs_num < 10 and 5) or 6) * 10 + cs_num % 10


def newgetset(inst, name):
    """
    Returns a callable from a property
    """
    def _getset(value=None):
        if value is None:
            return getattr(inst, name)
        else:
            getattr(inst.__class__, name).fset.__get__(inst)(value)
    return _getset


class PmacVariables:
    pmac = property(lambda self: self._pmac)
    lock_write = property(lambda self: self._lock_write)
    
    def __init__(self, pmac, default=0, lock_write=False, lock_read=True):
        """
        default: default value to be returned if nothing is stored
        lock_read: raises an exception if trying to read a value that wasnt
                stored before.
        write: raises an exception if trying to write to value that wasnt
                stored before (i.e: at initialization).
        """
        self._pmac = pmac
        self._default = default
        self._lock_write = lock_write
        self._lock_read = lock_read
        self._variables = {}
        
    @lock_write.setter
    def lock_write(self, lock_write):
        self._lock_write = lock_write
        
    def write(self, address, value, unlock=False):
        lock = False if unlock else self.lock_write
        if lock:
            old = self._variables[address]
        else:
            old = self._variables.get(address)
        if callable(old) and not callable(value):
            old(value)
        else:
            self._variables[address] = value
        
    def read(self, address, unlock=False):
        lock = False if unlock else self.lock_write
        if lock:
            value = self._variables[address]
        else:
            value = self._variables.get(address, self._default)
        if callable(value):
            val = value()
            return val
        return value
    

class PmacIVariables(PmacVariables):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        lock = self.lock_write
        self.lock_write = 0
        self.write(3, 2)
        self.write(4, 0)
        self.lock_write = lock
        
    def write_axis_i_variable(self, motor_addr, var_addr, value, **kwargs):
        self.write(int(motor_addr) * 100 + int(var_addr), value, **kwargs)
        
    def init_pmac_motor_variables(self, motor_addr):
        # Ixx08 : motor position scale factor
        # Ixx15 : motor abort/limit deceleration rate
        # Ixx19 : max jog/home acceleration
        # Ixx20 : acceleration time msec
        # Ixx21 : jog/home S-Curve time
        # Ixx22 : jog speed counts / ms
        # Ixx23 : home speed and direction
        
        motor = self.pmac.get_pmac_motor(motor_addr)
        lock = self.lock_write
        self.lock_write = 0
        self.write_axis_i_variable(motor_addr, 8, 96)
        self.write_axis_i_variable(motor_addr, 0, newgetset(motor, 'motor_activation'))
        self.write_axis_i_variable(motor_addr, 15, newgetset(motor, 'deceleration_rate'))
        self.write_axis_i_variable(motor_addr, 19, 1000.)
        self.write_axis_i_variable(motor_addr, 20, newgetset(motor, 'acceleration_time'))
        self.write_axis_i_variable(motor_addr, 21, 0.)
        self.write_axis_i_variable(motor_addr, 22, newgetset(motor, 'jog_speed'))
        self.write_axis_i_variable(motor_addr, 23, newgetset(motor, 'home_speed'))
        self.lock_write = lock
        
        
class PmacMVariables(PmacVariables):
    def write(self, address, value, *args, **kwargs):
        if isinstance(value, (PmacAddrMap,)):
            self.set_mapping(address, value, *args, **kwargs)
        else:
            super().write(address, value, *args, **kwargs)

    def set_mapping(self, address, memmap, *args, **kwargs):
        # currently:
        # only supports Mxx61 mapping to axis commanded position
        # only supports Mxx62 mapping to axis real position
        # only support Mxx45 homed flag
        # only support Msx82 cs runtime error bit
        # e.g:
        # M161->D:$000088 commanded pos (1/[Ixx08*32] cts)
        # M162->D:$00008B actual pos (1/[Ixx08*32] cts)
        # M145->Y:$0000C0,10,1 home complete
        # M6682->Y:$002F3F,22,1       ; CS 16 run-time error bit
        # M6687->Y:$002F3F,17,1         ; &16 In-position bit (AND of motors)
        # M6680->X:$002F40,0,1      ; &16 Program-running bit
        base_reg_home = 0x0C0
        base_reg_cs_err = 0x203F
        base_reg_cs_ctl = 0x2040

        for axis_addr in range(0, 32):
            offset = axis_addr*2**7
            if memmap.memtype == 'D' and memmap.addr == 0x88 + offset or memmap.addr == 0x8B + offset:
                # commanded or current position
                motor = self._pmac.get_pmac_motor(axis_addr + 1)
                def func():
                    ix08 = self.pmac.pmac_get_i_value((axis_addr + 1) * 100 + 8)
                    return motor.position * ix08 * 32
                mapping = PmacMemMapping(address, memmap, func)
                break
            if (memmap.addr == base_reg_home + offset
                and memmap.memtype == 'Y'
                and memmap.offset == 10
                and memmap.width == 1):
                motor = self._pmac.get_pmac_motor(axis_addr + 1)
                func = newgetset(motor, 'homed')
                mapping = PmacMemMapping(address, memmap, func)
                break
        else:
            for cs_addr in range(0, 16):
                offset = cs_addr*2**8
                if (memmap.addr == base_reg_cs_err + offset
                    and memmap.memtype == 'Y'
                    and memmap.offset == 22
                    and memmap.width == 1):
                    cs = self._pmac.get_coord_sys(cs_addr + 1)
                    func = newgetset(cs, 'pmac_cs_runtime_error_bit')
                    mapping = PmacMemMapping(address, memmap, func)
                    break
                if (memmap.addr == base_reg_cs_err + offset
                    and memmap.memtype == 'Y'
                    and memmap.offset == 17
                    and memmap.width == 1):
                    cs = self._pmac.get_coord_sys(cs_addr + 1)
                    func = newgetset(cs, 'pmac_cs_in_position')
                    mapping = PmacMemMapping(address, memmap, func)
                    break
                if (memmap.addr == base_reg_cs_ctl + offset
                    and memmap.memtype == 'X'
                    and memmap.offset == 0
                    and memmap.width == 1):
                    cs = self._pmac.get_coord_sys(cs_addr + 1)
                    func = newgetset(cs, 'pmac_cs_program_running')
                    mapping = PmacMemMapping(address, memmap, func)
                    break
            else:
                raise RuntimeError(f'Unsupported mapping: {address}->{memmap}.')
        
        self.write(address, mapping, *args, **kwargs)

    def init_pmac_motor_variables(self, motor_addr):
        # commanded position mapping
        mapping = PmacAddrMap(0x88 + (motor_addr - 1)*2**7, 'D')
        self.write(motor_addr*100 + 61, mapping)
        # actual position mapping
        mapping = PmacAddrMap(0x8B + (motor_addr - 1)*2**7, 'D')
        self.write(motor_addr*100 + 62, mapping)
        # home flag mapping
        mapping = PmacAddrMap(0xC0 + (motor_addr - 1)*2**7,
                              'Y', offset=10, width=1)
        self.write(motor_addr*100 + 45, mapping)

    def init_pmac_cs_variables(self, cs_addr):
        # M6682->Y:$002F3F,22,1       ; CS 16 run-time error bit
        # M6687->Y:$002F3F,17,1         ; &16 In-position bit (AND of motors)
        # M6680->X:$002F40,0,1      ; &16 Program-running bit
        sx = cs_to_sx(cs_addr)
        base_reg_cs_err = 0x203F
        base_reg_cs_ctl = 0x2040
        mapping = PmacAddrMap(base_reg_cs_ctl + (cs_addr - 1)*2**8, 'X', 0, 1)
        self.write(sx*100 + 80, mapping)
        mapping = PmacAddrMap(base_reg_cs_err + (cs_addr - 1)*2**8, 'Y', 22, 1)
        self.write(sx*100 + 82, mapping)
        mapping = PmacAddrMap(base_reg_cs_err + (cs_addr - 1)*2**8, 'Y', 17, 1)
        self.write(sx*100 + 87, mapping)


class PmacAddrMap:
    def __init__(self, addr, memtype, offset=None, width=None, format=None):
        self.addr = addr
        self.memtype = memtype
        self.offset = offset
        self.width = width
        self.format = format

    def __eq__(self, other):
        return (other.add == self.add and
                other.memtype == self.memtype and
                other.offset == self.offset and
                other.width == self.width and
                other.format == self.format)

    def __repr__(self):
        repr = f'PmacAddrMap({self.memtype}:${hex(self.addr)}'
        if self.offset is not None:
            repr += f',{self.offset}'
        if self.width is not None:
            repr += f',{self.width}'
        if self.format is not None:
            repr += f',{self.format}'
        repr += ')'
        return repr


class PmacMemMapping:
    def __init__(self, address, mapping, func):
        self._address = address
        # self._mem_type = mem_type
        self._mapping = mapping
        self._func = func

    def __call__(self, *args, **kwargs):
        return self._func(*args, **kwargs)