from bliss.controllers.motor import CalcController


def _cs_num_to_sx(cs_addr):
    # see pmac turbo srm page 216:
    # In the generic description of these I-variables, the thousands digit is represented by the letter s, and the
    # hundreds digit by the letter x; for example, Isx11. s and x can take the following values:
    # s is equal to 5 for Coordinate Systems 1 – 9;
    # s is equal to 6 for Coordinate Systems 10 – 16;
    # x is equal to the coordinate system number for Coordinate Systems 1 – 9;
    # x is equal to the (coordinate system number minus 10) for Coordinate Systems 10 – 16.
    return ((cs_addr < 10 and 5) or 6) * 10 + cs_addr % 10


class PmacCoordinateSystem(CalcController):
    inverse_opened = property(lambda self: self._inverse_opened)
    forward_opened = property(lambda self: self._forward_opened)
    opened = property(lambda self: self._inverse_opened or self._forward_opened)
    forward = property(lambda self: "\r".join(self._forward))
    inverse = property(lambda self: "\r".join(self._inverse))
    address = property(lambda self: self._cs_id)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._axis_def = {}
        self._inverse_opened = False
        self._forward_opened = False
        self._inverse = []
        self._forward = []
        self._prog_pointer = None
        self._cs_id = self.config.get('address')
        self._cs_sx = _cs_num_to_sx(self._cs_id)
        self._pmac = None
        self._running = 0

    def _set_pmac(self, pmac):
        self._pmac = pmac
        pmac.pmac_set_i_value(self._cs_sx * 100 + 90, 1000)

    def set_program_pointer(self, value):
        self._prog_pointer = value

    def run_program(self):
        print(f'RUNNING PROG {self._prog_pointer} on CS {self.name}')
        kwargs = {}
        for tag in ('x', 'y', 'z'):
            # TODO: handle case
            tooltip = self._tagged.get(tag, [None])[0]
            if tooltip:
                kwargs[tag.upper()] = tooltip
        self._pmac.get_program(self._prog_pointer).run_motion_program(cs=self, **kwargs)
        
    def open_inverse(self):
        if self.opened:
            raise RuntimeError('CS already opened.')
        self._inverse_opened = True
        
    def close_inverse(self):
        self._inverse_opened = False
        
    def open_forward(self):
        if self.opened:
            raise RuntimeError('CS already opened.')
        self._forward_opened = True
        
    def close_forward(self):
        self._forward_opened = False
        
    def close_buffer(self):
        self.close_forward()
        self.close_inverse()
        
    def clear_buffer(self):
        if self._forward_opened:
            self._forward.clear()
        elif self._inverse_opened:
            self._inverse.clear()
        else:
            raise RuntimeError(f'No buffer opened in CS {self.num}.')
        
    def add_code(self, data):
        if self._forward_opened:
            self._forward.append(data)
        elif self._inverse_opened:
            self._inverse.append(data)
        else:
            raise RuntimeError(f'No buffer opened in CS {self.num}.')
        
    def set_running(self, running):
        self._running = 1 if running else 0

    @property
    def pmac_cs_runtime_error_bit(self):
        return (any('ERROR' in real.state for real in self.reals) and 1) or 0

    @property
    def pmac_cs_program_running(self):
        return (any('MOVING' in real.state for real in self.reals) and 1) or self._running

    @property
    def pmac_cs_in_position(self):
        return (not self.pmac_cs_program_running and 1) or 0