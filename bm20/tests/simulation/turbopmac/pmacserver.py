"""
Server handling the communication to the SIMU PMAC.
"""


import enum
import datetime
from collections import deque
import traceback

import gevent

from gevent import server, pool, socket, select

import struct
from collections import deque

from bliss.common import session
from bliss.common.axis import Axis, AxisOnLimitError
from bliss import global_map
from bliss.common.logtools import log_info, log_debug, log_critical
from bliss.config.static import get_config
from bliss.config.plugins.emotion import create_objects_from_config_node

from .d20mockup import Mockup
from .pmacmotor import PmacMotor
from .pmacprogram import PmacProgram
from .pmaccoordsys import PmacCoordinateSystem
from .pmacvariables import PmacVariables, PmacMVariables, PmacIVariables



class PmacRequestType(enum.IntEnum):
    VR_DOWNLOAD = 0x40
    VR_UPLOAD = 0xc0


class PmacRequest(enum.IntEnum):
    VR_PMAC_SENDLINE = 0xb0
    VR_PMAC_GETLINE = 0xb1
    VR_PMAC_FLUSH = 0xb3
    VR_PMAC_GETMEM = 0xb4
    VR_PMAC_SETMEM = 0xb5
    VR_PMAC_SETBIT = 0xba
    VR_PMAC_SETBITS = 0xbb
    VR_PMAC_PORT = 0xbe
    VR_PMAC_GETRESPONSE = 0xbf
    VR_PMAC_READREADY = 0xc2
    VR_CTRL_RESPONSE = 0xc4
    VR_PMAC_GETBUFFER = 0xc5
    VR_PMAC_WRITEBUFFER = 0xc6
    VR_PMAC_WRITEERROR = 0xc7
    VR_FWDOWNLOAD = 0xcb
    VR_IPADDRESS = 0xe0


_CONTROLLER_NAME = 'SIMU_PMAC'
_CONTROLLER_PORT = 26666
_CONTROLLER_BUF_SIZE = 1024


class PmacServer:
    ACK = '\x06'
    ERR = '\x07'
    SEP = '\x0D'
    SINGLE  = '@'

    pmac = property(lambda self: self._pmac)

    def __init__(self, pmac):
        global_map.register(self,
                            children_list=[],
                            tag='PmacServer')

        self._pmac = pmac
        self._server = server

        self._output_buffer = deque()
        
        from .pmacparser import PmacParser
        self._pmac_parser = PmacParser(self._pmac, sep=self.SEP)

    def clear_reply_queue(self):
        """
        Clears the reply queue.
        """
        log_debug(self, 'Clearing the reply queue.')
        self._output_buffer.clear()

    def _append_reply(self, reply):
        if reply:
            log_debug(self, 'Appending {0} to buffer ({1}).'
                            ''.format(reply, self._output_buffer))
            self._output_buffer.append(reply)
        else:
            log_debug(self, 'No reply to append to queue.')

    def consume_data(self, input_buffer):
        reply = None

        header = input_buffer[0:8]
        req_type, req, value, index, lg = struct.unpack('BBHHH', header)
        lg_data = socket.ntohs(lg)

        req_type = PmacRequestType(req_type)
        req = PmacRequest(req)

        log_debug(self, '\n\tUnpacked:\n'
                            '\t   - req_type = %s\n'
                            '\t   - req = %s\n'
                            '\t   - value = %d\n'
                            '\t   - index = %d\n'
                            '\t   - lg = %d' %
                            (req_type, req, value, index, lg_data))

        try:
            if req_type == PmacRequestType.VR_DOWNLOAD:
                data = input_buffer[8:8 + lg_data]
                cmd = data.decode()

                if lg > 0:
                    lg_buffer = len(input_buffer)
                    if lg_buffer < lg_data:
                        log_debug(self, 'Did not receive enough data,'
                                        ' waiting for more (%d / %d).'
                                        % (lg_buffer, lg_data))
                        return 8
                if req == PmacRequest.VR_PMAC_SENDLINE:
                    # process the command and replies with an ACK
                    self._pmac_parser.process_data(cmd)
                    reply = self.SINGLE
                elif req == PmacRequest.VR_PMAC_FLUSH:
                    # flushes the buffer
                    self.clear_reply_queue()
                    reply = self.SINGLE
                else:
                    raise ValueError('Unknown or unsupported VR_DOWNLOAD '
                                     'request : %d' % req_type)

            elif req_type == PmacRequestType.VR_UPLOAD:
                if req == PmacRequest.VR_PMAC_GETBUFFER:
                    # returns all data until the next ACK or LF.
                    reply = self._pmac_parser.replies(clear=True)
                    if reply is not None:
                        reply += self.ACK
                    else:
                        reply = '\x00'
                    log_debug(self, 'Unqueued next reply %s' % reply)
                else:
                    raise ValueError('Unknown or unsupported VR_UPLOAD '
                                     'request: %d' % req_type)
            else:
                raise ValueError('Unknown request_type: %d' % req_type)

        except Exception as ex:
            log_debug(self, 'consume_data: %s' % ex)
            reply = self.ERR + '\rERR666'
            print('consume', ex, traceback.format_stack())

        log_debug(self, '==============> {0}'.format(self._output_buffer))

        return reply

    def custom_command(self, data):
        return 0, data

    def pre_run_server(self,
                        reset_axes=False,
                        home_axes=False,
                        **kwargs):
        pass

    def run_server(self,
                   reset_axes=False,
                   home_axes=False,
                   **kwargs):
        """
        Starts the PMAC server.

        reset_axes: bool, True to reset all axes: reload config, reset limits
        home_axes: list of names, sets the "homed" flag on the gives axes.
        """
        log_debug(self, 'Starting server.')

        # dirty trick to force initialization of variables
        for name in self.pmac.axes.keys():
            self.pmac.get_axis(name).velocity

        self._port = _CONTROLLER_PORT
        self._server = None
        self._pool = pool.Pool(2)

        if reset_axes:
            self.pmac.reset_axes()

        if home_axes:
            self.pmac.set_axes_homed_flags()
        else:
            self.pmac.clear_axes_homed_flags()

        log_debug(self, 'Using port %d.' % self._port)
        self._server = server.StreamServer(('0.0.0.0', self._port),
                                            self._handle,
                                            spawn=self._pool)

        self.pre_run_server(**kwargs)
        log_info(self, 'Calling serve_forever.')
        log_info(self, 'Ready to accept requests.')
        print('Serving.')
        self._server.serve_forever()
        log_debug(self, 'Exited serve_forever.')

    def _handle(self, sock, address):
        log_info(self, 'Accepted connection from {0}.'
                       ''.format(sock.getpeername()))

        while(select.select([sock], [], [])):
            # socket.wait_read(sock)#, timeout=0.5)
            log_debug(self, '================')
            log_debug(self, '=== New data ===')
            log_debug(self, '================')
            reply = None

            data = sock.recv(_CONTROLLER_BUF_SIZE)
            log_debug(self, 'Received data %s.' % data)
            if not data:
                log_info(self, 'Client disconnected.')
                break

            try:
                reply = self.consume_data(data)
            except Exception as ex:
                print(ex)
                self.clear_reply_queue()
                self._append_reply(self.ERR)
                self._append_reply('ERR666')

            if reply:
                reply = reply.encode()
                log_debug(self, 'Sending reply: %s' % reply)
                sock.sendall(reply)

            log_debug(self, f'Replied: {reply}.')


# class PmacSimu:
#     runner_klass = PmacSimuRunner

#     def __init__(self, name, config):
#         global_map.register(self,
#                             children_list=[],
#                             tag='PmacSimu')
#         self._pmac_mockup = config.get('controller')

    