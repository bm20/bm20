import time

from bliss.common.axis import AxisOnLimitError
from bliss.common.logtools import log_info, log_debug, log_critical, log_warning

import gevent
from gevent import pool, Greenlet


def set_home_flag(pmac, motor_id, *args, **kwargs):
    log_info(pmac, f'Setting home flags for motor {motor_id}.')
    axis = pmac.get_pmac_motor(motor_id).axis
    home_pos = axis.config.config_dict.get('home_pos', 0)
    log_info(pmac, f'Axis {axis.name} is at position {axis.position}. Home position is {home_pos}.')
    if axis.position >= home_pos - axis.tolerance and axis.position <= home_pos + axis.tolerance:
        log_info(pmac, f'Home reached for axis {axis.name} ({axis.position}).')
        pmac.pmac_set_user_state_flags(motor_id, 1 << 10)
    else:
        log_info(pmac, f'Home not reached for axis {axis.name}.')
    pmac.pmac_clear_user_state_flags(motor_id, 1 << (10 + 24))


def clear_home_flags(pmac, motor_id, *args, **kwargs):
    log_info(pmac, f'Clearing home flags for motor {motor_id}.')
    pmac.pmac_clear_user_state_flags(motor_id, 1 << 10)
    pmac.pmac_clear_user_state_flags(motor_id, 1 << (10 + 24))


class PmacMotor:
    address = property(lambda self: self._addr)
    axis = property(lambda self: self._axis)
    position = property(lambda self: self.axis.position)
    
    def __init__(self, pmac, axis_addr, axis):
        self._axis = axis
        self._addr = axis_addr
        self._pmac = pmac
        self._pool = pool.Pool()
        self._homed = 0
        self._closed_loop = False

    def  _run_in_thread(self,
                        func,
                        *args,
                        on_success=None,
                        on_failure=None,
                        on_limit=None,
                        updt_ival_s=1,
                        **kwargs):
        def _loop():
            t0 = 0
            mvth = gevent.spawn(func, *args, **kwargs)
            axis = self.axis
            address = self.address
            log_info(self, f'Started, axis {axis.name}.')
            gevent.sleep(0.1)
            while 'MOVING' in axis.state:
                t1 = time.time()
                if t1 - t0 >= updt_ival_s:
                    t0 = t1
                    log_info(self, f'Axis {axis.name} is MOVING, pos={axis.position}.')
                gevent.sleep(0.1)
            mvth.join()
            try:
                results = mvth.get()
            except AxisOnLimitError as ex:
                log_info(self, f'Axis {axis.name} hit the limit: {ex}.')
                if on_limit:
                    on_limit(self, address, *args, **kwargs)
            except Exception as ex:
                log_critical(self, f'ERROR in {func}({address}, {args}, {kwargs})')
                log_critical(self, f'{ex}')
                if on_failure:
                    on_failure(self, address, *args, **kwargs)
            else:
                log_info(self, f'Success (axis={axis.name}) {func}.')
                if on_success:
                    on_success(self, address, *args, **kwargs)
            log_info(self, f'DONE threadloop {address}, {func}')

        gl = Greenlet(_loop)
        self._pool.start(gl)
        gevent.sleep(0.1)

    @property
    def deceleration_rate(self):
        """
        Deceleration rate in steps/ms
        """
        accel_steps = self.axis.acceleration * abs(self.axis.steps_per_unit)
        decel_steps_ms = 2 * accel_steps / 1000**2.0
        return decel_steps_ms
    
    @deceleration_rate.setter
    def deceleration_rate(self, decel_rate):
        """
        decel_rate in steps/ms
        """
        # not writing anything as this is not something known by bliss
        # bliss' turbopmac module just sets it to the same value as acceleration
        pass
        
    @property
    def acceleration_time(self):
        """
        Acceleration time in ms
        """
        accel_time_ms = int(0.5 + 1000.0 * self.axis.velocity / self.axis.acceleration)
        return accel_time_ms
    
    @acceleration_time.setter
    def acceleration_time(self, accel_time_ms):
        """
        accel_time in ms
        bliss accel in units/s
        """
        self.axis.acceleration = 1000. * self.axis.velocity / accel_time_ms
    
    @property
    def jog_speed(self):
        """
        jog speed in steps/ms
        """
        return self.axis.velocity * abs(self.axis.steps_per_unit) / 1000.
    
    @jog_speed.setter
    def jog_speed(self, jog_speed):
        """
        jog speed in steps/ms
        POSITIVE number, the direction is controlled by the jog move
        velocity in units/s
        """
        assert jog_speed >= 0.
        self.axis.velocity = 1000. * jog_speed / self.axis.steps_per_unit
        
    @property
    def home_speed(self):
        """
        jog speed in steps/ms
        """
        return self.axis.velocity * abs(self.axis.steps_per_unit) / 1000.
    
    @home_speed.setter
    def home_speed(self, jog_speed):
        """
        jog speed in steps/ms
        velocity in units/s
        """
        self.axis.velocity = 1000. * jog_speed / self.axis.steps_per_unit
        
    @property
    def motor_activation(self):
        return 1 if 'OFF' in self.axis.state else 0
    
    @motor_activation.setter
    def motor_activation(self, activation):
        if activation == 1:
            self.axis.on()
        elif activation == 0:
            self.axis.off()
        else:
            raise RuntimeError(f'Error in motor_activate, expected 0 or 1, '
                               f'got {activation}.')

    @property
    def homed(self):
        return self._homed

    @homed.setter
    def homed(self, homed):
        self._homed = homed

    @property
    def status(self):
        # motor = self.get_pmac_motor(address)
        # axis = motor.axis
        state = self.axis.state

        # log_debug(self, f'Motor #{address} state: {axis.state}')

        # plc = self._get_running_plc()
        # if plc:
        #     status = plc.status(axis)
        #     if status is not None:
        #         return status

        # status = self._pmac_axis_status_override.get(address)
        status = 0
        # activated
        status |= (not state.OFF) << (23 + 24)
        # lim_pos
        status |= state.LIMPOS << (21 + 24)
        # lim_neg
        status |= state.LIMNEG << (22 + 24)
        # moving
        status |= state.MOVING << (17 + 24)
        status |= state.MOVING << (19 + 24)
        # ? open_loop
        if not self._closed_loop:
            status |= 1 << (18 + 24)
        # ? integration_mode
        status |= 1 << (16 + 24)
        # coordinates
        status |= 7 << 16

        if not state.MOVING and not state.OFF and not state.FAULT:
            status |= 1

        # status |= self.pmac_user_state_flags(self.address)

        log_debug(self, 'motor_status : returning status %x.' % status)
        return status

    def stop(self):
        log_info(self, f'Stopping axis {self.address} (state={self.axis.state})')
        self.axis.stop(wait=True)
        self._closed_loop = True
        log_info(self, f'Stopped axis {self.address} (state={self.axis.state}).')

    def open_loop(self):
        self._closed_loop = False

    def jog_to(self, position):
        value = float(position)/self.axis.steps_per_unit
        log_info(self, f'Jogging axis {self.address} to {value} (no_wait).')
        self._run_in_thread(self.axis.move, value, wait=True)

    def jog_relative(self, distance):
        if distance == 0:
            self.open_loop()
        else:
            value = float(distance)/self.axis.steps_per_unit
            log_info(self, f'Jogging axis {self.address} relatively to '
                        f'{value} (no_wait).')
            self._run_in_thread(self.axis.rmove, value, wait=True)

    def jog_positive(self):
        axis = self.axis
        log_info(self, f'Starting jog+ on axis {self.address} '
                       f'at velocity {axis.velocity}.')
        axis.jog(self.axis.velocity)
        self._run_in_thread(self.axis.wait_move)

    def jog_negative(self):
        axis = self.axis
        log_info(self, f'Starting jog- on axis {self.address} '
                       f'at velocity {axis.velocity}.')
        self.axis.jog(-1 * self.axis.velocity)
        self._run_in_thread(self.axis.wait_move)

    def home_axis(self):
        self._run_in_thread(self._pmac_home_axis,
                            on_success=set_home_flag,
                            on_limit=clear_home_flags)

    def _pmac_home_axis(self):
        axis = self.axis
        velocity = self.home_speed
        direction = 1 if velocity >= 0 else -1
        home_pos = axis.config.config_dict.get('home_pos', 0)
        do_home = True
        if direction == 1:
            if axis.position > home_pos:
                do_home = False
        else:
            if axis.position < home_pos:
                do_home = False
        self.pmac_clear_user_state_flags(self.address, 1 << 10)
        self.pmac_set_user_state_flags(self.address, 1 << (10 + 24))
        log_info(self, 'Homing axis %s, %s, speed %s', self.address, axis.name, direction)
        log_info(self, f'Home position set to {home_pos}.')
        if do_home:
            axis.move(home_pos, wait=True)
        else:
            log_info(self, 'Homing axis %s, %s, speed %s', self.address, axis.name, direction)
            log_info(self, f'Home position set to {home_pos}.')
            log_info(self, f'BUT, axis is already at position {axis.position}.')
            log_info(self, 'DOING a jog until death or limit.')
            axis.jog(velocity*10000)
            axis.wait_move()
