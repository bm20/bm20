import gevent

from bliss import global_map


class PmacPLC:
    def __init__(self, name, mockup):
        self.name = name
        self.mockup = mockup
        self._gl = None

        global_map.register(self,
                    children_list=[],
                    tag='PmacPLC')

    def ready(self):
        ready = self._gl.ready() if self._gl else True
        return ready

    def start(self, *args, **kwargs):
        self._gl = gevent.spawn(self.run, *args, **kwargs)

    def run(self, *args,  **kwargs):
        pass

    def kill(self):
        if not self._gl:
            return
        self._gl.kill()