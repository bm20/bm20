from bliss.common.hook import MotionHook
from bliss.controllers.motor import CalcController
from bliss.config.static import get_config


class SimuHook(MotionHook):
    def __init__(self, name, config):
        super(SimuHook, self).__init__()
        self._calct10_axis = None
        # A DECOMMENTER POUR REPRODUIRE LE "bug"
        # self._calc10
        
    @property
    def _calc10(self):
        if self._calct10_axis is None:
            glob_config = get_config()
            self._calct10_axis = glob_config.get('calc10')
        return self._calct10_axis
     
    def pre_move(self, motion_list):
        pass
    
    
class SimuCalc(CalcController):
    """ Calculation controller for energy and wavelength.
    """
    def calc_from_real(self, real_user_positions):
        return {'simu_out':real_user_positions['simu_in']}

    def calc_to_real(self, pseudo_dial_positions):
        return {'simu_in':pseudo_dial_positions['simu_out']}
        

class SimuHook2(MotionHook):
    def __init__(self, name, config):
        super().__init__()
        self._name = name
        # self._calct10_axis = None
        # A DECOMMENTER POUR REPRODUIRE LE "bug"
        # self._calc10
        
    # @property
    # def _calc10(self):
    #     if self._calct10_axis is None:
    #         glob_config = get_config()
    #         self._calct10_axis = glob_config.get('calc10')
    #     return self._calct10_axis
     
    def pre_move(self, motion_list):
        print("PM", self._name)

    def pre_scan(self, axes_list):
        print("PS", self._name)

    def post_move(self, motion_list):
        print("POSTM", self._name)

    def post_scan(self, axes_list):
        print("POSTS", self._name)