from bliss.common.axis import AxisState
from bliss.common.logtools import log_debug, log_info, log_error
from bliss.config.static import get_config
from bliss.controllers.motor import Controller, CalcController

import traceback
import sys





class SimuD20Mono(CalcController):
    def __init__(self, *args, **kwargs):
        super(SimuD20Mono, self).__init__(*args, **kwargs)

        wago_name = self.config.get('wago')
        config = get_config()
        self._wago = config.get(wago_name)

        lat_pos = {mode['mode']: float(mode['monolat'])
                 for mode in self.config.config_dict.get('modes')}
        self._lat_pos = lat_pos

    def initialize(self):
        config = get_config()
        self._monolat = config.get('monolat')
        super(SimuD20Mono, self).initialize()

    def calc_from_real(self, real_positions):
        print('==== Cfr', real_positions)
        crystal = real_positions.get('crystal')
        monolat_pos = real_positions.get('mono_lat')
        calc_positions = {'nothing': -1}

        # for line in traceback.format_stack():
        #     print(line.strip())

        # traceback.print_stack(file=sys.stdout)

        if crystal is not None and crystal in (1, 2, 3, 4, 5):
            # only works with the monocrystalsimu
            crystal = int(crystal)
            crystal_out = self._wago.get(f'outmode{crystal}')
            # monolat = self._tagged['mono_lat'][0]

            if crystal != crystal_out:
                state = self._monolat.state
                target_lat = self._lat_pos[crystal]

                if 'MOVING' not in state:
                    for mode in range(1, 6):
                        self._wago.set(f'inmode{mode}', 0)
                    log_info(self, f'Moving mono_lat to {target_lat}')
                    self._monolat.move(target_lat, wait=False)
                else:
                    if monolat_pos == target_lat:
                        self._wago.set(f'inmode{crystal}', 1)
        else:
            log_error(self, f'Invalid mode {crystal}.')
            # raise RuntimeError(f'Invalid mode {crystal}.')

        print('##### Cfr out', calc_positions)

        return calc_positions

    def calc_to_real(self, positions_dict):
        print('--> sCtr', positions_dict)
        return {}
