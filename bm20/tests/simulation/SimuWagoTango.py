#!/usr/bin/env python

import sys
import gevent

from tango import DevState
from bliss.config.static import get_config
from bliss.comm.util import get_comm
from bliss.controllers.wago.wago import Wago, ModulesConfig, WagoController
from bliss.tango.servers.wago_ds import Wago as WagoTango


SPECIAL_NAMES = 'homelat', 'homeyaw', 'abort'


class BM20MonoWagoMockup(Wago):
    def __init__(self, name, config_tree):
        self.modules_config = ModulesConfig.from_config_tree(config_tree)

        from bliss.controllers.wago.emulator import WagoEmulator

        self._mockup = WagoEmulator(self.modules_config)
        # configure comm.
        config_tree["modbustcp"] = {"url": f"localhost:{self._mockup.port}"}
        super().__init__(name, config_tree)

    def close(self):
        super().close()
        try:
            self._mockup.close()
        except AttributeError:
            pass


class BM20MonoCrystalSimu(Wago):

    def __init__(self, name, config_tree):
        # force to simulate (don't want to fu things)
        # assert config_tree.get('simulate')

        # self._out_channels = [e.value.chan_out for e in D20Crystals]
        # self._in_channels = [e.value.chan_in for e in D20Crystals]
        
        super(BM20MonoCrystalSimu, self).__init__(name, config_tree)
        self._pmac_obj = None

    def _pmac(self):
        if self._pmac_obj is None:
            pmac = get_config().get('d20pmac')
            version = pmac.pmac_comm.pmac_version()
            if not version.startswith('SIMU'):
                raise RuntimeError('Not a simulator.')
            self._pmac_obj = pmac
        return self._pmac_obj

    def _get_in_channel(self):
        chans = [i for i in D20Crystals if self.get(i.value.chan_in)]
        if len(chans) != 1:
            return 0
        return chans[0].value.index

    def _get_out_channel(self):
        chans = [i for i in D20Crystals if self.get(i.value.chan_out)]
        if len(chans) != 1:
            return 0
        return chans[0].value.index

    def set(self, *args, **kwargs):
        specials = dict(zip(SPECIAL_NAMES, self.get(*SPECIAL_NAMES)))

        pre_channel = self._get_out_channel()

        # print('SEEEET0', *args, **kwargs)

        super(BM20MonoCrystalSimu, self).set(*args, **kwargs)

        # print('SEEEET1')

        post_channel = self._get_out_channel()

        if post_channel in range(1, 6) and post_channel != pre_channel:
            self._pmac().pmac_comm.sendline_getbuffer(f'>MODE{post_channel}')

        # print('SEEEET2')

        specials_after = dict(zip(SPECIAL_NAMES, self.get(*SPECIAL_NAMES)))
        
        if (specials_after['abort'] - specials['abort'] > 0):
            # print('ABORT')
            self._pmac().pmac_comm.sendline_getbuffer('>ABORT')

        if (specials_after['homelat'] - specials['homelat'] > 0):
            # print('HOMELAT')
            self._pmac().pmac_comm.sendline_getbuffer('>HOMELAT')

        if (specials_after['homeyaw'] - specials['homeyaw'] > 0):
            # print('HOMEYAW')
            self._pmac().pmac_comm.sendline_getbuffer('>HOMEYAW')


class SimuWagoTango(WagoTango):
    def init_device(self, *args, **kwargs):
        super().init_device(*args, **kwargs)
        self.set_state(DevState.STANDBY)

        # configuration can be given through Beacon if beacon_name is provided
        # this will generate self.iphost and self.config
        if self.beacon_name:
            config = get_config()
            yml_config = config.get_config(self.beacon_name)
            print('BEACON_NAME', self.beacon_name)

            from bliss.controllers.wago.emulator import WagoEmulator
            modules_config = ModulesConfig.from_config_tree(yml_config)
            self._s = WagoEmulator(modules_config)

            if yml_config is None:
                raise RuntimeError(
                    f"Could not find a Beacon object with name {self.beacon_name}"
                )
            try:
                self.iphost = f'{self._s.host}:{self._s.port}'#yml_config["modbustcp"]["url"]
            except KeyError:
                raise RuntimeError(
                    "modbustcp url should be given in Beacon configuration"
                )
            modules_config = ModulesConfig.from_config_tree(yml_config)
            self.config = modules_config.mapping_str
            self.extended_mode = modules_config.extended_mode
        else:
            self.extended_mode = False

        self.TurnOn()  # automatic turn on to mimic C++ Device Server


def main(argv=sys.argv):

    from tango import GreenMode
    from tango.server import run

    run([SimuWagoTango], green_mode=GreenMode.Gevent)


if __name__ == "__main__":
    main()