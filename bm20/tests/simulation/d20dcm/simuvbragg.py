from ..turbopmac.pmaccoordsys import PmacCoordinateSystem
from bm20.oh.mono.energy import bragg_to_perp


class SimuVBragg(PmacCoordinateSystem):
    def calc_from_real(self, real_positions):
        # print('CFR', real_positions)
        # print('TOTO0')
        return {'x':real_positions['bragg']}
        # 'motcalc': 2*real_positions['simot'],
                # 'motcalc2': -10*real_positions['simot']}

    def calc_to_real(self, calc_positions):
        # print('TOTO1')
        # print('CTR', calc_positions)
        perp = bragg_to_perp(calc_positions['x']/200000) * 20000
        return {'bragg': calc_positions['x'], "xtal2perp": perp}
        # return {'simot': 10*calc_positions['motcalc']}

    def set_tooltip_velocity(self, velocity):
        # print("SET VEL", velocity)
        self._tagged["bragg"][0].velocity = velocity
        self._tagged["xtal2perp"][0].velocity = velocity