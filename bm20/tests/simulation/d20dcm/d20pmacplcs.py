import gevent

from bm20.simulation.turbopmac.pmacplc import PmacPLC
from bliss.common.logtools import *


class HomeLatPLC(PmacPLC):
    def run(self, *args, **kwargs):
        # neg_limit = 2000000
        home_pos = self.mockup.pmac_get_p_value(217)

        # homing PLC:
        # 1) move to neg limit
        # 2) step back a little
        # 3) move to home (offset)
        # 4) set HOMED flat

        try:
            self.mockup.pmac_set_m_value(745, 0)
            lat = self.mockup.get_axis('simu_monolat')
            self.mockup._override_pmac_axis_status(lat.pmac_address, 0x8B0400148080)
            self.mockup.pmac_clear_user_state_flags(lat.pmac_address)
            # print('TOTO')
            # lat.move(neg_limit)
            log_info(self, f'CURRENT POS: {lat.position}')
            log_info(self, 'MOVING TO lim-')
            lat.hw_limit(-1)
            # print('m1')
            self.mockup._override_pmac_axis_status(lat.pmac_address, 0x8b0000148000)
            log_info(self, 'MOVING BACK 1000 steps')
            lat.rmove(1000)
            self.mockup._override_pmac_axis_status(lat.pmac_address, 0x8BA000148000)
            gevent.sleep(0.01)
            # print('m2')
            self.mockup._override_pmac_axis_status(lat.pmac_address, 0x8B0000148000)
            log_info(self, f'MOVING TO HOME: {home_pos}.')
            lat.move(home_pos)
            self.mockup._override_pmac_axis_status(lat.pmac_address, None)#lat.pmac_address, 0x8ba000148000)
            gevent.sleep(1)
            self.mockup.pmac_set_user_state_flags(lat.pmac_address, 1 + (1 << 10))
            self.mockup.pmac_set_m_value(745, 1)
            # 0x892000148401
            # print('m3')
            log_info(self, 'LAT HOMING DONE.')
        except Exception as ex:
            self.mockup._override_pmac_axis_status(lat.pmac_address, None)
            print('HomeLATPLC err', ex)
        except gevent.GreenletExit:
            self.mockup._override_pmac_axis_status(lat.pmac_address, None)
            print('#### KILLED ####')


class HomeYawPLC(PmacPLC):
    def run(self, *args, **kwargs):
        # neg_limit = -435657.15625
        home_pos = self.mockup.pmac_get_p_value(218)

        try:
            log_info(self, 'HOMING YAW')
            self.mockup.pmac_set_m_value(845, 0)
            yaw = self.mockup.get_axis('simu_monoyaw')
            self.mockup.pmac_clear_user_state_flags(yaw.pmac_address)
            self.mockup._override_pmac_axis_status(yaw.pmac_address, 0x8b0400218080)
            # print('HOME YAW',yaw.name, neg_limit, home_pos, yaw.position)
            log_info(self, 'CURRENT POS: {{yaw.position}.')
            log_info(self, 'MOVING TO lim-')
            yaw.hw_limit(-1)
            # print('m1')
            self.mockup._override_pmac_axis_status(yaw.pmac_address, 0x8b0000218000)
            log_info(self, 'MOVING BACK 2000 steps')
            yaw.rmove(2000)#25068.96875)
            self.mockup._override_pmac_axis_status(yaw.pmac_address, 0x8b0400218080)
            gevent.sleep(0.01)
            # print('m2')
            self.mockup._override_pmac_axis_status(yaw.pmac_address, 0x8b0400218080)
            log_info(self, f'MOVING TO HOME: {home_pos}.')
            yaw.move(home_pos)#-53097.65625)
            self.mockup._override_pmac_axis_status(yaw.pmac_address, 0x8b0000218000)
            gevent.sleep(0.01)
            self.mockup._override_pmac_axis_status(yaw.pmac_address, 0x8b0000218000)
            log_info(self, 'MOVING TO: 11.28125.')
            yaw.move(11.28125)
            self.mockup._override_pmac_axis_status(yaw.pmac_address, 0x8ba000218000)
            gevent.sleep(1)
            self.mockup._override_pmac_axis_status(yaw.pmac_address, 0x92000218401)
            gevent.sleep(0.01)
            self.mockup._override_pmac_axis_status(yaw.pmac_address, None)# 0x92000218401)

            self.mockup.pmac_set_user_state_flags(yaw.pmac_address, 1 + (1 << 10))
            self.mockup.pmac_set_m_value(845, 1)
            # 0x892000148401
            # print('m3')
            log_info(self, 'YAW HOMING DONE.')
        except Exception as ex:
            self.mockup._override_pmac_axis_status(yaw.pmac_address, None)
            print('HomeyawPLC err', ex)
        except gevent.GreenletExit:
            self.mockup._override_pmac_axis_status(yaw.pmac_address, None)
            print('#### KILLED ####')