from asyncio.log import logger
import re
import gevent

from bliss.common.cleanup import cleanup, axis as cleanup_axis

from bliss.common.logtools import *
# import gevent
from bliss.common.hook import MotionHook

from ..turbopmac.pmacsimu import PmacSimu, PmacSimuRunner, PmacPLC
from bliss.config.static import get_config


def _log_on():
    import logging
    logging.getLogger('global.controllers.PmacSimu').setLevel(logging.INFO)
    logging.getLogger('global.controllers.d20pmacmockup').setLevel(logging.INFO)


class MonoLatHook(MotionHook):

    def __init__(self, *args, **kwargs):
        # self.config = config
        # self.name = name
        # self.plc = config['plc']
        # self.channel = config['channel']
        super(MonoLatHook, self).__init__()

    def pre_move(self, motion_list):
        print('PREMOVE')
    #     self.plc.set(self.channel, 1)
    #     gevent.sleep(1)

    def post_move(self, motion_list):
        print('MOVE done')#, motion_list[0].axis.controller)
        config = get_config()
        pmac = config.get('simud20mono')
        pmac._monolat_done()


class D20PmacSimuRunner(PmacSimuRunner):
    mode_rx = re.compile(r'>MODE(?P<mode>[0-9]+).*')
    abort_rx = re.compile(r'>ABORT.*')
    homelat_rx = re.compile(r'>HOMELAT.*')
    homeyaw_rx = re.compile(r'>HOMEYAW.*')

    def custom_command(self, data):
        print('CUSTOM COMMAND', data)
        match = self.mode_rx.match(data)
        if match:
            mode = match.groupdict()['mode']
            log_info(self, f'Setting mode to {mode}.')
            self._server.set_mode(int(mode))
            return 5 + len(mode), '@'

        match = self.abort_rx.match(data)
        if match:
            controller = self._pmac_mockup
            print('Killing PLCs')
            controller._kill_plcs()
            for axis in controller.axes():
                print(f'stopping {axis.name}')
                axis.stop()
                print(f'stopped {axis.name}')
            return 6, '@'

        match = self.homelat_rx.match(data)
        if match:
            log_info(self, 'Running plc HOMELAT')
            self._pmac_mockup._run_plc('homelat')
            return 8, '@'

        match = self.homeyaw_rx.match(data)
        if match:
            log_info(self, 'Running plc HOMEYAW')
            self._pmac_mockup._run_plc('homeyaw')
            return 8, '@'

        return 0, None


class D20PmacSimu(PmacSimu):
    runner_klass = D20PmacSimuRunner

    def __init__(self, name, config):
        super(D20PmacSimu, self).__init__(name, config)

        self._lat_pos = {mode['mode']: {'position':float(mode['monolat']),
                                        'p_reg':int(mode['p_reg'][1:])}
                         for mode in config.get('modes')}
        # self._yaw_limits = config.get('yaw_limits')
        # self._lat_limits = config.get('lat_limits')

        # setting home position for lat and yaw
        self._pmac_mockup.pmac_set_p_value(217, 0, unlock=True)
        self._pmac_mockup.pmac_set_p_value(218, 0, unlock=True)

        self._pmac_mockup._add_plc(HomeLatPLC('homelat', self._pmac_mockup))
        self._pmac_mockup._add_plc(HomeYawPLC('homeyaw', self._pmac_mockup))

    def _d20_log_on(self):
        _log_on()

    def pre_run_server(self,
                       *args,
                       d20_home_lat=True,
                       d20_home_yaw=True,
                       **kwargs):
        """
        d20_home_lat: set to True to have the HOMED lateral axis register set at startup
        d20_home_yaw: set to True to have the HOMED yaw axis register set at startup
        """
        self._write_wago()
        self._set_p_vars()

        wago = self._mono_wago()
        wago.set('error', 0)
        wago.set('yawenable', 0)
        wago.set('latbrake', 0)
        wago.set('abort', 0)

        yaw = self._pmac_mockup.get_axis('simu_monoyaw')
        lat = self._pmac_mockup.get_axis('simu_monolat')
        # yaw.limits = self._yaw_limits
        # lat.limits = self._lat_limitsyaw.limits = self._yaw_limits
        # lat.limits = self._lat_limits
        # yaw.controller.set_hw_limits(yaw, *yaw.limits)
        # lat.controller.set_hw_limits(lat, *lat.limits)
        # uncomment to test the homing procedure

        if d20_home_lat:
            log_info(self, 'Setting the HOMED register for axis LAT')
            self._pmac_mockup.pmac_set_m_value(745, 1, unlock=1)
        else:
            log_info(self, 'Clearing the HOMED register for axis LAT')
            self._pmac_mockup.pmac_set_m_value(745, 0, unlock=1)
        if d20_home_yaw:
            log_info(self, 'Setting the HOMED register for axis YAW')
            self._pmac_mockup.pmac_set_m_value(845, 1, unlock=1)
        else:
            log_info(self, 'Clearing the HOMED register for axis YAW')
            self._pmac_mockup.pmac_set_m_value(845, 0, unlock=1)

        self._write_wago()

        # if 0:
        #     self._pmac_mockup.pmac_set_m_value(745, 0)
        #     self._pmac_mockup.pmac_set_m_value(845, 0)
        #     self._pmac_mockup._override_pmac_axis_status(lat.pmac_address, 0x50000218000)
        #     self._pmac_mockup._override_pmac_axis_status(yaw.pmac_address, 0x50000218000)
        # else:
        #     self._pmac_mockup.pmac_set_m_value(745, 1)
        #     self._pmac_mockup.pmac_set_m_value(845, 1)
        #     self._pmac_mockup.pmac_set_user_state_flags(lat.pmac_address, 1 + (1 << 10))
        #     self._pmac_mockup.pmac_set_user_state_flags(yaw.pmac_address, 1 + (1 << 10))

        super(D20PmacSimu, self).pre_run_server(*args, **kwargs)

    def _set_p_vars(self):
        mock = self._pmac_mockup
        for value in self._lat_pos.values():
            mock.pmac_set_p_value(value['p_reg'],
                                  value['position'],
                                  unlock=1)

    def clear_outmodes(self):
        wago = self._mono_wago()
        for i in range(1, 6):
            wago.set(f'outmode{i}', 0)

    def clear_inmodes(self):
        wago = self._mono_wago()
        w_args = []
        for i in range(1, 6):
            w_args += [f'inmode{i}', 0]
        wago.set(*w_args)

    def _write_wago(self, clear=True):
        wago = self._mono_wago()
        mode = self.mode()
        print('MODE', mode)
        if clear:
            self.clear_inmodes()
            wago.set('error', 0)
        if mode > 0:
            wago.set(f'inmode{mode}', 1)

    def _mono_wago(self):
        config = get_config()
        return config.get('simu_wcbm20i')

    def _xtal_thread(self, monolat, mode):
        log_info(self, 'Xtal thread started.')
        while 'MOVING' in monolat.state:
            log_info(self, f'Xtal still moving to mode {mode}.')
            gevent.sleep(2)
        log_info(self, 'Xtal Move done.')
        log_info(self, f'New mode: {self.mode()}')
        self._write_wago(clear=False)
        log_info(self, 'Xtal thread done.')

    def set_mode(self, mode):
        wago = self._mono_wago()
        wago.set('error', 0)

        # self._target_mode = mode
        # self._mode = 0

        self.clear_inmodes()

        # wago.set('latbrake', 1)
        lat_pos = self._lat_pos[mode]['position']
        # monolat = config.get('simu_monolat')
        # monolat.move(lat_pos, wait=False)
        monolat = self._pmac_mockup.get_axis('simu_monolat')
        self._pmac_mockup.pmac_jog_to(monolat.pmac_address, lat_pos)

        gevent.sleep(0.1)
        gl = gevent.Greenlet(self._xtal_thread, monolat, mode)
        self._pool.start(gl)

    def mode(self):
        config = get_config()
        monolat = config.get('simu_monolat')
        lat_pos = monolat.position
        # print('LAT_POS?', lat_pos)
        for mode, value in self._lat_pos.items():
            # print('MODE', mode, value)
            if value['position'] == lat_pos:
                return mode
        return 0

    def _monolat_done(self):
        print('In _monolat_done')
        self._write_wago()
        print('Out _monolat_done')
        # wago = self._mono_wago()
        # config = get_config()

        # if self._target_mode > 0:
        #     lat_pos = self._lat_pos[self._target_mode]
        #     monolat = config.get('monolat')
        #     if monolat.position == lat_pos:
        #         wago.set(f'inmode{self._target_mode}', 1)
        #         self._mode = self._target_mode
        #         self._target_mode = -1
        # # wago.set('latbrake', 0)
        # print('Out _monolat_done', self._target_mode, self._mode)


