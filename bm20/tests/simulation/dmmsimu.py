from math import floor

from bliss.controllers.motors.mockup import Mockup
from bliss.common.logtools import log_info


class DmmSimu(Mockup):
    def initialize_axis(self, axis):
        rc = super().initialize_axis(axis)
        neg_l, pos_l = axis.limits
        hw_limits = floor(neg_l * 0.95), floor(pos_l * 0.95)
        log_info(self, f'Axis {axis.name}: setting HW limits to {hw_limits}')
        print(f'Axis {axis.name}: setting HW limits to {hw_limits}')
        self.set_hw_limits(axis, *hw_limits)
        return rc
