wcbm20i
-------

Simulation of the Wago box linked to the TurboPmac: xtal selection, abort, error, etc...

Either start it via multivisor if configured, or start it manualy:

  . blissenv -d
  
  ./local/bm20.git/bm20/simulation/mono/SimuWagoTango.py simu_wcbm20i
  
  
turbopmac/DCM
-------------

Simulation of the DCM (7 axis). IcePAP axis not implemented yet.

  . blissenv -d
  
  bliss -s pmacsimu --no-tmux --log-level=INFO
   
then, at the python prompt

  simud20mono.run_server()
  
  
enable logging
--------------
  import logging; s = logging.getLogger('global.controllers.PmacSimu'); s.setLevel(logging.DEBUG)

or

  debugon('*Pmac*')
