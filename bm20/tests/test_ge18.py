from collections import OrderedDict

import numpy as np

from bliss.common.cleanup import cleanup
from bliss.common.session import get_current_session
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan, ScanState, DataWatchCallback
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.scanning.group import Sequence
from bliss.config.static import get_config

from bliss.controllers.counter import CalcCounterController, SamplingCounterController, CounterController
from bliss.controllers.simulation_counter import SimulationCounterController
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.mca.base import TriggerMode, BaseMCA
from bm20.oh.mono.energy import energy2bragg

config = get_config()



def scantool_stepscan_get_acquisition_chain(
    master,
    counters,
    points,
    acq_times,
    fx_trigger="GATE",
    fx_block_size=5,
):
    
    chain = AcquisitionChain(parallel_prepare=True)
    builder = ChainBuilder(counters)

    ###################################
    # P201
    #
    acq_node_params = {
            "npoints": points,
            "acq_expo_time": list(acq_times)
    }
    if isinstance(acq_times, (list, np.ndarray)):
        acq_child_params = {
            "count_time": 1,
            "npoints": 1,
        }
    else:
        acq_child_params = {
            "count_time": list(acq_times),
            "npoints": points,
        }
    
    for node in builder.get_nodes_by_controller_type(CT2Controller):
        node.set_parameters(acq_params=acq_node_params)
        for child_node in node.children:
            child_node.set_parameters(acq_params=acq_child_params)
        node_p201 = node
        chain.add(master, node)
        
    tmast = SoftwareTimerMaster(0.1, points)
    chain.add(master, tmast)

    ###################################
    # FalconX
    #
    if fx_trigger == "SOFTWARE":
        acq_params = {
            "npoints": points,
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : acq_times
        }
    else:
        acq_params = {
            "npoints": points,
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : 0.1
        }
        
    for node in builder.get_nodes_by_controller_type(BaseMCA):
        node.set_parameters(acq_params=acq_params)
        # chain.add(node_p201, node)
        chain.add(master, node)

    ###################################
    # SamplingCounterController
    #
    acq_params = {
            "count_time": acq_times,
            "npoints": points,
        }
    # for node in builder.nodes:  
    for node in builder.get_nodes_by_controller_type(CounterController):  
        if isinstance(node.controller, SamplingCounterController):
            node.set_parameters(acq_params=acq_params)
            # chain.add(node_p201, node)
        

            
    # for node in builder.nodes:
    #     if isinstance(node.controller, SamplingCounterController):
    #         node.set_parameters(acq_params=acq_params)
    # SimulationCounter

    ###################################
    # CalcCounterController
    
    acq_params = {
        "count_time": acq_times,
    }
    for node in builder.nodes:
        if isinstance(node.controller, CalcCounterController):
            node.set_parameters(acq_params=acq_params)
            chain.add(node_p201, node)

    return (builder, chain)


def ge18_test(*counters):

    egy_axis = config.get('energy')
    s5hg = config.get('s5hg')

    hg_pos = np.repeat([0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6], 10)

    master = VariableStepTriggerMaster(
                    s5hg,
                    hg_pos)


    # ACQUISITION CHAIN
    builder, chain = scantool_stepscan_get_acquisition_chain(
            master,
            counters,
            len(hg_pos),
            np.repeat(1, len(hg_pos)),
            fx_trigger=TriggerMode.GATE
            #fx_trigger=self._fx_trigger_mode,
            #fx_block_size=self._fx_block_size,
        )

    # master,
    #     counters,
    #     points,
    #     acq_times,
    #     fx_trigger="GATE",
    #     fx_block_size=5,

    print(chain._tree)


    nbp = len(hg_pos)
    # scan_info = {
    #     "title": f"EXAFS {self._name}",
    #     "element": f"{self._element}",
    #     "edge": f"{self._edge}",
    # }
    # scan_info = {
    #     "title": f"EXAFS {self._name}",
    #     "instrument":{"exafs": self._exafs_params.metadata()}
    # }


    #     "title": f"{self._name}.exafs_step {nbp} points", 
    #     "type": f"{self._name}.exafs_step",
    #     #"instrument": instrument
    # }
        
    # print(f"\nSCAN #{nscan+1}(/{repeat})")

    # SCAN
    scan_obj = Scan(
        chain,
        name=f"BLA"
        # ,
        # data_watch_callback=scan_display
    )
    # self._scan_state = "RUNNING"
    # print(f'Scanning from {scan_pos[0]} to {scan_pos[-1]}')
    scan_obj.run()
    #self.plotter.run(self._scan_obj)

