import numpy as np
from tests.legacy.params import *
import h5py

from silx.gui import qt as Qt
from silx.gui.plot import Plot1D

params = ExafsParameters("Mo", "K")

s = params._calc_cont()
p, t = s._calc_cont()

print(s)
print(np.sum(t))



fname ="/data/bm20/inhouse/bliss_tmp/testd20fs/bm20/20230401/RAW_DATA/MoK/MoK_0001/MoK_0001.h5"
s = "6.1"
with h5py.File(fname, "r") as f5:
    meas = f5[s]["measurement"]
    egy = meas["fbragg_energy"]
    fbragg = meas["fbragg_center"]

x = np.arange(egy.shape[0])

app = Qt.QApplication([])
plot = Plot1D()
plot.addCurve(x, egy, legend="p0")
plot.show()
app.exec()