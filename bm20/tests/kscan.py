from bliss.config.static import get_config

# config = get_config()

# kscan_eh1_defaults = config.get("simu_kscan_eh1_defaults")

# global metadata
meta_dict = {"foo":123, "bar":"hello"}


# scan1 metadata: adding something
scan1_meta = meta_dict | {"extra_info": "yada yada"}
scan1 = kscan_eh1_defaults("Se_15K_ReL3", element="Re", edge="L3",
                           ref_element="Ta", ref_edge="L3", metadata=scan1_meta)
scan1.ref_scan.e_inc = 1
scan1.pre_scan_0.ctime = 3

# scan2, changing some parameters from the defaults
scan1_meta = meta_dict | {"mydata": "blabla 1234"}
scan2 = kscan_eh1_defaults("Re_S4_15K_ReL3", edge_ev=12345,
                           ref_element="Ta", ref_edge="L3", metadata=meta_dict)
scan1.ref_scan.e_inc = 1
scan1.pre_scan_0.ctime = 2
scan2.k_scan.ctime_max = 7

scan3 = kscan_eh1_defaults("Re_S4_666K_ReL3", element="Re", edge="L2",
                           ref_element="Ta", ref_edge="L3", metadata=meta_dict)
scan1.ref_scan.e_inc = 1
scan1.pre_scan_0.ctime = 3

# moving some motor(s)
mv(s5vg, 5)

while True:

    sg(667) # set gain
    mv(suph, 21.1)
    scan1.run()

    sg(768)
    mv(suph, 21.5)
    scan2.run()

    sg(768)
    mv(suph, 42.5)
    scan2.run()