import gevent
import datetime

import numpy as np

from bliss.common.scans import loopscan
from bliss.config.static import get_config

from bliss.shell.standard import move
from bliss.scanning.scan_saving import ScanSaving
from bliss.common.session import get_current_session


def test_cryostat(tstart, tstop, tinc, scan_time_s, sleep_time_s):
    config = get_config()
    cryostat = config.get('cryostat')
    samplehead = config.get('samplehead')

    strtime = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')

    cur_temp = np.around(cryostat.input.read(), 1)

    n_points = int(scan_time_s / (sleep_time_s + 0.1))
    print(f'Will count {n_points} ({scan_time_s} seconds).')

    # timescan
    session = get_current_session()
    scan_saving = ScanSaving(session.name)
    steps = np.arange(tstart, tstop, tinc)

    print(steps)

    try:
        for setpoint in steps:
            filename = f'cryo_{strtime}_{cur_temp}_{setpoint}'
            scan_saving.data_filename = filename
            print(f'Currently at {cur_temp}, moving to {setpoint}K')
            print(f'Writing scan to {scan_saving.base_path}/{filename}')
            move(cryostat.axis, setpoint, wait=False)
            loopscan(n_points,
                     0.1,
                     cryostat.counters.cryo_coldh,
                     cryostat.counters.cryo_out,
                     samplehead.counters.cryo_samp,
                     name=filename,
                     sleep_time=sleep_time_s)
            cur_temp = setpoint
    except Exception as ex:
        print(ex)
    scan_saving.data_filename = 'data'


