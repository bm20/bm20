import numpy as np

from bm20.oh.mono.energy import energy2bragg, bragg2energy
import tabulate

from bm20.oh.mono.energy import *
from bm20.oh.mono.turbopmac_cs import *

# 8.11869734375 8.11883193466418 0.4 14000.101887415269 13999.871354451465 685.1

e0 = 13999.871353497132
e1 = 13999.583495861649
d_spacing = 3.13542
ev_per_sec = 685.1111524995861
# e1 = e0 + 0.387*ev_per_sec
# step_s = 1.0

ptm = energy_to_bragg_steps_pos_tm(e0,
                        e1,
                        d_spacing,
                        ev_per_sec,
                        # step_s,
                        200000,
                        0)

prog = pos_tm_to_prog(ptm["tm"]*1000., ptm["pos"], 666, 80000)


print(ptm)
print("\n".join(prog.split("\r")))



# f = xes_fast_fbragg("fastx", 14000, 14200, 1, 0.1, run=1)

# Preparing ...
# 8.118831935221353 8.11883193466418 1.3929302156157064e-09 0.4
# =================
# 8.118831935221353 8.11883193466418 0.4 13999.871353497132 13999.871354451465 685.1260600650386 1.0
# 5 1.3929302156157064e-09 2.785860431231413e-10
# CLOSE
# OPEN PROG 2.785860431231413e-10
# CLEAR
# ABS
# LINEAR
# F80000.0
# X666
# TM0.0
# X1623766.3870442705
# X1623766.387021984
# X1623766.3869996972
# X1623766.38697741
# X1623766.3869551232
# X1623766.3869328361
# CLOSE

