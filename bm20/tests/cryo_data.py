import os

import csv
import h5py
from silx import io



f = '/users/opd20/dev/cryo/cryo_15Feb2022bup.dat'
cf = os.path.splitext(os.path.basename(f))[0]

with io.open(f) as s:
    t = s['2.1/measurement/elapsed_time'][:]
    ch = s['1.1/measurement/cryo_coldh'][:]
    sh = s['2.1/measurement/cryo_coldh'][:]


print(cf)