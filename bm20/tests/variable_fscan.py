import numpy as np

from bliss.config.static import get_config


config = get_config()

kscan_def = config.get("simu_kscan_eh1_defaults")

kscan = kscan_def("test", element="Sr", edge="K",
                  ref_element="Pb", ref_edge="L1")

print(kscan.__info__())

kpos, ksec = kscan.positions()

vel = (kpos[1:] - kpos[:-1]) / ksec[1:]

# print(np.stack([ksec, kpos], axis=1)[-300:-200])
# print(ksec.shape)

ref = kscan.ref_scan
pre = kscan.pre_scans
edge = kscan.edge_scan
exafs = kscan.k_scan


ref_e, ref_t = ref.scan_pos, ref.scan_t
ref_e0, ref_e1 = ref_e[0], ref_e[-1]
ref_total = ref_t.sum()
# print(ref_e0, ref_e1, ref_total)

n_pre = len(pre)
pre_e0 = [None] * n_pre
pre_e1 = [None] * n_pre
pre_total = [None] * n_pre

for i, scan in enumerate(pre):
    s_e, s_t = scan.scan_pos, scan.scan_t
    pre_e0[i] = s_e[0]
    pre_e1[i] = s_e[-1]
    pre_total[i] = s_t.sum()
    # print(pre_e0[i], pre_e1[i], pre_total[i])

edge_e, edge_t = edge.scan_pos, edge.scan_t
edge_e0, edge_e1 = edge_e[0], edge_e[-1]
edge_total = edge_t.sum()
# print(edge_e0, edge_e1, edge_total)

exafs_e, exafs_t = exafs.scan_pos, exafs.scan_t
exafs_e0, exafs_e1 = exafs_e[0], exafs_e[-1]
exafs_total = exafs_t.sum()
# print(exafs_e0, exafs_e1, exafs_total)

e_diff = exafs_e[1:] - exafs_e[:-1]

exafs_v = e_diff / exafs_t[1:]

segment_s = 5

from bm20.techniques.kscan.kscan import k_to_e, e_to_k

if 0:
    v_end_ev_s = 0.5
    v_start_ev_s = 2
    k_m = (v_end_ev_s - v_start_ev_s) / np.power(exafs.k_end, 2)
    v = v_start_ev_s - k_m * np.power(exafs.k_pos, 2)

    egy = exafs_e0 + segment_s * v_start_ev_s
    e_pos = [exafs_e0, egy]
    while egy < exafs_e1:
        k = e_to_k(egy - kscan.edge_ev)
        v = v_start_ev_s + k_m * np.power(k, 2)
        egy = e_pos[-1] + segment_s * v
        e_pos += [egy]

else:

    v_factor = 1
    v_min = 1
    v_max = 5

    k_pos = exafs.k_pos
    k_start = k_pos[0]
    k_end = k_pos[-1]

    exafs_v = v_factor * exafs_t
    # egy = exafs_e[0] + segment_s * v_factor * exafs_t[0]

    e_pos = []#exafs_e0]
    v_pos = []#exafs_v[0]]
    # egy = exafs_e[0]

    # k_m = v_factor * (exafs_v[-1] - exafs_v[0]) / np.power(k_end, 2)

    k_m = (exafs.ctime_max - exafs.ctime_min) / np.power(k_end, 2)

    v = None

    tab = []

    # print(e_to_k(exafs_e[0] - exafs.edge_ev), e_to_k(exafs_e[-1] - exafs.edge_ev), k_start, k_end, k_start + k_m * np.power(0, 2), k_start + k_m * np.power(k_end, 2))
    # print(2 + k_m * np.power(e_to_k(7740 - 7709), 2))
    print(exafs.ctime_min + k_m * np.power(e_to_k(exafs_e[0] - exafs.edge_ev), 2), exafs_t[0], exafs_e[0])
    print(k_m)
    idx = 0
    next_egy = exafs_e[0]
    while next_egy < exafs_e1:
        egy = next_egy
        # idx0 = idx
        # idx = idx0 + np.searchsorted(exafs_e[idx0:], [egy], side="right")[0]
        # # TODO: if idx < 0 or > len(exafs_e)
        
        # v0 = exafs_t[idx-1]
        # v1 = exafs_t[idx]
        # e0 = exafs_e[idx-1]
        # e1 = exafs_e[idx]

        v = exafs.ctime_min + k_m * np.power(e_to_k(egy - exafs.edge_ev), 2)
        

        # v = v0 + abs(v1 - v0) * (e1 - egy) / (e1 - e0)
        # v = exafs_t[idx-1] + () * k_m * np.power(e_to_k(egy - exafs.edge_ev), 2)
        # v = exafs_v[0] + k_m * np.power(e_to_k(egy - exafs.edge_ev), 2)

        if not np.isfinite(egy):
            raise RuntimeError()
        next_egy = egy + v * segment_s

        # tab += [[idx, idx0, egy, next_egy, e0, e1, v0, v1, v]]

        e_pos += [egy]
        v_pos += [v]

    v_pos = v_min + (np.array(v_pos) - v_pos[0]) * abs(v_min - v_max) / (v_pos[-1] - v_pos[0])

import tabulate

print(tabulate.tabulate(tab, headers=["idx", "idx0", "egy", "next_egy", "e0", "e1", "v0", "v1", "v"]))

# def k_to_v2(v_pwr, v_min_ev_s, v_max_ev_s, k_values, k_min=None):
#     if k_min is None:
#         k_min = k_values[0]
#     k_m = (v_max_ev_s - v_min_ev_s) * np.power(k_min, v_pwr)
#     return v_min_ev_s + k_m / np.power(k_values, v_pwr)


# def k_to_v(v_pwr, v_min_ev_s, v_max_ev_s, k_values, k_max=None):
#     if k_max is None:
#         k_max = k_values[-1]
#     k_m = (v_max_ev_s - v_min_ev_s) / np.power(k_max, v_pwr)
#     return v_max_ev_s - k_m * np.power(k_values, v_pwr)

# np.power(exafs_e1)

from silx.gui import qt as Qt
from silx.gui.plot import Plot1D
app = Qt.QApplication([])
plot = Plot1D()
# plot.addCurve(k_pos, exafs_e, legend="kve")
plot.addCurve(exafs_e, exafs_t, legend="kvt")#, yaxis="left")
plot.addCurve(e_pos, v_pos, legend="ev", yaxis="right")
# plot.addCurve(2*np.arange(len(e_pos)), e_pos, legend="v")
plot.show()
app.exec()