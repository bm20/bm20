from bliss.config.static import get_config

from bliss.scanning.scan import Scan
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.controllers.simulation_counter import SimulationCounterController
from bliss.controllers.counter import CalcCounterController, SamplingCounterController, CounterController

from bliss.controllers.mca import BaseMCA
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
import numpy as np


def chain():
    config = get_config()
    chain = AcquisitionChain()
    ct1 = config.get('sim_ct_1')
    energy = config.get('energy')
    builder = ChainBuilder([ct1])
    acq_params={"npoints":10}
    # ctrl_params={"preset_mode":"REALTIME", "preset_value":1.2}

    master = VariableStepTriggerMaster(energy, np.linspace(12000, 12500, 10))
    # master = SoftwareTimerMaster(1, )

    for node in builder.get_nodes_by_controller_type(CounterController):  
        if isinstance(node.controller, SamplingCounterController):
            node.set_parameters(acq_params=acq_params)

    # for mca in builder.get_nodes_by_controller_type(BaseMCA):
    #     mca.set_parameters(acq_params=acq_params)#, ctrl_params=ctrl_params)
    #     chain.add(master, mca)

    print(chain._tree)

    scan = Scan(chain)
    scan.run()
    return scan




# chain = AcquisitionChain()                               │
#   energy = config.get('energy')                            │
#   builder = ChainBuilder([sim_ct_0])                       │
#   acq_params={"npoints":10}                                │
#   master = VariableStepTriggerMaster(energy, np.linspace(12│
#   n =list(builder.get_all_nodes())                         │
#   from bliss.controllers.counter import CalcCounterControll│
#   from bliss.controllers.simulation_counter import Simulati│
#   m2 = SoftwareTimerMaster(0.1)                            │
#   chain.add(master, m2)                                    │
#   b = builder.get_nodes_by_controller_type(SimulationCounte│
#   b.set_parameters(acq_params={"shape_param":{}, "distribut│
#   chain.add(m2, b)                                         │
#   s = Scan(chain)        

# from bliss.config.static import get_config
#       ...: from bliss.scanning.scan import Scan
#       ...: from bliss.scanning.chain import AcquisitionChain
#       ...: from bliss.scanning.chain import AcquisitionChain
#       ...: from bliss.scanning.toolbox import ChainBuilder
#       ...: from bliss.scanning.acquisition.timer import SoftwareTimerMaster
#       ...: from bliss.controllers.mca import BaseMCA
#       ...: from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
#       ...: import numpy as np
#       ...: chain = AcquisitionChain()
#       ...: energy = config.get('energy')
#       ...: builder = ChainBuilder([sim_ct_0])
#       ...: acq_params={"npoints":10}
#       ...: master = VariableStepTriggerMaster(energy, np.linspace(12000, 12010, 10))
#       ...: n =list(builder.get_all_nodes())
#       ...: from bliss.controllers.counter import CalcCounterController, SamplingCounterController, CounterController
#       ...: from bliss.controllers.simulation_counter import SimulationCounterController
#       ...: m2 = SoftwareTimerMaster(0.1, npoints=10)
#       ...: chain.add(master, m2)
#       ...: b = builder.get_nodes_by_controller_type(SimulationCounterController)[0]
#       ...: b.set_parameters(acq_params={"shape_param":{}, "distribution":"gaussian", "npoints":10, "start":[0], "stop"
#       ...: :[10]})
#       ...: chain.add(m2, b)
#       ...: s = Scan(chain)
