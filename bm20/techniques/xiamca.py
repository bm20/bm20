import re

from bliss.common.logtools import user_info


def _to_list(val):
    if isinstance(val, (str,)):
        return [val]
    try:
        i = iter(val)
        return val
    except TypeError:
        return [val]
    

"""
# roi_counters is optional, as well as
# enable_channels and disable_channels.
- plugin: generic
  package: bm20.scans.xiamca
  class: XiaMCA
  name: m4
  mca: $xesm4
  roi_name: ketek
  enable_channels: 0
  # disable_channels: [1, 2, 3]
  # roi_sum: True
  roi_counters:
  - icr
  - ocr
  - trigger_livetime
  - spectrum
  - realtime
"""


class XiaMCA:
    """
    Just a small helper class for the MCAs, as used by BM20: only one roi.
    A call to an instance of this class allows for easy
    roi configuration.
    Allows to select/filter channels and counters
    from the yaml config, and add them all to a
    measurement group with one method call.
    """
    mca = property(lambda self: self._mca)
    roi = property(lambda self: self.mca.rois.get(self._roi_name))
    name = property(lambda self: self._name)

    def __init__(self, config):
        self._name = config["name"]
        self._mca = config["mca"]
        self._enable = _to_list(config.get("enable_channels", []))
        self._disable = _to_list(config.get("disable_channels", []))
        self._roi_name = config["roi_name"]
        self._roi_counters = config.get("roi_counters", [])
        self._counter_names = []

        # make sure some channels aren't in both groups
        if set(self._enable) & set(self._disable):
            raise RuntimeError(f"{self._name}: some channels are both in the "
                               "enable and disable settings.")
        self._init()

    def _init(self):
        mv1 = self._enable
        mv2 = self._mca.elements
        if not self._enable:
            self._enable = set(mv2) - set(mv1)
        diff = set(self._enable) - set(self._mca.elements)
        #breakpoint()
        if diff:
            raise RuntimeError(f"{self._name}: some channels found in enable_chan "
                                "aren't found on the MCA.")
        if self._roi_name not in self._mca.rois:
            self._mca.rois.set(self._roi_name, *self._mca.spectrum_range)
        self._update_counter_names()
        # self._add_counters_to_measurement_group()

    def __call__(self, bin_0, bin_1):
        user_info("Setting ROI %s to [%d, %d]", self._roi_name, bin_0, bin_1)
        self._mca.rois.set(self._roi_name, bin_0, bin_1)

    def _update_counter_names(self):
        if not self._roi_counters:
            chan = self._enable[0]
            rx = re.compile(f"^(.*)_det{chan}$")
            # this will raise if a name doesnt end with _detX
            roi_counters = [rx.match(name).groups()[0]
                            for name in self.mca.counter_groups[f"det{chan}"]._fields]
            self._roi_counters = roi_counters
        self._counter_names = [f"{name}_det{chan}"
                               for chan in self._enable
                               for name in self._roi_counters]
        
    def add_counters_to_measurement_grp(self, meas_g):
        """
        Adds this MCA counters to the given measurement group.
        """
        self._update_counter_names()
        for name in self._counter_names:
            meas_g.add(self.mca.counters[name])

    def __info__(self):
        txt = "==========\n"
        txt += f"   MCA {self.name}\n"
        txt += "==========\n"
        txt += "ROI:\n"
        txt += f"  - range: {self.mca.rois.get(self._roi_name)}\n"
        txt += f"  - name: {self._roi_name}\n"
        txt += f"  - counters:\n"
        txt += f"     *" + f"\n     *".join(self._roi_counters)
        txt += "\n"
        return txt

    
