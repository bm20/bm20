from pprint import pformat
import numpy
from bliss import current_session
from bliss.scanning import scan_meta
from bliss.scanning.scan_saving import ScanSaving
from bliss.scanning.scan import Scan
from bliss.config.static import get_config
import numpy as np
import fabio
import os


def mono_metadata(scan):
    crystal = get_config().get("d20xtal")
    # monolat = get_config().get("monolat")
    return {"mono":
            {"crystal": crystal.crystal_metadata()}
        }

scan_meta_obj = scan_meta.get_user_scan_meta()
#scan_meta_obj.instrument.set("d20pmac", mono_metadata)