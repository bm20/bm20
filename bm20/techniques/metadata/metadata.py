from pprint import pformat
import numpy
from bliss import current_session
from bliss.scanning import scan_meta
from bliss.scanning.scan_saving import ScanSaving
from bliss.scanning.scan import Scan
import numpy as np
from id09.unit import convert_float2str
import fabio
import os

try:
    from blissdata import settings
except ImportError:
    from bliss.config import settings


def format_info(info):
    rows = [(str(name), pformat(value).split("\n")) for name, value in info.items()]
    lengths = numpy.array(
        [[len(name), max(len(s) for s in value)] for name, value in rows]
    )
    fmt = "   ".join(["{{:<{}}}".format(n) for n in lengths.max(axis=0)])
    lines = list()
    for name, value in rows:
        for i, s in enumerate(value):
            if i == 0:
                lines.append(fmt.format(name, s))
            else:
                lines.append(fmt.format("", s))
    return "\n ".join(lines)


class WithPersistentParameters:
    """Adds parameters as properties that will be stored in Redis

    .. code:: python

        class MyClass(WithPersistentParameters, parameters=["a", "b"])
            pass

        myobj = MyClass()
        myobj.a = 10
        myobj.b = None  # remove
    """

    _PARAMETERS = set()

    def __init__(self) -> None:
        self._parameters = settings.HashObjSetting(
            f"blissoda:{current_session.name}:{self.__class__.__name__}"
        )

    def __init_subclass__(cls, parameters=None, **kw) -> None:
        super().__init_subclass__(**kw)
        if parameters is None:
            parameters = set()
        cls._PARAMETERS |= set(parameters)
        for name in parameters:
            add_parameter_property(cls, name)

    def __info__(self):
        # LINES BELOW WERE MAKING A MESS IN THE PRINTOUT:
        # info = self._parameters.get_all()
        # missing = set(self._PARAMETERS) - set(info)
        # for name in missing:
        #     info[name] = None
        # exclude_from_info = self._exclude_from_info
        # info = {k: v for k, v in info.items() if k not in exclude_from_info}
        return "Parameters:\n " + format_info(self._parameters.get_all())

    @property
    def _exclude_from_info(self) -> set:
        return set()

    def _get_parameter(self, name):
        return self._parameters.get(name)

    def _set_parameter(self, name, value):
        self._parameters[name] = value

    def _del_parameter(self, name):
        self._parameters[name] = None

    def _set_parameter_default(self, name, value):
        if self._get_parameter(name) is None:
            self._set_parameter(name, value)

    def _raise_when_missing(self, *names):
        for name in names:
            if self._get_parameter(name) is None:
                raise AttributeError(f"parameter '{name}' is not set")


def add_parameter_property(cls, name):
    method = property(lambda self: self._get_parameter(name))
    setattr(cls, name, method)
    method = getattr(cls, name).setter(
        lambda self, value: self._set_parameter(name, value)
    )
    setattr(cls, name, method)


class DetectorParameters(
        WithPersistentParameters, 
        parameters=[
            "detector_name", 
            "center_col", 
            "center_row", 
            "sensor_material", 
            "sensor_thickness", 
            "sensor_density"
        ]
):
    def __init__(self) -> None:
        super().__init__()
        self._set_parameter_default("detector_name", "rayonix")
        self._set_parameter_default("center_col", 0)
        self._set_parameter_default("center_row", 0)
        self._set_parameter_default("sensor_material", "Gd2O2S")
        self._set_parameter_default("sensor_density", 4.3) # in gr/cm3,
        self._set_parameter_default("sensor_thickness", 40e-6) # in m,


class DataReductionParameters(
        WithPersistentParameters, 
        parameters=["norm", 'qlim', "ref_delay", "xray_energy"]
):
    def __init__(self) -> None:
        super().__init__()
        self._set_parameter_default("norm", (2.1, 2.2) )
        self._set_parameter_default("qlim", (0,10))
        self._set_parameter_default("ref_delay", "auto")
        self._set_parameter_default("xray_energy", _get_xray_energy())


def _get_xray_energy():
    u17e = config.get("u17e")
    return u17e.position


def detector_metadata(scan):
    # we tried to use the line check below to add or not the metadata,
    # the check works but the metadata are written even if this funciton returns
    # an empty dictionary
    # is_rayonix_used = any([name.find("rayonix")==0 for name in scan.get_channels_dict.keys()])


    dict_to_return = {
        detector_pars.detector_name: {
            "distance": detx.position/1e3, 
            "distance@units": "m",
            "xray_energy": _get_xray_energy(), 
            "xray_energy@units": "keV",
            "beam_center_x": detector_pars.center_col, 
            "beam_center_x@units" : "pixel",
            "beam_center_y": detector_pars.center_row, 
            "beam_center_y@units" : "pixel",
            "sensor_material": detector_pars.sensor_material,
            "sensor_density": detector_pars.sensor_density, 
            "sensor_density@units" : "gr/cm3",
            "sensor_thickness": detector_pars.sensor_thickness, 
            "sensor_thickness@units" : "m"
        }
    }
    try:
        mask = fabio.open(rayonix.processing.mask).data
    except OSError:
        mask = None
    dict_to_return[detector_pars.detector_name]["mask"]=mask
    return dict_to_return
        
def datareduction_metadata(scan):
    dict_to_return = {
        "datareduction": {
            "norm": datareduction_pars.norm, 
            "norm@units": "q_A^-1",
            "qlim": datareduction_pars.qlim, 
            "qlim@units": "q_A^-1",
            "ref_delay": datareduction_pars.ref_delay,
        }
    }

    return dict_to_return

def _metadata_time_converter(delays):
    delays = np.round(delays,11) # 10 ps is the resolution of the delay
    delay_labels = []
    for delay in delays:
        if delay == n354.lxt_laser_off_position:
            label = "off"
        else:
            label = convert_float2str(delay,unit='s',decimals=3)
        delay_labels.append(label)
    return delay_labels


def scan_metadata(scan):
    channels = scan.scan_info['channels'].keys()
    motors = [s.replace("axis:","") for s in channels if s.find("axis:") == 0]
    counters = [s for s in channels if s.find("axis:") == -1]
    counters_mne = [s.split(":")[-1] for s in counters]
    dict_to_return = {
            "scan":    {
                "motors": motors,
                "counters" : counters_mne
                }
           }
    if "lxt_ps" in motors:
        delays = scan.get_data()['axis:lxt_ps']
        delay_labels = _metadata_time_converter(delays)
        dict_to_return['scan']['lxt_ps_labels'] = delay_labels
    if "lxt_ns" in motors:
        delays = scan.get_data()['axis:lxt_ns']
        delay_labels = _metadata_time_converter(delays)
        dict_to_return['scan']['lxt_ns_labels'] = delay_labels
    return dict_to_return
        


scan_meta_obj = scan_meta.get_user_scan_meta()

scan_meta_obj.instrument.set("detector", detector_metadata)
scan_meta_obj.instrument.set("scan_pars", scan_metadata)
scan_meta_obj.instrument.set("datareduction_pars", datareduction_metadata)
detector_pars = DetectorParameters()
datareduction_pars = DataReductionParameters()
