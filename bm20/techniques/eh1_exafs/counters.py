import numpy as np
from bliss.controllers.counter import CalcCounterController


class ExafsFluoCounterController(CalcCounterController):
    def calc_function(self, input_dict):
        scas = [key for key in input_dict.keys() if key.startswith('sca')]

        n_pts = len(input_dict[scas[0]])
        
        scas_sum = np.zeros((n_pts,))

        for sca in scas:
            scas_sum += input_dict[sca]
            
        i0 = input_dict['i0']
        fluo = np.divide(scas_sum / len(scas), i0, where=i0!=0)
        
        return {self.tags[self.outputs[0].name]: fluo}
    

class ExafsTrsampCounterController(CalcCounterController):
    def calc_function(self, input_dict):
        i0 = input_dict['i0']
        i1 = input_dict['i1']
        div = np.divide(i1, i0, out=np.zeros_like(i1), where=i0>0)
        trsamp = -1 * np.log(div, where=div>0)
        return {self.tags[self.outputs[0].name]: trsamp}
    

class ExafsTrrefCounterController(CalcCounterController):
    def calc_function(self, input_dict):
        i1 = input_dict['i1']
        i2 = input_dict['i2']
        div = np.divide(i2, i1, out=np.zeros_like(i1), where=i1>0)
        trref = -1 * np.log(div, where=div>0)
        return {self.tags[self.outputs[0].name]: trref}
    