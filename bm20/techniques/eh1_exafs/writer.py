import os
import re
from datetime import datetime
from collections import OrderedDict

import tabulate
import numpy as np
import h5py
from bliss.scanning.scan import Scan
from silx.io import spech5


MEAS_KEYS_COMMON = ["sec", "i0", "i1", "trsamp", "ge18_fluo"]
MEAS_KEYS_STEP = ["energy"]
MEAS_KEYS_CONT = ["fbragg_energy", "fbragg_egy_delta"]

CONT_KEYS_ALIASES = {"fbragg_energy":"energy",
                     "fbragg_egy_delta":"edelta",
                     "ge18_fluo":"fluo"}

DET_KEYS_ALIASES = {"elapsed_time":"realt",
                    "fractional_dead_time":"deadt"}

detkey_re = re.compile("^.*_det([0-9]{1,2})_(.+$)")


def get_detector_channels(keys):
    channels = set()
    for key in keys:
        m = detkey_re.match(key)
        if m:
            chan = m.groups()[0]
            channels.add(chan)
    return sorted(list(channels), key=int)


def is_continuous_scan(scan_g):
    return "fbragg" in scan_g["measurement"].keys()


def cont_keys_alias(key):
    alias = CONT_KEYS_ALIASES.get(key)
    if alias is None:
        return det_keys_alias(key)
    return alias


def det_keys_alias(key):
    m = detkey_re.match(key)
    if m:
        chan, counter = m.groups()
        alias = DET_KEYS_ALIASES.get(counter, counter)
        key = f"{alias}{chan}"
    return key


def exafs_to_ascii(source, out_file, scan_n=None, overwrite=False, comments=None, xy=None, _energy="energy", is_spec=False):
    if isinstance(source, (Scan,)):
        print("ERROR, exafs_to_ascii from Scan object not "
              "implemented yet.")
    elif scan_n is None:
        raise RuntimeError("Scan number (scan_n) not provided.")
    
    if os.path.isfile(out_file) and not overwrite:
        raise RuntimeError(f"File {out_file} already exists.")
    
    if xy is not None:
        xy_key = xy
        xy = True
    
    os.makedirs(os.path.dirname(out_file), exist_ok=True)

    if is_spec:
        reader = spech5.SpecH5
        reader_args = set()
    else:
        reader = h5py.File
        reader_args = 'r',
    
    with reader(source, *reader_args) as h5f:
        scan_g = h5f[f"{scan_n}.1"]
        meas_g = scan_g["measurement"]
        meas_k = meas_g.keys()
        channels = get_detector_channels(meas_k)

        w_keys = MEAS_KEYS_COMMON[:]

        is_cont = is_continuous_scan(scan_g)
        if xy:
            if is_cont:
                w_keys = ["fbragg_energy", xy_key]
            else:
                w_keys = [_energy, xy_key]
            w_data = OrderedDict()
        else:
            n_meas = meas_g[w_keys[0]].shape[0]
            if is_cont:
                w_keys += MEAS_KEYS_CONT[:]
                elapsed = meas_g["epoch_trig"][:]
                elapsed -= elapsed[0]
            else:
                w_keys = MEAS_KEYS_STEP[:]
                start_t = datetime.fromisoformat(scan_g['start_time'][()].decode()).timestamp()
                end_t = datetime.fromisoformat(scan_g['end_time'][()].decode()).timestamp()
                elapsed = np.linspace(0, end_t - start_t, n_meas)

            det_keys = [f"fx_ge18_det{chan}_{counter}"
                        for counter in ["sca",
                                        "fractional_dead_time",
                                        "elapsed_time"]
                        for chan in channels]
            
            w_keys += det_keys
            w_data = OrderedDict([("# point", np.arange(n_meas)),
                                ("elapsed", elapsed)])

        w_data.update(OrderedDict(
            (cont_keys_alias(key), meas_g[key][:])
            for key in w_keys
        ))

        with open(out_file, 'w') as out_f:
            if not xy:
                out_f.write(f"# source: {source}, scan #{scan_n}.1\n")
                if comments:
                    out_f.write(comments)
                    out_f.write("\n\n")
            out_f.write(tabulate.tabulate(w_data,
                                          headers=(not xy and w_data.keys()) or (),
                                          tablefmt="plain"))
        
        # shape = len(w_keys) + 1, n_meas

        # w_data = np.zeros(shape)

        # w_data[0, :] = np.arange(n_meas)
        # for i_key, key in enumerate(w_keys):
        #     w_data[i_key, :] = meas_g[key][:]

        

        # with open(out_file, 'w') as out_f:
        #     np.savetxt(out_file, w_data.T, delimiter=" ", fmt="%.4f",
        #                header=header)



cmt = "\n# ".join(["MKMETA_EDGE_E_TAB['1'] = 20000",
"MKMETA_REF_NAME['1'] = 'Y-K'",
"MKMETA_COMPOUND['1'] = 'Mo'",
"MKMETA_SAMPLE_DESCR['1'] = 'No'",
"MKMETA_COMMENT['1'] = ''",
"MKMETA_LABBOOK_VOL['1'] = '42'",
"MKMETA_LABBOOK_PAGE['1'] = '00140'",
"MKSCAN_BASE_FILENAME['1'] = ''",
"MKSCAN_SUB_DIRECTORY['1'] = ''",
"",
"MKSCAN_REF_E0['1'] = 0  ### Set to 0 if you dont want to have a ref scan",
"MKSCAN_REF_E_MIN['1'] = -20",
"MKSCAN_REF_E_MAX['1'] = 15",
"MKSCAN_REF_E_INC['1'] = 0.5",
"MKSCAN_REF_CTIME['1'] = 1",
"",
"# Energy of absorption edge, edited value",
"MKSCAN_E0['1'] = 20000",
"MKSCAN_E_OPT['1'] = 20000",
"",
"",
"# Pre-edge 1",
"MKSCAN_PRE_E_MIN['1']['0'] = -70",
"MKSCAN_PRE_E_INC['1']['0'] = 10",
"MKSCAN_PRE_CTIME['1']['0'] = 0.5",
"",
"# Pre-edge 2",
"MKSCAN_PRE_E_MIN['1']['1'] = -30",
"MKSCAN_PRE_E_INC['1']['1'] = 1",
"MKSCAN_PRE_CTIME['1']['1'] = 0.5",
"",
"# Scan over the absorption edge",
"MKSCAN_EDGE_E_MIN['1'] = -20",
"MKSCAN_EDGE_E_MAX['1'] = 30",
"MKSCAN_EDGE_E_INC['1'] = 1",
"MKSCAN_EDGE_CTIME['1'] = 0.5",
"",
"# Parameters for the k-region ",
"MKSCAN_POST_K_END['1']['0'] = 13",
"MKSCAN_POST_K_INC['1']['0'] = 0.05",
"MKSCAN_POST_T_MIN['1']['0'] = 0.5    # Minimum  counting time at k=0",
"MKSCAN_POST_T_MAX['1']['0'] = 2   # Maximum counting time at k=K_END",
"# Power of the increase in counting time, ",
"#   e.g. 1.0= linear, 2.0=square, 3.0=cubic",
"MKSCAN_POST_T_POW['1']['0'] = 2  "])


if __name__ == "__main__":
    # f = "/data/bm20/inhouse/bliss_tmp/testbm20exafs/bm20/20230401/raw/first/first_0001/first_0001.h5"
    # sn = 10
    # comments = "# continuous scan\n# constant velocity: 10ev/s\n# 0.1s points (i.e: 10 points/s, 1 point spans 1ev)"
    f = "/data/bm20/inhouse/bliss_tmp/testbm20exafs/bm20/20230401/raw/sample/sample_0001/sample_0001.h5"
    sn = 5
    # comments = "# step scan with the following parameters:\n# " + cmt
    # f = "/data/bm20/inhouse/bliss_tmp/testbm20exafs/spec/MoSPEC_000_000.dat"
    out_dir = "/data/bm20/inhouse/bliss_tmp/testbm20exafs/bm20/20230401/ascii"
    for xy in ("ge18_fluo", "trsamp"):
    #     for sn in (4, 9, 10):
        out_file = os.path.splitext(os.path.basename(f))[0]
        out_file = os.path.join(out_dir, f"{out_file}_{sn}_{xy}.xy")
        exafs_to_ascii(f, out_file, sn, overwrite=1, xy=xy)#, _energy="Energy", is_spec=True)#, comments=comments)