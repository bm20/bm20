from bm20.techniques.kscan import KScanKeyProcessor
from bm20.macros.optics_tools import rc_xl2pitch_i0



def str_gains_to_int(str_gain):
    return map(int, str_gain)


class EH1KScanKeyProcessor(KScanKeyProcessor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._femto = self.config["femto"]
        
    def process_key(self, scan, keyword, value):
        if keyword == "gain":
            gain = sum(int(g) * 10**n for n, g in enumerate(value[::-1]))
            print(f"Setting gain to {gain} on {self._femto.name} (scan [{scan.title}]).")
            self._femto(gain)
        elif keyword == "rc_xl2pitch":
            rc_xl2pitch_i0()

    def validate_key(self, scan, keyword, value):
        if keyword == "gain":
            gains = str_gains_to_int(value)
            return all(gain >= 3 and gain <= 9 for gain in gains)
        
        if keyword == "rc_xl2pitch":
            return True

        return super().validate_key(scan, keyword, value)
