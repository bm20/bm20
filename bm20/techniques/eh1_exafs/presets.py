
from bliss.config.static import get_config
from bliss.scanning.scan import ScanPreset

from bliss.controllers.motors.turbopmac import (pmac_raw_write_read,
                                                PMAC_MOTOR_STATUS,
                                                pmac_axis_is_open_loop,
                                                pmac_axis_close_loop,
                                                pmac_axis_kill)

from bm20.config.names import bm20names #OPIOM_EH1, MONO, XTAL2_PERP


class FastExafsPreset(ScanPreset):
    def __init__(self):
        super().__init__()
        self._opiom = bm20names.OPIOM_EH1
        self._mono = bm20names.MONO
        self._xl2perp = bm20names.XTAL2_PERP

    def prepare(self, scan):
        self._opiom.switch('SEL_TRIG', 'ZAP')

    def start(self, scan):
        print(f"Starting scan {scan.name}")
        fbragg = bm20names.FBRAGG
        fbragg.sync_hard()
        # fbragg.controller._upload_exafs_params()
#         print(f"Opening the shutter")
        print("STARTED")

    def stop(self, scan):
        print("STOP")
        self._opiom.switch('SEL_TRIG', 'STEP')
        fbragg = bm20names.FBRAGG
        fbragg.controller.clear_scans_params()
        print("STOPPED")
        # # making sure the xtal2perp motor is in open loop
        # # because it is in vacuum but not cooled.
        # # TODO: add others?
        if not pmac_axis_is_open_loop(self._xl2perp):
            pmac_axis_kill(self._xl2perp)
            print(f"xtal2perp in open loop: {pmac_axis_is_open_loop(self._xl2perp)}")

        # saving = scan.scan_saving
        # snun = scan.scan_number
        # data = scan.get_data()

        # dat_dir = os.path.join(saving.base_path, saving.proposal_dirname, saving.beamline, saving.proposal_session_name)
        # dat_file = f"{saving.collection_name}_{saving.dataset_name}_{snun}.dat"
        # data_file = os.path.join(dat_dir, dat_file)

        # columns = ['musstoh:fbragg_energy', "musstoh:fbragg_egy_delta",
        #            'p201_0:ct2_counters_controller:i0', 'xesm4:roi_det0',
        #             'xesm4:realtime_det0',
        #             'xesm4:trigger_livetime_det0', 'xesm4:energy_livetime_det0', 
        #             'xesm4:triggers_det0', 'xesm4:events_det0', 
        #             'xesm4:icr_det0', 'xesm4:ocr_det0', 'xesm4:deadtime_det0']
        # # print(data.keys())
        # # print(scan.scan_info)
        # print('====', (len(data[columns[0]]), len(columns)), data['musstoh:fbragg_energy'].shape)
        # data_ar = np.ndarray((len(data[columns[0]]), len(columns)))
        # for i, col in enumerate(columns):
        #     print(i, col, data[col].shape)
        #     data_ar[:, i] = data[col][:]
        # with open(data_file, "w") as ofile:
        #     ofile.write(tabulate(data_ar, headers=columns, tablefmt="plain"))
        # print(f"File writen: {data_file}")
