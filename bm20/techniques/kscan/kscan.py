import os
from collections import OrderedDict

import numpy as np

from ruamel import yaml

from bliss import current_session
from bliss.common.cleanup import cleanup
from bliss import current_session
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.scanning.group import Sequence

from bliss.controllers.counter import CalcCounterController, SamplingCounterController, CounterController
from bliss.controllers.simulation_counter import SimulationCounterController
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.mca.base import TriggerMode, BaseMCA
from bm20.oh.mono.energy import energy2bragg
from bm20.apps.elements.elements import get_binding_energy, get_natural_width


# from .params import ExafsParameters

# Physical constant
E_MASS = 9.1093836e-31
E_CHARGE = 1.602176634e-19
HBAR = 1.0545718176461565e-34
K_TO_E = 2.0 * E_CHARGE * E_MASS * 1e-20 / (np.power(HBAR, 2))


def e_to_k(delta_e_ev):
    """ Given a value of the energy (in eV above the edge)
        as input, returns the corresponding value in k.

    Args:
        delta_e_ev (float): energy above the edge, in eV

    Returns:
        float: k
    """
    # taken from SPEC's kscan
    # even then the code didnt explain where this constant comes from
    # TODO: improve
    # return 0.51242415 * np.sqrt(delta_e_ev)
    
    # taken from GB code
    return np.sqrt(K_TO_E * delta_e_ev)


def k_to_e(k):
    """Given a value of k, returns the corresponding
       value in energy above the edge in eV.

    Args:
        k (float): k

    Returns:
        float: energy above the edge in eV
    """
    # taken from SPEC's kscan
    # even then the code didnt explain where this constant comes from
    # TODO: improve
    # return np.power(k/0.51242415, 2)

    # taken from GB code
    return np.power(k, 2) / K_TO_E


def k_to_s(t_pwr, t_min_s, t_max_s, k_values, k_max=None):
    """
    Computes the integration time for the given K values.
    t_pwr = 1, 2
    t_min_s, t_max_s : min and max integration times
    k_end
    """
    if k_max is None:
        k_max = k_values[-1]
    k_m = (t_max_s - t_min_s) / np.power(k_max, t_pwr)
    return t_min_s + k_m * np.power(k_values, t_pwr)


def k_to_v2(v_pwr, v_min_ev_s, v_max_ev_s, k_values, k_min=None):
    if k_min is None:
        k_min = k_values[0]
    k_m = (v_max_ev_s - v_min_ev_s) * np.power(k_min, v_pwr)
    return v_min_ev_s + k_m / np.power(k_values, v_pwr)


def k_to_v(v_pwr, v_min_ev_s, v_max_ev_s, k_values, k_max=None):
    if k_max is None:
        k_max = k_values[-1]
    k_m = (v_max_ev_s - v_min_ev_s) / np.power(k_max, v_pwr)
    return v_max_ev_s - k_m * np.power(k_values, v_pwr)
        

def k_range_to_k_e(edge_ev, post_edge_ev, k_end, k_inc):
    k_start = e_to_k(post_edge_ev)
    if (k_end - k_start) % k_inc != 0.:
        k_end = k_end + k_inc
    else:
        k_end = k_end
    k_pos = np.arange(k_start, k_end, k_inc)
    return k_pos, k_to_e(k_pos) + edge_ev


def recalc(fn):
    from functools import wraps
    @wraps(fn)
    def wrapper(self, *args, **kwargs):
        self._dirty = True
        rc = fn(self, *args, **kwargs)
    return wrapper


class EnergyScanParams:
    """ Constant step/time scan paramters.
    """
    _type = None

    e_min = property(lambda self: self._config["e_min"])
    e_max = property(lambda self: self._config["e_max"])
    e_inc = property(lambda self: self._config["e_inc"])
    ctime = property(lambda self: self._config["ctime"])
    edge_ev = property(lambda self: self._edge_ev)
    defaults = property(lambda self: self._defaults.copy())
    params = property(lambda self: self._config.copy())
    param_names = property(lambda self: list(self._config.keys()))

    # scan_pos = property(lambda self: self._scan_pos)
    # scan_t = property(lambda self: self._scan_t)
    
    def __init__(self, edge_ev, e_min, e_max,
                 e_inc, ctime):
        # TODO: set a range for e_min and e_max? like +/- 200 max?
        if e_min >= 0:
            raise ValueError(f'e_min should be < 0 [got {e_min}]')
        
        if e_max <= e_min:
            raise ValueError(f'e_max should be > e_min [e_min={e_min}; e_max={e_max}]')
        
        if e_inc <= 0:
            raise ValueError(f'e_inc should be > 0 [got {e_inc}.')
        
        if ctime <= 0:
            raise ValueError(f'acq_t_s should be > 0 [got {ctime}.')
        
        self._edge_ev = edge_ev

        self._config = OrderedDict([("e_min", e_min),
                                    ("e_max", e_max),
                                    ("e_inc", e_inc),
                                    ("ctime", ctime)
                                    ])
        self._defaults = self._config.copy()

        self._scan_pos = None
        self._scan_t = None
        self._dirty = True

    @e_min.setter
    @recalc
    def e_min(self, e_min):
        self._config["e_min"] = e_min

    @e_max.setter
    @recalc
    def e_max(self, e_max):
        self._config["e_max"] = e_max

    @e_inc.setter
    @recalc
    def e_inc(self, e_inc):
        self._config["e_inc"] = e_inc

    @ctime.setter
    @recalc
    def ctime(self, ctime):
        self._config["ctime"] = ctime

    @property
    def scan_pos(self):
        if self._dirty:
            self._calc()
        return self._scan_pos
    
    @property
    def scan_t(self):
        if self._dirty:
            self._calc()
        return self._scan_t
        
    def _calc(self):
        # warning: the last value of the E array may not be e_max
        # depending on the increment (last E either = or < that e_max)
        e_pos = np.arange(self.e_min + self.edge_ev,
                          self.e_max + self.edge_ev,
                          self.e_inc)
        acq_t = np.repeat(self.ctime, e_pos.shape[0])
        self._scan_pos = e_pos
        self._scan_t = acq_t
        self._dirty = False
        
    def __str__(self):
        txt = f"   * edge: {self.edge_ev} eV]\n"
        txt += f'   * e_min = {self.e_min:> 5.1f} [{self.e_min + self.edge_ev:> 5.1f} eV]\n'
        txt += f'   * e_max = {self.e_max:> 5.1f} [{self.e_max + self.edge_ev:> 5.1f} eV]\n'
        txt += f'   * e_inc = {self.e_inc:> 5.1f} eV\n'
        txt += f'   * e_max = {self.e_max:> 5.1f} [{self.e_max + self.edge_ev:> 5.1f} eV]\n'
        txt += f'   * ct    = {self.ctime:> 5.1f} s\n'
        return txt


class RefScanParams(EnergyScanParams):
    _type = "Ref scan"


class PreEdgeScanParams(EnergyScanParams):
    _type = "Pre Edge scan"


class EdgeScanParams(EnergyScanParams):
    _type = "Edge scan"


class KScanParams:
    edge_ev = property(lambda self: self._edge_ev)
    e_min = property(lambda self: self._config["e_min"])
    e_max = property(lambda self: self._config["e_max"])
    k_end = property(lambda self: self._config["k_end"])
    k_inc = property(lambda self: self._config["k_inc"])
    ctime_min = property(lambda self: self._config["ctime_min"])
    ctime_max = property(lambda self: self._config["ctime_max"])
    ctime_pwr = property(lambda self: self._config["ctime_pwr"])
    defaults = property(lambda self: self._defaults.copy())
    params = property(lambda self: self._config.copy())
    param_names = property(lambda self: list(self._config.keys()))
    
    def __init__(self,
                 edge_ev,
                 e_min,
                 k_end,
                 k_inc,
                 ctime_min,
                 ctime_max,
                 ctime_pwr):
        
        # TODO error msg
        assert ctime_pwr in (0, 1, 2)
        self._edge_ev = edge_ev

        self._config = OrderedDict([("e_min", e_min),
                                    ("e_max", k_to_e(k_end)),
                                    ("k_end", k_end),
                                    ("k_inc", k_inc),
                                    ("ctime_min", ctime_min),
                                    ("ctime_max", ctime_max),
                                    ("ctime_pwr", ctime_pwr)
                                    ])
        self._defaults = self._config.copy()
        
        self._k_pos = None
        self._scan_pos = None
        self._scan_t = None
        self._dirty = True

    @e_min.setter
    @recalc
    def e_min(self, e_min):
        self._config["e_min"] = e_min

    @e_max.setter
    @recalc
    def e_max(self, e_max):
        self._config["k_end"] = e_to_k(e_max - self.edge_ev)
        self._config["e_max"] = e_max

    @k_end.setter
    @recalc
    def k_end(self, k_end):
        self._config["k_end"] = k_end

    @k_end.setter
    @recalc
    def k_end(self, k_end):
        self._config["e_max"] = k_to_e(k_end) + self.edge_ev
        self._config["k_end"] = k_end

    @ctime_min.setter
    @recalc
    def ctime_min(self, ctime_min):
        self._config["ctime_min"] = ctime_min

    @ctime_max.setter
    @recalc
    def ctime_max(self, ctime_max):
        self._config["ctime_max"] = ctime_max

    @ctime_pwr.setter
    @recalc
    def ctime_pwr(self, ctime_pwr):
        self._config["ctime_pwr"] = ctime_pwr

    @property
    def scan_pos(self):
        if self._dirty:
            self._calc()
        return self._scan_pos
    
    @property
    def scan_t(self):
        if self._dirty:
            self._calc()
        return self._scan_t
    
    @property
    def k_pos(self):
        if self._dirty:
            self._calc()
        return self._k_pos

    def _calc(self):
        k_values, scan_pos = k_range_to_k_e(self.edge_ev,
                                            self.e_min,
                                            self.k_end,
                                            self.k_inc)
        scan_t = k_to_s(self.ctime_pwr,
                        self.ctime_min,
                        self.ctime_max,
                        k_values)
        
        self._k_pos = k_values
        self._e_max = self.edge_ev + k_to_e(self.k_end)
        self._scan_pos = scan_pos
        self._scan_t = scan_t

    def __str__(self) -> str:
        txt = [f'   * edge: {self.edge_ev} eV',
               f'   * e_min = {self.e_min:> 5.1f} [{self.e_min + self.edge_ev:> 5.1f} eV]',
               f'   * e_max = {self.e_max:> 5.1f} [{self.e_max + self.edge_ev:> 5.1f} eV]',
               f'   * k_end = {self.k_end} (={self.edge_ev + k_to_e(self.k_end)})',
               f'   * k_inc = {self.k_inc}',
               f'   * ctime_min = {self.ctime_min} s',
               f'   * ctime_max = {self.ctime_max} s',
               f'   * ctime_pwr = {self.ctime_pwr}']
        return '\n'.join(txt)


class KScanFactory:
    config = property(lambda self: self._config)
    name = property(lambda self: self._name)
    defaults = property(lambda self: self.config.get('defaults'))

    def __init__(self, config):
        self._config = config
        self._name = config["name"]
        self._energy_axis = config["energy_axis"]
        self._shutter = config.get("shutter")

    def __info__(self):
        txt = "----------------\n"
        txt += f"KScanConfig name={self.name}"
        defaults = self.defaults
        txt += "----------------\n"
        for scan_name, scan_cfg in defaults.items():
            txt += f"> {scan_name}:\n  * "
            txt += "\n  * ".join([f"{k} = {v}" for k, v in scan_cfg.items()])
            txt += "\n----------------\n"
        return txt

    def __call__(self, name, element=None, edge=None, edge_ev=None,
                 ref_element=None, ref_edge=None, ref_edge_ev=None,
                 metadata=None,
                 run=False, **kwargs):
        # config = self.config
        defaults = self.defaults

        # if title is None:
        #     title = f"not set"

        if edge_ev is None:
            edge_ev = get_binding_energy(element, edge)

        if (ref_edge_ev is None and ref_element is not None
            and ref_edge is not None):
            ref_edge_ev = get_binding_energy(ref_element, ref_edge)

        return KScan(name, edge_ev, self._energy_axis,
                     config=defaults,
                     shutter=self._shutter, ref_edge_ev=ref_edge_ev,
                     metadata=metadata,
                     run=False)


class KScan:
    ref_scan = property(lambda self: self._ref_scan)
    pre_scans = property(lambda self: self._pre_scans)
    edge_scan = property(lambda self: self._edge_scan)
    k_scan = property(lambda self: self._k_scan)
    edge_ev = property(lambda self: self._edge_ev)
    scans_dict = property(lambda self: self._scans_dict)
    sub_scan_names = property(lambda self: list(self._scans_dict.keys()))
    title = property(lambda self: self._title)
    
    def __init__(self, title, edge_ev, energy_axis,
                 config=None, shutter=None,
                 ref_edge_ev=None, metadata=None, run=False):
        if config is None:
            defaults_file = os.path.join(os.path.dirname(__file__), "defaults.yml")
            with open(defaults_file) as df:
                config = yaml.safe_load(df)["defaults"]
        self.config = config
        self._edge_ev = edge_ev
        self._energy_axis = energy_axis
        ref_config = config.get("ref_scan")
        pre_config = [(key, value) for key, value in config.items() if key.startswith("pre_scan")]
        self._metadata = metadata or {}

        if len(pre_config) == 0:
            raise RuntimeError("No pre scan found in config.")
        
        if len(pre_config) == 1 and pre_config[0][0] not in ("pre_scan", "pre_scan_0"):
            raise RuntimeError("Invalid pre_scan name, expected pre_scan or"
                               f" pre_scan_0, got {pre_config[0][0]}")
        else:
            prev_idx = -1
            try:
                for key, cfg in pre_config:
                    # removing "pre_scan_"
                    idx = int(key[9:])
                    if idx != prev_idx + 1:
                        raise RuntimeError(f"Invalid pre_scan index, expected {prev_idx +1},"
                                           f" got {idx}")
                    prev_idx = idx
            except ValueError:
                raise RuntimeError("Invalid pre_scan name, expected pre_scan"
                                   f" pre_scan_<int>, got {pre_config[0][0]}")

        pre_config = [cfg for _, cfg in pre_config]

        edge_config = config.get("edge_scan")
        k_config = config.get("k_scan")

        self._shutter = shutter
        self._stab_time = 0
        self._title = title

        pre_scan_max = [None] * len(pre_config)
        for i, elem in enumerate(pre_config[:-1]):
            pre_scan_max[i] = elem.get('e_max')
            if pre_scan_max[i] is None:
                pre_scan_max[i] = pre_config[i+1]['e_min']

        pre_scan_max[-1] = pre_config[-1].get('e_max')
        if pre_scan_max[-1] is None:
            pre_scan_max[-1] = edge_config['e_min']

        edge_scan_max = edge_config.get('e_max')
        if edge_scan_max is None:
            edge_scan_max = k_config['e_min']

        if ref_edge_ev is not None:
            self._ref_scan = RefScanParams(ref_edge_ev,
                                           ref_config["e_min"],
                                           ref_config["e_max"],
                                           ref_config["e_inc"],
                                           ref_config["ctime"])
        else:
            self._ref_scan = None
        
        self._pre_scans = [
            PreEdgeScanParams(edge_ev, pre_scan["e_min"], pre_scan_max[i],
                             pre_scan["e_inc"], pre_scan["ctime"])
            for i, pre_scan in enumerate(pre_config)]
        
        self._edge_scan = EdgeScanParams(edge_ev,
                                           edge_config["e_min"],
                                           edge_scan_max,
                                           edge_config["e_inc"],
                                           edge_config["ctime"])
        
        self._k_scan = KScanParams(edge_ev,
                                   k_config["e_min"],
                                   k_config["k_end"],
                                   k_config["k_inc"],
                                   k_config["ctime_min"],
                                   k_config["ctime_max"],
                                   k_config["ctime_pwr"])
        
        # for ease of access
        self._scans_dict = OrderedDict({"ref_scan": self._ref_scan})
        self._scans_dict.update({f"pre_scan_{i}": scan
                                 for i, scan in enumerate(self._pre_scans)})
        self._scans_dict.update({"edge_scan": self._edge_scan,
                                 "k_scan": self._k_scan})
        
        
        if run:
            self.run()

    @property
    def param_names(self):
        return {sub_name: sub_scan.param_names
                for sub_name, sub_scan in self.scans_dict.items()}

    def get_sub_scan(self, name):
        scan = None
        if name == "ref_scan":
            scan = self.ref_scan
        elif name == "edge_scan":
            scan = self.edge_scan
        elif name == "k_scan":
            scan = self.k_scan
        elif name == "pre_scan" and len(self.pre_scans) == 1:
            scan = self.pre_scans[0]
        elif name.startswith("pre_scan"):
            split = name.split("_")
            if len(split) == 3:
                try:
                    idx = int(split[-1])
                    if idx >= 0 and idx < len(self.pre_scans):
                        scan = self.pre_scans[idx]
                except ValueError:
                    pass
        if scan is None:
            raise RuntimeError(f"Unknown scan {name}. "
                               f"Available: {', '.join(list(self.scans_dict.keys()))}")
        return scan

    def diff_params(self):
        diff_dict = OrderedDict()
        for name, scan in self._scans_dict.items():
            defaults = scan.defaults
            params = scan.params
            diff = {k: [defaults[k], params[k]]
                    for k in defaults.keys() if defaults[k] != params[k]}
            if diff:
                diff_dict[name] = diff
        return diff_dict
        
    def validate_params(self):
        params = self._scan_params()
        e_max = 0
        for scan in params[1:]:
            scan._calc()
            e_min = params.edge_ev + params[0].e_min
            if e_min < e_max:
                raise RuntimeError("TODO: e_min < previous scan e_max")
            e_max = params.edge_ev + params[0].e_max
            if e_min >= e_max:
                raise RuntimeError("TODO: e_min >= scan e_max")

    def __info__(self):
        txt = "================\n"
        txt += ".ref_scan\n"
        if self._ref_scan:
            txt += f"{self._ref_scan}"
        else:
            txt += "  --> No reference scan."
        txt += "------\n"
        for i, scan in enumerate(self._pre_scans):
            txt += f".pre_scan_{i}\n"
            txt += f"{scan}"
        txt += "------\n"
        txt += f".edge_scan\n"
        txt += f"{self.edge_scan}"
        txt += "------\n"
        txt += f".k_scan\n"
        txt += f"{self.k_scan}"
        return txt

    def _scan_params(self, with_ref=True):
        params = []
        if with_ref and self._ref_scan:
            params = [self._ref_scan]
            
        return params + self._pre_scans + \
               [self._edge_scan] + [self._k_scan]

    def positions(self, with_ref=True):
        return self._calc(with_ref=with_ref)

    def _calc(self, with_ref=True):
        idx_list = []
        new_len = 0
        scans_params = self._scan_params(with_ref=with_ref)
        for scan_idx in range(len(scans_params[:-1])):
            scan0 = scans_params[scan_idx]
            scan1 = scans_params[scan_idx + 1]
            idx = np.searchsorted(scan0.scan_pos, scan1.scan_pos[0])
            new_len += idx
            # TODO: check that idx > 0
            idx_list += [(0, idx)]
            
        idx_list += [(0, scans_params[-1].scan_pos.shape[0])]
        new_len += scans_params[-1].scan_pos.shape[0]
        
        scan_pos = np.ndarray(new_len, dtype=np.float64)
        scan_t = np.ndarray(new_len, dtype=np.float64)
        i_start = 0
        i_end = 0
        for scan_idx, scan in enumerate(scans_params):
            i0, i1 = idx_list[scan_idx]
            i_end = i_start + i1
            scan_pos[i_start:i_end] = scan.scan_pos[i0:i1]
            scan_t[i_start:i_end] = scan.scan_t[i0:i1]
            i_start += i1

        # self._scan_pos = scan_pos
        # self._scan_t = scan_t
        return scan_pos, scan_t
    
    def run(self, *counters, with_ref=True, repeat=1, scan_info=None):
        metadata = self._metadata.copy()
        if scan_info:
            metadata.update(scan_info)
        if not counters:
            counters = current_session.active_mg
            if not counters:
                raise RuntimeError('Please provide counters or set '
                                'the active measurement group.')
            self.start(counters, with_ref=with_ref, repeat=repeat, scan_info=scan_info)
        else:
            self.start(*counters, with_ref=with_ref, repeat=repeat, scan_info=scan_info)

    def _get_chain(self, with_ref=True, *counters):
        """
        Creating the chain.
        TODO: improve
        """
        default_chain = current_session.default_acquisition_chain
        
        # if default_chain:
        #     default_settings = default_chain._settings
        # else:
        #     default_settings = {}

        # node_p201 = None
        # timer = None

        # scan_pos, scan_t = map(list, self.positions(with_ref=with_ref))

        # master = VariableStepTriggerMaster(
        #                 self._energy_axis,
        #                 scan_pos,
        #                 stab_time=self._stab_time)
        


        # chain = AcquisitionChain()
        # builder = ChainBuilder(counters)

        # npoints = len(scan_pos)

        # # synchro, using a soft timer if no p201
        # acq_node_params = {"npoints": npoints, "acq_expo_time": scan_t}
        # acq_child_params = {"npoints": npoints, "count_time": scan_t}
        # ct2_nodes = builder.get_nodes_by_controller_type(CT2Controller)
        # if ct2_nodes:
        #     for node in ct2_nodes:
        #         node.set_parameters(acq_params=acq_node_params)
        #         for child_node in node.children:
        #             child_node.set_parameters(acq_params=acq_child_params)
        #         node_p201 = node
        #         chain.add(master, node)
        # else:
        #     raise NotImplemented("Need a p201")
        #     # timer = SoftwareTimerMaster(0.1, npoints=10)
        #     # chain.add(master, timer)


        # for node in builder.get_nodes_by_controller_type(BaseMCA):
        #     mca = node.controller
        #     mca_params = default_settings.get(mca, {"trigger_mode": "SOFT"})
        #     mca_params.update({"npoints": npoints,
        #                        "preset_time" : scan_t})

        #     if "trigger_mode" not in mca_params:
        #         mca_params["trigger_mode"] = "SOFT"

        #     mca_master

        #     node.set_parameters(acq_params=mca_params)
        #     # chain.add(node_p201, node)

        #     chain.add(master, node)

# ###################################
#     # FalconX
#     #
#     if fx_trigger == "SOFTWARE":
#         acq_params = {
#             "npoints": scan_par["points"],
#             "trigger_mode": fx_trigger,
#             "read_all_triggers": False,
#             "block_size": fx_block_size,
#             "preset_time" : scan_par["time"]
#         }
#     else:
#         acq_params = {
#             "npoints": scan_par["points"],
#             "trigger_mode": fx_trigger,
#             "read_all_triggers": False,
#             "block_size": fx_block_size,
#             "preset_time" : 0.1
#         }
        


#     ###################################
#     # SamplingCounterController
#     #
#     acq_params = {
#             "count_time": scan_par["time"],
#             "npoints": scan_par["points"],
#         }
#     # for node in builder.nodes:  
#     for node in builder.get_nodes_by_controller_type(CounterController):  
#         if isinstance(node.controller, SamplingCounterController):
#             node.set_parameters(acq_params=acq_params)
#             # chain.add(node_p201, node)
        

            
#     # for node in builder.nodes:
#     #     if isinstance(node.controller, SamplingCounterController):
#     #         node.set_parameters(acq_params=acq_params)
#     # SimulationCounter

#     ###################################
#     # CalcCounterController
    
#     acq_params = {
#         "count_time": scan_par["time"],
#     }
#     for node in builder.nodes:
#         if isinstance(node.controller, CalcCounterController):
#             node.set_parameters(acq_params=acq_params)
#             chain.add(node_p201, node)




    def start(self, *counters, with_ref=True, repeat=1, scan_info=None):

        print("Starting EXAFS scan with the following parameters:")
        print(self.__info__())

        # if len(counters) == 0:    
        #     raise RuntimeError('At least one counter must be provided.')
    #     exafs_param, 
    #     *counters, 
    #     RC=False, 
    #     sleep_time=0.2, 
    #     step=False, 
    #     nbscan=1,
    #     ascii=False,
    # ):
    #     """
    #     Step Scans with variable step size and variable step time
    #     Positions and times are taken from exafs_param object
    #     """
        scan_info = scan_info or {}
        scan_idx = 0
        while scan_idx < repeat:
            scan_idx += 1
            print(f"Starting scan {scan_idx}/{repeat}")
            scan_pos, scan_t = map(list, self.positions(with_ref=with_ref))
                        
            # SCAN STATE
            # self._scan_state = "PREPARING"
            # self._scan_name = "exafs_step"
            
            # INIT VARIABLES        
            # positions = scantool_get_step_position2(exafs_param, bragg, step)
            # print(numpy.all(numpy.isfinite(positions)))
            if scan_pos[0] > scan_pos[-1]:
                way = "down"
            else:
                way = "up"
            
            # BEAMLINE SETUP
            # Open beamshutter
            if self._shutter is not None:
                self._shutter.open()

            with cleanup(self._scan_end):
                # MULTIPLEXER
                #self._multpx.switch("SEL_TRIG", "STEP")
                
                # SCAN PARAMETERS
                self._scan_par = scantool_set_scan_parameters(
                    motor=self._energy_axis,
                    positions=scan_pos,         
                    time=scan_t, 
                    points=len(scan_pos),
                    way=way)
                
                for nscan in range(repeat):
                    # MOTOR MASTER
                    self._master = VariableStepTriggerMaster(
                        self._energy_axis,
                        scan_pos,
                        stab_time=self._stab_time)
                    print(counters)
                    # ACQUISITION CHAIN
                    self._builder, self._chain = scantool_stepscan_get_acquisition_chain(
                            self._master,
                            self._scan_par,
                            counters,
                            fx_trigger=TriggerMode.GATE
                            #fx_trigger=self._fx_trigger_mode,
                            #fx_block_size=self._fx_block_size,
                        )
                    print(self._chain._tree)
                
                    
                    nbp = self._scan_par["points"]
                    # scan_info = {
                    #     "title": f"EXAFS {self._name}",
                    #     "element": f"{self._element}",
                    #     "edge": f"{self._edge}",
                    # }
                    if "title" not in scan_info:
                        scan_info["title"] = f"EXAFS {self._title}"
                    #,
                        # "instrument":{"exafs": self.scan_params().metadata()}
                    # }
                    

                    #     "title": f"{self._name}.exafs_step {nbp} points", 
                    #     "type": f"{self._name}.exafs_step",
                    #     #"instrument": instrument
                    # }
                        
                    print(f"\nSCAN #{nscan+1}(/{repeat})")

                    # SCAN
                    self._scan_obj = Scan(
                        self._chain,
                        name=f"{self._title}.exafs_step",
                        scan_info=scan_info
                        # ,
                        # data_watch_callback=scan_display
                    )
                    # self._scan_state = "RUNNING"
                    print(f'Scanning from {scan_pos[0]} to {scan_pos[-1]}')
                    self._scan_obj.run()
                    #self.plotter.run(self._scan_obj)
                    
                    # if ascii:
                    #     save_exafs_ascii(self._scan_obj)
                    
    def _scan_end(self):
        print('SCAN END')

        
#######################################################################
#####
##### Store Scan Parameters and create defaults
#####
def scantool_set_scan_parameters(
    motor=None,
    start=np.nan,
    stop=np.nan,
    positions=None,
    points=1,
    time=None,
    way="up",
    trig_start="TIME",
    trig_point="TIME"
):
    scan_par = {
        "motor": motor,
        "positions": positions,
        "start": start,
        "stop": stop,
        "points": points,
        "time": time,
        "way" : way,
        "trig_start": trig_start,
        "trig_point": trig_point,
    }
    
    return scan_par


#######################################################################
#####
##### Build Step scan Acquisition Chain
#####
def scantool_stepscan_get_acquisition_chain(
    master,
    scan_par,
    counters,
    fx_trigger="GATE",
    fx_block_size=5,
):
    
    chain = AcquisitionChain(parallel_prepare=True)
    builder = ChainBuilder(counters)

    ###################################
    # P201
    #
    acq_node_params = {
            "npoints": scan_par["points"],
            "acq_expo_time": scan_par["time"],
    }
    if isinstance(scan_par["time"], (list, np.ndarray)):
        acq_child_params = {
            "count_time": 1,
            "npoints": 1,
        }
    else:
        acq_child_params = {
            "count_time": scan_par["time"],
            "npoints": scan_par["points"],
        }
    
    for node in builder.get_nodes_by_controller_type(CT2Controller):
        node.set_parameters(acq_params=acq_node_params)
        for child_node in node.children:
            child_node.set_parameters(acq_params=acq_child_params)
        node_p201 = node
        chain.add(master, node)
        
    # tmast = SoftwareTimerMaster(0.1, scan_par["points"])
    # chain.add(master, tmast)


    ###################################
    # FalconX
    #
    if fx_trigger == "SOFTWARE":
        acq_params = {
            "npoints": scan_par["points"],
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : scan_par["time"]
        }
    else:
        acq_params = {
            "npoints": scan_par["points"],
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : 0.1
        }
        
    for node in builder.get_nodes_by_controller_type(BaseMCA):
        node.set_parameters(acq_params=acq_params)
        # chain.add(node_p201, node)
        chain.add(master, node)

    ###################################
    # SamplingCounterController
    #
    acq_params = {
            "count_time": scan_par["time"],
            "npoints": scan_par["points"],
        }
    # for node in builder.nodes:  
    for node in builder.get_nodes_by_controller_type(CounterController):  
        if isinstance(node.controller, SamplingCounterController):
            node.set_parameters(acq_params=acq_params)
            # chain.add(node_p201, node)
        

            
    # for node in builder.nodes:
    #     if isinstance(node.controller, SamplingCounterController):
    #         node.set_parameters(acq_params=acq_params)
    # SimulationCounter

    ###################################
    # CalcCounterController
    
    acq_params = {
        "count_time": scan_par["time"],
    }
    for node in builder.nodes:
        if isinstance(node.controller, CalcCounterController):
            node.set_parameters(acq_params=acq_params)
            chain.add(node_p201, node)
    return (builder, chain)