import os
import re
import time
import configparser
from collections import OrderedDict

import tabulate

from ruamel import yaml

from bliss import current_session
from bliss.common.axis import Axis
from bliss.config.static import get_config

from .kscan import get_binding_energy


def _pprint(dd, title=None, keys=None, indent=2, prepend=4, symbols=["* ", "- ", "- "], ignore_none_value=True, ignore_empty_dict=True):
    if not dd:
        return ""
    if title is None:
        txt = ""
    else:
        txt = f"{title}\n"

    symbols += ["- "] * (2 - len(symbols))
    if keys is None:
        keys = dd.keys()
    else:
        keys = [k for k in keys if k in dd]
    for key in keys:
        value = dd[key]
        if isinstance(value, (dict,)):
            if not value and ignore_empty_dict:
                continue
            txt += f"{' ' * prepend}{symbols[0]}{key}:\n"
            txt += _pprint(value, indent=indent, prepend=prepend+indent, symbols=symbols[1:])
        else:
            if value is None and ignore_none_value:
                continue
            txt += f"{' ' * prepend}{symbols[0]}{key} = {value}\n"
    return txt


def load_yml(filename):
    with open(filename) as fd:
        config_dict = yaml.safe_load(fd)
    return config_dict


def load_ini(filename):
    ini_dict = configparser.ConfigParser()
    ini_dict.read(filename)
    config_dict = OrderedDict()
    for section in ini_dict.sections():
        config_dict[section] = ini_dict[section]


class KScanLoader:
    name = property(lambda self: self._name)
    kw_processor = property(lambda self: self._kw_processor)
    def __init__(self, config):
        self._name = config["name"]
        self._kscan = config["kscan"]
        self._kw_processor = config.get("kscan_kw")

    def __call__(self, config_file):
        if not os.path.isabs(config_file) and not os.path.exists(config_file):
            if current_session is None:
                raise RuntimeError(f"File not found {config_file}.")
            scan_saving = current_session.scan_saving
            idx = scan_saving.root_path.index("RAW_DATA")
            root_path = scan_saving.root_path[:idx]
            script_dir = os.path.join(root_path, "SCRIPTS")
            config_file = os.path.join(script_dir, config_file)

        ext = os.path.splitext(config_file)[1].lstrip(".")
        if ext in ("yml", "yaml"):
            config_dict = load_yml(config_file)
        elif ext in ("cfg", "ini"):
            config_dict = load_ini(config_file)
        else:
            raise ValueError(f"Invalid extension, expected *.yml or *.ini: {config_file}")

        scan_grp = KScanGroup(config_file, config_dict, self._kscan, kw_processor=self.kw_processor)
        return scan_grp


class KScanKeyProcessor:
    config = property(lambda self: self._config)
    name = property(lambda self: self._name)
    keywords = property(lambda self: self._keywords)

    def __init__(self, config):
        self._name = config["name"]
        self._config = config
        self._keywords = config["scan_kwords"]

        # TODO : better
        # TODO : check if keyword is an axis?
        assert "repeat" not in self._keywords

    def process_key(self, scan, keyword, value):
        pass

    def validate_key(self, scan, keyword, value):
        return True


def get_edge_ev(element, edge, edge_ev):
    if edge_ev is None:
        if (element is None and edge is None):
            return None
        if (element is None or edge is None):
            raise RuntimeError("element AND edge need to be "
                               "both set OR edge_ev set.")
        return get_binding_energy(element, edge)
    try:
        edge_ev = float(edge_ev)
    except ValueError:
        raise RuntimeError("Error while reading edge_ev, not a valid number.")
    return edge_ev


def parse_key_params(key):
    rx = re.compile(r"^(?P<step>ref_scan|pre_scan(?P<idx>_\d+)|edge_scan|k_scan)\.(?P<param>.+)$")
    m = rx.match(key)
    if m:
        mdict = m.groupdict()
        s_type = mdict["step"]
        s_idx = mdict["idx"]
        s_param = mdict["param"]

        if s_type.startswith("pre_scan"):
            if s_idx:
                s_idx = int(s_idx[1:])
            else:
                s_idx = 0
            s_type = f"pre_scan_{s_idx}"
        return s_type, s_param
    return None


class KScanGroup:
    name = property(lambda self: self._name)
    kscans = property(lambda self: self._kscans)
    global_config = property(lambda self: self._global_cfg)
    scans_config = property(lambda self: self._global_cfg.scans_config)

    def __init__(self, name, config_dict, kscan, kw_processor=None):
        self._name = name
        self._cfg_dict = None
        self._kscan = kscan
        self._kw_processor = kw_processor

        if kw_processor:
            keywords = kw_processor.keywords
        else:
            keywords = None

        self._scans_obj = OrderedDict()
        self._global_cfg = KScanGlobalConfig(config_dict, scan_keywords=keywords)
        self._kscans = self._init_scans()
        self._validate()

    def _init_scans(self):
        scans_dict = OrderedDict()
        ksc = self._kscan
        self._scans_obj = OrderedDict()
        ref_dict = self.global_config.reference_dict
        global_params = self.global_config.parameters

        for scan_name, scan_cfg in self.scans_config.items():
            edge_dict = scan_cfg.edge_dict
            if not edge_dict:
                edge_dict = self.global_config.edge_dict
            kscan = ksc(scan_name,
                        run=False,
                        **edge_dict,
                        **ref_dict)

            scan_params = self.global_config.scan_config(scan_name).parameters
            for params in (global_params, scan_params):
                if params:
                    for sub_scan_name, sub_params in params.items():
                        sub_scan = kscan.get_sub_scan(sub_scan_name)
                        for sub_param_name, value in sub_params.items():
                            try:
                                setattr(sub_scan, sub_param_name, value)
                            except AttributeError:
                                raise RuntimeError(f"Error, scan {sub_scan_name}, "
                                                   f"unknown parameter {sub_param_name}")
            scans_dict[scan_name] = kscan
        return scans_dict
    
    def run(self):
        repeat = self.global_config.repeat
        loop_idx = 1
        remaining_txt = f"/{repeat}" if repeat > 0 else " (infinite run)"
        while True:
            print(f"Starting run {loop_idx}{remaining_txt}.")
            for scan_name, scan in self.kscans.items():
                keywords = self.global_config.scans_keywords(name=scan_name)
                for key, value in keywords.items():
                    self._kw_processor.process_key(scan, key, value)
                print(f"Scan {scan_name}.")
                # running the ref only on the first run.
                scan.run(with_ref=loop_idx!=1)
                time.sleep(10)
            loop_idx += 1
            if repeat > 0:
                if loop_idx > repeat:
                    print("Kscan done.")
                    print(f"Was: {self.name}")
                    break

    def __info__(self):
        main_cfg = self.global_config
        
        txt = f"================\n"
        txt += "KScanGroup\n"
        txt += f" Name: {self.name}\n"
        txt += f"======\n"
        txt += main_cfg.global_info()
        txt += main_cfg.scans_info()
        return txt
    
    def global_info(self):
        print(self.global_config.global_info())

    def scans_info(self):
        print(self.global_config.scans_info())

    def _validate(self):
        config = get_config()

        for motor_name in self.global_config.motors.keys():
            bliss_obj = config.get(motor_name)
            if not isinstance(bliss_obj, (Axis,)):
                raise RuntimeError(f"Error on scan {self.name}: "
                                   f"object {motor_name} is not a motor.")
        for scan_name, scan in self.kscans.items():
            info = self.global_config.scan_config(scan_name)
            # scan.validate_params()
            motors = info.motors
            for motor_name, _ in motors.items():
                bliss_obj = config.get(motor_name)
                if not isinstance(bliss_obj, (Axis,)):
                    raise RuntimeError(f"Error on scan {scan_name}: "
                                       f"object {motor_name} is not a motor.")
            for keyword, value in info.keywords.items():
                if not self._kw_processor.validate_key(scan, keyword, value):
                    raise RuntimeError(f"Scan [{scan_name}], invalid key "
                                       f"[{keyword}] with value [{value}].")
        return True
    
    def scan_defaults(self):
        return self._kscan.defaults
    
    def print_defaults(self):
        print(self._kscan.__info__())
    
    def print_diff(self):
        kscans = self.kscans
        kscan_names = list(kscans.keys())
        scan0 = kscans[kscan_names[0]]
        param_names = scan0.param_names
        sub_names = scan0.sub_scan_names

        p_diffs = {s_name:scan.diff_params() for s_name, scan in kscans.items()}

        for sub_name in sub_names:
            tab = [["defaults"] + list(scan0.get_sub_scan(sub_name).defaults.values())]
            for s_name, scan in kscans.items():
                tab += [[s_name] + [p_diffs[s_name].get(sub_name, {}).get(p_name, [None, None])[1] for p_name in param_names[sub_name]]]
            txt = tabulate.tabulate(tab, tablefmt="simple_grid", headers=[f"={sub_name.upper()}="] + param_names[sub_name])
            print(txt)


class KScanGlobalConfig:
    proposal = property(lambda self: self._main["proposal"])
    repeat = property(lambda self: self._main["repeat"])
    edge_dict = property(lambda self: self._edge.copy())
    edge = property(lambda self: self._edge["edge"])
    element = property(lambda self: self._edge["element"])
    edge_ev = property(lambda self: self._edge["edge_ev"])
    reference_dict = property(lambda self: self._reference.copy())
    reference_element = property(lambda self: self._reference["ref_element"])
    reference_edge = property(lambda self: self._reference["ref_edge"])
    reference_ev = property(lambda self: self._reference["ref_edge_ev"])
    # copy?
    metadata = property(lambda self: self._metadata)
    motors = property(lambda self: self._motors)
    parameters = property(lambda self: self._parameters)
    scans_config = property(lambda self: self._scans.copy())

    def __init__(self, config_dict, scan_keywords=None):
        scan_keys = [k for k in config_dict.keys()
                     if k not in ("main", "edge", "parameters",
                                  "reference", "metadata", "motors")]
        self._main = self._parse_main(config_dict["main"])
        self._edge = self._parse_edge(config_dict["edge"])
        self._reference = self._parse_reference(config_dict.get("reference", {}))
        self._metadata = self._parse_metadata(config_dict.get("metadata", {}))
        self._motors = self._parse_motors(config_dict.get("motors", {}))
        self._parameters = self._parse_parameters(config_dict.get("parameters", {}))
        self._scans = self._parse_scans({key: config_dict[key] for key in scan_keys},
                                        scan_keywords=scan_keywords)
        
    def global_info(self):
        txt = f"Main scan config [proposal={self.proposal}]\n"
        txt += _pprint(self._main, title="> Main:")
        if self.reference_dict:
            txt += _pprint(self.reference_dict, title="> Reference:")
        txt += _pprint(self.edge_dict, title="> Edge:")
        if self.parameters:
            txt += _pprint(self.parameters, title="> Parameters:")
        if self.motors:
            txt += _pprint(self.motors, title="> Motors:")
        if self.metadata:
            txt += _pprint(self.metadata, title="> Metadata:")
        txt += f"> Number of scans: {len(self.scans_config)}\n"
        return txt
    
    def scans_info(self):
        txt = "> Scans:\n"
        txt += "->" + "\n->".join(["\n    ".join(scan.__info__().rstrip().split("\n"))
                                   for scan in self.scans_config.values()])
        txt += f"\n"
        return txt
    
    def scan_config(self, name):
        try:
            return self.scans_config[name]
        except KeyError:
            raise RuntimeError(f"Unknown scan name {name}.")
        
    def scans_motors(self, name=None):
        if name:
            return self.scan_config(name).motors
        return {name:self.scan_config(name).motors
                for name in self.scans_config.keys()}
    
    def scans_keywords(self, name=None):
        if name:
            return self.scan_config(name).keywords
        return {name:self.scan_config(name).keywords
                for name in self.scans_config.keys()}

    def scans_metadata(self, name=None):
        if name:
            return self.scan_config(name).metadata
        return {name:self.scan_config(name).metadata
                for name in self.scans_config.keys()}

    def __info__(self):
        txt = self.global_info()
        txt += "---------\n"
        txt += self.scans_info()
        return txt
    
    def _parse_main(self, main_cfg):
        main = {}
        # TODO : warn if unknown key
        main["proposal"] = main_cfg["proposal"]
        main["repeat"] = int(main_cfg["repeat"])
        return main

    def _parse_edge(self, edge_cfg):
        edge_dict = {}

        edge_dict["element"] = edge_cfg.get("element")
        edge_dict["edge"] = edge_cfg.get("edge")
        edge_ev = edge_cfg.get("edge_ev")
        edge_dict["edge_ev"] = get_edge_ev(edge_dict["element"],
                                           edge_dict["edge"],
                                           edge_ev)
        return edge_dict

    def _parse_reference(self, reference_cfg):
        edge_dict = {}

        if reference_cfg:
            edge_dict["ref_element"] = reference_cfg.get("ref_element")
            edge_dict["ref_edge"] = reference_cfg.get("ref_edge")
            edge_ev = reference_cfg.get("ref_edge_ev")
            edge_dict["ref_edge_ev"] = get_edge_ev(edge_dict["ref_element"],
                                                   edge_dict["ref_edge"],
                                                   edge_ev)
        return edge_dict

    def _parse_metadata(self, metadata_cfg):
        # not using metadata_cfg.copy() to support
        # ConfigParser objects.
        if metadata_cfg:
            return {k: v for k, v in metadata_cfg.items()}
        return {}
    
    def _parse_motors(self, motors_cfg):
        # not using metadata_cfg.copy() to support
        # ConfigParser objects.
        if motors_cfg:
            return {k: v for k, v in motors_cfg.items()}
        return {}

    def _parse_parameters(self, parameters_cfg):
        params = {}
        try:
            for key, value in parameters_cfg.items():
                sub_name, param = key.split(".")
                try:
                    sub_scan = params[sub_name]
                except KeyError:
                    sub_scan = {}
                    params[sub_name] = sub_scan
                sub_scan[param] = float(value)
        except Exception as ex:
            raise RuntimeError(f"Error while parsing [paramters]: invalid key {key}.")
        return params

    def _parse_scans(self, scans_cfg, scan_keywords=None):
        kscan_scan_config = OrderedDict()
        for scan_name, scan_cfg in scans_cfg.items():
            kscan_scan_config[scan_name] = KScanScanConfig(scan_name,
                                                           scan_cfg,
                                                           scan_keywords=scan_keywords)
        return kscan_scan_config


class KScanScanConfig:
    name = property(lambda self: self._name)
    edge_dict = property(lambda self: self._edge.copy())
    edge = property(lambda self: self._edge["edge"])
    element = property(lambda self: self._edge["element"])
    edge_ev = property(lambda self: self._edge["edge_ev"])
    # copy?
    parameters = property(lambda self: self._parameters)
    metadata = property(lambda self: self._metadata)
    motors = property(lambda self: self._motors)
    keywords = property(lambda self: self._keywords)
    scan_names = property(lambda self: list(self.parameters.keys()))

    def __init__(self, name, config_dict, scan_keywords=None):
        self._name = name
        cfg_keys = [k for k in config_dict.keys()
                    if k not in ("element", "edge", "edge_ev")]
        self._edge = self._parse_edge(config_dict)
        self._parameters = self._parse_parameters(config_dict, cfg_keys)
        self._motors = self._parse_motors(config_dict, cfg_keys)
        self._keywords = self._parse_keywords(config_dict, cfg_keys, scan_keywords)
        self._metadata = self._parse_others(config_dict, cfg_keys)

    def __info__(self):
        txt = f"Scan: {self.name}\n"
        if self.edge_dict:
            txt += _pprint(self.edge_dict, title="> Edge:")
        if self.parameters:
            txt += _pprint(self.parameters, title="> Parameters:")
        if self.motors:
            txt += _pprint(self.motors, title="> Motors:")
        if self.keywords:
            txt += _pprint(self.keywords, title="> Keywords:")
        if self.metadata:
            txt += _pprint(self.metadata, title="> Metadata:")
        return txt

    def _parse_edge(self, edge_cfg):
        edge_dict = {}
        edge_dict["element"] = edge_cfg.get("element")
        edge_dict["edge"] = edge_cfg.get("edge")
        edge_ev = edge_cfg.get("edge_ev")
        edge_dict["edge_ev"] = get_edge_ev(edge_dict["element"],
                                           edge_dict["edge"],
                                           edge_ev)
        if edge_dict["edge_ev"] is None:
            return {}
        return edge_dict
    
    def _parse_parameters(self, config_dict, cfg_keys):
        scan_params = {}
        for item_name in cfg_keys[:]:
            result = parse_key_params(item_name)
            if result:
                try:
                    s_dict = scan_params[result[0]]
                except KeyError:
                    s_dict = {}
                    scan_params[result[0]] = s_dict
                s_dict[result[1]] = float(config_dict[item_name])
                cfg_keys.remove(item_name)
        return scan_params
    
    def _parse_motors(self, config_dict, cfg_keys):
        motors_dict = {}
        config = get_config()
        for key in cfg_keys[:]:
            if (key in config.names_list
                and config.get_config(key).plugin == "emotion"):
                # this is an axis (or a motor controller...)
                motors_dict[key] = config_dict[key]
                cfg_keys.remove(key)
        return motors_dict
    
    def _parse_keywords(self, config_dict, cfg_keys, scan_keywords):
        keywords_dict = {}
        if scan_keywords:
            for keyword in scan_keywords:
                try:
                    keywords_dict[keyword] = config_dict[keyword]
                    cfg_keys.remove(keyword)
                except KeyError:
                    pass
        return keywords_dict
    
    def _parse_others(self, config_dict, cfg_keys):
        return {key: config_dict[key] for key in cfg_keys}


if __name__ == "__main__":
    # from bliss.config.static import get_config
    # cfg_file = "/data/bm20/inhouse/bm202309/bm20/20230901/SCRIPTS/exafs.ini"
    cfg_file = "/users/blissadm/local/bm20.git/bm20/tests/exafs.ini"
    mks = get_config().get('simu_kscan_eh1')

    # scancfg =_parse_ini_file(cfg_file)
    # print(scancfg._main, scancfg._edge, scancfg._reference, scancfg._parameters, scancfg._motors)

    sgrp = mks(cfg_file)
    sgrp.run()
    # kscans = list(sgrp.kscans.values())
    # # print(kscans[0].__info__())
    # # print(sgrp.global_config.__info__())
    # # print(sgrp.global_config.metadata)
    # # print(sgrp.global_config.scans_motors())
    # # print(sgrp.global_config.scans_metadata())
    # # print(sgrp.global_config.scans_keywords())
    # print(sgrp.__info__())
    # # sgrp._validate()
    # print(sgrp.print_diff())
    # # print(sgrp.kscans)
    # for sc in sgrp.kscans.values():
    #     print(sc.__info__())
    #     # print(sc.diff_params())
    # # print(sgrp.print_defaults())