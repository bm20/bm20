from bliss.config.static import get_config
from bliss.common.cleanup import cleanup

# from ...scans.exafs.exafs import ExafsStepScan, ExafsContinuousScan
# from .kscanconfig import KScanConfig

import os
import numpy as np
from tabulate import tabulate
from bliss.config.static import get_config
from bliss.common.cleanup import cleanup
# from bliss import setup_globals
# from bliss.shell.standard import new
from bliss import setup_globals, current_session
from bm20.oh.mono.energy import bragg2energy, energy2bragg


from bliss.config.static import get_config
# from bliss.scanning.scan import ScanPreset
from bliss import global_map
from bliss.common.protocols import HasMetadataForScan


from fscan.mussttools import MusstChanUserCalc


class FEnergyCenterCalc(MusstChanUserCalc):
    """
    Calc that computes the energy from the motor channel.
    """

    def prepare(self):
        self._d_spacing = get_config().get("monxtal").controller.d_spacing()
        super().prepare()
        
    def select_input_channels(self, channels, pars):
        return ["fbragg_center"]

    def get_output_channels(self):
        return ["energy_center"]

    def calculate(self, data):
        return {"energy_center": bragg2energy(data["fbragg_center"], self._d_spacing)}
    

class FEnergyDeltaCalc(MusstChanUserCalc):
    """
    Calc that computes the energy from the motor channel.
    """

    def prepare(self):
        self._d_spacing = get_config().get("monxtal").controller.d_spacing()
        super().prepare()
        
    def select_input_channels(self, channels, pars):
        return ["fbragg_center", "fbragg_delta"]

    def get_output_channels(self):
        return ["energy_delta"]

    def calculate(self, data):
        e_mid = data["fbragg_delta"] / 2.
        e_0 = bragg2energy(data["fbragg_center"] - e_mid, self._d_spacing)
        e_1 = bragg2energy(data["fbragg_center"] + e_mid, self._d_spacing)
        return {"energy_delta": e_1 - e_0}
        

class D20FScan(HasMetadataForScan):
    def __init__(self, config):

        global_map.register(self)
        self.disable_scan_metadata()

        cfg = config.to_dict()
        self.name = cfg["name"]
        self._crystal = cfg["crystal"]
        self._bragg = cfg["bragg"]
        self._fbragg = cfg["fbragg"]
        # self._opiom = cfg["opiom"]
        self._fscan_cfg = cfg["fscan_cfg"]
        self._musst = cfg["musst"]

        fegy_center_calc = FEnergyCenterCalc()
        self._fscan_cfg.fscan.master.add_user_calc(fegy_center_calc)
        self._fscan_cfg.fscan2d.master.add_user_calc(fegy_center_calc)
        fegy_delta_calc = FEnergyDeltaCalc()
        self._fscan_cfg.fscan2d.master.add_user_calc(fegy_delta_calc)

    def __info__(self):
        return "BLABLA"
    
    def sync(self):
        self._bragg.sync_hard()
        self._fbragg.sync_hard()

    def cleanup(self):
        self._fbragg.controller.clear_scans_params()
        self.disable_scan_metadata()
        self.sync()

    def fscan(self, e0, e1, intervals, count_time):
        step_ev = (e1 - e0) / intervals
        return self._fscan(e0, e1, step_ev, count_time)

    def _fscan(self, e0, e1, step_ev, step_s,
            #   dataset_name=None,
              scan_info=None):
        # opiom = self._opiom
        bragg = self._bragg
        fbragg = self._fbragg
        fscan_cfg = self._fscan_cfg
        d_spacing = self._crystal.controller.d_spacing()

        fscan = fscan_cfg.fscan
        self.sync()

        fbragg.offset = bragg.offset
        fscan.pars.motor = fbragg

        self.enable_scan_metadata()

        with cleanup(self.cleanup): 
            bragg_0 = energy2bragg(e0, d_spacing)
            bragg_1 = energy2bragg(e1, d_spacing)
            npoints = abs(e1 - e0) / step_ev

            fscan.pars.start_pos = bragg_0
            fscan.pars.step_size = (bragg_1 - bragg_0) / npoints
            fscan.pars.latency_time = 0.001

            fscan.pars.acc_margin = 0.001 # / bragg.steps_per_unit
            
            fscan.pars.npoints = npoints
            fscan.pars.step_time = 0#step_s + 0.001
            fscan.pars.acq_time = step_s
            fscan.pars.gate_mode = 'TIME'
            fscan.pars.scan_mode = 'TIME'

            if not scan_info:
                scan_info = {'instrument':
                            {self.name:
                            {"type": "fscan",
                                "e0": e0,
                                "e1": e1,
                                "step_ev": step_ev,
                                "step_s": step_s}
                            }
                    }

            fscan.prepare()
            # if dataset_name is not None:
            #     current_session.scan_saving.newdataset(dataset_name)
            if scan_info:
                fscan.run(scan_info)
            else:
                fscan.run()
        return fscan.scan
        
        # self.disable_scan_metadata()
    
    def fscan2d(self, slow_axis, slow_e0, slow_e1, slow_ivals,
                fast_e0, fast_e1, fast_ivals, count_time):
        slow_step_ev = (slow_e1 - slow_e0) / slow_ivals
        fast_step_ev = (fast_e1 - fast_e0) / fast_ivals
        return self._fscan2d(slow_axis, slow_e0, slow_e1, slow_step_ev,
                             fast_e0, fast_e1, fast_step_ev, count_time)
        
    def _fscan2d(self, slow_axis,
                slow_start, slow_end,
                slow_step_ev, e0, e1,
                step_ev, step_s,
                scan_info=None):
        # opiom = self._opiom
        bragg = self._bragg
        fbragg = self._fbragg
        fscan_cfg = self._fscan_cfg
        d_spacing = self._crystal.controller.d_spacing()

        fscan = fscan_cfg.fscan2d
        self.sync()

        fbragg.offset = bragg.offset
        fscan.pars.fast_motor = fbragg

        self.enable_scan_metadata()

        with cleanup(self.cleanup): 
            bragg_0 = energy2bragg(e0, d_spacing)
            bragg_1 = energy2bragg(e1, d_spacing)
            npoints = abs(e1 - e0) / step_ev
            
            slow_npoints = abs(slow_end - slow_start) / slow_step_ev
            
            fscan.pars.slow_motor = slow_axis
            fscan.pars.slow_start_pos = slow_start
            fscan.pars.slow_npoints = slow_npoints
            fscan.pars.slow_step_size = slow_step_ev

            fscan.pars.fast_start_pos = bragg_0
            fscan.pars.fast_step_size = (bragg_1 - bragg_0) / npoints
            fscan.pars.latency_time = 0.001

            fscan.pars.fast_acc_margin = 0.001 # / bragg.steps_per_unit
            
            fscan.pars.fast_npoints = npoints
            fscan.pars.fast_step_time = 0#step_s + 0.001
            fscan.pars.acq_time = step_s
            fscan.pars.gate_mode = 'TIME'
            fscan.pars.scan_mode = 'TIME'

            fscan.prepare()
            
            if not scan_info:
                scan_info = {'instrument':
								{self.name:
								{"type": "fscan",
								 #"name": dataset_name,
								 "slow_start":slow_start,
								 "slow_end":slow_end,
								 "slow_step_ev":slow_step_ev,
								 "e0": e0,
								 "e1": e1,
                                 "step_ev": step_ev,
								 "step_s": step_s}
								}
							 }
            
            if scan_info:
                fscan.run(scan_info)
            else:
                fscan.run()

        return fscan.scan

    # def scan_metadata(self):
    #     return {'instrument':
    #                 {self.name:
    #                 {"type": "constant_vel",
    #                  "name": name,
    #                  "element": element,
    #                  "edge": edge,
    #                  "e0": e0,
    #                  "e1": e1,
    #                  "step_ev": step_ev,
    #                  "step_s": step_s}
    #                 }
    #                 }
    
    # def exafs_constant_vel(self, name, element, edge, step_ev=0.2, step_s=0.05, run=False):
    #     # opiom = self._opiom
    #     bragg = self._bragg
    #     fbragg = self._fbragg
    #     fscan_cfg = self._fscan_cfg
    #     d_spacing = self._crystal.controller.d_spacing()

    #     params = ExafsParameters(element, edge)
    #     egy = params.scan_pos
    #     e0 = egy[0]
    #     e1 = egy[-1]

    #     fscan = fscan_cfg.fscan
    #     self.sync()
    #     # self._musst.RESET

    #     fbragg.offset = bragg.offset
    #     fscan.pars.motor = fbragg

    #     with cleanup(self.cleanup): 
    #         bragg_0 = energy2bragg(e0, d_spacing)
    #         bragg_1 = energy2bragg(e1, d_spacing)
    #         npoints = abs(e1 - e0) / step_ev

    #         fscan.pars.start_pos = bragg_0
    #         fscan.pars.step_size = (bragg_1 - bragg_0) / npoints
    #         fscan.pars.latency_time = 0.001

    #         fscan.pars.acc_margin = 0.001 # / bragg.steps_per_unit
            
    #         fscan.pars.npoints = npoints
    #         fscan.pars.step_time = 0#step_s + 0.001
    #         fscan.pars.acq_time = step_s
    #         fscan.pars.gate_mode = 'TIME'
    #         fscan.pars.scan_mode = 'TIME'

    #         # opiom.switch('SEL_TRIG', 'ZAP')
    #         fscan.prepare()
    #         if run:
    #             current_session.scan_saving.newsample(name)
    #             return fscan.run(
    #                 {'instrument':
    #                 {self.name:
    #                 {"type": "constant_vel",
    #                  "name": name,
    #                  "element": element,
    #                  "edge": edge,
    #                  "e0": e0,
    #                  "e1": e1,
    #                  "step_ev": step_ev,
    #                  "step_s": step_s}
    #                 }
    #                 })

    # def exafs_variable_vel(self, name, element, edge, step_s=0.1, run=False):
    #     bragg = self._bragg
    #     fbragg = self._fbragg
    #     fscan_cfg = self._fscan_cfg
    #     d_spacing = self._crystal.controller.d_spacing()

    #     params = ExafsParameters(element, edge)
    #     egy = params.scan_pos
    #     e0 = egy[0]
    #     e1 = egy[-1]

    #     fscan = fscan_cfg.fscan
    #     self.sync()
    #     # self._musst.RESET

    #     fbragg.offset = bragg.offset
    #     fscan.pars.motor = fbragg

    #     egy, tm = params._calc_cont()
    #     e0 = egy[0]
    #     e1 = egy[-1]
    #     print(e0, e1)
    #     # e0 = params.scan_pos[0]
    #     # e1 = params.scan_pos[-1]

    #     with cleanup(self.cleanup):
    #         bragg_0 = energy2bragg(e0, d_spacing)
    #         bragg_1 = energy2bragg(e1, d_spacing)
    #         # n_points = abs(e1 - e0) / step_ev
    #         n_points = np.sum(tm) / step_s
    #         # npoints = params.scan_pos.shape[0]#abs(e1 - e0) / step_ev

    #         # print("FAST", e0, e1, bragg_0, bragg_1)

    #         fscan.pars.start_pos = bragg_0
    #         fscan.pars.step_size = (bragg_1 - bragg_0) / n_points
    #         fscan.pars.latency_time = 0.001

    #         fscan.pars.acc_margin = 0.001 # / bragg.steps_per_unit
            
    #         fscan.pars.npoints = n_points
    #         fscan.pars.step_time = 0#step_s + 0.001
    #         fscan.pars.acq_time = step_s
    #         fscan.pars.gate_mode = 'TIME'
    #         fscan.pars.scan_mode = 'TIME'

    #         fbragg.controller.set_next_scan_params(None)
    #         fbragg.controller.set_next_scan_params(params)

    #         fscan.prepare()
    #         if run:
    #             current_session.scan_saving.newsample(name)
    #             return fscan.run(
    #                 {'instrument':
    #                 {self.name:
    #                 {"type": "variable_vel",
    #                  "name": name,
    #                  "element": element,
    #                  "edge": edge,
    #                  "e0": e0,
    #                  "e1": e1,
    #                  "step_ev": "nan",
    #                  "step_s": step_s}
    #                 }
    #                 })
    #         else:
    #             print(fscan)
