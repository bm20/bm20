import os
from bliss.config.static import get_config
from bliss.scanning.scan import ScanPreset, ScanState

from bm20.config.names import bm20names
#from .xes5_writer import xes5_to_ascii

# from blissdata.data.node import get_node
# from nexus_writer_service.utils.scan_utils import scan_uris, scan_info
# from blissoda.utils.directories import get_processed_dir

from bliss.controllers.motors.turbopmac import (pmac_raw_write_read,
                                                PMAC_MOTOR_STATUS,
                                                pmac_axis_is_open_loop,
                                                pmac_axis_close_loop,
                                                pmac_axis_kill)

from ewokscore import Task
# from ewoks import execute_graph

# from ewoksjob.client import submit
from bliss.scanning.scan import ScanState


class XesWriter(Task,
                input_names=["db_name"],
                output_names=["outfiles"]):
    def run(self):
        db_name = self.inputs.db_name
        node = get_node(db_name)
        info = scan_info(node)
        state = info["state"]
        while state not in (ScanState.DONE, ScanState.USER_ABORTED):
            info = scan_info(node)
            state = info["state"]
        print(state)
        self.outputs.outfiles = [f"__{state}__"]


class TestPreset(ScanPreset):
    def __init__(self):
        super().__init__()

    def stop(self, scan):
        print(scan.node.db_name)
        # c = submit(args=(workflow,), kwargs={"inputs":inputs})


nodes = [
    {
        "id": "task1",
        "task_type": "class",
        "task_identifier": "bm20.scans.xes5.presets.XesWriter",
    },
]

links = [
    # {
    #     "source": "task1",
    #     "target": "task2",
    #     "data_mapping": [{"source_output": "result", "target_input": "a"}],
    # },
    # {
    #     "source": "task2",
    #     "target": "task3",
    #     "data_mapping": [{"source_output": "result", "target_input": "a"}],
    # },
]
workflow = {"graph": {"id": "testworkflow"}, "nodes": nodes, "links": links}

# Define task inputs'
# inputs = [{"name":"scan_id", "value": 128}]


class FastXes5Preset(ScanPreset):
    def __init__(self):
        super().__init__()
        self._opiom = bm20names.OPIOM_EH1
        self._mono = bm20names.MONO
        self._xl2perp = bm20names.XTAL2_PERP

    def prepare(self, scan):
        self._opiom.switch('SEL_TRIG', 'ZAP')
        print(self._opiom.__info__())

    def start(self, scan):
        print(f"Starting scan {scan.name}")
        fbragg = bm20names.FBRAGG
        fbragg.sync_hard()
        # fbragg.controller._upload_exafs_params()
#         print(f"Opening the shutter")
        # print("STARTED")

    def stop(self, scan):
        self._opiom.switch('SEL_TRIG', 'STEP')
        fbragg = bm20names.FBRAGG
        fbragg.controller.clear_scans_params()
        scan_saving = scan.scan_saving
        raw_dir = os.path.join(scan_saving.base_path,
                               scan_saving.proposal_name,
                               scan_saving.beamline,
                               scan_saving.date,
                               "PROCESSED_DATA")
        f_basename = (f"{scan_saving.collection_name}_"
                      f"{scan_saving.dataset_name}_"
                      f"{scan.scan_number}")
        out_f_base = os.path.join(raw_dir,
                                  f_basename)
        # # making sure the xtal2perp motor is in open loop
        # # because it is in vacuum but not cooled.
        # # TODO: add others?
        if not pmac_axis_is_open_loop(self._xl2perp):
            pmac_axis_kill(self._xl2perp)
            print(f"xtal2perp in open loop: {pmac_axis_is_open_loop(self._xl2perp)}")

        # if scan.state in (ScanState.DONE, ScanState.STOPPING):
        #     print(f"Scan {scan.name} terminated, state = {scan.state.name}.")
        #     print(f"Writing scan data as ASCII file.")
        #     print(f"File basename: {out_f_base}.")
        #     xes5_to_ascii(scan, out_file_basename=out_f_base)
        # else:
        #     print(f"WARNING! Scan {scan.name} final state is {scan.state.name}.")
        #     print(f"Not converting data to ascii. "
        #           "Data is still available in the HDF5 file.")


        
        # saving = scan.scan_saving
        # snun = scan.scan_number
        # data = scan.get_data()

#         dat_dir = os.path.join(saving.base_path, saving.proposal_dirname, saving.beamline, saving.proposal_session_name)
#         dat_file = f"{saving.collection_name}_{saving.dataset_name}_{snun}.dat"
#         data_file = os.path.join(dat_dir, dat_file)

#         columns = ['musstoh:fbragg_energy', "musstoh:fbragg_egy_delta",
#                    'p201_0:ct2_counters_controller:i0', 'xesm4:roi_det0',
#                     'xesm4:realtime_det0',
#                     'xesm4:trigger_livetime_det0', 'xesm4:energy_livetime_det0', 
#                     'xesm4:triggers_det0', 'xesm4:events_det0', 
#                     'xesm4:icr_det0', 'xesm4:ocr_det0', 'xesm4:deadtime_det0']
#         # print(data.keys())
#         # print(scan.scan_info)
#         print('====', (len(data[columns[0]]), len(columns)), data['musstoh:fbragg_energy'].shape)
#         data_ar = np.ndarray((len(data[columns[0]]), len(columns)))
#         for i, col in enumerate(columns):
#             print(i, col, data[col].shape)
#             data_ar[:, i] = data[col][:]
#         with open(data_file, "w") as ofile:
#             ofile.write(tabulate(data_ar, headers=columns, tablefmt="plain"))
#         print(f"File writen: {data_file}")


# class FastXes5Preset(ScanPreset):
#     def __init__(self):
#         super().__init__()
#         # self._xes_params = name, e0, e1, step_ev, step_s
#         self._opiom = get_config().get(OPIOM_EH1)
#         self._mono = get_config().get(MONO)

#     def prepare(self, scan):
#         self._opiom.switch('SEL_TRIG', 'ZAP')

#     def start(self, scan):
#         print(f"Starting scan {scan.name}")
#         fbragg = get_config().get('fbragg')
#         fbragg.sync_hard()
# #         print(f"Opening the shutter")

#     def stop(self, scan):
#         self._opiom.switch('SEL_TRIG', 'STEP')
#         # making sure the xtal2perp motor is in open loop
#         # because it is in vacuum but not cooled.
#         # TODO: add others?
#         if not self._mono.pmac_comm.is_open_loop(4):
#             self._mono.pmac_comm.open_loop(4)
#             print(f"xtal2perp in open loop: {self._mono.pmac_comm.is_open_loop(4)}")

#         saving = scan.scan_saving
#         snun = scan.scan_number
#         data = scan.get_data()

#         dat_dir = os.path.join(saving.base_path, saving.proposal_dirname, saving.beamline, saving.proposal_session_name)
#         dat_file = f"{saving.collection_name}_{saving.dataset_name}_{snun}.dat"
#         data_file = os.path.join(dat_dir, dat_file)

#         columns = ['musstoh:fbragg_energy', "musstoh:fbragg_egy_delta",
#                    'p201_0:ct2_counters_controller:i0', 'xesm4:roi_det0',
#                     'xesm4:realtime_det0',
#                     'xesm4:trigger_livetime_det0', 'xesm4:energy_livetime_det0', 
#                     'xesm4:triggers_det0', 'xesm4:events_det0', 
#                     'xesm4:icr_det0', 'xesm4:ocr_det0', 'xesm4:deadtime_det0']
#         # print(data.keys())
#         # print(scan.scan_info)
#         print('====', (len(data[columns[0]]), len(columns)), data['musstoh:fbragg_energy'].shape)
#         data_ar = np.ndarray((len(data[columns[0]]), len(columns)))
#         for i, col in enumerate(columns):
#             print(i, col, data[col].shape)
#             data_ar[:, i] = data[col][:]
#         with open(data_file, "w") as ofile:
#             ofile.write(tabulate(data_ar, headers=columns, tablefmt="plain"))
#         print(f"File writen: {data_file}")

