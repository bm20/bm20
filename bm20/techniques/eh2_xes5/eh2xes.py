import gevent

from bliss import setup_globals, current_session
from bliss.common.scans import ascan, dscan, amesh, dmesh

from bm20.workflows.extract import convert_scan


class EH2Xes:
    name = property(lambda self: self._name)
    spectro = property(lambda self: self._components["spectro"])
    energy = property(lambda self: self._components["energy"])
    xes_en = property(lambda self: self.spectro.energy_axis)
    mca = property(lambda self: self._components["mca"])
    fscan = property(lambda self: self._components["fscan"])

    def __init__(self, name, config):
        self._name = name
        self._config = config
        self._components = config["components"]

    def __info__(self):
        s_sav = current_session.scan_saving
        spectro = self.spectro
        crystal = spectro.crystal
        xes_en = self.xes_en
        txt = "============\n"
        txt += f"{self.name}"
        txt = "-----------------------\n"
        txt += f"> SPECTRO: {spectro.name}.\n"
        txt += f"  - xes_en/motors aligned: {spectro.is_aligned()}\n"
        txt +=  "  - Crystal: \n"
        txt += f"    * Name:       {crystal.name}\n"
        txt += f"    * Radius:     {crystal.radius_m}\n"
        txt += f"    * Reflection: {crystal.reflection}\n"
        txt += f"    * Dspacing:    {crystal.crystal.d}\n"
        txt += f"  - Energy:       {xes_en.position}   (axis = {xes_en.name})\n"
        txt += "============\n"
        txt += "> Data Policy\n"
        txt += f"  - Proposal:   {s_sav.proposal_name}\n"
        txt += f"  - Collection: {s_sav.collection_name}\n"
        txt += f"  - Dataset:    {s_sav.dataset_name}\n"
        txt += f"  - Directory:  {s_sav.get_path()}\n"
        txt += "============\n"
        return txt
    
    def _scan_info(self, **kwargs):
        return {"instrument":{"bm20_meta": kwargs}}
    
    def post_scan(self, scan):
        print(f"BLISS Data written to {scan.scan_info['filename']}")
        print("Converting to ascii.")
        gevent.sleep(4)
        try:
            convert_scan(scan)
        except Exception as ex:
            print(f"Failed to convert hdf5 to ascii. Error was: {ex}.")

    def xes(self, e_start, e_stop, intervals, count_time, **kwargs):
        scan_info = {"scan": "EH2Xes:xes",
                     "params": {"e_start": e_start, 
                                "e_stop": e_stop,
                                "intervals": intervals,
                                "count_time": count_time}}
        scan = ascan(self.energy, e_start, e_stop, intervals, count_time,
                     scan_info=self._scan_info(**scan_info))
        self.post_scan(scan)
        return scan

    def dxes(self, e_start, e_stop, intervals, count_time, **kwargs):
        scan_info = {"scan": "EH2Xes:dxes",
                     "params": {"e_start": e_start, 
                                "e_stop": e_stop,
                                "intervals": intervals,
                                "count_time": count_time}}
        scan = dscan(self.energy, e_start, e_stop, intervals, count_time,
                     scan_info=self._scan_info(**scan_info), **kwargs)
        self.post_scan(scan)
        return scan
    
    def rixs(self, e_start, e_stop, e_intervals, xes_start, xes_stop,
             xes_intervals, count_time, **kwargs):
        scan_info = {"scan": "EH2Xes:rixs",
                     "params": {"e_start": e_start, 
                                "e_stop": e_stop,
                                "e_intervals": xes_intervals,
                                "xes_start": xes_start, 
                                "xes_stop": xes_stop,
                                "xes_intervals": xes_intervals,
                                "count_time": count_time}}
        scan = amesh(self.energy, e_start, e_stop, e_intervals,
                     self.xes_en, xes_start, xes_stop, xes_intervals, count_time,
                     scan_info=self._scan_info(**scan_info), **kwargs)
        self.post_scan(scan)
        return scan
        
    def drixs(self, e_start, e_stop, e_intervals, xes_start, xes_stop,
              xes_intervals, count_time, **kwargs):
        scan_info = {"scan": "EH2Xes:drixs",
                     "params": {"e_start": e_start, 
                                "e_stop": e_stop,
                                "e_intervals": xes_intervals,
                                "xes_start": xes_start, 
                                "xes_stop": xes_stop,
                                "xes_intervals": xes_intervals,
                                "count_time": count_time}}
        scan = dmesh(self.energy, e_start, e_stop, e_intervals,
                     self.xes_en, xes_start, xes_stop, xes_intervals, count_time,
                     scan_info=self._scan_info(**scan_info), **kwargs)
        self.post_scan(scan)
        return scan
    
    def fxes(self, e_start, e_stop, intervals, count_time, **kwargs):
        fscan = self.fscan
        scan_info = {"scan": "EH2Xes:fxes",
                     "params": {"e_start": e_start, 
                                "e_stop": e_stop,
                                "intervals": intervals,
                                "count_time": count_time}}
        scan = fscan.fscan(e_start, e_stop, intervals, count_time,
                           scan_info=self._scan_info(**scan_info), **kwargs)
        self.post_scan(scan)
        return scan

    def frixs(self, e_start, e_stop, e_intervals, xes_start, xes_stop,
              xes_intervals, count_time, **kwargs):
        fscan = self.fscan
        scan_info = {"scan": "EH2Xes:frixs",
                     "params": {"e_start": e_start, 
                                "e_stop": e_stop,
                                "e_intervals": e_intervals,
                                "xes_start": xes_start, 
                                "xes_stop": xes_stop,
                                "xes_intervals": xes_intervals,
                                "count_time": count_time}}
        scan = fscan.fscan2d(self.xes_en, xes_start, xes_stop, xes_intervals,
                             e_start, e_stop, e_intervals, count_time,
                             scan_info=self._scan_info(**scan_info), **kwargs)
        self.post_scan(scan)
        return scan