from bm20.macros.optics_tools import rocking_curve
from bm20.config.names import bm20names


def rc2_mrh():
    rocking_curve(bm20names.XTAL2_PICH, -0.001, 0.001, 30, bm20names.I0_XES, 0.1)