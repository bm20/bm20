from math import cos, sin, radians
from bliss.config.settings import SimpleSetting
from bliss.controllers.motor import CalcController, Controller


def _to_sxx(sx, sy, stheta, rot_ofst):
    return sx * cos(radians(stheta + rot_ofst)) - sy * sin(radians(stheta + rot_ofst))

def _to_syy(sx, sy, stheta, rot_ofst):
    return sx * sin(radians(stheta + rot_ofst)) + sy * cos(radians(stheta + rot_ofst))


def _to_sx(sxx, syy, stheta, rot_ofst):
    return sxx * cos(radians(360 - (stheta + rot_ofst))) - syy * sin(radians(360-(stheta + rot_ofst)))

def _to_sy(sxx, syy, stheta, rot_ofst):
    return sxx * sin(radians(360 - (stheta + rot_ofst))) + syy * cos(radians(360-(stheta + rot_ofst)))


class Sxxsyy(CalcController):
    """
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        default_rot = float(self.config.get('rot_offset_default'))
        if default_rot is None:
            default_rot = 45.
        self._rot_offset = SimpleSetting(f'{self.name}:rot_offset',
                                         default_value=default_rot,
                                         read_type_conversion=float)

    def calc_from_real(self, real_pos):
        rot_offset = self.rot_offset
        args = (real_pos['sx'],
                real_pos['sy'],
                real_pos['stheta'],
                rot_offset)
        sxx = _to_sxx(*args)
        syy = _to_syy(*args)
        return {'sxx': sxx,
                'syy': syy}

    def calc_to_real(self, calc_pos):
        rot_offset = self.rot_offset
        args = (calc_pos['sxx'],
                calc_pos['syy'],
                self._tagged['stheta'][0].position,
                rot_offset)
        sx = _to_sx(*args)
        sy = _to_sy(*args)
        return {'sx': sx,
                'sy': sy}
    
    @property
    def rot_offset(self):
        return self._rot_offset.get()

    @rot_offset.setter
    def rot_offset(self, rot_offset):
        if rot_offset is None:
            self._rot_offset.clear()
        else:
            self._rot_offset.set(rot_offset)
        self._tagged['sxx'][0].update_position()