import datetime

import numpy as np

from bliss.common.scans import timescan
from bliss.config.static import get_config

from bliss.shell.standard import move, flint
from bliss.scanning.scan_saving import ScanSaving
from bliss.common.session import get_current_session


def cryomv(setpoint, sleep_time_s=3):
    """
    Sets the cryostat septoint to "setpoint" then
    starts a timescan to monitor the coldhead and the sample
    temperatures.
    """
    flint()
    config = get_config()
    cryostat = config.get('cryostat')
    samplehead = config.get('samplehead')

    strtime = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')

    cur_temp = np.around(cryostat.input.read(), 1)

    # timescan
    session = get_current_session()
    scan_saving = ScanSaving(session.name)
    cur_filename = scan_saving.data_filename
    try:
        filename = f'cryo_{strtime}_{cur_temp}_{setpoint}'
        scan_saving.data_filename = filename
        print(f'Currently at {cur_temp}, moving to {setpoint}K')
        print(f'Writing scan to {scan_saving.base_path}/{filename}')
        move(cryostat.axis, setpoint, wait=False)
        timescan(0.1,
                 cryostat.counters.cryo_coldh,
                 cryostat.counters.cryo_out,
                 samplehead.counters.cryo_samp,
                 name=filename,
                 sleep_time=sleep_time_s)
    except Exception as ex:
        print(ex)
    scan_saving.data_filename = cur_filename