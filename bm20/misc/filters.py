from bliss.config.static import get_config


def set_filter(*filters):
	filtA = get_config().get("filtA")
	mask = sum([2**(f-1) for f in filters])
	print(f"Setting mask {mask}.")
	filtA.filter = mask
	print(f"Tansmission is now at {filtA.transmission}")
