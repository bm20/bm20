# some macros to manage the attenuators
#tabulate

__all__ = ['show_attenuators',
           'bm20_attenuators_tube_show',
           "bm20_attenuators_tube_hide"]


from bliss.common.logtools import *
from bliss.config.static import get_config
from ..config.names import bm20names
from ..macros.motors import names_to_axes


def _bm20_attenuator_has_tube_position(atten):
    """
    Number of tube labels present in the position list.
    Sometimes there can be more than one if something went wrong.
    """
    tube_labels = [True for position in atten.positions_list
                   if position['label'] == 'Tube']
    return len(tube_labels)


def _bm20_attenuator_remove_tube_position(atten, silent=False):
    n_labels = _bm20_attenuator_has_tube_position(atten)
    # sometimes for some reason you get more than one position...
    for _ in range(n_labels):
        atten.remove_position('Tube')
    if not silent:
        print(f'Removed Tube position for attenuator {att.name}.')


def _bm20_attenuator_add_tube_position(atten):
    # sometimes for some reason you get more than one position...
    n_labels = _bm20_attenuator_has_tube_position(atten)
    if n_labels > 0:
        # cleaning, just in case
        _bm20_attenuator_remove_tube_position(atten, silent=True)
    
    # just in case the axis are renamed some day
    axis = list(atten.motors.values())[0]
    atten.create_position('Tube',
                           [(axis, 1.5, 0.05)],
                           description='Cu Tube')
    print(f'Added Tube position for attenuator {atten.name}.')


def bm20_attenuators_tube_show():
    """
    Shows the "Cu tube" position for the attenuators
    atten1 and atten2.
    Tubes are at position 1.5.
    """
    _bm20_attenuator_add_tube_position(bm20names.ATTEN1)
    _bm20_attenuator_add_tube_position(bm20names.ATTEN2)


def bm20_attenuators_tube_hide():
    """
    Hides the "Cu tube" position for the attenuators
    atten1 and atten2.
    """
    _bm20_attenuator_remove_tube_position(bm20names.ATTEN1)
    _bm20_attenuator_remove_tube_position(bm20names.ATTEN2)


def _bm20_attenuators_show():
    class _foo():
        def __info__(self):
            attens = (bm20names.ATTEN1, bm20names.ATTEN2)
            info = '\n'.join([f'============ {att.name} ============\n'
                              f' {att.__info__()}'
                              for att in attens])
            return info

    return _foo()


show_attenuators = _bm20_attenuators_show()
