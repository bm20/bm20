from bliss.controllers.motors.turbopmac import (pmac_raw_write_read,
                                                PMAC_MOTOR_STATUS,
                                                pmac_axis_is_open_loop,
                                                pmac_axis_close_loop,
                                                pmac_axis_kill)
from bliss.common.hook import MotionHook
import gevent


class PmacKillHook(MotionHook):
    """
    Hook that kills its axis after move.
    Used to protect in vacuum axis (xl2perp)
    because sometimes the embedded PLC doesnt trigger
    the kill.
    """
    def __init__(self, name, config):
        super().__init__()

    # def post_move(self, *args, **kwargs):
    #     print("post_move", args, kwargs)

    def post_scan(self, *args, **kwargs):
        for name, axis in self.axes.items():
            pmac_axis_kill(axis)
            print(f"Scan done, making sure {name} is killed (result={pmac_axis_is_open_loop(axis)})")


class PmacMoveHook(MotionHook):
    """
    This is a quick and dirty hack until we find a better solution.
    Hook that waits until the axis is READY.
    Because sometimes
    """
    def __init__(self, name, config):
        super().__init__()
        self._timeout = config.get("timeout", 3.)

    def pre_move(self, motion_list):
        # print("Pre Move Hook: checking state.")
        axes = list(self.axes.values())
        ready = [False] * len(axes)
        try:
            with gevent.Timeout(self._timeout):
                while not all(ready):
                    for i_axis, axis in enumerate(axes):
                        if ready[i_axis]:
                            continue
                        state = axis.state
                        if state.MOVING:
                            if not axis.hw_state.MOVING:
                                print(f"Resetting axis {axis.name} state cache.")
                                axis.sync_hard()
                            continue
                        ready[i_axis] = True
                    gevent.sleep(0.1)
        except gevent.Timeout:
            print("Hook: timeout while waiting for MONO to stabilize...")



class EnergyMoveHook(MotionHook):
    """
    This is a quick and dirty hack until we find a better solution.
    Hook that waits until the axis is READY.
    Because sometimes
    """
    def __init__(self, name, config):
        super().__init__()
        self._timeout = config.get("timeout", 3.)
        self._bragg = config.get("bragg")

    def pre_move(self, motion_list):
        # print("Pre Move Hook: checking state.")
        bragg = self._bragg
        try:
            with gevent.Timeout(self._timeout):
                while True:
                    state = bragg.state
                    if state.MOVING:
                        if not bragg.hw_state.MOVING:
                            print(f"Resetting axis {bragg.name} state cache.")
                            bragg.sync_hard()
                        gevent.sleep(0.1)
                    else:
                        break
        except gevent.Timeout:
            print("Hook: timeout while waiting for MONO to stabilize...")