"""
BM20 DCM/DMM crystal selection and information
"""

from typing import NamedTuple
from bliss.common import event
from bliss.config.channels import Channel


class CrystalDef(NamedTuple):
    name: str
    index: int
    mode: str         # DCM or DMM
    register: str     # PMAC P register with the lateral axis position
    d_spacing: float
    label: str
    chan_out: str     # wago (write to pmac)
    chan_in: str      # wago (read from pmac)
    description: str


NoCrystal = CrystalDef("NOT SET", -1, 'Unknown', '', float('nan'), 'Unknown', '', '', 'Unknown')


class BM20Crystals:
    def __init__(self, config):
        crystals = {crystal['index']:CrystalDef(**crystal)
                    for crystal in config.get('crystals')}
        crystals[-1] = NoCrystal
        self._crystals = crystals

    # def index(self, index):
    #     return self._crystals[index]
    
    def from_index(self, index):
        return self._crystals[index]
    
    def d_spacing(self, index):
        return self.from_index(index).d_spacing
    
    def label(self, index):
        return self.from_index(index).label
    
    def register(self, index):
        return self.from_index(index).register
    
    def out_chan(self, index):
        return self.from_index(index).chan_out

    def in_chan(self, index):
        return self.from_index(index).chan_in
    
    def valid_indices(self):
        return [xtal.index for xtal in self._crystals.values() if xtal.index >= 0]
    
    def default(self):
        return self.config["default"]

import re
import enum
import socket
import struct
from time import monotonic
from collections import OrderedDict

import numpy as np
import gevent

from bliss import global_map
from bliss.config import settings
from bliss.config.static import get_config
from bliss.common.axis import Axis
from bliss.common.logtools import log_debug, log_error, log_info
from bliss.comm.util import get_comm
from bliss.common.axis import AxisState
from bliss.controllers.motor import Controller
from bliss.controllers.motors.turbopmac import PMAC_MOTOR_STATUS
from bliss.common.hook import MotionHook



from bm20.oh.mono.energy import bragg2energy, energy2bragg



class D20Crystal(Controller):
    crystals = property(lambda self: self._crystals)
    crystal = property

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.axis_settings.config_setting["velocity"] = False
        self.axis_settings.config_setting["acceleration"] = False
        self.axis_settings.config_setting["steps_per_unit"] = False

        self._monolat_axis = self.config.get("monolat")
        self._mono_wago = self.config.get("wago")
        self._crystals = self.config.get("crystals")

        self._was_moving = None

        global_map.register(
            self, parents_list=["controllers"],
            children_list=[self._mono_wago, self._monolat_axis]
        )

        self._channel = Channel('d_spacing')
        self.sync_dspacing()

    @property
    def crystal(self):
        return self.crystals.from_index(self._wago.read_crystal_index())
    
    def crystal_metadata(self):
        crystal = self.crystal._asdict()
        return {k: crystal[k] for k in ["name", "index", "mode", "d_spacing", "label", "description"]}
    
    def d_spacing_channel(self):
        return self._channel

    def d_spacing(self):
        index = self._wago.read_crystal_index()
        return self._crystals.d_spacing(index)

    def sync_dspacing(self, **kwargs):
        self._channel.value = self.d_spacing()

    @property
    def _monolat(self):
        if self._monolat_axis is None:
            self._monolat_axis = self.config.get("monolat")
        return self._monolat_axis
    
    @property
    def _wago(self):
        if self._mono_wago is None:
            self._mono_wago = self.config.get("wago")
        return self._mono_wago

    def initialize_axis(self, axis):
        event.connect(axis, "sync_hard", self.sync_dspacing)

    def close(self):
        for axis in self.axes:
            event.disconnect(axis, "sync_hard", self.sync_dspacing)
        super().close()
        
    def set_velocity(self, axis, new_velocity):
        pass

    def read_velocity(self, axis):
        return 1

    def read_acceleration(self, axis):
        return 1
        
    def read_position(self, axis):
        if axis.state.MOVING:
            return self._monolat._hw_position
        return self._wago.read_crystal_index()

    def stop(self, axis):
        self._wago.abort()
        self._monolat.sync_hard()
        
    def stop_all(self, *motions):
        self._wago.abort()
        self._monolat.sync_hard()

    def state(self, axis):
        state = self._monolat.hw_state
        if state.MOVING:
            self._was_moving = True
            return AxisState("MOVING")
        if self._was_moving != False:
            self._was_moving = False
            # giving it time to update the wago channel
            gevent.sleep(1)
            self.sync_dspacing()
        return AxisState("READY")

    def start_one(self, motion):
        log_debug(self, "start_one on {motion.axis.name}")
        if not motion.type == "move":
            raise ValueError(f"Unsupported motion type: {motion.type}.")

        target = motion.target_pos
        if target is None:
            raise RuntimeError(f"Axis {motion.axis.name}: relative move not "
                            f"supported. Absolute move only.")
        indices = self._crystals.valid_indices()
        if target not in indices:
            raise RuntimeError(f"Invalid crystal position: {target}."
                               f" Allowed values = {indices}")

        crystal = self._crystals.from_index(target)
        pmac = self._monolat.controller
        lat_pos = int(pmac.raw_write_read(crystal.register))
        print(f"Moving to crystal {crystal.label}.")
        print(f"Showing lateral axis position, target is {lat_pos}.")
        self._wago.write_crystal_index(crystal.index)
        # giving time to the PMAC to start
        gevent.sleep(1)
        # log_info(self, f"Moving {self._lateral.name} to {crystal.label}.")

        # ans = prompt_user_yes_no(f"Move crystal from [{current.description}] "
        #                          f"to [{crystal.description}]?")
        # ans = 'y'
        # ans = ans == 'y'
        # if ans:
        #     # current = self._wago.read_crystal_index()
        #     crystal = self._crystals.from_index(target)
        #     pmac = self._lateral.controller
        #     p0 = self._lateral._hw_position# * self._lateral.steps_per_unit
        #     p1 = int(pmac.pmac_comm.sendline_getbuffer(crystal.register))
        #     self._wago.write_dcm_crystal(crystal)
        #     user_print('======')
        #     user_print(f'Axis {self._lateral.name} will move'
        #                f' to {p1/self._lateral.steps_per_unit}.')
        #     user_print('======')
        #     t_start = monotonic()
        #     try:
        #         while 'MOVING' in self._lateral.hw_state or monotonic() - t_start< 5:
        #             latpos = self._lateral._hw_position
        #             print(f"\r{self._lateral.name}: {latpos:>12.4} (remaining: {abs(latpos-p1):>12.4})", end="", flush=True)
        #             gevent.sleep(0.2)
        #         else:
        #             # check crystal position
        #             new = self._wago.read_dcm_crystal()
        #             user_info(f'\nDone. Crystal set to {new.description}')
        #     except:
        #         self.abort()
        #         user_info("\nABORTED.")
        #         current = self._wago.read_dcm_crystal()
        #         user_info(f"Current crystal: [{current.description}].")

        #     self.sync_dspace()
#     def d_spacing(self):
#         return self.crystal().d_spacing

#     def sync_dspace(self, **kwargs):
#         self._channel.value = self.d_spacing()
#         # verbose = kwargs.get('verbose', True)
#         # crystal = self.crystal()
#         # energy = self.energy_axis()
#         # dspace = energy.settings.get('dspace')
#         # print(f'Crystal selected: {crystal.label}, dspace={crystal.d_spacing}.')
#         # if crystal.d_spacing != dspace:
#         #     if verbose:
#         #         print('WARNING: dspace mismatch.')
#         #         print(f'Current dspace setting: {dspace}.')
#         #         print(f'Setting the current dspace to {crystal.d_spacing}.')
#         #     energy.settings.set('dspace', crystal.d_spacing)
#         # dspace = energy.settings.get('dspace')
#         # user_info(f'{energy.name} axis d_space is set to {dspace}')




# class EnergyHook(MotionHook):
#     def __init__(self, name, config):
#         super(EnergyHook, self).__init__()
#         self._xtal = config.get('crystal')
#         self._energy_axis = None

#     @property
#     def _energy(self):
#         if self._energy_axis is None:
#             self._energy_axis = get_config().get(OpticsAliases.ENERGY.value)
#         return self._energy_axis

#     def _check_dspace(self):
#         crystal = self._xtal.crystal()
#         d_space = self._energy.settings.get('dspace')
#         if crystal.index == -1:
#             user_error('The crystal is in an uknown position.')
#             user_error(f'Please do mv({self._xtal._axis.name}, <index>). (index in [1..5])')
#             user_error(f'Type "{self._xtal.name}" for the crystals description:\n'
#                        f'{self._xtal.__info__()}')
#             raise RuntimeError('Unknown crystal selected.')
#         if d_space != crystal.d_spacing:
#             msg = (f'The energy axis d_space ({d_space}) doesn\'t match'
#                    f'the selected crystal {crystal.label} '
#                    f'(with d_space={crystal.d_spacing}).')
#             log_critical(self, msg)
#             print('=============================')
#             print('=============================')
#             print()
#             if getval_yes_no(f"Do you want to set dspace to {crystal.d_spacing}?"):
#                 self._xtal.sync_dspace()
#             else:
#                 print("== User aborted, dspacing not updated.")
#                 print(f'== Call {OpticsAliases.CRYSTAL.value}.sync_dspace() to do it manually.')
#             print('=============================')
#             print('=============================')
#             raise RuntimeError(msg)

#     def pre_scan(self, axes_list):
#         # print('EPRESCAN')
#         self._check_dspace()

#     def pre_move(self, motion_list):
#         # print('EPREMOVE')
#         self._check_dspace()
        
#         # self._energy_axis.controller._is
        
#     def post_scan(self, axes_list):
#         print('EPOSTSCAN')
#     #     self._on_move_axis(axes_list)
#     #     # self._xl2perp.sync_hard()
#     #     # self._check_dspace()

#     def post_move(self, motion_list):
#         pass
#         # print('EPOSTMOVE')
#         # config = get_config()
#         # vbragg = config.get('vbragg')
#         # vbragg.sync_hard()
#     #     self._on_move_motion(motion_list)
#         # self._xl2perp.sync_hard()
        
#         # self._check_dspace()
