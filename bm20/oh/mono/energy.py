"""
Energy calc motor for BM20
"""

import numpy as np
from bliss.common.hook import MotionHook
from bliss.controllers.motor import CalcController
from bliss.shell.getval import getval_yes_no
from bliss.common.logtools import log_critical, user_error


HC_OVER_E = 12.39842
g_mo_s = 18.


def bragg_to_perp(angle, xtal_ofst=g_mo_s):
    return xtal_ofst / (2 * np.cos(np.radians(angle)))


def bragg2energy(bragg, dspace):
    wl = 2 * dspace * np.sin(np.radians(bragg))
    energy = 1000. * HC_OVER_E / wl
    return energy


def energy2bragg(energy, dspace):
    bragg = np.degrees(np.arcsin(HC_OVER_E * 1000. / (energy * 2 * dspace)))
    return bragg


class EnergyCalc(CalcController):
    """ Calculation controller for energy and wavelength.
    """

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
        self.no_offset = self.config.get("no_offset", bool, True)
        self._dspacing = np.nan
        crystal = self.config.get("crystal")
        self._channel = crystal.controller.d_spacing_channel()
        self._dspacing = self._channel.value
        self._channel.register_callback(self._updated_dspace)

    def initialize_axis(self, axis):
        """ Initialize the axis and create dspace settings.
        Args:
            axis (Axis): Motor axis.
        """
        CalcController.initialize_axis(self, axis)
        axis.no_offset = self.no_offset

    def _updated_dspace(self, d_spacing):
        self._dspacing = d_spacing
        self._calc_from_real()

    def calc_from_real(self, real_positions):
        bragg = real_positions['monoang']
        energy_axis = self._tagged["energy"][0]
        wl = 2 * self._dspacing * np.sin(np.radians(bragg))
        energy = HC_OVER_E / wl
        try:
            if energy_axis.unit == "eV":
                energy *= 1000
        except AttributeError:
            pass
        return {'energy':energy}

    def calc_to_real(self, positions_dict):
        energy = positions_dict['energy']
        energy_axis = self._tagged["energy"][0]
        try:
            if energy_axis.unit == "eV":
                energy /= 1000.0
        except AttributeError:
            pass
        bragg = np.degrees(np.arcsin(HC_OVER_E / (energy * 2 * self._dspacing)))
        return {'monoang':bragg}


class EnergyFixedExitCalc(EnergyCalc):
    """ Calculation controller for energy and wavelength.
    """
    def calc_to_real(self, positions_dict):
        real_positions = super().calc_to_real(positions_dict)
        dcm2perp = bragg_to_perp(real_positions['monoang'])
        real_positions['dcm2perp'] = dcm2perp
        return real_positions


class EnergyHook(MotionHook):
    def __init__(self, name, config):
        super(EnergyHook, self).__init__()
        self._xtal = config.get('crystal')
        self._energy_axis = None
        self._is_scan = False

    @property
    def _energy(self):
        return self.axes['energy']

    def _check_dspace(self):
        crystal = self._xtal.controller

        d_spacing = crystal.d_spacing()
        egy_dspacing = self._energy.controller._dspacing

        # if d_spacing not in crystal.crystals.valid_indices():
        #     msg = "Unknown or no crystal selected."
        #     user_error(msg)
        #     raise RuntimeError(msg)
        if egy_dspacing != d_spacing:
            msg = (f'The energy axis d_space ({egy_dspacing}) doesn\'t match'
                   f'the current crystal ({d_spacing}).')
            log_critical(self, msg)
            print('=============================')
            print('=============================')
            print()
            if getval_yes_no(f"Do you want to set dspace to {d_spacing}?"):
                crystal.sync_dspace()
            else:
                print("== User aborted, dspacing not updated.")
                print(f'== Call sync() to do it manually.')
            print('=============================')
            print('=============================')
            raise RuntimeError(msg)

    def pre_scan(self, axes_list):
        self._check_dspace()

    def pre_move(self, motion_list):
            self._check_dspace()
