"""
Calc motor for the mono height.
Reals: dj1, dj2, dj3
Calc: monhgt
"""

from bliss.controllers.motor import CalcController


class D20MonoHeight(CalcController):
    def calc_from_real(self, real_positions):
        dj1 = real_positions["jack_1"]
        dj2 = real_positions["jack_2"]
        dj3 = real_positions["jack_3"]
        monohgt = (dj1 + dj2 + dj3) / 3
        return {'monhgt':monohgt}

    def calc_to_real(self, positions_dict):
        monohgt = positions_dict['monhgt']
        return {"jack_1": monohgt, "jack_2": monohgt, "jack_3": monohgt}
        