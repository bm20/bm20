"""
Wrapper around the wago box controlling the pmac:
- crystal set/get
- error
- abort
- lat/yaw homng
"""

import gevent
from bliss.config.static import get_config


class D20MonoWago:
    """
    Wago I/O interface to the PMAC.
    - homing channels
    - abort
    - error
    - crystal selection
    """
    name = property(lambda self: self._wago.name)
    chan_abort = property(lambda self: self._channels["abort"])
    chan_home_lat = property(lambda self: self._channels["home_lat"])
    chan_home_yaw = property(lambda self: self._channels["home_yaw"])
    chan_enable_yaw = property(lambda self: self._channels["enable_yaw"])
    chan_lateral_brake = property(lambda self: self._channels["lateral_brake"])
    chan_error = property(lambda self: self._channels["error"])

    def __init__(self, config):
        self._wago = get_config().get(config['wago'])
        self._channels = {key:value
                          for key, value in config.get("channels").items()}
        self._crystals = config.get("crystals")
        
    def read_crystal_index(self):
        """
        Returns the current DCM mode (index of the crystal).
        """
        indices = self._crystals.valid_indices()
        active_chans = [idx for idx in indices if self._wago.get(self._crystals.in_chan(idx))]
        if len(active_chans) > 1:
            raise RuntimeError(f'Error, more than one active channels set to 1'
                            f' on {self._wago.name}.')
        if len(active_chans) == 0:
            return -1
        return active_chans[0]

    def write_crystal_index(self, index):
        """
        Selects the given crystal.
        index: integer matching a known crystal index.
        """
        self.clear()
        channel = self._crystals.out_chan(index)
        self._wago.set(channel, 1)

    def clear(self):
        """
        Clears all channels except the ERROR channel.
        (crystals, home lat, home yaw)
        """
        wago_args = []
        for idx in self._crystals.valid_indices():
            wago_args += [self._crystals.out_chan(idx), 0]
        wago_args += [self.chan_abort, 0.0]
        wago_args += [self.chan_home_lat, 0.0]
        wago_args += [self.chan_home_yaw, 0.0]
        self._wago.set(*wago_args)

    def clear_error(self, wait_time_s=1):
        """
        Clears the error channel.
        """
        self._wago.set(self.chan_error)
        if wait_time_s > 0:
            gevent.sleep(wait_time_s)
        if self.is_error(raise_ex=False):
            raise RuntimeError(f'Failed to clear the error '
                               f'channel ({self._wago.name}:'
                               f'{self.chan_error}), '
                               'please investigate.')

    def abort(self):
        """
        Writes to the abort channel.
        """
        self._wago.set(self.chan_abort, 1.0)

    def is_error(self, raise_ex=False):
        """
        Reads the error channel.
        """
        error = self._wago.get(self.chan_error)
        if error and raise_ex:
            raise RuntimeError(f'ERROR: the PMAC has set the wago error '
                               f'channel ({self._wago.name}:'
                               f'{self.chan_error}), '
                                'please investigate.')
        return error != 0.0

    def start_home_lat(self, force=False):
        """
        Signals the PMAC to start the lateral homing PLC
        """
        if not force:
            raise RuntimeError('Safeguard: please set the force keyword to True.')
        self.clear()
        self._wago.set(self.chan_home_lat, 1.0)
        gevent.sleep(2)
        self._wago.set(self.chan_home_lat, 0.0)

    def start_home_yaw(self, force=False):
        """
        Signals the PMAC to start the yaw homing PLC
        """
        if not force:
            raise RuntimeError('Safeguard: please set the force keyword to True.')
        self.clear()
        self._wago.set(self.chan_home_yaw, 1.0)
        gevent.sleep(2)
        self._wago.set(self.chan_home_yaw, 0.0)