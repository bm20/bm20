import re
import enum
from collections import deque
import time

import numpy as np
import gevent

from bliss import global_map
from bliss.config import settings
from bliss.config.static import get_config
from bliss.common.axis import Axis
from bliss.common.logtools import log_debug, log_error, log_info
from bliss.comm.util import get_comm
from bliss.common.axis import AxisState
from bliss.controllers.motor import Controller
from bliss.controllers.motors.turbopmac import (pmac_raw_write_read,
                                                PMAC_MOTOR_STATUS,
                                                pmac_axis_is_open_loop,
                                                pmac_axis_close_loop,
                                                pmac_axis_kill)
from bliss.common.hook import MotionHook


from bm20.oh.mono.energy import bragg2energy, energy2bragg
from ...config.names import XTAL2_PERP


class CS_MOVE_MODES(enum.Enum):
    FEEDTHROUGH = enum.auto()
    TAJECTORY = enum.auto()


PMAC_CS_STATUS = (
    ((1 << (23 + 48), "Z-Axis Used in Feedrate Calculations"),
    (1 << (22 + 48), "Z-Axis Incremental Mode"),
    (1 << (21 + 48), "Y-Axis Used in Feedrate Calculations"),
    (1 << (20 + 48), "Y-Axis Incremental Mode"),
    (1 << (19 + 48), "X-Axis Used in Feedrate Calculations"),
    (1 << (18 + 48), "X-Axis Incremental Mode"),
    (1 << (17 + 48), "W-Axis Used in Feedrate Calculations"),
    (1 << (16 + 48), "W-Axis Incremental Mode"),
    (1 << (15 + 48), "V-Axis Used in Feedrate Calculations"),
    (1 << (14 + 48), "V-Axis Incremental Mode"),
    (1 << (13 + 48), "U-Axis Used in Feedrate Calculations"),
    (1 << (12 + 48), "U-Axis Incremental Mode"),
    (1 << (11 + 48), "C-Axis Used in Feedrate Calculations"),
    (1 << (10 + 48), "C-Axis Incremental Mode"),
    (1 << (9 + 48), "B-Axis Used in Feedrate Calculations"),
    (1 << (8 + 48), "B-Axis Incremental Mode"),
    (1 << (7 + 48), "A-Axis Used in Feedrate Calculations"),
    (1 << (6 + 48), "A-Axis Incremental Mode"),
    (1 << (5 + 48), "Radius Vector Incremental Mode"),
    (1 << (4 + 48), "Continuous Motion Request"),
    (1 << (3 + 48), "Move-Specified-by-Time Mode"),
    (1 << (2 + 48), "Continuous Motion Mode"),
    (1 << (1 + 48), "Single-Step Mode"),
    (1 << (0 + 48), "Running Program"),
    (1 << (23 + 24), "Lookahead in Progress"),
    (1 << (22 + 24), "Run-Time Error"),
    (1 << (21 + 24), "Move In Stack"),
    (1 << (20 + 24), "Amplifier Fault Error"),
    (1 << (19 + 24), "Fatal Following Error"),
    (1 << (18 + 24), "Warning Following Error"),
    (1 << (17 + 24), "In Position"),
    (1 << (16 + 24), "Rotary Buffer Request"),
    (1 << (15 + 24), "Delayed Calculation Flag"),
    (1 << (14 + 24), "End of Block Stop"),
    (1 << (13 + 24), "Synchronous M-variable One-Shot"),
    (1 << (12 + 24), "Dwell Move Buffered"),
    (1 << (11 + 24), "Cutter Comp Outside Corner"),
    (1 << (10 + 24), "Cutter Comp Move Stop Request"),
    (1 << (9 + 24), "Cutter Comp Move Buffered"),
    (1 << (8 + 24), "Pre-jog Move Flag"),
    (1 << (7 + 24), "Segmented Move in Progress"),
    (1 << (6 + 24), "Segmented Move Acceleration"),
    (1 << (5 + 24), "Segmented Move Stop Request"),
    (1 << (4 + 24), "PVT/SPLINE Move Mode"),
    (1 << (3 + 24), "2D Cutter Comp Left/3D Cutter Comp On"),
    (1 << (2 + 24), "2D Cutter Comp On"),
    (1 << (1 + 24), "CCW Circle/Rapid Mode"),
    (1 << (0 + 24), "CIRCLE/SPLINE Move Mode"),
    (1 << (23 + 0), "Lookahead Buffer Wrap"),
    (1 << (22 + 0), "Lookahead Lookback Active"),
    (1 << (21 + 0), "Lookahead Buffer End"),
    (1 << (20 + 0), "Lookahead Synchronous M-variable"),
    (1 << (19 + 0), "Lookahead Synchronous M-variable Overflow"),
    (1 << (18 + 0), "Lookahead Buffer Direction"),
    (1 << (17 + 0), "Lookahead Buffer Stop"),
    (1 << (16 + 0), "Lookahead Buffer Change"),
    (1 << (15 + 0), "Lookahead Buffer Last Segment"),
    (1 << (14 + 0), "Lookahead Buffer Recalculate"),
    (1 << (13 + 0), "Lookahead Buffer Flush"),
    (1 << (12 + 0), "Lookahead Buffer Last Move"),
    (1 << (11 + 0), "Lookahead Buffer Single-Segment Request"),
    (1 << (10 + 0), "Lookahead Buffer Change Request"),
    (1 << (9 + 0), "Lookahead Buffer Movement Request"),
    (1 << (8 + 0), "Lookahead Buffer Direction Request"),
    (1 << (7 + 0), "RFU"),
    (1 << (6 + 0), "RFU"),
    (1 << (5 + 0), "RFU"),
    (1 << (4 + 0), "RFU"),
    (1 << (3 + 0), "Radius Error"),
    (1 << (2 + 0), "Program Resume Error"),
    (1 << (1 + 0), "Desired Position Limit Stop"),
    (1 << (0 + 0), "In-Program PMATCH"))
)


def pmac_cs_status_to_str(state):
    text = "\n".join(
        [
            f"{i//24}-{23-i%24}: {desc}"
            for i, (flag, desc) in enumerate(PMAC_CS_STATUS)
            if flag & state
        ]
    )
    return hex(state) + "\n" + text



def _cs_num_to_sx(cs_addr):
    # see pmac turbo srm page 216:
    # In the generic description of these I-variables, the thousands digit is represented by the letter s, and the
    # hundreds digit by the letter x; for example, Isx11. s and x can take the following values:
    # s is equal to 5 for Coordinate Systems 1 – 9;
    # s is equal to 6 for Coordinate Systems 10 – 16;
    # x is equal to the coordinate system number for Coordinate Systems 1 – 9;
    # x is equal to the (coordinate system number minus 10) for Coordinate Systems 10 – 16.
    return ((cs_addr < 10 and 5) or 6) * 10 + cs_addr % 10


def energy_to_bragg_steps_pos_tm(e_start,
                                 e_end,
                                 d_spacing,
                                 ev_per_s,
                                #  step_s,
                                 steps_per_unit,
                                 offset,
                                 segment_t_s_max=2):
    """
    Computes a PMAC POSITION array, with TM=segment_t_s
    Returns a dictionary {"pos":[<positions], "tm":<TM value>}
    """
    # total distance
    egy_ival = abs(e_end - e_start)
    # total move time
    move_time_s = egy_ival / ev_per_s
    # number of segments and time per segment

    if move_time_s < segment_t_s_max:
        n_segments = 1
        segment_t_s = move_time_s
    else:
        n_segments = int(move_time_s / segment_t_s_max + 0.5)
        segment_t_s = move_time_s / n_segments

    egys = np.linspace(e_start, e_end, n_segments + 1)
    
    # bragg angle (with offset)
    braggs = energy2bragg(egys, d_spacing) - offset
    # position in motor steps
    bragg_steps = braggs * steps_per_unit

    return {"tm":segment_t_s, "pos":bragg_steps}


CS_PROG_START = ["CLOSE",
                 "OPEN PROG {prog_num}",
                 "CLEAR",
                 "{move_type}"]

CS_PROG_END = ["CLOSE"]


def feedthrough_prog(e_target, vel_ev_s, isx90, steps_per_unit, prog_num):
    feedthrough = vel_ev_s * steps_per_unit * 1000 / isx90
    target = e_target * steps_per_unit
    # print("FT", target, feedthrough)
    prog = "\r".join(CS_PROG_START).format(prog_num=prog_num, move_type="ABS")
    prog += f"\rF{feedthrough}"
    prog += f"\rX{target}"
    prog += "\rCLOSE"
    return prog


def pos_tm_to_prog(tm_ms,
                   positions,
                   prog_num,
                   max_feedthrough,
                   spline1=False,
                   pos_diff_max=5):
    """
    Writes a PMAC motion program (LINEAR or SPLINE1).
    With TM=tm_ms (time to move in ms)
    and positions in tooltip units
    (cf PMAC documentation about coordinate systems).
    The first move of the program will use the given max_feedthrough
        (F<value>).
    The following moves will use the given TM.
    WARNING/REMINDER: the feedthrough is the number of steps per <Isx90> ms.
    """
    prog_mode = "SPLINE1" if spline1 else "LINEAR"
    prog = '\r'.join(CS_PROG_START).format(prog_num=prog_num, move_type="ABS")

    # moving as fast as possible to the first position
    prog += f"\r{prog_mode}"
    prog += f"\rF{max_feedthrough}"
    prog += f"\rX{positions[0]}"
    prev_pos = positions[0]
    tm_prog = False

    if not isinstance(tm_ms, (np.ndarray, list, set,)):
        tm_ms = np.repeat(tm_ms, len(positions))
    
    assert len(tm_ms) == len(positions)
    
    for pos, tm in zip(positions[1:], tm_ms[1:]):
        if abs(pos - prev_pos) > pos_diff_max:
            if not tm_prog or tm_prog != tm:
                prog += f"\rTM{tm}"
                tm_prog = tm
            prog += f"\rX{pos}"
            prev_pos = pos
    prog += "\r" + '\r'.join(CS_PROG_END)
    return prog


class TurboPmacCoordSys(Controller):
    move_mode = property(lambda self: self._move_mode)

    def __init__(self, *args, **kwargs):
        super(TurboPmacCoordSys, self).__init__(*args, **kwargs)

        self._cs_status = AxisState()
        # TODO : check parameters

        self._move_mode = CS_MOVE_MODES.TAJECTORY

        self._pmac = self.config.get("pmac")
        self._crystal = self.config.get("crystal")
        self._cs_num = self.config.get("cs_num")
        self._prog_num = self.config.get("prog_num")
        self._move_type = self.config.get("move_type")
        self._segment_t_s = self.config.get("resolution_s", 2.)
        self._cs_sx = _cs_num_to_sx(self._cs_num)

        global_map.register(self, parents_list=["controllers"], children_list=[self._pmac])

        # velocity, acceleration and steps_per_unit are not mandatory in config
        # self.axis_settings.config_setting["velocity"] = False
        self.axis_settings.config_setting["acceleration"] = False
        self.axis_settings.config_setting["steps_per_unit"] = False

        forward = [l.split(";")[0] for l in re.split("\r|\n", self.config.config_dict["forward"])]
        self._cs_forward = [l.strip() for l in forward if l]
        inverse = [l.split(";")[0] for l in re.split("\r|\n", self.config.config_dict["inverse"])]
        self._cs_inverse = [l.strip() for l in inverse if l]
        self._exafs_params = deque()

    @move_mode.setter
    def move_mode(self, move_mode: CS_MOVE_MODES):
        self._move_mode = move_mode

    def cs_status(self):
        state = pmac_raw_write_read(self._pmac, f"&{self._cs_num}??")
        return pmac_cs_status_to_str(state)

    # def get_id(self, axis):
        # return self.pmac_comm.pmac_version()

    def initialize(self):
        super(TurboPmacCoordSys, self).initialize()
        self._real_axes = {axis.name:axis for axis in self.config.get("reals")}

    def initialize_hardware(self):
        super(TurboPmacCoordSys, self).initialize_hardware()

    def initialize_axis(self, axis):
        if axis.steps_per_unit != self._real_axes["bragg"].steps_per_unit:
            raise RuntimeError(f"Expected axis {axis.name} steps_per_unit "
                               "to be he same as axis "
                               f"{self._real_axes['bragg'].name}")

    def initialize_hardware_axis(self, axis):
        super(TurboPmacCoordSys, self).initialize_hardware_axis(axis)
        self._clear_cs()
        self._enable_kinematics()
        # TODO : lock the comm to make this operation atomic
        # self._pmac._write_multiline("\r".join(self._cs_forward), write=True)
        # self._pmac._write_multiline("\r".join(self._cs_inverse), write=True)
        self._pmac.raw_write("\r".join(self._cs_forward))
        self._pmac.raw_write("\r".join(self._cs_inverse))

    def prepare_move(self, motion):
        if not motion.type == "move":
            raise ValueError(f"Unsupported motion type: {motion.type}.")
        if motion.target_pos is not None:
            move_type = 'ABS'
            target = motion.target_pos
        elif motion.delta is not None:
            move_type = 'INC'
            target = motion.delta + motion.axis.position
        p_start = motion.axis.position
        target = motion.axis.offset + target / motion.axis.steps_per_unit

        self.prepare_trajectory(motion.axis,
                                p_start,
                                target,
                                motion.axis.velocity)
        
    def set_velocity(self, axis, new_velocity):
        vel_setting = settings.SimpleSetting(f"{axis.name}_cs_vel")
        vel_setting.set(new_velocity)

    def read_velocity(self, axis):
        vel_setting = settings.SimpleSetting(f"{axis.name}_cs_vel",
                                             default_value=axis.velocity)
        return vel_setting.get()

    def read_acceleration(self, axis):
        return self._real_axes["bragg"].acceleration
        
    def read_position(self, axis):
        pos = self.position_from_reals({axis.name:axis
                                         for axis in self._real_axes.values()})
        # print(pos/200000 + axis.offset)
        return pos
    
    def set_next_scan_params(self, exafs_params):
        self._exafs_params.append(exafs_params)

    def clear_scans_params(self):
        self._exafs_params.clear()

    def _upload_exafs_params(self, params):
        # segment_t_s = self._segment_t_s
        bragg = self._real_axes["bragg"]
        d_spacing = self._crystal.controller.d_spacing()

        e_pos, tm = params._calc_cont()
        tm[0] = 0
        # b_pos = energy2bragg(e_pos, d_spacing)

        isx90 = float(pmac_raw_write_read(self._pmac, f'I{self._cs_sx}90'))
        # max feedthrough = number of tooltip steps per isx90 ms
        max_feedthrough = (bragg.velocity * bragg.steps_per_unit) * 1000 / isx90
        # position in motor steps
        # bragg angle (with offset)
        braggs = energy2bragg(e_pos, d_spacing) - bragg.offset
        bragg_steps = braggs * bragg.steps_per_unit
        prog = pos_tm_to_prog(tm*1000.,
                              bragg_steps,
                              self._prog_num,
                              max_feedthrough,
                              spline1=self._move_type == "spline")
        try:
            gevent.sleep(1)
            self._pmac.raw_write(prog)
        except:
            self._clear_prog()
        self._enable_kinematics()

    def _clear_prog(self):
        prog = ["CLOSE",
                f"OPEN PROG {self._prog_num}",
                "CLEAR",
                "CLOSE"]
        self._pmac.raw_write('\r'.join(prog))
        
    def start_one(self, motion):
        cmd = f"&{self._cs_num}B{self._prog_num}R"
        # for i in range(5):
        #     print("ISOPEN?", [pmac_axis_is_open_loop(x) for x in self._real_axes.values()])
        #     gevent.sleep(3)

        # TODO: waiting for the D20 PMAC KILL PLC #4 to kill the axis
        # find something better
        gevent.sleep(1.5)
        for axis in self._real_axes.values():
            pmac_axis_close_loop(axis)
            pmac_axis_close_loop(axis)
        self._enable_kinematics()
        # print("START")
        # for i in range(5):
        #     print("ISOPEN?", [pmac_axis_is_open_loop(x) for x in self._real_axes.values()])
        #     gevent.sleep(1)
        pmac_raw_write_read(self._pmac, cmd)
        in_pos = f"M{self._cs_sx}87"
        prog_running = f"M{self._cs_sx}80"
        reply = pmac_raw_write_read(self._pmac, in_pos + prog_running)
        # print("RUNNING?", reply)
        # in_pos = f"M{self._cs_sx}87"
        # prog_running = f"M{self._cs_sx}80"
        while not int(pmac_raw_write_read(self._pmac, prog_running)):
            # print("NOT RUNNING!")
            gevent.sleep(0.5)
        # gevent.sleep(0.2)
        self.sync_reals()
        # print("START OUT")

    def stop(self, axis):
        pmac_raw_write_read(self._pmac, f"&{self._cs_num}A")
        pmac_axis_kill(self._real_axes["xl2perp"])
        
    def stop_all(self, *motions):
        pmac_raw_write_read(self._pmac, f"&{self._cs_num}A")

    def state(self, axis):
        #TODO: error states
        state = self._cs_status.new()
        in_pos = f"M{self._cs_sx}87"
        prog_running = f"M{self._cs_sx}80"
        reply = pmac_raw_write_read(self._pmac, in_pos + prog_running)
        in_pos, prog_running = map(int, reply) 
        if prog_running:
            state.set("MOVING")
            # print("=========> PROG MOVING")
        else:
            # checking if bragg is still running, because it seems
            # that the prog returns before the axis has actually settled.
            # if self._real_axes["bragg"].hw_state.MOVING:
            #     # print("=========> MOVING")
            #     state.set("MOVING")
            # else:
                # print("=========> READY")
            state.set("READY")
        return state

    def sync_reals(self):
        for axis in self._real_axes.values():
            axis.sync_hard()

    def position_from_reals(self, reals_dict):
        bragg = reals_dict["bragg"]
        return bragg.controller.read_position(bragg)
        # if bragg.hw_state.MOVING:
        #     return bragg._hw_position * bragg.steps_per_unit
        # else:
        #     return self._pmac.pmac_comm.

    def prepare_trajectory(self, axis, p_start, p_end, velocity):
        if self._exafs_params:
            params = self._exafs_params.popleft()
            if params:
                self._upload_exafs_params(params)
                return
        segment_t_s = self._segment_t_s
        bragg = self._real_axes["bragg"]
        d_spacing = self._crystal.controller.d_spacing()
        e_start = bragg2energy(p_start, d_spacing)
        e_end = bragg2energy(p_end, d_spacing)

        t_move = abs(p_end - p_start) / velocity
        ev_per_sec = abs(e_end - e_start) / t_move

        isx90 = float(pmac_raw_write_read(self._pmac, f'I{self._cs_sx}90'))
        if self._move_mode == CS_MOVE_MODES.FEEDTHROUGH:
            prog = feedthrough_prog(e_start, velocity, isx90, bragg.steps_per_unit, self._prog_num)
        else:
            ptm = energy_to_bragg_steps_pos_tm(e_start,
                                                e_end,
                                                d_spacing,
                                                ev_per_sec,
                                                bragg.steps_per_unit,
                                                bragg.offset,
                                                segment_t_s_max=segment_t_s)
            # max feedthrough = number of tooltip steps per isx90 ms
            max_feedthrough = (bragg.velocity * bragg.steps_per_unit) * 1000 / isx90
            prog = pos_tm_to_prog(ptm["tm"]*1000.,
                                ptm["pos"],
                                self._prog_num,
                                max_feedthrough,
                                spline1=self._move_type == "spline")
        # gevent.sleep(4)
        # time.sleep(3)
        self._pmac.raw_write(prog)
        # time.sleep(3)
        # print("PROG =", pmac_raw_write_read(self._pmac, "LIST PROG 666"))
        self._enable_kinematics()

    def _enable_kinematics(self):
        cmd = f"I{self._cs_sx}50=1"
        pmac_raw_write_read(self._pmac, cmd)

    def _clear_cs(self):
        cmd = f"&{self._cs_num}\rOPEN INVERSE\rCLEAR\rCLOSE"
        pmac_raw_write_read(self._pmac, cmd)
        cmd = f"&{self._cs_num}\rOPEN FORWARD\rCLEAR\rCLOSE"
        pmac_raw_write_read(self._pmac, cmd)


class PmacCSHook(MotionHook):
    def __init__(self, name, config):
        super().__init__()
        self._xtal = config.get("crystal")
        self.config = config
        self._energy_axis = None
        self._is_scan = False
        self._xtal2perp = None
        self._bragg = None
        self._scan_params = None

    @property
    def xtal2perp(self):
        if self._xtal2perp is None:
            self._xtal2perp = get_config().get(self.config["xtal2perp"])
        return self._xtal2perp
    
    @property
    def bragg(self):
        if self._bragg is None:
            self._bragg = get_config().get(self.config["bragg"])
        return self._bragg

    # def _close_axes(self):
        # print("CLOSING AXES", pmac_axis_is_open_loop(self.bragg), pmac_axis_is_open_loop(self.xtal2perp))
        # self._open_axes()
        # pmac_axis_close_loop(self.bragg)
        # pmac_axis_close_loop(self.xtal2perp)
        # print("CLOSED?", pmac_axis_is_open_loop(self.bragg), pmac_axis_is_open_loop(self.xtal2perp))

    def _open_axes(self):
        # print("OPENING", pmac_axis_is_open_loop(self.bragg), pmac_axis_is_open_loop(self.xtal2perp))
        pmac_axis_kill(self.xtal2perp)
        # print("OPENED?", pmac_axis_is_open_loop(self.bragg), pmac_axis_is_open_loop(self.xtal2perp))

    def _sync_reals(self):
        self.axes["fbragg"].controller.sync_reals()

    def set_scan_params(self, scan_params):
        self._scan_params = scan_params
        # print(pos_arr)
        # print(tm_arr)
        
    # def clear_scan_traj(self):
    #     pass
        # print("CLEAR SCAN TRAJ")

    def pre_scan(self, axes_list):
        # print(">>>>> PS")
        self._sync_reals()
        # self._close_axes()
        self._scan_params = None

    def pre_move(self, motion_list):
        # print(">>>>> PM")
        self._sync_reals()
        # self._close_axes()
        
    def post_scan(self, axes_list):
        # print("POST 0")
        self._sync_reals()
        self._open_axes()
        # print("POST 1")

    def post_move(self, motion_list):
        # print("PM 0")
        self._sync_reals()
        self._open_axes()
        # print("PM 1")
