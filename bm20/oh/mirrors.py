"""
Mirror 1 and  Mirror 2 functions
"""

__all__ = ['show_mirrors']


from bliss.config.static import get_config
from bliss.controllers.motor import CalcController
from bm20.config.names import bm20names


# #######################
# calculations and constants taken
# from BM20's SPEC macros mirrctrl.mac
# (as of January 2020)
# #######################

### From Drawings AHM5251-M, AHM5258-M

 # mm Distance between the front jack (J1) and the two back jacks (left J2, right J3)
_MIRR_D_LONG = 1351.5

 # mm Distance between the two back jacks J2 (left) and J3 (right)
_MIRR_D_PERP = 400.0

 # mm Distance between the two actuators latf andlatb for lateral movement and yaw of the crystal
_MIRR_D_LAT = 1301.5

# putting those in the source
# instead of redis as we dont
# want them modified easily
# see below for some more info about how therse params
# were obtained
_M1BEND_A0 = 0.00328767
_M1BEND_A1 = 6.84066e-07

_M2BEND_A0 = -0.00497
_M2BEND_A1 = 5.01491e-7

# =========================================
# =========== MIRR 1 PARAMETERS ===========
# =========================================
# COPIED FROM BM20's mirrctl.mac (Jan. 2020)
#
# The conversion is based on the LTP measurements
#as given by SESO:
# #
# # Calibration table from SESO:
# # Mirror Manual, Section 12,
# # Document "PV de controle", No. 88922, page 3/11:
# # "3- calibration central stripe (S2)"
# # Table: "b. under vacuum"
# 
# HalfSteps   Radius/[km]
# 0.          133.62
# #53600.      27.58 # unknown origin
# 88600.       17.80
# #142745.     10.00 # unknown origin
# 182745.       7.67
# 212745.       6.69
# 368745.       3.85
# 405145.       3.61
# 
# Fitted using gnuplot as:
#
# R(x)=a0+a1*x
# fit R(x) "m1.dat" using 1:(1/$2) via a0,a1  
# print "m1.dat" using 1:(1/$2), R(x) 
# 
# Final set of parameters            Asymptotic Standard Error
# =======================            ==========================
# 
# a0              = 0.00328767       +/- 0.003821     (116.2%)
# a1              = 6.84066e-07      +/- 1.505e-08    (2.2%)

# =========================================
# =========== MIRR 2 PARAMETERS ===========
# =========================================
# COPIED FROM BM20's mirrctl.mac (Jan. 2020)
#
# # Calibration table from SESO:
# # Mirror Manual, Section 13,
# # Document "PV de controle", No. 89811, page/section 4:
# # "4- calibration stripe 2 (meridional cylinder)
# # Table: "stripe 2: meridional cylinder" columns "... in vacuum"
# 
# HalfSteps   Radius/[km]
# 0.           389.22
# 113200.      19.21 
# 277200.      6.51
# 302200.      5.74
# 363200.      4.98
# 420000.      4.31
# 
# R(x)=a0+a1*x 
# fit R(x) "m2.dat" using ($1):(1/$2) via a0,a1  
#
# Final set of parameters            Asymptotic Standard Error
# =======================            ==========================
# 
# a0              = -0.00234061      +/- 0.005052     (215.8%)
# a1              = 5.61944e-07      +/- 1.769e-08    (3.148%)

# =========================================
# =========================================
# Vessel positioning
# - 3 jacks (left, right, front)
# - 2 lateral motors (front, back)
# =========================================
# =========================================

def _jacks2height(j_left_pos, j_right_pos, j_front_pos):
    return (j_left_pos + j_right_pos + 2 * j_front_pos) / 4.


def _jacks2pitch(j_left_pos, j_right_pos, j_front_pos):
    return  1000 * ((((j_left_pos + j_right_pos) / 2.) - j_front_pos) / \
            _MIRR_D_LONG)


def _jacks2roll(j_left_pos, j_right_pos, j_front_pos):
    return (j_left_pos - j_right_pos) / _MIRR_D_PERP * 1000


def _pseudos2frontj(height_pos, pitch_pos, roll_pos):
    return height_pos - _MIRR_D_LONG / 2 * pitch_pos / 1000


def _pseudos2leftj(height_pos, pitch_pos, roll_pos):
    return height_pos + _MIRR_D_LONG / 2 * pitch_pos / 1000 + \
           _MIRR_D_PERP / 2 * roll_pos / 1000


def _pseudos2rightj(height_pos, pitch_pos, roll_pos):
    return height_pos + _MIRR_D_LONG / 2 * pitch_pos / 1000 - \
           _MIRR_D_PERP / 2 * roll_pos / 1000




class D20MirrorTable(CalcController):
    """
    Calc motor to control the axis linked to the jacks:
    - height
    - pitch
    - roll

    real tags: jack_left, jack_right, jack_front
    pseudo tags: height, pitch, roll
    """
    def calc_from_real(self, real_pos):
        args = (real_pos['jack_left'],
                real_pos['jack_right'],
                real_pos['jack_front'])
        height = _jacks2height(*args)
        pitch = _jacks2pitch(*args)
        roll = _jacks2roll(*args)
        return {'height': height,
                'pitch': pitch,
                'roll': roll}

    def calc_to_real(self, calc_pos):
        args = (calc_pos['height'],
                calc_pos['pitch'],
                calc_pos['roll'])
        jleft = _pseudos2leftj(*args)
        jright = _pseudos2rightj(*args)
        jfront = _pseudos2frontj(*args)
        return {'jack_left': jleft,
                'jack_right': jright,
                'jack_front': jfront}


def _real2lateral(lat_front_pos, lat_back_pos):
    return (lat_front_pos + lat_back_pos) / 2.


def _real2yaw(lat_front_pos, lat_back_pos):
    return (lat_front_pos - lat_back_pos) / _MIRR_D_LAT * 1000.


def _pseudos2latfront(lat_pos, yaw_pos):
    return lat_pos + _MIRR_D_LAT / 2 * yaw_pos / 1000.


def _pseudos2latback(lat_pos, yaw_pos):
    return lat_pos - _MIRR_D_LAT / 2. * yaw_pos / 1000
    

class D20MirrorLateral(CalcController):
    """
    Calc motor to control the axis linked to the
        lateral (front and back) actuators:
    - lateral
    - yaw

    real tags: lateral_front, lateral_back
    pseudo tags: lateral, yaw
    """
    def calc_from_real(self, real_pos):
        args = (real_pos['lateral_front'],
                real_pos['lateral_back'])
        lateral = _real2lateral(*args)
        yaw = _real2yaw(*args)
        return {'lateral': lateral,
                'yaw': yaw}

    def calc_to_real(self, calc_pos):
        args = (calc_pos['lateral'],
                calc_pos['yaw'])
        lat_front = _pseudos2latfront(*args)
        lat_back = _pseudos2latback(*args)
        return {'lateral_front': lat_front,
                'lateral_back': lat_back}


# =========================================
# =========================================
# Mirror bending
# - bending motor
# See this file's header for some more info
# =========================================
# =========================================


def _bender2radius(bender_pos, a0, a1):
    """
    Returns the bending radius of the mirror in km as a function of
    the position of the bender motor in half-steps.
    """
    one_over_r = a0 + a1 * bender_pos
    if one_over_r == 0:
        return 1e8
    return 1./one_over_r


def _radius2bender(radius_pos, a0, a1):
    """
    Returns the bender position of the mirror as a function of
    the bending radius in km.
    """
    if radius_pos == 0:
        one_over_r = 1e8
    else:
        one_over_r = 1 / radius_pos
    return (one_over_r - a0) / a1


class D20MirrorBending(CalcController):
    """
    Calc motor to control the mirror bending:
    
    real tags: bender
    pseudo tags: radius
    """
    def _mirror_params(self):
        if self.name == 'm1bending':
            a0 = _M1BEND_A0
            a1 = _M1BEND_A1
        elif self.name == 'm2bending':
            a0 = _M2BEND_A0
            a1 = _M2BEND_A1
        else:
            raise RuntimeError(f'Unknown bender name {self.name}.')
        return a0, a1

    def calc_from_real(self, real_pos):
        a0, a1 = self._mirror_params()
        radius = _bender2radius(real_pos['bender'], a0, a1)
        return {'radius': radius}

    def calc_to_real(self, calc_pos):
        a0, a1 = self._mirror_params()
        bender = _radius2bender(calc_pos['radius'], a0, a1)
        return {'bender': bender}


# =========================================
# =========================================
# Misc.
# =========================================
# =========================================


def _mirrors_show():
    class _foo():
        def __info__(self):
            mirr1 = bm20names.MIR_SURF_1
            mirr2 = bm20names.MIR_SURF_2
            inf1 = f'============ MIRROR 1 ============\n {mirr1.__info__()}'
            inf2 = f'============ MIRROR 2 ============\n {mirr2.__info__()}'
            return f'{inf1}\n{inf2}'

    return _foo()


show_mirrors = _mirrors_show()
