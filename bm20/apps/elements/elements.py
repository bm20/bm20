# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
# 

"""
Binding energies for K, L1, L2, L3, M1, M2, M3, M4.

from ~.ements import BINDING_ENERGIES
be = BINDING_ENERGIES['P']
be.L1
be.M4
...
"""

import os
import numpy as np
import pandas
from types import MappingProxyType
from typing import NamedTuple, OrderedDict


def _egy_conv(val):
    try:
        return np.float64(val)
    except ValueError:
        return np.nan


_DB_DIR = os.path.join(os.path.dirname(__file__), 'elemdata')

# binding energies file params
_BINDING_ENERGIES_F = os.path.join(_DB_DIR, 'BindingEnergies.dat')
_BE_COL_NAMES = ['Z', 'Element', 'K', 'L1', 'L2', 'L3',
                 'M1', 'M2', 'M3', 'M4']
_BE_CONVERTERS = {'Z': int, 'K': _egy_conv,
                  'L1': _egy_conv, 'L2': _egy_conv, 'L3': _egy_conv,
                  'M1': _egy_conv, 'M2': _egy_conv, 'M3': _egy_conv,
                  'M4': _egy_conv}
_BE_SKIP_ROWS = 3

# natural width
_NATURAL_WIDTH_F = os.path.join(_DB_DIR, 'NaturalWidth.dat')
_NW_COL_NAMES = ['Z', 'Element', 'K', 'L1', 'L2', 'L3']
_NW_SKIP_ROWS = 1
_NW_CONVERTERS = {'Z': int, 'K': _egy_conv,
                  'L1': _egy_conv, 'L2': _egy_conv, 'L3': _egy_conv}

_PHOTON_EN_F = os.path.join(_DB_DIR, 'PhotonEn.dat')


class BindingEnergies(NamedTuple):
    Z: int
    Element: str
    K:  np.float64 = np.nan
    L1: np.float64 = np.nan
    L2: np.float64 = np.nan
    L3: np.float64 = np.nan
    M1: np.float64 = np.nan
    M2: np.float64 = np.nan
    M3: np.float64 = np.nan
    M4: np.float64 = np.nan
    
    def __repr__(self):
        str = '\n'.join([f'{k}:  {e}' for k, e in self._asdict().items()])
        return str
    
    
class NaturalWidth(NamedTuple):
    Z: int
    Element: str
    K:  np.float64 = np.nan
    L1: np.float64 = np.nan
    L2: np.float64 = np.nan
    L3: np.float64 = np.nan
    
    def __repr__(self):
        str = '\n'.join([f'{k}:  {e}' for k, e in self._asdict().items()])
        return str
    
    
def _load_csv(filename, klass, col_names, skip_rows, converters):
    be_df = pandas.read_csv(filename,
                            names=col_names,
                            delim_whitespace=True,
                            skiprows=skip_rows,
                            converters=converters)
    be_dict = OrderedDict()
    for row in be_df.iloc(0):
        be = klass(**row.to_dict())
        be_dict[be.Element] = be
        
    return be_dict


__BINDING_ENERGIES = _load_csv(_BINDING_ENERGIES_F,
                               BindingEnergies,
                               _BE_COL_NAMES,
                               _BE_SKIP_ROWS,
                               _BE_CONVERTERS)
BINDING_ENERGIES = MappingProxyType(__BINDING_ENERGIES)


__NATURAL_WIDTH = _load_csv(_NATURAL_WIDTH_F,
                            NaturalWidth,
                            _NW_COL_NAMES,
                            _NW_SKIP_ROWS,
                            _NW_CONVERTERS)
NATURAL_WIDTH = MappingProxyType(__NATURAL_WIDTH)


def get_binding_energy(element, edge):
    try:
        binding_egy = BINDING_ENERGIES[element]
    except KeyError:
        # TODO: log
        raise ValueError(f'Binding energy: unknown element {element}.')
    
    try:
        edge_ev = binding_egy._asdict()[edge]
    except KeyError:
        raise ValueError(f'Binding energy: unknown edge {edge}.')

    if not np.isfinite(edge_ev):
        raise ValueError('TODO: binding energy: value not found in DB.')
    
    return edge_ev
    
    
def get_natural_width(element, edge):
    try:
        natural_width = NATURAL_WIDTH[element]
    except KeyError:
        # TODO: log
        raise ValueError(f'Natural width: unknown element {element}.')
        
    try:
        natural_width_ev = natural_width._asdict()[edge]
    except KeyError:
        raise ValueError(f'Natural width: unknown edge {edge}.')
    
    if not np.isfinite(natural_width_ev):
        raise ValueError('TODO: natural width: value not found in DB.')
    
    return natural_width_ev
        
