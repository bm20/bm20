import os
from datetime import datetime

import json
import numpy
import requests
from base64 import b64encode, b64decode


import fabio
from silx import io as silx_io
from silx.gui.plot import Plot1D, PlotWidget
from silx.gui.plot.tools.CurveLegendsWidget import CurveLegendsWidget
from silx.gui import qt as Qt



IP = 'd20pilatus500k.esrf.fr'
PORT = '80'

backup_dir = "~blissadm/local/beamline_configuration/files/500k/"

# new_pixels_file = "~blissadm/local/beamline_configuration/files/500k/new_pixels_500k_2023_09_26.txt"
new_pixels_file = "~blissadm/local/beamline_configuration/files/500k/new_pixels_500k_2024_02_09.txt"


def get_mask(ip, port):
    """
    Return the pixel mask of host EIGER system as numpy.ndarray
    """
    url = 'http://{}:{}/detector/api/1.8.0/config/pixel_mask'.format(ip, port)
    reply = requests.get(url)
    darray = reply.json()['value']
    return numpy.frombuffer(b64decode(darray['data']),
        dtype=numpy.dtype(str(darray['type']))).reshape(darray['shape'])


def set_mask(ndarray, ip, port):
    """
    Put a pixel mask as ndarray on host EIGER system and return its reply
    """
    url = 'http://%s:%s/detector/api/1.8.0/config/pixel_mask' % (ip, port)
    data_json = json.dumps({'value': {
                            '__darray__': (1,0,0),
                            'type': ndarray.dtype.str,
                            'shape': ndarray.shape,
                            'filters': ['base64'],
                            'data': b64encode(ndarray.data).decode('ascii') }
                            })
    headers = {'Content-Type': 'application/json'}
    return requests.put(url, data= data_json, headers=headers)


if __name__ == '__main__':
    # initialize the detector
    url = 'http://{}:{}/detector/api/1.8.0/command/initialize'.format(IP, PORT)
    print(f"Downloading the mask from {IP}:{PORT}.")
    assert (requests.put(url).status_code == 200), 'Detector could not be initialized'
    # get the mask
    mask = numpy.copy(get_mask(ip=IP, port=PORT))

    # mask = numpy.load("/data/bm20/inhouse/mask.sav.npy")
    print("Received mask, shape=", mask.shape)

    

    indices = numpy.asarray(mask!=0).nonzero()
    values = mask[indices]

    print(f"Downloaded mask contains {values.shape[0]} marked pixels (i.e: != 0).")

    now = datetime.now()
    epoch = datetime.now().timestamp()
    date_str = now.strftime("%Y-%m-%dT%H-%M-%S")
    
    save_dir = os.path.expanduser(backup_dir)
    save_file = os.path.join(save_dir, f"500k_mask_{date_str}_read.txt")
    save_array = numpy.ndarray((values.shape[0], 3))
    save_array[:, 0] = indices[0]
    save_array[:, 1] = indices[1]
    save_array[:, 2] = values
    print(f"Saving downloaded mask to {save_file}.")
    numpy.savetxt(save_file, save_array, fmt="%d")

    new_pixels = numpy.loadtxt(os.path.expanduser(new_pixels_file),
                               delimiter=",",
                               dtype=numpy.int32)

    print(f"Found {new_pixels.shape[0]} coordinates in the new pixel file.")
    mask[new_pixels[:,0], new_pixels[:, 1]] = 2

    indices = numpy.asarray(mask!=0).nonzero()
    values = mask[indices]
    print(f"New mask contains {values.shape[0]} marked pixels (i.e: != 0).")

    save_file = os.path.join(save_dir, f"500k_mask_{date_str}_write.txt")
    save_array = numpy.ndarray((values.shape[0], 3))
    save_array[:, 0] = indices[0]
    save_array[:, 1] = indices[1]
    save_array[:, 2] = values
    print(f"Saving updated mask to {save_file}.")
    numpy.savetxt(save_file, save_array, fmt="%d")

    # upload the new mask
    reply = set_mask(mask, ip=IP, port=PORT)
    # reply.status_code should be 200, then arm and disarm to test the system
    if reply.status_code == 200:
        for command in ('arm', 'disarm'):
            url = 'http://{}:{}/detector/api/1.8.0/command/%s'.format(IP, PORT, command)
            requests.put(url)
        else:
            print(reply.content)
        print("OK")
    else:
        print("Something went wrong.")
        print(reply.content)

    # app = Qt.QApplication([])
    # plot = PlotWidget()
    # plot.addImage(mask)
    # plot.show()
    # app.exec_()



    # # set a new dead pixel [y,x]
    # mask[123, 234] = 2
    # # set a new noisy pixel [y,x]
    # mask[234, 123] = 8