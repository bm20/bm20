"""
Controller for Femto amplifier @ BM20, using WuT WebIO for comm.
Didn't use the one from BLISS as it is only used with WAGO.
"""

from contextlib import contextmanager
from time import monotonic
import numpy as np
from tabulate import tabulate

from bliss import global_map
from bliss.common.protocols import HasMetadataForScan
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.controllers.ct2.client import CT2Controller

from bliss.scanning.toolbox import DefaultAcquisitionChain
DEFAULT_CHAIN = DefaultAcquisitionChain()


def _count_once(count_time, *counters, ct2v=None):
    n_points = 1
    # tmast = SoftwareTimerMaster(count_time, npoints=n_points)
    # chain = AcquisitionChain(parallel_prepare=True)
    # builder = ChainBuilder(counters)
    # nodes = builder.get_nodes_by_controller_type(CT2Controller)
    # acqp = {"npoints": n_points, "acq_expo_time": count_time}
    # acqpc = {"npoints": n_points, "count_time": count_time}
    # for node in nodes:
    #     node.set_parameters(acq_params=acqp)
    #     for ch in node.children: ch.set_parameters(acq_params=acqpc)
    #     chain.add(tmast, node)
    # scan = Scan(chain, save=False)
    scan_params = {
        "npoints": n_points,
        "count_time": count_time,
        "type": "ct",
        "sleep_time": 0,
    }
    scan_info = {}
    scan_info.update({"type": "ct", "save": False, "sleep_time": 0})
    chain = DEFAULT_CHAIN.get(scan_params, counters)
    scan = Scan(chain, scan_info=scan_info, save=False)
    scan.run()
    data = scan.get_data()
    data = np.array([data[c.fullname][0] for c in counters])
    if ct2v is not None:
        return ct2v * data
    else:
        return data
    

@contextmanager
def _disable_meta(obj):
    enab = obj.scan_metadata_enabled
    if enab:
        obj.disable_scan_metadata()
    yield
    if enab:
        obj.enable_scan_metadata()


class D20Femto(HasMetadataForScan):
    # Loop until all counter readings are within an 10% intervall from the
    # last reading, or 20 readings (2 sec) have been reached
    SETTLE_MAX_COUNT = 20  
    # count time in seconds
    COUNT_S = 0.1
    CT_2_V = 10e-5

    def __init__(self, name, config):
        self.name = name
        self.config = config
        self._comm = config["comm"]

        global_map.register(self, children_list=[self._comm])
        
        # checking if the WebIO is on.
        self._check()

        self._i_counters = [config["i0"], config["i1"], config["i2"]]

    def __call__(self, gains):
        assert gains <= 999 and gains >= 333
        gains = [(gains // 10**n) % 10 for n in range(2, -1, -1)]
        self.set_gains(*gains)

    def scan_metadata(self):
        gains = self.gains
        metadata = {f"i{i}_gain":gain
                    for i, gain in enumerate(gains)}
        metadata["@NX_class"] = "NXcollection"
        return metadata

    def _check(self):
        input = self._comm.input
        if (input & 0x002) == 0: raise RuntimeError("Amplifier for I0 is OFF.")
        if (input & 0x020) == 0: raise RuntimeError("Amplifier for I1 is OFF.")
        if (input & 0x200) == 0: raise RuntimeError("Amplifier for I2 is OFF.")

    def __info__(self):
        gains = self.gains
        voltages = self.voltage
        arr = [["Gain"] + [f"1e{g} V/A" for g in gains],
               ["Voltage"] + [f"{v:1.3f} V" for v in voltages]]
        return tabulate(arr,
                        headers=["Amplifier", "I0", "I1", "I2"],
                        tablefmt="psql")

    @property
    def voltage(self):
        return self._get_counts(ct2v=self.CT_2_V)
    
    @property
    def gains(self):
        output = self._comm.output
        for i in range(3):
            if (output & (1 << (3 + 4*i))) == 0:
                raise RuntimeError(f"Amplifier for I{i} is set to HIGH speed."
                                    " If this is the first time please set "
                                    " the gains once and set it to LOW noise.")

        g0 = 3 + (output & 0x7)
        g1 = 3 + ((output & 0x70) >> 4)
        g2 = 3 + ((output & 0x700) >> 8)

        return g0, g1, g2

    def set_gains(self, g0, g1, g2):
        try:
            ival = range(3, 9)
            assert g0 in ival
            assert g1 in ival
            assert g2 in ival
        except AssertionError:
            raise ValueError("Gains must be in the [3..9] range.")

        command = hex(0x888 | (g2 - 3) * 16**2 + (g1 - 3) * 16 + (g0 - 3))
        self._comm.write_output(command[2:])
        self._count_settle()
        print(self.__info__())
    
    def _get_counts(self, ct2v=None):
        return _count_once(self.COUNT_S, *self._i_counters, ct2v=ct2v)
    
    def _count_settle(self):
        prev_data = np.array(self._get_counts())
        for _ in range(self.SETTLE_MAX_COUNT):
            data = self._get_counts()
            if np.all(np.fabs(data - prev_data) <= 0.1 * prev_data):
                return data
            prev_data[:] = data
        return prev_data.tolist()
            