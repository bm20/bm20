# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
CCS Inc. PD3 series controller, rs485 communication.
https://www.ccs-grp.com/products/series/132
"""


import re
from functools import reduce

import serial

from bliss.comm.serial import Serial


class RS485ReplyError(Exception):
    pass


class PD3CommandError(Exception):
    pass


class RS485Command(object):
    instruction = None
    data_fmt = '{data}'
    data_lg = None
    reply_rx = ''

    def __init__(self, controller, data=None, **kwargs):
        assert self.instruction is not None

        cmd_pattern = '@{{channel:0>2d}}{{instruction}}{data_fmt}{{unit:0>2d}}'
        self._reply_rx_pattern = ('@(?P<channel>{channel:0>2d})'
                                  '((O{reply_rx})|(N(?P<error>[0-9]{{2}})))'
                                  '(?P<unit>{unit:0>2d})'
                                  '(?P<checksum>[0-9A-Fa-f]{{2}})')

        data_fmt = ((self.data_lg and '{{data:0>{0}d}}'.format(self.data_lg))
                    or (self.data_lg is None and self.data_fmt)
                    or '')

        if data is None:
            data = ''
        elif not data_fmt:
            raise ValueError('Some data has been given, but '
                             'data_fmt is empty.')

        self._cmd_pattern = cmd_pattern.format(data_fmt=data_fmt)

        self._command = {'channel': controller.channel,
                         'instruction': self.instruction,
                         'data': data,
                         'unit': controller.unit}
        self._command.update(kwargs)

    def command(self):
        cmd = self._cmd_pattern.format(**self._command)
        return _to_pd3_cmd(cmd)

    def check_reply(self, reply):
        # TODO : check checksum

        reply_rx = self._reply_rx_pattern.format(reply_rx=self.reply_rx,
                                                 **self._command)
        reply_re = re.compile(reply_rx)

        m = reply_re.match(reply)

        if not m:
            raise RS485ReplyError('Expected reply not received. (got : [{0}],'
                                  'expected :[{1}].).'
                                  ''.format(reply, reply_rx))

        return m.groupdict()


# returns total mod 256 as checksum
# input - string
def _checksum256(text):
    return reduce(lambda x, y: x+y, map(ord, text)) % 256


def _to_pd3_cmd(cmd):
    h = format(_checksum256(cmd), 'X')
    result = cmd + h + '\r\n'
    return result


class IntensitySettingCmd(RS485Command):
    instruction = 'F'
    data_lg = 3


class ModeSettingCmd(RS485Command):
    instruction = 'S'
    data_lg = 2


class OnOffCmd(RS485Command):
    instruction = 'L'
    data_lg = 1


class SettingsCmd(RS485Command):
    instruction = 'M'
    data_lg = 0
    reply_rx = 'F(?P<intensity>[0-9]{3}).S(?P<mode>[0-9]{2}).L(?P<onoff>[0-9])'


class StatusCmd(RS485Command):
    instruction = 'C'
    data_lg = 0
    reply_rx = '(?P<status>(00)|(11))'


class AllChannelInitCmd(RS485Command):
    instruction = 'R'


class PD3Controller(object):
    MAX_INTENSITY = 255

    unit = property(lambda self: self._pd3_unit)
    channel = property(lambda self: self._lamp_channel)
    echo = property(lambda self: self._echo)
    reply = property(lambda self: self._reply)
    raise_on_error = property(lambda self: self._raise_on_err)

    def __init__(self, name, config):
        address = config['address']
        self._name = name
        self._echo = config.get('echo', True)
        self._pd3_unit = config.get('unit', 0)
        self._lamp_channel = config.get('channel', 0)
        self._reply = config.get('reply', True)
        self._raise_on_err = True

        self._serial = Serial(address,
                              baudrate=19200,
                              bytesize=serial.EIGHTBITS,
                              parity=serial.PARITY_NONE,
                              stopbits=serial.STOPBITS_ONE)

    def flush(self):
        self._serial.flush()

    def _write_readlines(self, data, n_lines):
        return self._serial.write_readlines(data, n_lines, eol='\r\n')

    def _send_command(self, command):
        cmd_out = command.command()

        n_lines = 0
        if self._echo:
            n_lines += 1
        if self._reply:
            n_lines += 1

        data_in = self._write_readlines(cmd_out, n_lines)

        if not self._reply:
            return None

        if self._echo:
            echo = data_in[0]
            # echo = self._recv()
            if echo != cmd_out.rstrip():
                raise ValueError('Expected an echo. Not received'
                                 ' (received : {0}).'.format(echo))
        # reply = self._recv()
        reply = data_in[n_lines - 1]

        check = None

        try:
            check = command.check_reply(reply)
        except RS485ReplyError:
            if self._raise_on_err:
                raise
        else:
            error = check['error']
            if error and self._raise_on_err:
                raise PD3CommandError('PD3 command failed. Sent {0}, '
                                      'unit replied {1}.'
                                      ''.format(cmd_out, reply))

        return check

    def _set_on(self, on):
        command = OnOffCmd(self, data=(on and 1) or 0)
        return self._send_command(command)

    def set_intensity(self, intensity):
        command = IntensitySettingCmd(self, data=intensity)
        return self._send_command(command)

    def get_settings(self):
        command = SettingsCmd(self)
        return self._send_command(command)

    def set_mode(self, mode):
        command = ModeSettingCmd(self, data=mode)
        return self._send_command(command)

    def set_on(self):
        return self._set_on(True)

    def set_off(self):
        return self._set_on(False)

    def get_status(self):
        command = StatusCmd(self)
        return self._send_command(command)

    def all_channel_init(self):
        command = AllChannelInitCmd(self)
        return self._send_command(command)
