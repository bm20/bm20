import re
import gevent
from bliss import global_map
from bliss.comm.util import get_comm
from bliss.comm.exceptions import CommunicationTimeout
from bliss.common.logtools import (log_debug,
                                   log_error,
                                   log_info,
                                   log_warning)

from bliss.controllers.regulator import Controller
# from bliss.common.regulation import lazy_init
from bliss.config.beacon_object import BeaconObject


"""
CryoVac's TIC 500 temperature controller.

http://www.cryovac.de/downloads/TIC_500_v1.7.pdf

Channels:
- 0-3: 4 inputs
- 4-11: 2 DC current sources
- 12-15: 4 voltage I/O
- 16: digital I/O, 8 lines
- 17: 4 relays
- 18-20: virtual channels

1) Make sure the two output channels are off
    -> "channel.off"
2) if necessary load the tuning parameters (TBD)
3) press twice the on button on the front panel (maybe this switches on the output automatically, not sure)
4) set a target temperature and a ramp temp (=0 to get there asap)
    -> "channel.pid.setpoint X"
    -> "channel.pid.ramp Y"
5) maybe: start the channel
    -> "channel.pid.mode on"

To stop the heating: -> "channel.off"

IMPORTANT:
The ohm symbol is coded as the ASCII character 234 (\xea).
See page 64 of the manual (operation, channel setup, Channel.Range).

"""



class TIC500(Controller):
    def __init__(self, config):
        super().__init__(config)
        self._ti500_idn = None
        self._comm = None

    def __info__(self):
        info = super().__info__()
        if self._comm is None:
            self.initialize_controller()
        info += self._comm.__info__()
        return info

    # ------ init methods ------------------------

    def initialize_controller(self):
        self._comm = CryovacTic500(self.config)
        super().initialize_controller()
        self._ti500_idn = self._comm.identification()

    # def initialize_input(self, tinput):
    #     # making sure we can read the channel
    #     lazy_init(tinput)
    #     self._comm.channel_value(tinput.channel)
    #     super().initialize_input(tinput)

    # def initialize_output(self, toutput):
    #     lazy_init(toutput)
    #     self._comm.channel_value(toutput.channel)
    #     super().initialize_output(toutput)

    # def initialize_loop(self, tloop):
    #     lazy_init(tloop)
    #     super().initialize_loop(tloop)

    # # ------ get methods ------------------------

    def read_input(self, tinput):
        return self._comm.channel_value(tinput.channel)

    def read_output(self, toutput):
        return self._comm.channel_value(toutput.channel)

    # def state_input(self, tinput):
    #     pass

    # def state_output(self, toutput):
    #     pass

    # # ------ PID methods ------------------------

    # def set_kp(self, tloop, kp):
    #     pass

    def get_kp(self, tloop):
        return self._comm.channel_pid_p(tloop.output.channel)

    # def set_ki(self, tloop, ki):
    #     pass

    def get_ki(self, tloop):
        return self._comm.channel_pid_i(tloop.output.channel)

    # def set_kd(self, tloop, kd):
    #     pass

    def get_kd(self, tloop):
        return self._comm.channel_pid_d(tloop.output.channel)

    def start_regulation(self, tloop):
        self._comm.channel_set_pid_mode(tloop.output.channel, 'On')

    def stop_regulation(self, tloop):
        self._comm.channel_off(tloop.output.channel)

    # ------ setpoint methods ------------------------

    def set_setpoint(self, tloop, sp, **kwargs):
        self._comm.channel_pid_set_setpoint(tloop.output.channel, sp)

    def get_setpoint(self, tloop):
        return self._comm.channel_pid_setpoint(tloop.output.channel)

    def get_working_setpoint(self, tloop):
        return self._comm.channel_pid_ramp_t(tloop.output.channel)
        
    # # ------ setpoint ramping methods (optional) ------------------------

    def start_ramp(self, tloop, sp, **kwargs):
        self.set_setpoint(tloop, sp)
        self.start_regulation(tloop)

    def stop_ramp(self, tloop):
        self.stop_regulation(tloop)

    # def is_ramping(self, tloop):
    #     # output is off
    #     is_off = self._comm.channel_pid_mode(tloop.output.channel) == 'Off'
    #     if is_off:
    #         return False

    #     # TODO: validate...
    #     # if temp < setpoint and output == 0 => not ramping
    #     # if sp - deadband < temp < sp + deadband -> not ramping
    #     sp = self._comm.channel_pid_setpoint(tloop.output.channel)
    #     wsp = self._comm.channel_pid_ramp_t(tloop.output.channel)

    #     return wsp != sp

    # #     # wsp = self._comm.channel_pid_ramp_t(tloop.output.channel)
    # #     # is_moving = self._comm.channel_status(tloop.output.channel) != 0
    # #     # output = self.read_output(tloop.output)
    # #     # return is_off and not is_moving and output == 0

    def set_ramprate(self, tloop, rate):
        self._comm.channel_pid_set_setpoint(tloop.output.channel, rate)

    def get_ramprate(self, tloop):
        return self._comm.channel_pid_ramp(tloop.output.channel)


class CryovacTic500:
    def __init__(self, config):
        self._output_names = []

        global_map.register(self, parents_list=["comms"])
        self._comm = get_comm(config)

        self._rlock = gevent.lock.RLock()

    def _tic_read(self, timeout=None, throw_timeout=True, cast_fn=None):
        log_debug(self, 'CryovacTi500Ds._tic_read')
        try:
            with self._rlock:
                reply = self._comm.readline(timeout=timeout)
        except CommunicationTimeout:
            if throw_timeout:
                raise
            return ''

        log_debug(self, f'Read: <{reply}>')
        # replacing \xea with "Ohms" (see manual p64)
        # b'\xe2\x84\xa6'
        reply = reply.replace(b'\xea', b'Ohms')
        reply = reply.decode().rstrip('\r\n')

        if reply.startswith('Error:'):
            log_error(self, f'Tic500 returned an error: {reply}')
            raise RuntimeError(reply)

        log_debug(self, f'CryovacTi500Ds._tic_request, controller replied: {reply}')

        if cast_fn:
            try:
                reply = cast_fn(reply)
            except:
                raise RuntimeError(f'Error casting result: {reply} using {cast_fn}.')
                # raise

        return reply

    def _tic_write(self, command):
        with self._rlock:
            b_command = command.encode() + b'\n'
            log_debug(self, f'Sending command:<{b_command}>')
            self._comm.write(b_command)
            gevent.sleep(0.05)

    def _flush(self):
        log_debug(self, 'Flushing.')
        with self._rlock:
            self._comm.flush()
            try:
                self._tic_read(timeout=0.001)
            except CommunicationTimeout:
                pass

    def _tic_request(self, command, cast_fn=None, retries_on_read=1):
        log_debug(self, f'CryovacTi500Ds._tic_request({command})')
        if retries_on_read < 0:
            retries_on_read = 0
        with self._rlock:
            while retries_on_read >= 0:
                try:
                    self._flush()
                    self._tic_write(command)
                    return self._tic_read(cast_fn=cast_fn)
                except RuntimeError:
                    if retries_on_read >= 0:
                        retries_on_read -= 1
                        continue
                    raise

    def _tic_command(self, command):
        # TODO: find a better way to check for errors
        # (in a polling loop?)
        log_debug(self, f'CryovacTi500Ds._tic_command({command})')
        with self._rlock:
            self._flush()
            self._tic_write(command)
            try:
                self._tic_read(timeout=0.1)
            except CommunicationTimeout:
                log_debug(self, 'CryovacTi500Ds._tic_command: '
                                'timeout waiting for error message.')

    def __info__(self):
        """
        Channels:
        - 0-3: 4 inputs
        - 4-11: 2 DC current sources
        - 12-15: 4 voltage I/O
        - 16: digital I/O, 8 lines
        - 17: 4 relays
        - 18-20: virtual channels
        """

        hw_id = self.identification()
        names = self.channel_names()
        values = self.channel_values()

        maxlen = len(max(names, key=len))

        info = '=========\n=========\n'
        info += f'{hw_id}\n'

        getiotype = lambda n: f'   ({self.channel_io_type(n)})'

        # sensors
        info += '\n == Input sensors: ==\n  - '
        info += '\n  - '.join(
            [f'{names[i]:<{maxlen}} = {values[i]:11.4f}'
             for i in range(4)])

        # sensors
        info += '\n\n == DC sources: ==\n  - '
        lst =[f'{names[i]:<{maxlen}} = {values[i]:11.4f}'
              for i in range(4, 12)]
        lst[0] += getiotype(names[4])
        lst[4] += getiotype(names[8])
        info += '\n  - '.join(lst)

        # voltage IO
        info += '\n\n == Analog voltage IOs: ==\n  - '
        info += '\n  - '.join(
            [f'{names[i]:<{maxlen}} = {values[i]:11.4f}{getiotype(names[i])}'
             for i in range(12, 16)])

        # digital IOs
        info += '\n\n == Digital IOs: ==\n  - '
        dioval = values[16]
        info += f'{names[16]:<{maxlen}} = {dioval:11d}{getiotype(names[16])}'
        info += '\n    -  '
        info += '\n    -  '.join([f'.{l} = H' if dioval & l**2 else f'.{l} = L'
                                  for l in range(8)])

        # relays
        info += '\n\n == Relays: ==\n  - '
        relval = values[17]
        info += f'{names[17]:<{maxlen}} = {relval:11d}'
        info += '\n    -  '
        info += '\n    -  '.join([f'.{l} = H' if relval & l**2 else f'.{l} = L'
                                  for l in range(4)])

        # virtual channels
        info += '\n\n == Virtual channels: ==\n  - '
        info += '\n  - '.join(
            [f'{names[i]:<{maxlen}} = {values[i]:11.4f}{getiotype(names[i])}'
             for i in range(18, 21)])

        info += '\n=========\n=========\n'
        return info

    def channel_io_type(self, name):
        """
        Returns the IOType of the channel:
            "Input", "Set out" or "Meas out".
        Will raise an exception if channel doesn't
            support this operation.
        """
        return self._tic_request(f'"{name}.IOtype?"')

    def channel_names(self):
        return self._tic_request('"getOutput.names"',
                                cast_fn=lambda x: x.split(', '))

    def identification(self):
        return self._tic_request('*IDN?')

    def channel_values(self):
        cast_fn=lambda x: [float(e) if i not in (16, 17) else int(e)
                           for i, e in enumerate(x.split(', '))]
        return self._tic_request('getOutput', cast_fn=cast_fn)

    def channel_value(self, channel):
        # TODO: digital IOs must be casted to int
        return self._tic_request(f'"{channel}?"', cast_fn=float)
    
    def value_by_name(self, name):
        # TODO: digital IOs must be casted to int
        return self._tic_request(f'"{name}?"', cast_fn=float)

    def channel_cal_type(self, channel):
        return self._tic_request(f'"{channel}.cal.type?"')

    def channel_cal_details(self, channel):
        with self._rlock:
            self._tic_command(f'"{channel}.cal.details?"')
            reply = []
            try:
                while True:
                    reply += [self._tic_read(timeout=0.1)]
            except:
                if not re.match('Total of [0-9]+ data points', reply[-1]):
                    raise
        return '\n'.join(reply)

    def channel_off(self, channel):
        # TODO: check command success
        self._tic_command(f'"{channel}.off"')
        mode = self.channel_pid_mode(channel)
        if mode.lower() != 'off':
            raise RuntimeError('Failed to stop.')

    def channel_pid_mode(self, channel):
        return self._tic_request(f'"{channel}.pid.mode?"')

    def channel_set_pid_mode(self, channel, mode):
        assert mode.lower() in ('on', 'off', 'follow')
        with self._rlock:
            self._tic_command(f'"{channel}.pid.mode" {mode}')
            new_mode = self.channel_pid_mode(channel)
            if new_mode != mode:
                raise RuntimeError(f'Mode: set={mode}, get={new_mode}')

    def channel_pid_p(self, channel):
        return self._tic_request(f'"{channel}.pid.p?"', cast_fn=float)

    def channel_pid_i(self, channel):
        return self._tic_request(f'"{channel}.pid.i?"', cast_fn=float)

    def channel_pid_d(self, channel):
        return self._tic_request(f'"{channel}.pid.d?"', cast_fn=float)

    def channel_pid_setpoint(self, channel):
        return self._tic_request(f'"{channel}.pid.setpoint?"', cast_fn=float)

    def channel_pid_set_setpoint(self, channel, setpoint):
        """
        Sets the channel temperature setpoint.
        Returns the new setpoint.
        """
        with self._rlock:
            self._tic_command(f'"{channel}.pid.setpoint" {setpoint}')
            new_sp = self.channel_pid_setpoint(channel)
            if new_sp != setpoint:
                    raise RuntimeError(f'Setpoint: set={setpoint}, get={new_sp}')

    def channel_pid_ramp_t(self, channel):
        """
        Returns the working setpoint.
        """
        return self._tic_request(f'"{channel}.pid.rampt?"', cast_fn=float)

    def channel_pid_ramp(self, channel):
        return self._tic_request(f'"{channel}.pid.ramp?"', cast_fn=float)

    def channel_pid_set_ramp(self, channel, ramp):
        with self._rlock:
            self._tic_command(f'"{channel}.pid.ramp = {ramp}"')
            new_ramp = self.channel_pid_ramp(channel) 
            if new_ramp != ramp:
                raise RuntimeError(f'Ramp: set={ramp}, get={new_ramp}')

    def channel_status(self, channel):
        return self._tic_request(f'"{channel}.status?"', cast_fn=int)