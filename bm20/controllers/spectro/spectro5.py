import numpy as np

from bm20.controllers.spectro.spectro_dev import Detector, Analyzer



# RADIUS_M = 0.5  # ?
DET_DEG = 64.5 # ?


#######
# START Formulas from KK
#######

def xtal2det(radius_m, bragg_deg):
    """
    Distance of:
    - center of xtal analyzer <-> center of detector
    - sample <-> center of the xtal analyzer
    SC in KK AS 2016
    """
    return 2 * radius_m * np.sin(np.deg2rad(bragg_deg)) * 1000

def det_angle(bragg):
    return 2 * bragg

def samp2det(bragg_deg, a2x):
    return 2 * a2x * np.cos(np.deg2rad(bragg_deg))

def _xpi(bragg_deg, radius_m, y_pos):
    return np.sqrt((2 * radius_m * 1000.) ** 2 * np.sin(np.deg2rad(bragg_deg))**4 - y_pos**2)

def someval(bragg_deg, y_pos, xpi):
    """
    cell 5 crystals_edit:N30 to N34
    """
    return np.pi - np.arctan(y_pos / (xpi * np.sin(np.deg2rad(bragg_deg))))

def get_achi(someval):
    return np.rad2deg(someval) - 180.

def someval2(bragg_deg, y_pos, xpi):
    """
    cell 5 crystals_edit:N35 to N39
    """
    bragg_rad = np.deg2rad(bragg_deg)
    return np.arctan(
            np.sqrt(
                y_pos ** 2 + xpi ** 2 * np.sin(bragg_rad) ** 2
            ) /
            (xpi * np.cos(bragg_rad))
        )

def get_ath(someval2):
    return np.rad2deg(someval2)

def get_dth(bragg_deg):
    return 180 - np.rad2deg(np.pi * 3./2 - 2 * np.deg2rad(bragg_deg))

def detector_theta_2_bragg(dth):
    return np.rad2deg(-(np.deg2rad(180-dth) - 3/2.*np.pi)/2.)

def get_dlong(bragg_deg, s2d_mm, det_deg):
    return s2d_mm * np.cos(np.deg2rad(det_deg - bragg_deg))

def get_dx(bragg_deg, s2d_mm, det_deg):
    return s2d_mm * np.sin(np.deg2rad(det_deg - bragg_deg))

def get_ax(bragg_deg, radius_m, xpi):
    bragg_rad = np.deg2rad(bragg_deg)
    return 2 * radius_m * 1000 * np.sin(bragg_rad) * np.cos(bragg_rad) ** 2 + xpi * np.sin(bragg_rad)
    
def get_az(bragg_deg, radius_m, xpi):
    bragg_rad = np.deg2rad(bragg_deg)
    return 2 * radius_m * 1000 * np.cos(bragg_rad) * np.sin(bragg_rad) ** 2 - xpi * np.cos(bragg_rad)

#######
# END Formulas from KK
#######



from bm20.config.names import bm20names
class D20Analyzer(Analyzer):
    def __init__(self, config, **kwargs):
        super().__init__(config, **kwargs)
        self.ypos = self.config.config_dict["ypos"]

    def bragg2reals(self, bragg):
        """
        Returns a dictionary with all real motors positions
        for the given bragg angle (degrees).
        The dictionary keys are the motor tags.
        """
        y_pos = self.ypos
        xpi = _xpi(bragg, self.radius_m, y_pos)
        tmp0 = someval(bragg, y_pos, xpi)
        achi = get_achi(tmp0)
        tmp1 = someval2(bragg, y_pos, xpi)
        ath = get_ath(tmp1)
        ax = get_ax(bragg, self.radius_m, xpi)
        az = get_az(bragg, self.radius_m, xpi)
        reals_pos = {
            "chi": achi,
            "theta": ath,
            "xpos": ax,
            "zpos": az}
        return reals_pos
   

class D20Detector(Detector):
    def __init__(self, config, **kwargs):
        super().__init__(config, **kwargs)
        self.axtab = self.config.config_dict["axtab"]

    def _crystal_check(self):
        """
        Checks the axtab axis position:
        radius_m == 0.5m -> axtab should be 0
        radius_m == 1m -> axtab should be 500
        """
        ax_pos = self.axtab.position
        if self.radius_m == 0.5:
            if abs(ax_pos) > self.axtab.tolerance:
                raise RuntimeError(f"Analyzer {self.name}: "
                                   f"crystal radius is {self.radius_m}m but {self.axtab.name} "
                                   f"position is {ax_pos}. It should be 0.")
        if self.radius_m == 1:
            if abs(ax_pos - 500) > self.axtab.tolerance:
                raise RuntimeError(f"Analyzer {self.name}: "
                                   f"crystal radius is {self.radius_m}m but {self.axtab.name} "
                                   f"position is {ax_pos}. It should be 500.")
            
    def bragg2reals(self, bragg):
        """
        Returns a dictionary with all real motors positions
        for the given bragg angle (degrees).
        The dictionary keys are the motor tags.
        """
        #self._crystal_check()

        x2d = xtal2det(self.radius_m, bragg)
        sd = samp2det(bragg, x2d)
        dth = get_dth(bragg)
        dlong = get_dlong(bragg, sd, DET_DEG)
        dx = get_dx(bragg, sd, DET_DEG)
        
        reals_pos = {"theta": dth,
                     "dlong": dlong,
                     "dx": dx}
        return reals_pos
    