import numpy as np
from bliss.shell.getval import getval_idx_list, getval_yes_no
# from bliss.controllers.spectrometers.spectro_base import Analyser, Detector, Spectrometer
from bliss.physics.units import ur, units
from bliss.physics.diffraction import CrystalPlane, _get_all_crystals, MultiPlane, hc, string_to_crystal_plane, bragg_angle, bragg_energy
from bliss.controllers.motor import CalcController
# from bliss.physics.diffraction import MultiPlane, CrystalPlane, Ge, Si, hc, string_to_crystal_plane
from typing import NamedTuple, Any
from collections import namedtuple
from bliss.common.hook import MotionHook
from bliss.config.static import get_config
from bliss.config.settings import HashObjSetting
from tabulate  import tabulate
from collections import OrderedDict

from bliss.common.protocols import HasMetadataForScan


class CrystalInfo(NamedTuple):
    name: str
    crystal: CrystalPlane
    reflection: int = 1.
    radius_m: float = 0.5
    dspacing: float = 0


def crystal_info(crystal, dspacing_m=None, reflection=None, name=None, radius_m=None):
    """
    Returns a CrystalInfo.
    * crystal should be a string of the form Ge220, Si311, etc...
    * dspacing can be used to set a different dspacing than the one computed by
        the bliss.physics module.
    * reflection: order of reflection, default is 1
    * name: by default name == crystal
    """
    crystal = string_to_crystal_plane(crystal)
    if reflection is None:
        reflection = 1
    # if dspacing_m is not None:
    #     crystal.d = dspacing_m
    # else:
    if dspacing_m is None:
        dspacing_m = crystal.d
    if name is None:
        name = f"{crystal.crystal.name}{crystal.plane.tostring()}"
    return CrystalInfo(
            name = name,
            crystal = crystal,
            reflection = reflection,
            radius_m=radius_m,
            dspacing=dspacing_m)


class BraggLimitsHook(MotionHook):
    bragg_limits = property(lambda self: self._bragg_limits)

    def __init__(self, name):
        super().__init__()
        self._bragg_limits = None

    @bragg_limits.setter
    def bragg_limits(self, bragg_limits):
        # TODO : checks
        self._bragg_limits = bragg_limits

    # def pre_scan(self, axes_list):
    #     print("SCAN", axes_list)

    def pre_move(self, motion_list):
        if self.bragg_limits is not None:
            axis = motion_list[0].axis
            target = motion_list[0].target_pos
            low, high = self.bragg_limits
            if target < low:
                raise RuntimeError(f"Target {target} is below the bragg limit {low} for axis {axis.name}")
            if target > high:
                raise RuntimeError(f"Target {target} is above the bragg limit {high} for axis {axis.name}")


class SpectroItem(CalcController, HasMetadataForScan):
    """
    A spectrometer item (Analyzer or Detector).
    Inherit from this and overload the bragg2reals method.
    The last target (bragg angle) is stored into beacon
    and used to check alignment after a restart.
    """
    bragg_axis = property(lambda self: self._tagged["bragg"][0])
    energy_axis = property(lambda self: self._tagged["energy"][0])
    crystal = property(lambda self: self._crystal)
    energy = property(lambda self: self.energy_axis.position)
    bragg = property(lambda self: self.bragg_axis.position)
    bragg_limits = property(lambda self: self._bragg_limits)
    radius_m = property(lambda self: self.crystal.radius_m)
    # crystals = property(lambda self: self._crystals)

    def __init__(self, *args, **kwargs):
        self._spectro_item_type = kwargs.get("_spectro_item_type", "Spectro Item")
        super().__init__(*args, **kwargs)
        self._crystal = None
        self._current_target = {}
        self._spectro_settings = None
        self._bragg_lims = None
        # self._crystals = config.get("crystals")

    def __info__(self):

        aligned = self.is_aligned()

        txt = "====================\n"
        txt += f"{self._spectro_item_type}: {self.name}\n"
        txt += "====================\n"
        if not aligned:
            txt += "\nWARNING!\n"
            txt += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
            txt += "!!! SPECTRO IS NOT ALIGNED!!!\n"
            txt += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
            txt += "WARNING!\n\n"

        txt += f"* Aligned: {aligned}\n"
        crystal = self.crystal
        txt += f"* Crystal:\n"
        txt += f"  - name:       {crystal.name}\n"
        txt += f"  - radius:     {crystal.radius_m} m\n"
        txt += f"  - neflection: {crystal.reflection}\n"
        txt += f"  - dspacing:   {crystal.crystal.d}.\n\n"

        txt += "* Reals:\n   - "
        txt += "\n  - ".join([f"{ax.name} @ {ax.position}" for ax in self.reals])
        txt += f"\n* Energy: {self.energy_axis.name} @ {self.energy}\n"
        txt += f"* Bragg: {self.bragg_axis.name} @ {self.bragg}\n"
        txt += "====================\n"
        return txt

    @property
    def _settings(self):
        if self._spectro_settings is None:
            self._spectro_settings = HashObjSetting(f"{self.name}_settings")
        return self._spectro_settings

    def _init(self):
        super()._init()
        self._current_target = self._settings.get("current_target", {})
        self.sync_pseudos()
        hook = BraggLimitsHook(f"{self.name}_hook")
        hook._add_axis(self.bragg_axis)
        self.bragg_axis.motion_hooks.append(hook)
        self.energy_axis.motion_hooks.append(hook)
        self._bragg_hook = hook

    @crystal.setter
    def crystal(self, crystal):
        """
        Currently used crystal
        """
        self._crystal = crystal
        self.sync_pseudos()

    @bragg_limits.setter
    def bragg_limits(self, bragg_limits):
        assert np.all(np.isfinite(bragg_limits))
        self._bragg_limits = bragg_limits
        self._bragg_hook.bragg_limits = bragg_limits

    def is_aligned(self):
        """
        True if the item is "aligned", i.e: pseudos and reals match.
        """
        return np.isfinite(self.bragg_axis.position)

    def calc_from_real(self, real_pos):
        """
        calc positions are nan if the current target (bragg) doesnt
        match the real positions.
        """
        bragg = np.nan
        energy = np.nan
        if self._crystal and self._current_target:
            reals_targ = self._current_target["reals"]
            inpos = [abs(reals_targ[name] - position) <= 0.005 #self._tagged[name][0].tolerance
                    for name, position in real_pos.items()]
            if np.all(inpos):
                bragg = self._current_target["bragg"]
                # energy = self._crystal.crystal.bragg_energy(bragg * ur.deg,
                #                                              n=self._crystal.reflection)
                energy = bragg_energy(bragg * ur.deg,
                                      d=self._crystal.dspacing,
                                      n=self._crystal.reflection)
                energy = energy.to(ur.eV).magnitude
        calc_pos = {"bragg": bragg, "energy": energy}

        # workaround the fact that set position of the 2nd calc axis
        # is not updated when moving the first calc
        self.bragg_axis._set_position = bragg
        self.energy_axis._set_position = energy
        return calc_pos

    def calc_to_real(self, calc_pos):
        e_move, b_move = False, False
        energy = calc_pos["energy"]
        bragg = calc_pos["bragg"]

        # checks first if the user is moving energy or bragg.
        e_finite = np.all(np.isfinite(energy))
        b_finite = np.all(np.isfinite(bragg))

        if not b_finite and not e_finite:
            raise RuntimeError("E abd B can't both be not finite.")

        if e_finite and not b_finite:
            e_move = True
        elif b_finite and not e_finite:
            b_move = True
        else:
            b_move = np.any(np.abs(self.bragg - bragg) >= self.bragg_axis.tolerance)
            e_move = np.any(np.abs(self.energy - energy) >= self.energy_axis.tolerance)

        if b_move and e_move:
            # is this possible at all?
            raise RuntimeError("Cant move E and B at the same time.")

        if e_move:
            # bragg = self._crystal.crystal.bragg_angle(energy * ur.eV,
            #                                                   n=self._crystal.reflection)
            bragg = bragg_angle(energy * ur.eV,
                                  d=self._crystal.dspacing,
                                  n=self._crystal.reflection)
            bragg = bragg.to(ur.deg).magnitude

        reals_pos = self.bragg2reals(bragg)
        self._current_target = {"bragg": bragg,
                                "reals": reals_pos.copy()}
        self._settings["current_target"] = self._current_target
        return reals_pos

    def bragg2reals(self, bragg):
        """
        Returns a dictionary with all real motors positions
        for the given bragg angle (degrees).
        The returned dictionary keys are the pseudo motor tags as written
        in the yaml.
        """
        raise NotImplementedError()

    def scan_metadata(self):
        crystal = self.crystal
        metadata = {}
        metadata["@NX_class"] = "NXcollection"
        metadata["crystal"] = {"name": crystal.name,
                               "dspacing": crystal.dspacing,
                               "reflection": crystal.reflection,
                               "radius": crystal.radius_m }
        return metadata


class Analyzer(SpectroItem):
    """
    A spectrometer analyzer.
    Overload Analyzer.bragg2reals to fit your needs.
    """

    def __init__(self, config, **kwargs):
        super().__init__(config, _spectro_item_type="Analyzer", **kwargs)


class Detector(SpectroItem):
    """
    A spectrometer detector.
    Overload Detector.bragg2reals to fit your needs.
    """
    def __init__(self, config, **kwargs):
        super().__init__(config, _spectro_item_type="Detector", **kwargs)


class Spectro(CalcController, HasMetadataForScan):
    """
    A spectrometer.
    By default, the energy of the spectro == the energy of the detector.
    Overload calc_to_real and calc_from_real to change that if needed.

    This object has a crystal property that returns the CrystalInfo
        that is currently used.
    This object has a set_crystal method to change the crystal.

    The crystal is stored in beacon and restored.
    """
    energy_axis = property(lambda self: self._tagged["energy"][0])
    bragg_axis = property(lambda self: self._tagged["bragg"][0])
    energy = property(lambda self: self.energy_axis.position)
    bragg = property(lambda self: self.bragg_axis.position)
    crystal = property(lambda self: self._crystal)
    bragg_limits = property(lambda self: self._bragg_limits)
    radius_m = property(lambda self: self.crystal.radius_m)
    analyzers = property(lambda self: list(self._analyzers.values()))
    detector = property(lambda self: self._detector)

    def __init__(self, config, **kwargs):

        analyzers = config.get("analyzers")
        detector = config.get("detector")

        self._energy_tol = config.get("energy_tol", 0.01)

        det_axis = detector.bragg_axis
        ana_axes = [{"name": f"${ana.bragg_axis.name}",
                     "tags": f"real {ana.bragg_axis.name} analyzer"}
                     for ana in analyzers]
        det_axis = {"name": f"${detector.bragg_axis.name}",
                    "tags": f"real {detector.bragg_axis.name} detector"}
        config.get("axes").extend(ana_axes)
        config.get("axes").extend([det_axis])

        super().__init__(config, **kwargs)

        # TODO: check that all analyzers are the same

        self._crystal = None
        self._spectro_settings = None
        # AnasNt = namedtuple("Analyzers", [ana.name for ana in analyzers])
        # self._analyzers = AnasNt(*[ana for ana in analyzers])
        self._analyzers = {ana.name: ana for ana in analyzers}
        self._detector = detector
        self._bragg_limits = [np.nan, np.nan]

    def _init(self):
        super()._init()
        crystal_name = self._settings.get("crystal_name")
        dspacing = self._settings.get("dspacing")
        reflection = self._settings.get("reflection")
        radius_m = self._settings.get("radius_m")
        bragg_limits = self._settings.get("bragg_limits")

        if crystal_name is None:
            crystal = self.config.config_dict["crystal"]
            crystal_name = crystal["type"]
            dspacing = crystal.get("dspacing")
            reflection = crystal.get("reflection")
            radius_m = crystal.get("radius_m")

        self.set_crystal(crystal_name, dspacing=dspacing,
                         reflection=reflection, radius_m=radius_m)

        if bragg_limits is None:
            bragg_low = self.config.config_dict["bragg_low_limit"]
            bragg_high = self.config.config_dict["bragg_high_limit"]
            bragg_limits = bragg_low, bragg_high
        self.bragg_limits = bragg_limits

        self.sync_pseudos()

    def scan_metadata(self):
        metadata = {}
        for item in self.analyzers:
            metadata[item.name] = item.scan_metadata()
        detector = self.detector
        metadata[detector.name] = detector.scan_metadata()
        return metadata

    def __info__(self):
        anas = self._analyzers.values()
        det = self._detector

        reals_ana = OrderedDict({ana.name: {ana._axis_tag(axis): axis.position
                                            for axis in ana.reals}
                                for ana in anas})
        reals_det = OrderedDict({det._axis_tag(axis): axis.position
                                 for axis in det.reals})

        for ana in anas:
            bragg = ana.bragg_axis.position
            energy = ana.energy_axis.position
            aligned = ana.is_aligned()
            reals_ana[ana.name]["bragg"] = bragg
            reals_ana[ana.name]["energy"] = energy
            reals_ana[ana.name]["aligned"] = "Yes" if aligned else "No"

        txt = "====================\n"
        txt += f"Spectro [{self.name}]:\n"
        txt += "====================\n"

        aligned = self.is_aligned()

        if not aligned:
            txt += "\nWARNING!\n"
            txt += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
            txt += "!!! SPECTRO IS NOT ALIGNED!!!\n"
            txt += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
            txt += "WARNING!\n\n"

        aligned = "Yes" if aligned else "No"
        low, high = self.bragg_limits

        txt += f"Aligned: {aligned}.\n"
        txt += f"Energy: {self.energy:.2f} eV\n"
        txt += f"Bragg: {self.bragg:.3f} deg\n\n"
        txt += f"E tol (alignment): {self._energy_tol} eV\n"
        txt += f"Bragg limits: [{low}, {high}]\n\n"

        crystal = self.crystal
        txt += f"* Crystal:\n"
        txt += f"  - name:       {crystal.name}\n"
        txt += f"  - radius:     {crystal.radius_m} m\n"
        txt += f"  - reflection: {crystal.reflection}\n"
        txt += f"  - dspacing:   {crystal.dspacing}.\n\n"

        txt += "====================\n"
        txt += "Analyzers:\n"
        txt += "====================\n"
        txt += tabulate(reals_ana.values(),
                        showindex=reals_ana.keys(),
                        headers="keys",
                        floatfmt=".3f")
        txt += "\n\n====================\n"
        txt += "Detector:\n"
        txt += "====================\n"
        aligned = "Yes" if det.is_aligned() else "No"
        bragg = det.bragg
        energy = det.energy
        reals_det["braggg"] = bragg
        reals_det["energy"] = energy
        reals_det["aligned"] = aligned
        txt += tabulate([reals_det.values()],
                                showindex=[det.name],
                                headers=reals_det.keys(),
                                floatfmt=".3f")
        txt += "\n\n====================\n"
        return txt

    @property
    def _settings(self):
        if self._spectro_settings is None:
            self._spectro_settings = HashObjSetting(f"{self.name}_settings")
        return self._spectro_settings

    def calc_from_real(self, real_pos):
        ana_bragg = [ax.position for ax in self._tagged["analyzer"]]
        energy = np.nan
        bragg = np.nan
        if (self._crystal and
            np.all(np.isfinite(ana_bragg)) and
            np.isfinite(self._tagged["detector"][0].position)):
            # energy = self._crystal.crystal.bragg_energy(ana_bragg[0] * ur.deg,
            #                                             n=self._crystal.reflection)
            energy = bragg_energy(ana_bragg[0] * ur.deg,
                                  d=self._crystal.dspacing,
                                  n=self._crystal.reflection)
            energy = energy.to(ur.eV).magnitude
            bragg = ana_bragg[0]
        self.bragg_axis._set_position = bragg
        self.energy_axis._set_position = energy
        return {"bragg": bragg, "energy": energy}

    def calc_to_real(self, calc_pos):
        e_move, b_move = False, False
        energy = calc_pos["energy"]
        bragg = calc_pos["bragg"]

        # checks first if the user is moving energy or bragg.
        e_finite = np.all(np.isfinite(energy)) 
        b_finite = np.all(np.isfinite(bragg)) 

        if not b_finite and not e_finite:
            raise RuntimeError("E abd B can't both be not finite.")

        if e_finite and not b_finite:
            e_move = True
        elif b_finite and not e_finite:
            b_move = True
        else:
            b_move = np.any(np.abs(self.bragg - bragg) >= self.bragg_axis.tolerance)
            e_move = np.any(np.abs(self.energy - energy) >= self.energy_axis.tolerance)

        if b_move and e_move:
            # is this possible at all?
            raise RuntimeError("Cant move E and B at the same time.")

        if e_move:
            # bragg = bragg = self._crystal.crystal.bragg_angle(energy * ur.eV,
            #                                                   n=self._crystal.reflection)
            bragg = bragg_angle(energy * ur.eV,
                                  d=self._crystal.dspacing,
                                  n=self._crystal.reflection)
            bragg = bragg.to(ur.deg).magnitude

        bragg = bragg_angle(energy * ur.eV,
                            d=self._crystal.dspacing,
                            n=self._crystal.reflection,)
        bragg = bragg.to(ur.deg).magnitude
        real_pos = {ax.name: bragg for ax in self.reals}
        return real_pos

    def set_crystal(self, crystal_name, dspacing=None, reflection=None, radius_m=None):
        """
        Sets the crystal used by this spectrometer.
        * crystal should be a string of the form Ge220, Si311, etc...
            OR an instance of CrystalInfo
        * IN METERS! dspacing can be used to set a different dspacing than the one computed by
            the bliss.physics module.
        * reflection: order of reflection, default is 1
        """
        if isinstance(crystal_name, (CrystalInfo,)):
            dspacing = dspacing or crystal_name.dspacing
            radius_m = radius_m or crystal_name.radius_m
            reflection = reflection or crystal_name.radius_m
            crystal_name = crystal_name.name
        elif radius_m is None:
            raise ValueError("Please provide radius_m.")

        crystal = crystal_info(crystal_name,
                               dspacing_m=dspacing,
                               reflection=reflection,
                               radius_m=radius_m)
        for analyzer in self._analyzers.values():
            analyzer.crystal = crystal
        self._detector.crystal = crystal
        self._crystal = crystal

        self._settings["crystal_name"] = crystal_name
        self._settings["dspacing"] = dspacing
        self._settings["reflection"] = reflection
        self._settings["radius_m"] = radius_m

    @bragg_limits.setter
    def bragg_limits(self, bragg_limits):
        assert np.all(np.isfinite(bragg_limits))

        for ana in self._analyzers.values():
            ana.bragg_limits = bragg_limits
        self._detector.bragg_limits = bragg_limits
        self._settings["bragg_limits"] = bragg_limits
        self._bragg_limits = bragg_limits

    # @radius_m.setter
    # def radius_m(self, radius_m):
    #     for ana in self._analyzers.values():
    #         ana.radius_m = radius_m
    #     self._detector.radius_m = radius_m
    #     self._settings["radius_m"] = radius_m
    #     self._radius_m = radius_m

    def bragg2reals(self, bragg):
        reals_ana = OrderedDict({ana.name: {key: value for key, value in ana.bragg2reals(bragg).items()}
                                 for ana in self._analyzers.values()
                                  })
        reals_det = {self._detector.name: self._detector.bragg2reals(bragg)}
        return reals_ana, reals_det

    def is_aligned(self):
        anas_aligned = np.all([ana.is_aligned() for ana in self._analyzers.values()])
        det_aligned = self._detector.is_aligned()
        aligned = anas_aligned and det_aligned
        if aligned:
            ana_e = [ana.energy for ana in self._analyzers.values()]
            return np.all(np.isclose(ana_e, self._detector.energy,
                                     rtol=0, atol=self._energy_tol))
        return False

    def b_positions(self, bragg):
        """
        Returns the motor positions for a given bragg (degrees)
        """
        # try:
        #     bragg = bragg.to(ur.deg)
        # except AttributeError:
        #     bragg = bragg * ur.deg
        reals_ana, reals_det = self.bragg2reals(bragg)
        txt = "====================\n"
        txt += "Analyzers:\n"
        txt += "====================\n\n"
        txt += tabulate(reals_ana.values(),
                        showindex=reals_ana.keys(),
                        headers="keys")
        txt += "\n\n====================\n"
        txt += "Detector:\n"
        txt += "====================\n"
        txt += "\n" + tabulate(reals_det.values(),
                               showindex=reals_det.keys(),
                               headers="keys")
        txt += "\n\n====================\n"
        print(txt)

    def e_positions(self, energy):
        """
        Returns the motor positions for a given energy
        """
        try:
            energy = energy.to(ur.eV)
        except AttributeError:
            energy = energy * ur.eV
        bragg = bragg_angle(energy,
                                                  d=self._crystal.dspacing,
                                                  n=self._crystal.reflection)
        # crystal = self._crystal
        # bragg = np.arcsin(6.19926*crystal.reflection/(crystal.dspacing*1e10)/(energy.magnitude/1000))
        # bragg *= 180 / np.pi
        bragg = bragg.to(ur.deg).magnitude
        self.b_positions(bragg)

    def set_energy(self, energy):
        try:
            energy = energy.to(ur.eV)
        except AttributeError:
            energy = energy * ur.eV
        bragg = bragg_angle(energy,
                            d=self._crystal.dspacing,
                            n=self._crystal.reflection)
        reals_ana, reals_det = self.bragg2reals(bragg.to(ur.deg).magnitude)

        index = ["Current", "Target", "Diff"]

        txt = "====================\n"
        txt += "Analyzers:\n"
        txt += "====================\n\n"

        for ana_name, ana_pos in reals_ana.items():
            ana = self._analyzers[ana_name]
            info_tab = []
            for mot_name, target in ana_pos.items():
                axis = ana._tagged[mot_name][0]
                current = axis.position
                info_tab += [[current, target, current - target]]

            tab = tabulate(np.array(info_tab).T,
                           showindex=index,
                           headers=ana_pos.keys()).split("\n")
            txt += f"* {ana_name}\n"
            txt += "    " + "\n    ".join(tab)
            txt += "\n\n"

        txt += "\n\n====================\n"
        txt += "Detector:\n"
        txt += "====================\n"
            
        detector = self._detector
        det_pos = reals_det[detector.name]
        
        info_tab = []
        for mot_name, target in det_pos.items():
            axis = detector._tagged[mot_name][0]
            current = axis.position
            info_tab += [[current, target, current - target]]

        tab = tabulate(np.array(info_tab).T,
                        showindex=index,
                        headers=det_pos.keys()).split("\n")
        txt += f"* {detector.name}\n"
        txt += "    " + "\n    ".join(tab)
        txt += "\n\n"

        print(txt)

        if not getval_yes_no("Continue?"):
            print("Aborted.")
            return
        
        for ana_name, ana_pos in reals_ana.items():
            ana = self._analyzers[ana_name]
            for mot_name, target in ana_pos.items():
                axis = ana._tagged[mot_name][0]
                axis.position = target
        for mot_name, target in det_pos.items():
            axis = detector._tagged[mot_name][0]
            axis.position = target

    def apply_config(self, reload=False):
        if reload:
            crystal = self.config.config_dict["crystal"]
            crystal_name = crystal["type"]
            dspacing = crystal.get("dspacing")
            reflection = crystal.get("reflection")
            radius_m = crystal.get("radius_m")
            self.set_crystal(crystal_name,
                             dspacing=dspacing,
                             reflection=reflection,
                             radius_m=radius_m)
            bragg_low = self.config.config_dict["bragg_low_limit"]
            bragg_high = self.config.config_dict["bragg_high_limit"]
            self.bragg_limits = bragg_low, bragg_high
        else:
            self._init()