import time
import enum
import numpy as np
from typing import NamedTuple
from functools import partial
from contextlib import contextmanager

from bliss import global_map
from bliss.comm.util import get_comm
from bliss.common.utils import autocomplete_property
from bliss.common.protocols import HasMetadataForScan
from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.controllers.counter import (SamplingCounterController,
                                       IntegratingCounterController,
                                       SamplingCounterAcquisitionSlave)
from bliss.controllers.bliss_controller import BlissController


class I404Error(RuntimeError):
    def __init__(self,
                 i404_name,
                 i404_cmd,
                 i404_err_code,
                 i404_error,
                 *args,
                 **kwargs):
        super().__init__(i404_error.decode(), *args, **kwargs)
        self.i404_name = i404_name
        self.i404_cmd = i404_cmd
        self.i404_err_code = i404_err_code
        self.i404_error = i404_error

    def __str__(self) -> str:
        return f"device {self.i404_name}, error {self.i404_err_code}, " \
               f"{self.i404_error}]"


# limiting the sampling freq, not sure how much the serial line can handle
MAX_SAMPLING_FREQ = 50


class TRIG_SOURCE(enum.Enum):
    INTERNAL = "internal"
    EXTERNAL_START = "external_start"


BLADE_LAYOUTS = ["fourblade", "slitblade", "noblade"]


def unit2ampsfn(unit):
    if unit == "pA":
        return lambda val: val * 1e-12
    if unit == "nA":
        return lambda val: val * 1e-9
    if unit == "uA":
        return lambda val: val * 1e-6
    if unit == "mA":
        return lambda val: val * 1e-3
    raise RuntimeError(f"Unsupported unit: {unit}")


def amps2unitfn(unit):
    if unit == "pA":
        return lambda val: val * 1e12
    if unit == "nA":
        return lambda val: val * 1e9
    if unit == "uA":
        return lambda val: val * 1e6
    if unit == "mA":
        return lambda val: val * 1e3
    raise RuntimeError(f"Unsupported unit: {unit}")


class I404Device:
    """
    Pyramid I404 communication code.

    Expected reply format: <cmd>\n<reply>\r\n

    Tested on I404, Internal trigger only.
    As of May 2023 I400 support has not been tested.

    WARNING: device comm needs to be configured as follow:
    - terminal mode on
    - checksum off
    """

    TRIG_SOURCE = TRIG_SOURCE

    name = property(lambda self: self._name)

    def __init__(self, name, config, layout="noblade"):
        self._name = name
        self._config = config
        self._idn = None
        self._model = None
        self._comm = get_comm(config)
        self._last_data_counter = 0
        # last read data: integration time, 4 x values, overflow, cs, cx, cy
        self._last_data = np.repeat(np.nan, 9)
        self._last_calc_data = np.repeat(np.nan, 3)
        self._busy_timeout = None
        self._layout = layout

    @contextmanager
    def busy_retry_ctx(self, busy_timeout=5):
        """
        Dirty way of retrying commands until the device is ready.
        There should be some register to check, but I can't seem
        to get it to work.
        """
        if self._busy_timeout is not None:
            raise RuntimeError("This ctx must only be called once at a time.")
        self._busy_timeout = busy_timeout
        try:
            yield
        except:
            raise
        finally:
            self._busy_timeout = None

    def send_cmd(self, str_cmd, flush=True, nb_lines=2, has_ok=False):
        """
        Sends a command, and parse the reply, if any.
        str_cmd: the command to send, str (will be encoded as byte)
        flush: flush the line before sending
        nb_lines: for i400 compatibility, see has_ok
        has_ok: this is for i400 compatibility. On the i400 the READ:CURR
                doesnt return the same reply as the i404, but this:
                <cmd>\nOK\n<reply>

        Expected reply format: <cmd>\n<reply>\r\n
        """
        if flush:
            self._comm.flush()

        t_start = time.monotonic()

        b_cmd = str_cmd.encode()
        if not b_cmd.endswith(b"\n"):
            b_cmd += b"\n"
        
        while True:
            b_reply = self._comm.write_readlines(b_cmd, nb_lines=nb_lines)
            # reply is made of the cmd + \n + reply + \r\n
            reply = [elem.decode().strip("\r") for elem in b_reply]
            if len(reply) != nb_lines or reply[0] != str_cmd:
                raise RuntimeError(f"Unexpected reply to the command [{b_cmd}]:"
                                f" [{b_reply}].")
            
            # if this is not a query then the device should reply OK.
            if (has_ok or reply[0][-1] != "?") and reply[1] != "OK":
                i404_err_code, i404_error = b_reply[1].split(b',', 1)
                if (self._busy_timeout is None or time.monotonic() - t_start >= self._busy_timeout
                    or b"Device busy" not in i404_error):
                    raise I404Error(self.name, b_cmd, int(i404_err_code), i404_error)
                print(f"Device busy, retrying command {b_cmd} (timeout=self._busy_timeout).")
                continue

            return reply[-1]
    
    def abort(self):
        """
        Aborts the running measurement
        """
        self.send_cmd("ABORT")

    def start_acq(self):
        # TODO: check if an acq is already running
        self.trig_points = 0
        if self.integrating:
            raise RuntimeError(f"Device {self.name} is already integrating.")
        self._last_data_counter = 0
        self.send_cmd("INIT")

    def calibrate(self, reset=False):
        """
        Start a calibration or reset the gains to nominal.
        """
        self.trig_points = 1
        if reset:
            self.send_cmd("CALIB:GAIN CLE")
        else:
            self.send_cmd("CALIB:GAIN")

    def reset(self):
        return self.send_cmd("*RST")
    
    @property
    def calib_source(self):
        return self.send_cmd("CALIB:SOUR?")
    
    @calib_source.setter
    def calib_source(self, source):
        self.send_cmd(f"CALIB:SOUR {source}")
    
    @property
    def status_oper_cond(self):
        return int(self.send_cmd("STAT:OPER:COND?"))

    @property
    def gains(self):
        gains = self.send_cmd("CALIB:GAI?").split(',')
        return [float(g) for g in gains]

    @property
    def integrating(self):
        """
        Checking bit 16 to see if an integration is already running
        """
        return self.status_oper_cond & 16 == 16
    
    @property
    def idn(self):
        """
        Device description
        """
        if self._idn is None:
            self._idn = self.send_cmd("*IDN?")
        return self._idn
    
    @property
    def model(self):
        """
        Device model, 
        """
        if self._model is None:
            idn = self.idn
            self._model = idn.split(",")[1][0:4]
        return self._model

    @property
    def capacitor(self):
        capa = self.send_cmd("CONF:CAP?").split(",")
        return int(capa[0]), float(capa[1].rstrip(" fF"))
    
    @capacitor.setter
    def capacitor(self, cap):
        self.send_cmd(f"CONF:CAP {cap}")

    @property
    def range_amps(self):
        if self.model == "I400":
            rng = self.send_cmd("CONF:GAT:INT:RANG?")
            rng = rng.split(",")[0]
        else:
            rng = self.send_cmd("CONF:RANG?")
        return float(rng.rstrip(" A"))
    
    @range_amps.setter
    def range_amps(self, rng):
        if self.model == "I400":
            rng = self.send_cmd(f"CONF:GAT:INT:RANG {rng}")
        else:
            self.send_cmd(f"CONF:RANG {rng}")

    @property
    def period(self):
        if self.model == "I404":
            period = self.send_cmd(f"CONF:PERIOD?")
        else:
            # I400 returns the period + number of samples
            period = self.send_cmd(f"CONF:GAT:INT:PER?").split(",")[0]
        return float(period.rstrip(" Ss"))
    
    @period.setter
    def period(self, period):
        """
        Set the period.

        Note: I404 has different commands from the other models.
        """
        if self.model == "I404":
            self.send_cmd(f"CONF:PERIOD {period}")
        else:
            self.send_cmd(f"CONF:GAT:INT:PER {period}")
        
    @property
    def samp_avg(self):
        if self.model == "I404":
            samp_avg = self.send_cmd(f"CONF:READavg?")
        else:
            # I400 returns the period + number of samples
            samp_avg = self.send_cmd(f"CONF:GAT:INT:PER?").split(",")[1]
        return int(samp_avg)

    @samp_avg.setter
    def samp_avg(self, samp_avg):
        if self.model == "I404":
            self.send_cmd(f"CONF:READavg {samp_avg}")
        else:
            # I400 returns the period + number of samples
            period = self.period
            self.send_cmd(f"CONF:GAT:INT:PER {period} {samp_avg}")
        return int(samp_avg)

    @property
    def trig_points(self):
        reply = self.send_cmd("TRIG:POIN?")
        return np.inf if reply.startswith("INF") else int(reply)
    
    @trig_points.setter
    def trig_points(self, trig_points):
        if trig_points <= 0 or trig_points == np.inf:
            cmd = "TRIG:POIN INF"
        else:
            cmd = f"TRIG:POIN {trig_points}"
        self.send_cmd(cmd)

    @property
    def trig_source(self):
        return TRIG_SOURCE[self.send_cmd("TRIG:SOUR?")]
    
    @trig_source.setter
    def trig_source(self, trig_source: TRIG_SOURCE):
        cmd = f"TRIG:SOURCE {trig_source.value}"
        self.send_cmd(cmd)

    @property
    def count(self):
        return self.send_cmd("TRIG:COUN?")
    
    def read_curr(self):
        if self.model == "I400":
            data = self.send_cmd("READ:CURR?", nb_lines=3, has_ok=True)
        else:
            data = self.send_cmd("READ:CURR?")
        return np.array([float(v.rstrip("AS")) for v in data.split(",")])
    
    @property
    def last_data(self):
        """
        Returns the last available data. Data is updated only
        if the counter has been incremented since last call,
        otherwise it returns the same data as the previous call.
        Returns a tuple: counter + READ:CURRent data
            [integration time, 4 values, overflow]
        """
        count = self.count
        if count != self._last_data_counter:
            self._last_data_counter = count
            last = self.read_curr()
            # checking overflow, 0-15, 1 bit per channel
            oflow = np.unpackbits(np.uint8(last[5]))[4:]
            last[1:5][oflow==1] = np.nan

            self._last_data[0:6] = last

            cx = np.nan
            cy = np.nan

            if self._layout == "fourblade":
                c1, c2, c3, c4 = last[1:5]
                cs = c1 + c2 + c3 + c4
                if cs != 0:
                    cx = ((c2+c3)-(c1+c4))/cs
                    cy = ((c1+c2)-(c3+c4))/cs
            elif self._layout == "slitblade":
                # code straight from the old i400 macros
                c1, c2, c3, c4 = np.fabs(last[1:5])
                cs = c1 + c2 + c3 + c4
                if cs != 0:
                    if c1 > 1e-12 and c2 > 1e-12:
                        cy = (c2 - c1) / (c2 + c1)
                    if c1 > 1e-12 and c2 > 1e-12:
                        cx = (c4 - c3) / (c4 + c3)
                else:
                    cx = np.nan
                    cy = np.nan
            else:
                cs = c1 + c2 + c3 + c4

            self._last_data[6:] = cs, cx, cy

        return self._last_data_counter, self._last_data
    

def channel2index(channel):
    return {'a':1, 'b':2, 'c':3, 'd':4, "ov": 5,
            "cs": 6, "cx": 7, "cy":8}.get(channel.lower())


class I404Counter(SamplingCounter):

    # channel index in the data array (1, 2, 3 or 4)
    channel_idx = property(lambda self: self._channel_idx)

    def __init__(
        self,
        name,
        controller,
        channel,
        conversion_function=None,
        mode=SamplingMode.MEAN,
        unit=None,
        **kwargs
    ):
        if channel.lower() not in ["a", "b", "c", "d", "cs"]:
            unit = None
        super().__init__(
            name, controller, conversion_function, mode, unit
        )
        self.channel = channel
        # attention if for some reason you change this to a
        # zero base index, you have to change the I404CC::read method
        try:
            self._channel_idx = channel2index(channel)
        except KeyError:
            raise RuntimeError(f"{name}: unknown channel type {channel}.")


class I404CC(SamplingCounterController):
    bctrl = property(lambda self: self._bctrl)

    def __init__(self, name, bctrl):
        super().__init__(name)
        self._bctrl = bctrl

    def read(self, counter):
        # last_data returns counter number + READ:CURRENT
        return self._bctrl.hardware.last_data[1][counter.channel_idx]
    
    def read_all(self, *counters):
        values = []
        last_data = self._bctrl.hardware.last_data[1]
        for cnt in counters:
            values.append(last_data[cnt.channel_idx])
        return values
    
    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):
        return I404SCAS(self, ctrl_params=ctrl_params, **acq_params)


class I404SCAS(SamplingCounterAcquisitionSlave):
    def prepare_device(self):
        period = self.device.bctrl.hardware.period

        if self.count_time < period:
            raise ValueError(
                f"count_time cannot be smaller than I404 integration_time "
                f"{period}."
            )

        self.device.max_sampling_frequency = max(MAX_SAMPLING_FREQ,
                                                 1. / period)
        self.device.bctrl.hardware.start_acq()

    # def trigger(self):
    #     self.device.ah401.dump_data()
    #     super().trigger()

    def stop_device(self):
        self.device.bctrl.hardware.abort()


class I404(BlissController, HasMetadataForScan):
    """
    Config:
    - name: bpm3
      plugin: generic
      package: bm20.controllers.i404
      class: I404
      tcp:
          url: netcom:2051
          timeout: 5

      layout: fourblade # or slitblade, noblade
      unit: nA # default: A
      range: 50 # same unit as the unit setting
      # capacitor: 1
      # period: 0.01 # seconds
      # samples: 1
      counters:           
        - name: bpm3d1
          channel: a
        - name: bpm3d2
          channel: b
          unit: nA
        - name: bpm3d3
          channel: c
          unit: nA
        - name: bpm3d4
          channel: d
          unit: nA
        - name: bpm3s
          channel: cs
          unit: nA
        - name: bpm3x
          channel: cx
        - name: bpm3y
          channel: cy
        - name: bpm3it
          channel: it
        - name: bpm3ov
          channel: ov

    IMPORTANT:
    - range is in Ampers
    - units: nA, pA, A
    - if present, parameters are applied in that order:
      1. gain (changes capa, period and samples)
      2. capacitor (changes period)
      3. period (changes gain)
      4. samples
    """

    def __init__(self, config):
        super().__init__(config)
        self._hw_controller = None
        self._scc = I404CC(self.name, self)
        
        rng = config.get("range") 
        capacitor = config.get("capacitor")
        samples = config.get("samples")
        period = config.get("period")
        unit = config.get("unit", "A")
        self._layout = config.get("layout", "noblade")
        assert self._layout in BLADE_LAYOUTS

        global_map.register(self,
                            parents_list=["counters"],
                            children_list=[self.hardware])

        with self.hardware.busy_retry_ctx():
            if rng is not None:    
                rng = unit2ampsfn(unit)(rng)
                self.hardware.range_amps = rng
            if capacitor is not None:
                self.hardware.capacitor = capacitor
            if period is not None:
                self.hardware.period = period
            if samples is not None:
                self.hardware.samples = samples
        
    @autocomplete_property
    def hardware(self):
        if self._hw_controller is None:
            self._hw_controller = I404Device(self.name, self.config, self._layout)
        return self._hw_controller

    def __info__(self):
        info = super().__info__()

        info += f"\nDevice: {self.hardware.idn}"
        info += "\nRange:"
        range = self.hardware.range_amps
        info += "\nCurrent settings:"
        info += f"\n   - Range {range} A."
        capa = self.hardware.capacitor
        capapf = capa[1] / 10e-12
        capatype = "large" if capa[0] else "small"
        info += f"\n   - Capacitor: {capatype} ({capapf:.1f} pF)"
        info += f"\n   - Period:    {self.hardware.period} s"
        info += f"\n   - Samples: {self.hardware.samp_avg}"
        return info
    
    def scan_metadata(self):
        meta = {"idn": self.hardware.idn,
                "range": self.hardware.range_amps,
                "capacitor": self.hardware.capacitor,
                "period": self.hardware.period}
        return meta

    def _get_subitem_default_class_name(self, cfg, parent_key):
        if parent_key == "counters":
            return "I404Counter"
        else:
            raise NotImplementedError
        
    def _create_subitem_from_config(self,
                                    name,
                                    cfg,
                                    parent_key,
                                    item_class,
                                    item_obj=None):
        if parent_key == "counters":
            unit = self.config.get("unit", "A")
            name = cfg["name"]
            channel = cfg["channel"]
            mode = cfg.get("mode", "MEAN")

            if unit != "A":
                convfunc = amps2unitfn(unit)
            else:
                convfunc = None

            cnt = item_class(name,
                                self._scc,
                                channel,
                                conversion_function=convfunc,
                                mode=mode,
                                unit=unit)
            return cnt
        else:
            raise NotImplementedError

    def _load_config(self):
        for cfg in self.config["counters"]:
            self._get_subitem(cfg["name"])

    @autocomplete_property
    def counters(self):
        return self._scc.counters