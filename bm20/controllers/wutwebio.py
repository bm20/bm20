"""
Communication controller for the Wiesemann & Theiss Web-IO # DIO-Ethernet adapter.
Support TCP/UDP socket only.

https://www.wut.de/e-5763w-10-inus-000.php
"""

import time
import gevent
from bliss.comm.util import get_comm


def _check_hex(value, name):
    if isinstance(value, (int,)):
        value = f"0x{value:X}"
    elif not value.startswith("0x"):
        raise ValueError(f"{name}: Expected an hexadecimal "
                            f"value, got [{value}].")
    try:
        int(value, 16)
    except ValueError:
        raise ValueError(f"[{name}: {value}] is not a valid "
                         f"hexadecimal value.")
    return value[2:]


class WutWebIo:
    def __init__(self, name, config):
        self.name = name
        self.config = config
        self._comm = get_comm(self.config, port=80)
        self._pw = self.config.get('password', '')

    @property
    def time(self):
        return self._comm_get("time", strip_reply=True)
    
    def _parse_reply(self, reply, command):
        """
        Removes the header prepended to the reply received from the
        device.
        Only tested with the configuration where the device only
        sent the command + ";" + data (as opposed to when the device
        sends the device's IP as well).
        Only tested with the following command:
        - input
        - ouput
        - outputaccess
        - time

        reply: full reply
        command: request that was sent
        """
        return reply[reply.index(command) + len(command) + 1:]

    @property
    def output(self):
        outp = self._comm_get("output", strip_reply=True)
        return int(outp, 16)

    def write_output(self, output):
        """
        Writes to the device output.
        """
        output = f"{output}"
        output = int(output, 16) | 0x888
        output = f"{output:X}"
        reply = self._comm_get("outputaccess", State=output)
        return int(self._parse_reply(reply, "output"), 16)

    # def output_mask(self, output, mask=None):
    #     if mask is None:
    #         mask = "0xFFFF"
    #     # kinda forcing the user to provide an hex string
    #     _check_hex(output, "output")
    #     _check_hex(mask, "mask")
    #     return self._comm_get("outputaccess",
    #                           wait_s=0.3,
    #                           State=output)
    #     # time.sleep(0.2)
        # outp = self._comm_read()
        # return int(self._parse_reply(outp, "output"), 16)

    @property
    def input(self):
        inp = self._comm_get("input", strip_reply=True)
        return int(inp, 16)

    def _comm_get(self, command, strip_reply=False, wait_s=None, **kwargs):
        """
        Sends a GET http command to the device, then reads the reply.
        Waits read_wait seconds before reading.
        Example of commands:
        - output
        - outputaccess
        - input
        The configured password is used in the command.
        Extra keyword arguments are appended to the command:
            key0=value0&key1=value1&etc...
        wait_s: waits s seconds before reading the reply
        strip_reply: strips the header from the reply, to only return
            the data.
        
        Example: _comm_write("outputaccess", State="0x0A5C)
        Command sent: "GET /outputaccess?PW=mypass&State=)x0A5C"
        """
        cmd = f"GET /{command}?PW={self._pw}&"
        if kwargs:
            cmd += "&".join([f"{k}={v}" for k, v in kwargs.items()])
            cmd += "&"
        self._comm.write(cmd.encode())
        if wait_s and wait_s > 0:
            gevent.sleep(wait_s)
        reply = self._comm_read()
        if strip_reply:
            reply = self._parse_reply(reply, command)
        return reply

    def _comm_read(self):
        """
        Reads the reply from the device
        (usually following a _comm_write call).
        """
        return self._comm.readline(eol=b"\x00").decode()
