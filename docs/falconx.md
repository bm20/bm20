# Files

The latest **ProSpect** and **Handel-SiToro** source (or windows binaries) can be downloaded from the [XIA](http://support.xia.com/default.asp?W689) website.

In doubt you can have a look at the list of tested [versions](#tested-versions).


# Handel

## Windows

> **Warning:**`Make sure you download the binary that matches your python interpreter (x86 or x64).`

> **Note:**`supervisor and multivisor work fine in cygwin, they have been installed as a service that is run as soon as windows starts (no need to log in and manually start it).`

TODO : document it!


## Linux

### Setup

Extract the Handel-SiToro archive into a new directory, then cd into this folder.

E.g:

    $ unzip handel-sitoro-src-1.1.19.zip -d handel-sitoro-src-1.1.19
    $ cd handel-sitoro-src-1.1.19


### Configure

Run the configuration script:

    $ waf configure --libdir=/path/to/install/dir

The libdir option tells waf where you would like to install the lib, default is TBD.

For instance, if you use python-handel and want to install it in a conda envirionment you can activate it and do:

    $ waf configure --libdir=$CONDA_PREFIX/lib/


### Build

> **Note:** In case of compilation errors please have a look at the [known problems](#build-troubleshooting).

Now you're ready to build the library (and/or the tools):

    $ waf

This will build everything. By default this seems to build the *debug* library.

To build a specific version of the library:

    $ waf build_release

or

    waf build_debug


### Installing

To install the library (and tools):

    $ waf install

To install the release version (and only the library):

    $ waf install_release --target=handel


If you don't specify the **target**, *waf* will install all the tools.

You can get the targets with:

    $ waf list

### Build troubleshooting

* compilation error: c99 or c11 required:

    > **Note:**`Couldn't find how to add C options at the waf command line, so I edited the wscript file instead.`

    Open the file *wscript* in an editor. In the function **cc\_get\_flags**: add *'-std=gnu99'* to the commom_flags list.
    It should look something like this  (as of version 1.1.19):

        common_cflags = ['-pipe', '-std=gnu99']

    Then [reconfigure](#configure).

* missing base64.h (missing from the 1.1.17 source):

    get the missing header from [XIA](http://support.xia.com/default.asp?pg=pgDownload&pgType=pgWikiAttachment&ixAttachment=18407&sFileName=base64.h) (or copy it from the 1.1.19 source), and put it in *\<src folder\>/inc/*.

* uninitialized constant DateTime (NameError):

    Add the following line to the top of the ~/build\_helpers/gen\_version.rb file:

        require 'date'


# Prospect on Linux

## Wine setup

Install wine. It's better to start with a version that has been
tested (see the list of [tested versions](#tested-versions)), as programs may be working fine
with one version and be completely unusable with the next.

Install cabextract (with apt-get for instance).

Get the [winetricks](https://wiki.winehq.org/Winetricks) script. Use it to install dotnet4 and vcredist 2015.


## Tested versions

Tested versions (as of September 2018) are:


| Rel. date   | ProSpect  | Handel-SiToro  | Wine  |
|-------------|-----------|----------------|-------|
| 2017-10-20  | 1.1.16    | 1.1.16         | 3.0.3 |
| 2018-05-22  | 1.1.22    | 1.1.17         | 3.0.3 |
| 2018-06-21  | 1.1.24    | 1.1.19         | 3.0.3 |

## Wine Troubleshooting

* Ping permissions:

    If ProsPect complains that it can't ping (a PingException exception in a pop up, as well as a line printed out in the console : "Failed to use ICMP (network ping)"), you might need to (as root):

        $ chmod u+s /bin/ping

        $ setcap cap_net_raw+epi wine-preloader

    Then create a file in /etc/ld.so.conf.d (wine.conf for example), and add the path to the
       wine libraries (in my case it's /opt/wine-stable/lib)

        $ run ldconfig

* Performance:

  Setting the WINEDEBUG environment variable to -all might help performance wise:

        $ WINDEBUG=-all wine ProSpect.exe)
