SPEC:

- flush between each set of sendline/get_buffer

########
# At reconfig
########
- I3=2
   # IO handshake control, turbo srm page 85
   # <CR> command is ack by PMAC with an <ACK>  (ascii 0x06)
   # invalid command is ack with a <BELL>  (ascii 0x07)
   # messages are sent as DATA <CR> [ DATA <CR> ... ] <ACK>
   # <CR> = 0x0D or \r
   # <LF> = 0x0A or \n
- I4=0
   # communication integrity mode
   # 0 = checksum disabled
- I100=0
   # deactivate motor X (here X = 1)
- M161->D:$00008B
   # M register $00008B now points to the motor #1 actual position
- M162->D:$000088
   # M register $000088 now points to the motor #1 command position
- I100=1
   # reactivate motor X (here X = 1)
- #1?
   # using wireshark at spec startup we got : 85 00 00 07 00 00
   # (active & open loop & int mode)
   # status of motors
   # PMAC Turbo SRM page 310
   # returns 12 char [0-F] representing two status words, earch char represents
   # four status bits
   # ===========
   # first 6 chars:
   # ---
   # b23 : motor activated
   # b22 : neg limit set, 1 if true
   # b21 : pos limit set, 1 if true
   # b20 : extended servo algo enabled
   # ---
   # b19 : ampli enabled
   # b18 : open loop mode, 1 if open
   # b17 : move timer active (=1 where moving? probably yes)
   # b16 : integration mode
   # ---
   # b15 : 
   # b14 : 
   # b13 : 
   # b12 : 
   # ---
   # b11 : 
   # b10 : home search in progress
   # b9 : 
   # b8 : 
   # ---
   # b7 : 
   # b6 : 
   # b5 : 
   # b4 : 
   # ---
   # b3 : error trigger
   # b2 : 
   # b1 : 
   # b0 : 
   # last 6 chars:
   # ---
   # b23 : 
   # b22 : 
   # b21 : 
   # b20 : 
   # --- coordinate definition : 0, 1, 2, 3, 4 or 7
   # --- seven is xyz axes
   # b19 : 
   # b18 : 
   # b17 : 
   # b16 : 
   # ---
   # b15 : 
   # b14 : 
   # b13 : 
   # b12 : 
   # ---
   # b11 : 
   # b10 : home complete
   # b9 : 
   # b8 : 
   # ---
   # b7 : trigger move (jog until trigger)
   # b6 : integrated fatal error
   # b5 : 
   # b4 : 
   # ---
   # b3 : 
   # b2 : 
   # b1 : 
   # b0 : motor ready
- #1P

#######
# at mv
#######
- I122=8.00
   # motor jog speed
   # default 32.0
   # SPEC sets it to Steady state rate / 1000 ---> counts / ms

- I115=0.080
   # abort/limit deceleration rate
   # default 0.25
   # SPEC decel (ct.s-2) = 2 * (steady state rate / accel)
   #            = 2 * dv (ct.s-1) / dt (ms)
   #            = 2 * dv (ct.s-1) / (dt (s) / 1000)

- I119=1000
  # maximum jog/home acceleration
  # SPEC forces it to 1000

- I120=200
  # jog/home acceleration time
  # = SPEC's accel time
  # WARNING : must not set BOTH Ix21 and Ix20 to 0.


- I121=0
  # jog/home s-curve time
  # forced to 0 => pmac will use Ix20
  # WARNING : must not set BOTH Ix21 and Ix20 to 0.


-#1J=20000
  # jog to specific location (e.g 20000)