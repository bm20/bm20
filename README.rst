============
BM20 project
============

[![build status](https://gitlab.esrf.fr/BM20/bm20/badges/master/build.svg)](http://BM20.gitlab-pages.esrf.fr/BM20)
[![coverage report](https://gitlab.esrf.fr/BM20/bm20/badges/master/coverage.svg)](http://BM20.gitlab-pages.esrf.fr/bm20/htmlcov)

BM20 software & configuration

Latest documentation from master can be found [here](http://BM20.gitlab-pages.esrf.fr/bm20)
