#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `bm20` package."""

from functools import partial

import pytest
import mock

from bm20.controllers import ccs_light


@pytest.fixture()
def pd3_controller(beacon):
    name = 'pd3_led'
    config = beacon.get(name)
    controller = ccs_light.PD3Controller(name, config)
    yield controller


def _check_comm(controller, target, cmd, result, data=None):
    with mock.patch('bm20.controllers.ccs_light.Serial.write_readlines',
                    return_value=result) as wr_mock:

        if data is not None:
            reply = target(data)
        else:
            reply = target()

        expected_n_lines = (((controller.echo and 1) or 0) +
                            ((controller.reply and 1) or 0))

        wr_mock.assert_called_with(cmd, expected_n_lines, eol='\r\n')

        return reply


def test_intensity_nominal(pd3_controller):
    reply = _check_comm(pd3_controller,
                        pd3_controller.set_intensity,
                        '@00F10000D7\r\n',
                        ['@00F10000D7', '@00O004F'],
                        data=100)
    assert reply == {'checksum': '4F', 'channel': '00', 'unit': '00', 'error': None}


def test_mode_nominal(pd3_controller):
    reply = _check_comm(pd3_controller,
                        pd3_controller.set_mode,
                        '@00S0500B8\r\n',
                        ['@00S0500B8', '@00O004F'],
                        data=5)
    assert reply == {'checksum': '4F', 'channel': '00', 'unit': '00', 'error': None}


def test_off_nominal(pd3_controller):
    reply = _check_comm(pd3_controller,
                        pd3_controller.set_off,
                        '@00L0007C\r\n',
                        ['@00L0007C', '@00O004F'])
    assert reply == {'checksum': '4F', 'channel': '00', 'unit': '00', 'error': None}


def test_on_nominal(pd3_controller):
    reply = _check_comm(pd3_controller,
                        pd3_controller.set_on,
                        '@00L1007D\r\n',
                        ['@00L1007D', '@00O004F'])
    assert reply == {'checksum': '4F', 'channel': '00', 'unit': '00', 'error': None}


def test_settings_nominal(pd3_controller):
    reply = _check_comm(pd3_controller,
                        pd3_controller.get_settings,
                        '@00M004D\r\n',
                        ['@00M004D', '@00OF100.S05.L100B6'])
    assert reply == {'checksum': 'B6', 'intensity': '100', 'mode': '05',
                     'error': None, 'channel': '00', 'unit': '00', 'onoff': '1'}


def test_status_nominal(pd3_controller):
    reply = _check_comm(pd3_controller, pd3_controller.get_status,
                        '@00C0043\r\n',
                        ['@00C0043', '@00O0000AF'])
    assert reply == {'status': '00', 'checksum': 'AF', 'channel': '00', 'unit': '00', 'error': None}


def test_initialization_nominal(pd3_controller):
    reply = _check_comm(pd3_controller, pd3_controller.all_channel_init,
                        '@00R0052\r\n',
                        ['@00R0052', '@00O004F'])
    assert reply == {'checksum': '4F', 'channel': '00', 'unit': '00', 'error': None}


def test_intensity_no_echo(pd3_controller):
    expected_msg = ('Expected an echo. Not received'
                    ' (received : {0}).'.format('@00O004F'))
    with pytest.raises(ValueError) as excinfo:
        _check_comm(pd3_controller,
                    pd3_controller.set_intensity,
                    '@00F10000D7\r\n',
                    ['@00O004F'],
                    data=100)
    assert str(excinfo.value) == expected_msg


def test_intensity_unexpected_reply(pd3_controller):
    expected_msg = ('Expected reply not received. (got : [{0}],'
                    'expected :['
                    ''.format('@01O004F'))
    with pytest.raises(ccs_light.RS485ReplyError) as excinfo:
        _check_comm(pd3_controller,
                    pd3_controller.set_intensity,
                    '@00F10000D7\r\n',
                    ['@00F10000D7', '@01O004F'],
                    data=100)
    assert expected_msg in str(excinfo.value)


def test_intensity_out_of_range(pd3_controller):
    expected_msg = ('PD3 command failed. Sent {0}, '
                    'unit replied {1}.'
                    ''.format('@00F28000E0\r\n', '@00N0300B1'))
    with pytest.raises(ccs_light.PD3CommandError) as excinfo:
        _check_comm(pd3_controller,
                    pd3_controller.set_intensity,
                    '@00F28000E0\r\n',
                    ['@00F28000E0', '@00N0300B1'],
                    data=280)
    assert expected_msg in str(excinfo.value)