import numpy as np
from silx.io import open as h5open
from silx.gui import qt as Qt
from silx.gui.plot import Plot1D

from scipy.signal import find_peaks

from bm20.oh.mono.energy import bragg2energy, energy2bragg

shift = []

# bliss_f = '/users/opd20/monomusst.h5'
# shift = [(2, 10), (3, 30)]

# bliss_f = '/users/opd20/monomusst_fmove.h5'
# shift = [(1, -45), (3, -20), (4, 35)]
# bliss_f = '/users/opd20/monomusst_fmove2.h5'
bliss_f = '/users/opd20/monomusst_linear2.h5'



with h5open(bliss_f) as bliss_h5:
    datax = bliss_h5[f'entry/data0/x'][:]
    datay = bliss_h5[f'entry/data0/y'][:]
    
app = Qt.QApplication([])

plot = Plot1D()

height = 0.5 * datay.max()
peaks = find_peaks(datay, height=height, prominence=50, distance=100)
print(peaks)

# peaks[0][2] += 10
# peaks[0][3] += 30
for sh in shift:
    peaks[0][sh[0]] += sh[1]
    
l_min = max(2500, peaks[0][0])
r_min = min(2000, len(datay) - peaks[0][-1])

for i_peak, peak in enumerate(peaks[0]):
    # lb = peaks[1]['left_bases'][i_peak]
    # rb = peaks[1]['right_bases'][i_peak]
    y = datay[peak-l_min:peak+r_min]
    x = np.arange(len(y))
    plot.addCurve(x, y, legend=f'{i_peak}')
# plot.addCurve(x, ediff[0:len(x)], legend='ene', color='green', yaxis='right')
plot.show()

app.exec()