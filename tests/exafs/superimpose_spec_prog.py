import numpy as np
from silx.io import open as h5open
from silx.gui import qt as Qt
from silx.gui.plot import Plot1D

from scipy.signal import find_peaks, correlate

from bm20.oh.mono.energy import bragg2energy, energy2bragg

shift = []

bliss_f = '/users/opd20/mono_musst_prog.dat'
spec_f = '/users/opd20/mono_musst_prog3.dat'


with h5open(bliss_f) as bliss_h5:
    bdata = bliss_h5[f'2.1/measurement/bragg_raw'][:]
    
with h5open(spec_f) as spec_h5:
    sdata = spec_h5[f'2.1/measurement/bragg_raw'][:]
    
app = Qt.QApplication([])

plot = Plot1D()

shift = 1607

print(np.argmax(correlate(bdata, sdata)))

x = np.arange(sdata.shape[0]) - shift
plot.addCurve(x, sdata, legend='spec', color='blue')
x = np.arange(bdata.shape[0])
plot.addCurve(x, bdata, legend='bliss', color='green')
plot.show()

app.exec()