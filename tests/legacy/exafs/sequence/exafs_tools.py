
import numpy
import sys
import gevent
import time

from bliss.scanning.scan import ScanState, DataWatchCallback, WatchdogCallback
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.acquisition import ct2
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave, CalcHook
from bliss.scanning.acquisition.musst import MusstAcquisitionSlave

from bliss.data.node import DataNodeContainer

from bliss.controllers.counter import CalcCounterController, SamplingCounterController
from bliss.controllers.ct2.device import AcqMode
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.musst import musst
from bliss.controllers.emh import EMH
from bliss.controllers.speedgoat.speedgoat_client import SpeedgoatCountersController
from bliss.controllers.mca.base import TriggerMode, BaseMCA
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.pepu import PEPU, Signal
from bliss.controllers.musst import (
    MusstIntegratingCounterController,
    MusstSamplingCounterController,
)
    
            

#######################################################################
#####
#####  Get Step Scan positions from exafs_param object
#####  Adjust them to exact motor steps
#####
def scantool_get_step_position(exafs_param, mono, step):
    steps_per_unit = mono.motors["bragg"].steps_per_unit
    positions = list(exafs_param.Position)
    for ind in range(len(positions)):
        ene = positions[ind] / 1000.0
        if step:
            ang = mono.energy2bragg(ene)
            ang = int(ang * steps_per_unit + 0.5) / steps_per_unit
            # Bragg angle correction
            ang = ang * mono.bragg2cbragg
            ene = mono.bragg2energy(ang)
        positions[ind] = ene
    return positions


def scantool_get_step_position2(exafs_param, bragg, step):
    from bm20.oh.mono.energy import bragg2energy, energy2bragg
    steps_per_unit = bragg.steps_per_unit
    positions = list(exafs_param.Position)
    for ind in range(len(positions)):
        ene = positions[ind]# / 1000.0
        if step:
            ang = energy2bragg(ene)
            ang = int(ang * steps_per_unit + 0.5) / steps_per_unit
            # Bragg angle correction
            # ang = ang * mono.bragg2cbragg
            ene = bragg2energy(ang)
        positions[ind] = ene
    return positions


#######################################################################
#####
##### Store Scan Parameters and create defaults
#####
def scantool_set_scan_parameters(
    motor=None,
    start=numpy.nan,
    stop=numpy.nan,
    positions=None,
    points=1,
    time=None,
    way="up",
    trig_start="TIME",
    trig_point="TIME"
):
    scan_par = {
        "motor": motor,
        "positions": positions,
        "start": start,
        "stop": stop,
        "points": points,
        "time": time,
        "way" : way,
        "trig_start": trig_start,
        "trig_point": trig_point,
    }
    
    return scan_par
    
#######################################################################
#####
##### Build Step scan Acquisition Chain
#####
def scantool_stepscan_get_acquisition_chain(
    master,
    scan_par,
    counters,
    fx_trigger="SOFTWARE",
    fx_block_size=5,
):
    
    chain = AcquisitionChain(parallel_prepare=True)
    builder = ChainBuilder(counters)

    ###################################
    # P201
    #
    acq_node_params = {
            "npoints": scan_par["points"],
            "acq_expo_time": scan_par["time"],
    }
    if isinstance(scan_par["time"], list):
        acq_child_params = {
            "count_time": 1,
            "npoints": 1,
        }
    else:
        acq_child_params = {
            "count_time": scan_par["time"],
            "npoints": scan_par["points"],
        }
    
    for node in builder.get_nodes_by_controller_type(CT2Controller):
        node.set_parameters( acq_params=acq_node_params)
        for child_node in node.children:
            child_node.set_parameters(acq_params=acq_child_params)
        node_p201 = node
        chain.add(master, node)

    ###################################
    # FalconX
    #
    if fx_trigger == "SOFTWARE":
        acq_params = {
            "npoints": scan_par["points"],
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : scan_par["time"]
        }
    else:
        acq_params = {
            "npoints": scan_par["points"],
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : 0.1
        }
    for node in builder.get_nodes_by_controller_type(BaseMCA):
        node.set_parameters(acq_params=acq_params)
        chain.add(node_p201, node)

    ###################################
    # SamplingCounterController
    #
    acq_params = {
            "count_time": scan_par["time"],
            "npoints": scan_par["points"],
        }
    for node in builder.nodes:
        if isinstance(node.controller, SamplingCounterController):
            node.set_parameters(acq_params=acq_params)
            chain.add(node_p201, node)

    ###################################
    # CalcCounterController
    #
    acq_params = {
            "count_time": scan_par["time"],
        }
    for node in builder.nodes:
        if isinstance(node.controller, CalcCounterController):
            node.set_parameters(acq_params=acq_params)
            chain.add(node_p201, node)

    return (builder, chain)

##########################################################
#
# Display acquired nb points on acquisition devices during
# continuous scan
#
#######################################################################
#####
##### Display acquired nb points on acquisition devices during
##### continuous scan
#####
class ScanOutput(DataWatchCallback):

    STATE_MSG = {
        ScanState.PREPARING: "Preparing",
        ScanState.STARTING: "Running",
        ScanState.STOPPING: "Stopping",
    }

    def __init__(self, musst, points, motor, builder):

        super().__init__()
        
        self.nb_points = points

        self.musst_time = 0.0
        self.musst = musst
        self.musst_time_factor = 1.0
        self.musst_time_channel = "No Time Channel"
        if musst is not None:
            self.musst_time_channel = f"{self.musst.name}:musst_timer"
            self.musst_time_factor = float(self.musst.get_timer_factor())

        self.motor = motor

        self.controllers = {}
        if builder is not None:
            for cont_node in builder.get_top_level_nodes():
                #   breakpoint()
                self.controllers[cont_node.controller.name] = {}
                self.controllers[cont_node.controller.name][
                    "name"
                ] = cont_node.controller.name
                self.controllers[cont_node.controller.name]["updated"] = False
                self.controllers[cont_node.controller.name]["points"] = 0
        self.end_scan_time = None

        # print(self.controllers)

    def on_scan_new(self, scan, scan_info):

        title = scan_info["title"]
        print(f"\n  {title}\n")

        if self.motor is not None:
            display_str = "   %8s " % (self.motor.name)
        else:
            display_str = "  "

        if self.musst is not None:
            display_str += " %8s " % ("time")

        for cont_name in self.controllers:
            display_str += " %8s " % (self.controllers[cont_name]["name"])

        print(display_str)

    def on_state(self, state):
        return True

    def on_scan_data(self, data_events, nodes, info):

        if self.motor is not None:
            display_str = "   %8.4f " % (self.motor.position)
        else:
            display_str = "  "

        if len(data_events) >= 1:

            for cont_name in self.controllers:
                self.controllers[cont_name]["updated"] = False

            for channel, events in data_events.items():

                data_node = nodes.get(channel)

                #print("\n-------------------------------------------")
                #print(f"{data_node.name}")
                #print(f"{data_node.parent.name}")
                #print(f"{data_node.parent.parent.name}")
                #is_inst = isinstance(data_node, DataNodeContainer)
                #print(f"IS INSTANCE: {is_inst}")
                if not isinstance(data_node, DataNodeContainer):
                    
                    if data_node.name.lower() == self.musst_time_channel:
                        data = data_node.get(-1)
                        self.musst_time = float(data) / float(self.musst_time_factor)
                        cont_name = ""
                        nbp = len(data_node)
                    else:
                        cont_name = data_node.name
                        if cont_name not in self.controllers:
                            cont_name = data_node.parent.name
                            if cont_name not in self.controllers:
                                cont_name = data_node.parent.parent.name

                    if cont_name in self.controllers:
                        cont_obj = self.controllers[cont_name]
                        if not cont_obj["updated"]:
                            cont_obj["updated"] = True
                            #new_len = len(data_node)
                            #old_len = cont_obj["points"]
                            #if new_len != old_len:
                            #    print(f"{data_node.name} - {new_len} - {old_len}")
                            cont_obj["points"] = len(data_node)

        if self.musst is not None:
            display_str += f" {self.musst_time:8.3f} "

        for cont_obj in self.controllers.values():
            display_str += "     %04d " % (cont_obj["points"])

        print(f"{display_str}", end="\r")

        sys.stdout.flush()

    def on_scan_end(self, *args):
        print("\n")
        
class ScanWatchdog(WatchdogCallback):

    STATE_MSG = {
        ScanState.PREPARING: "Preparing",
        ScanState.STARTING: "Running",
        ScanState.STOPPING: "Stopping",
    }

    def __init__(self, musst, points, motor, builder, timeout=5.0):

        super().__init__(watchdog_timeout=timeout)
        
        self.nb_points = points

        self.musst_time = 0.0
        self.musst = musst
        self.musst_time_factor = 1.0
        self.musst_time_channel = "No Time Channel"
        if musst is not None:
            self.musst_time_channel = f"{self.musst.name}:musst_timer"
            self.musst_time_factor = float(self.musst.get_timer_factor())

        self.motor = motor

        self.controllers = {}
        if builder is not None:
            for cont_node in builder.get_top_level_nodes():
                self.controllers[cont_node.controller.name] = {}
                self.controllers[cont_node.controller.name][
                    "name"
                ] = cont_node.controller.name
                self.controllers[cont_node.controller.name]["updated"] = False
                self.controllers[cont_node.controller.name]["points"] = 0

        self._timeout_on = False
        self._end_of_scan_reached = False

    def on_scan_new(self, scan, scan_info):
        pass
        
    def on_state(self, state):
        return True

    def on_scan_data(self, data_events, nodes, info):

        if len(data_events) >= 1:
            
            self._timeout_on = True

            for cont_name in self.controllers:
                self.controllers[cont_name]["updated"] = False

            for channel, events in data_events.items():

                data_node = nodes.get(channel)

                if not isinstance(data_node, DataNodeContainer):
                    
                    if data_node.name.lower() == self.musst_time_channel:
                        cont_name = ""
                    else:
                        cont_name = data_node.name
                        if cont_name not in self.controllers:
                            cont_name = data_node.parent.name
                            if cont_name not in self.controllers:
                                cont_name = data_node.parent.parent.name

                    if cont_name in self.controllers:
                        cont_obj = self.controllers[cont_name]
                        if not cont_obj["updated"]:
                            cont_obj["updated"] = True
                            cont_obj["points"] = len(data_node)

    def on_scan_end(self, *args):
        #print("\n ON_SCAN_END\n")
        self._timeout_on = False
        self._end_of_scan_reached = True
        self._time_of_end_of_scan = time.time()
        
    def on_timeout(self):
        #print("\n TIMEOUT\n")
        if self._end_of_scan_reached:
            ttime = time.time()-self._time_of_end_of_scan
            print(f"\n   {ttime:.3f} since end of scan")
        if self._timeout_on:
            raise TimeoutError("Missing points")
