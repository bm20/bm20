
import numpy

from bliss import setup_globals

def save_exafs_ascii(scn):
    headers = [
        "Emono",
        "mu_fluo",
        "mu_fluo_micro",
        "mu_trans",
        "I0",
        "I1",
        "I2",
        "I3",
        "mu_ref",
        "mu_ref2",
        "eneenc"
    ]
    
    data=[]
    header = []
    
    scan_data = scn.get_data()
    scan_keys = scan_data.keys()
    for name in headers:
        key = find_in_keys(name, scan_keys)
        if key is not None:
            if key == "Emono":
                data_emono = numpy.copy(scan_data[key]) * 1000.0
                data.append(data_emono)
            else:
                data.append(scan_data[key])
            header.append(name)
            
    save_ascii(data, header)
    
def find_in_keys(name, keys):
    for key in keys:
        nkey = key.split(":")[-1]
        if nkey == name:
            return key
    return None
    
def save_ascii(data, headers):
    
    SCAN_SAVING = setup_globals.SCAN_SAVING
    filename = SCAN_SAVING.filename.split(".")[0]
    scann = SCAN_SAVING.writer_object.last_scan_number
    filename = f"{filename}_scan{scann}.dat"
    
    numpy.savetxt(
        filename,
        numpy.array(data).transpose(),
        delimiter="\t",
        header="\t".join(headers),
    ) 
