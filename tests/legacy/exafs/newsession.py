import os
import click
import tabulate
import numpy
import PIL

from bliss import setup_globals, current_session
from bliss.shell.standard import wm, newproposal
from bliss.common.axis import Axis
from bliss.common import measurementgroup, plot
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED#, ORANGE
from bliss.controllers.motor import CalcController
from bliss.scanning.scan_tools import get_selected_counter_name
from bliss.config.static import get_config

ORANGE = YELLOW

##################################################################
#####
##### SESSION
#####
def newsession(session=""):
    SCAN_SAVING = setup_globals.SCAN_SAVING

    # exafs_param = setup_globals.exafs_param
    # exafs_param.clean()
    
    # motpos = get_config().get("motpos")
    
    newproposal(session)
    
    # Create default directories
    proposal_dir = os.path.join(
        SCAN_SAVING.base_path,
        SCAN_SAVING.proposal_name,
        SCAN_SAVING.beamline
    )
    macros_dir = os.path.join(proposal_dir, "macros")
    if not os.path.isdir(macros_dir):
        os.makedirs(macros_dir)
    current_session.user_script_homedir(macros_dir)
    params_dir = os.path.join(proposal_dir, "exafs_param")
    if not os.path.isdir(params_dir):
        os.makedirs(params_dir)
    motpos_dir = os.path.join(proposal_dir, "motpos")
    if not os.path.isdir(motpos_dir):
        os.makedirs(motpos_dir)
    # motpos.clean(remove_file=False)
    
    # # For Laser experiment, reset the run number to 1
    # if "session_laser" in current_session.env_dict.keys():
    #     if "hplf_seq" in current_session.env_dict.keys():
    #         config = get_config()
    #         hplf_seq = config.get("hplf_seq")
    #         hplf_seq._save.run_reset()
            

##################################################################
#####
##### FLINT
#####
def edit_mask(detector):
    from bliss.common import plot as plot_mdl
    f = plot_mdl.get_flint()
    plot_id = f.get_live_scan_plot(detector.image.fullname, "image")
    plot_mdl.plot_image(existing_id=plot_id)
    p = plot_mdl.plot_image(existing_id=plot_id)
    return p.select_mask()

def curs(self):
    cnt_name = get_selected_counter_name
    print("Selected counter: {BLUE(cnt_name}")
    print("\n")
    print("Select a point on flint ...")
    f = plot.get_flint()
    c = f.get_live_plot("default-curve")
    res = c.select_point(1)
    position = c[0]
    

##################################################################
#####
##### MOTOR POSITIONS PER DEVICES
#####

    
##################################################################
#####
##### MOTORS
#####
def motor_esync(axis):
    axis.hw_state
    ch = axis.address
    axis.controller.raw_write(f"{ch}:esync")
    axis.controller.raw_write(f"{ch}:power on")
    axis.sync_hard()

def icepap_smaract_enable(axis):
    ch = axis.address
    axis.controller.raw_write(f"{ch}:infoa low normal")
    axis.controller.raw_write(f"{ch}:infoa high normal")
    
def get_axes_iter():
    for name in dir(setup_globals):
        elem = getattr(setup_globals, name)
        if isinstance(elem, Axis):
            yield elem

def MotorsForceInit():
    for motor in get_axes_iter():
        motor.hw_state

def ApplySessionAxisConfig():
    for motor in get_axes_iter():
        print(f"    \"{motor.name}\"")
        motor.hw_state

def id24wm(*motors):
    print("")
    lines = []
    line1 = [BOLD("    Name")]
    line2 = [BOLD("    Pos.")]
    for mot in motors:
        if isinstance(mot.controller, CalcController):
            line1.append(f"{ORANGE(mot.name)}({mot.unit})")
        else:
            line1.append(f"{BLUE(mot.name)}({mot.unit})")
        line2.append(f"{mot.position:.3f}")
    lines.append(line1)
    lines.append(line2)
    mystr = tabulate.tabulate(lines, tablefmt="plain")
    print(mystr)


##################################################################
#####
##### Attribute List
#####
class ID24attrList:
    
    def __init__(self, items):
        self._items = items
        for name, dev in self._items.items():
            self._set_device_attr(name, dev)
    
    def __info__(self):
        info_str = ""
        for name, dev in self._items.items():
            info_str += dev.__info__()
        return info_str
        
    def _set_device_attr(self, name, device):
        setattr(self, name, device)  
        
    def _del_device_attr(self, name):
        delattr(self, name)

##################################################################
#####
##### Saving data
#####
def ID24saveAscii(filename, data):
    header = ""
    for head in data.header():
        header += f"{head}\t"
    numpy.savetxt(
        filename,
        numpy.array(data.data()).transpose(),
        delimiter="\t",
        header=header,
    ) 
    
def ID24saveImage(filename, data):
    im = PIL.Image.fromarray(data)
    im.save(filename)
