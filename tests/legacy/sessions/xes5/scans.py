import os
import numpy as np
from tabulate import tabulate
from bliss.config.static import get_config
from bliss.common.cleanup import cleanup
# from bliss import setup_globals
# from bliss.shell.standard import new
from bliss import setup_globals, current_session
from bm20.oh.mono.energy import bragg2energy, energy2bragg


def _cleanup():
    config = get_config()
    eh1_mpx = config.get('eh1_mpx')
    eh1_mpx.switch('SEL_TRIG', 'STEP')


def xes_fast_fbragg(name, e0, e1, step_ev, step_s, run=False):
    config = get_config()
    eh1_mpx = config.get('eh1_mpx')
    bragg = config.get('bragg')
    fbragg = config.get('fbragg')
    fscan_obj = config.get('d20fscan')

    # preset = FastXes5Preset(name, e0, e1, step_ev, step_s)
    # # fscan_obj.add_scan_preset(preset)
    
    fscan = fscan_obj.fscan
    # energy = config.get('energy')
    bragg.sync_hard()
    fbragg.sync_hard()
    fbragg.offset = bragg.offset
    fscan.pars.motor = fbragg
    d_spacing = bragg.controller.d_spacing()

    with cleanup(_cleanup): 
        bragg_0 = energy2bragg(e0, d_spacing)
        bragg_1 = energy2bragg(e1, d_spacing)
        npoints = abs(e1 - e0) / step_ev

        fscan.pars.start_pos = bragg_0
        fscan.pars.step_size = (bragg_1 - bragg_0) / npoints
        fscan.pars.latency_time = 0.001

        fscan.pars.acc_margin = 0.1 # / bragg.steps_per_unit
        
        fscan.pars.npoints = npoints
        fscan.pars.step_time = 0#step_s + 0.001
        fscan.pars.acq_time = step_s
        fscan.pars.gate_mode = 'TIME'
        fscan.pars.scan_mode = 'TIME'

        eh1_mpx.switch('SEL_TRIG', 'ZAP')
        fscan.prepare()
        if run:
            current_session.scan_saving.newsample(name)
            return fscan.run(
                {'instrument':
                 {"xes":
                  {"e0": e0,
                   "e1": e1,
                   "step_ev": step_ev,
                   "step_s": step_s}
                 }
                })
        else:
            print(fscan)


from bliss.config.static import get_config
from bliss.scanning.scan import ScanPreset

from bm20.config.names import OPIOM_EH1, MONO



class FastXes5Preset(ScanPreset):
    def __init__(self):
        super().__init__()
        self._opiom = get_config().get(OPIOM_EH1)
        self._mono = get_config().get(MONO)

    def prepare(self, scan):
        self._opiom.switch('SEL_TRIG', 'ZAP')

    def start(self, scan):
        print(f"Starting scan {scan.name}")
        fbragg = get_config().get('fbragg')
        fbragg.sync_hard()
        # fbragg.controller._upload_exafs_params()
#         print(f"Opening the shutter")
        print("STARTED")

    def stop(self, scan):
        print("STOP")
        self._opiom.switch('SEL_TRIG', 'STEP')
        fbragg = get_config().get('fbragg')
        fbragg.controller.clear_scans_params()
        print("STOPPED")
        # # making sure the xtal2perp motor is in open loop
        # # because it is in vacuum but not cooled.
        # # TODO: add others?
        if not self._mono.pmac_comm.is_open_loop(4):
            self._mono.pmac_comm.open_loop(4)
            print(f"xtal2perp in open loop: {self._mono.pmac_comm.is_open_loop(4)}")


# class FastXes5Preset(ScanPreset):
#     def __init__(self):
#         super().__init__()
#         # self._xes_params = name, e0, e1, step_ev, step_s
#         self._opiom = get_config().get(OPIOM_EH1)
#         self._mono = get_config().get(MONO)

#     def prepare(self, scan):
#         self._opiom.switch('SEL_TRIG', 'ZAP')

#     def start(self, scan):
#         print(f"Starting scan {scan.name}")
#         fbragg = get_config().get('fbragg')
#         fbragg.sync_hard()
# #         print(f"Opening the shutter")

#     def stop(self, scan):
#         self._opiom.switch('SEL_TRIG', 'STEP')
#         # making sure the xtal2perp motor is in open loop
#         # because it is in vacuum but not cooled.
#         # TODO: add others?
#         if not self._mono.pmac_comm.is_open_loop(4):
#             self._mono.pmac_comm.open_loop(4)
#             print(f"xtal2perp in open loop: {self._mono.pmac_comm.is_open_loop(4)}")

#         saving = scan.scan_saving
#         snun = scan.scan_number
#         data = scan.get_data()

#         dat_dir = os.path.join(saving.base_path, saving.proposal_dirname, saving.beamline, saving.proposal_session_name)
#         dat_file = f"{saving.collection_name}_{saving.dataset_name}_{snun}.dat"
#         data_file = os.path.join(dat_dir, dat_file)

#         columns = ['musstoh:fbragg_energy', "musstoh:fbragg_egy_delta",
#                    'p201_0:ct2_counters_controller:i0', 'xesm4:roi_det0',
#                     'xesm4:realtime_det0',
#                     'xesm4:trigger_livetime_det0', 'xesm4:energy_livetime_det0', 
#                     'xesm4:triggers_det0', 'xesm4:events_det0', 
#                     'xesm4:icr_det0', 'xesm4:ocr_det0', 'xesm4:deadtime_det0']
#         # print(data.keys())
#         # print(scan.scan_info)
#         print('====', (len(data[columns[0]]), len(columns)), data['musstoh:fbragg_energy'].shape)
#         data_ar = np.ndarray((len(data[columns[0]]), len(columns)))
#         for i, col in enumerate(columns):
#             print(i, col, data[col].shape)
#             data_ar[:, i] = data[col][:]
#         with open(data_file, "w") as ofile:
#             ofile.write(tabulate(data_ar, headers=columns, tablefmt="plain"))
#         print(f"File writen: {data_file}")


# dict_keys(['axis:fbragg', 'musstoh:timer_raw', 'musstoh:fbragg_raw', 'musstoh:trigct_raw', 'musstoh:epoch_trig', 'musstoh:fbragg_center', 'musstoh:fbragg_delta', 'musstoh:fbragg_egy_delta', 'musstoh:fbragg_energy', 'musstoh:fbragg_period', 'musstoh:fbragg_trig', 'musstoh:timer_delta', 'musstoh:timer_period', 'musstoh:timer_trig', 'musstoh:trigct_delta', 'p201_0:ct2_counters_controller:i0', 'xesm4:roi_det0', 'xesm4:realtime_det0', 'xesm4:spectrum_det0', 'xesm4:trigger_livetime_det0', 'xesm4:energy_livetime_det0', 'xesm4:triggers_det0', 'xesm4:events_det0', 'xesm4:icr_det0', 'xesm4:ocr_det0', 'xesm4:deadtime_det0'])


# def start(self, *args, **kwargs):
#         # self._fscans = OrderedDict()
#         # self.bragg_axis.sync_hard()
#         # self.energy_axis.sync_hard()
#         sequence = Sequence()
#         with sequence.sequence_context() as scan_seq:
#             for i_params, scan_params in enumerate(self._exafs_params.pre_e_scans):
#                 fscan = self._setup_scan(scan_params, 0.02, 0.1)
#                 fscan.prepare()
#                 scan_seq.add_and_run(fscan.scan)
#                 # self._fscans[f'pre_{i_params}'] = fscan
#                 # self.bragg_axis.sync_hard()
#                 # self.energy_axis.sync_hard()
#             for i_params, scan_params in enumerate(self._exafs_params.e_scans):
#                 fscan = self._setup_scan(scan_params, 0.02, 0.1)
#                 fscan.prepare()
#                 scan_seq.add_and_run(fscan.scan)
#                 # self.bragg_axis.sync_hard()
#                 # self._fscans[f'edge_{i_params}'] = fscan
#             for i_params, scan_params in enumerate(self._exafs_params.k_scans):
#                 fscan = self._setup_scan(scan_params, 0.05, 0.1)
#                 fscan.prepare()
#                 scan_seq.add_and_run(fscan.scan)
#                 # self.bragg_axis.sync_hard()
#                 # self._fscans[f'exafs_{i_params}'] = fscan
            
#     def _setup_scan(self, scan_params, step_time, step_size):
#         from bm20.scans.continuous.fastscan import fastscan_setup
#         fscan = self._fscan_config.fscan
#         e0 = scan_params.scan_pos[0]
#         e1 = scan_params.scan_pos[-1]
#         fastscan_setup(fscan, e0, e1, step_size, step_time, self.energy_axis, self.bragg_axis)
#         return fscan
        