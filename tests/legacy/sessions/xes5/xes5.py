import datetime
import tabulate

from bliss.config import settings
from bliss.config.static import get_config
from bliss.shell.standard import newproposal, newcollection, newdataset, ascan
from bliss.scanning.toolbox import get_active_mg
from bliss import setup_globals

from bm20.config.names import *


XES5_ROI_NAME = 'ketek'


def xes5_mca():
    return get_config().get('xesm4')


def xes5_rois():
    return xes5_mca().rois


def xes5_roi_set(start, end):
    rois = xes5_rois()
    rois.set('sca', start, end)
    return rois


def xes5_roi_clear():
    rois = xes5_rois()
    mca = xes5_mca()
    start, end = mca.spectrum_range
    rois.set(XES5_ROI_NAME, start, end)
    return rois


def xes5_roi_init():
    rois = xes5_rois()
    if XES5_ROI_NAME not in rois:
        xes5_roi_clear()
    return rois



# def get_current_xes_settings():
#     xes_settings = settings.HashObjSetting("d20_xes_settings")



class XesExp:
    # proposal = property(lambda self: self._proposal)

    def __init__(self):#, proposal=None, collection_name=None):
        # TODO: make sure there's an active MG ready

        config = get_config()
        # self._proposal = proposal
        self._energy = config.get(ENERGY)
        # setup_globals.SCAN_SAVING
        # if collection_name is None:
        #     collection_name = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
        # newcollection(collection_name)

    def scan(self,
             e_start_ev,
             e_end_ev,
             n_steps,
             acq_time_s,
             collection_name,
             dataset_name=None,
             repeat=1,
             run=True):
        

    def __info__(self):
        ss = setup_globals.SCAN_SAVING
        # info = " XES PARAMS:\n"
        info = [["Proposal", ":", f"{ss.proposal_name}"],
               ["Collection", ":", f"{ss.collection_name}"],
               ["Dataset", ":", f"{ss.dataset_name}"],
               ["Current file", ":", f"{ss.icat_data_fullpath}"]]
        return tabulate.tabulate(info)

    # def fast(self, e_start_ev, e_end_ev, step_size_ev, acq_time_s, dataset_name, repeat=1, run=True):
    #     raise NotImplemented()

class XesScan:
    collection = property(lambda self: self._collection)
    dataset = property(lambda self: self._dataset)
    description = property(lambda self: self._description)
    scan_obj = property(lambda self: self._scan_obj)

    def __init__(self, energy_axis, e_start_ev, e_end_ev, n_steps, acq_time_s, collection=None, dataset=None, description=None):
        self._e_start_ev = e_end_ev
        self._e_end_ev = e_start_ev
        self._n_steps = n_steps
        self._acq_time_s = acq_time_s
        self._collection = None
        self._dataset = None
        self._description = None
        self._energy_axis = energy_axis
        self._scan_obj = None

    def __info__(self):
        pass

    def run(self):
        if self.collection:
            newcollection(self.collection)
        if self.dataset:
            newdataset(self.dataset)

        self._scan_obj = ascan(self._energy_axis,
                               self.e_start_ev, self.e_end_ev,
                               self.n_steps, self.acq_time_s,
                               get_active_mg())

