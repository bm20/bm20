from bliss.config.static import get_config
from bliss.common.cleanup import cleanup

from ...scans.exafs.exafs import ExafsStepScan, ExafsContinuousScan
from ...scans.exafs.params import ExafsParameters

import os
import numpy as np
from tabulate import tabulate
from bliss.config.static import get_config
from bliss.common.cleanup import cleanup
# from bliss import setup_globals
# from bliss.shell.standard import new
from bliss import setup_globals, current_session
from bm20.oh.mono.energy import bragg2energy, energy2bragg


from bliss.config.static import get_config
from bliss.scanning.scan import ScanPreset

from bm20.config.names import OPIOM_EH1, MONO


class D20FScan:
    def __init__(self, config):
        cfg = config.to_dict()
        self._name = cfg["name"]
        self._crystal = cfg["crystal"]
        self._bragg = cfg["bragg"]
        self._fbragg = cfg["fbragg"]
        self._opiom = cfg["opiom"]
        self._fscan_cfg = cfg["fscan_cfg"]
        self._musst = cfg["musst"]

    # def __info__(self):
    #     return "BLABLA"
    
    def sync(self):
        self._bragg.sync_hard()
        self._fbragg.sync_hard()

    def cleanup(self):
        self._fbragg.controller.clear_scans_params()
        self.sync()
    
    def fscan_constant_vel(self, name, element, edge, step_ev=0.1, step_s=0.1, run=False):
        bragg = self._bragg
        fbragg = self._fbragg
        fscan_cfg = self._fscan_cfg
        d_spacing = self._crystal.controller.d_spacing()

        params = ExafsParameters(element, edge)
        egy = params.scan_pos
        e0 = egy[0]
        e1 = egy[-1]

        fscan = fscan_cfg.fscan
        self.sync()
        # self._musst.RESET

        fbragg.offset = bragg.offset
        fscan.pars.motor = fbragg

        with cleanup(self.cleanup): 
            bragg_0 = energy2bragg(e0, d_spacing)
            bragg_1 = energy2bragg(e1, d_spacing)
            npoints = abs(e1 - e0) / step_ev

            fscan.pars.start_pos = bragg_0
            fscan.pars.step_size = (bragg_1 - bragg_0) / npoints
            fscan.pars.latency_time = 0.001

            fscan.pars.acc_margin = 0.001 # / bragg.steps_per_unit
            
            fscan.pars.npoints = npoints
            fscan.pars.step_time = 0#step_s + 0.001
            fscan.pars.acq_time = step_s
            fscan.pars.gate_mode = 'TIME'
            fscan.pars.scan_mode = 'TIME'

            fscan.prepare()
            if run:
                current_session.scan_saving.newsample(name)
                return fscan.run(
                    {'instrument':
                    {self._name:
                    {"type": "constant_vel",
                     "name": name,
                     "element": element,
                     "edge": edge,
                     "e0": e0,
                     "e1": e1,
                     "step_ev": step_ev,
                     "step_s": step_s}
                    }
                    })
            
    def fscan_variable_vel(self, name, element, edge, step_s=0.1, run=False):
        bragg = self._bragg
        fbragg = self._fbragg
        fscan_cfg = self._fscan_cfg
        d_spacing = self._crystal.controller.d_spacing()

        params = ExafsParameters(element, edge)
        egy = params.scan_pos
        e0 = egy[0]
        e1 = egy[-1]

        fscan = fscan_cfg.fscan
        self.sync()
        # self._musst.RESET

        fbragg.offset = bragg.offset
        fscan.pars.motor = fbragg

        egy, tm = params._calc_cont()
        e0 = egy[0]
        e1 = egy[-1]
        print(e0, e1)
        # e0 = params.scan_pos[0]
        # e1 = params.scan_pos[-1]

        with cleanup(self.cleanup):
            bragg_0 = energy2bragg(e0, d_spacing)
            bragg_1 = energy2bragg(e1, d_spacing)
            # n_points = abs(e1 - e0) / step_ev
            n_points = np.sum(tm) / step_s
            # npoints = params.scan_pos.shape[0]#abs(e1 - e0) / step_ev

            # print("FAST", e0, e1, bragg_0, bragg_1)

            fscan.pars.start_pos = bragg_0
            fscan.pars.step_size = (bragg_1 - bragg_0) / n_points
            fscan.pars.latency_time = 0.001

            fscan.pars.acc_margin = 0.001 # / bragg.steps_per_unit
            
            fscan.pars.npoints = n_points
            fscan.pars.step_time = 0#step_s + 0.001
            fscan.pars.acq_time = step_s
            fscan.pars.gate_mode = 'TIME'
            fscan.pars.scan_mode = 'TIME'

            fbragg.controller.set_next_scan_params(None)
            fbragg.controller.set_next_scan_params(params)

            fscan.prepare()
            if run:
                current_session.scan_saving.newsample(name)
                return fscan.run(
                    {'instrument':
                    {self._name:
                    {"type": "variable_vel",
                     "name": name,
                     "element": element,
                     "edge": edge,
                     "e0": e0,
                     "e1": e1,
                     "step_ev": "nan",
                     "step_s": step_s}
                    }
                    })
            else:
                print(fscan)

        # with cleanup(self.cleanup): 
        #     bragg_0 = energy2bragg(e0, d_spacing)
        #     bragg_1 = energy2bragg(e1, d_spacing)
        #     npoints = abs(e1 - e0) / step_ev

        #     fscan.pars.start_pos = bragg_0
        #     fscan.pars.step_size = (bragg_1 - bragg_0) / npoints
        #     fscan.pars.latency_time = 0.001

        #     fscan.pars.acc_margin = 0.001 # / bragg.steps_per_unit
            
        #     fscan.pars.npoints = npoints
        #     fscan.pars.step_time = 0#step_s + 0.001
        #     fscan.pars.acq_time = step_s
        #     fscan.pars.gate_mode = 'TIME'
        #     fscan.pars.scan_mode = 'TIME'

        #     eh1_mpx.switch('SEL_TRIG', 'ZAP')
        #     fscan.prepare()
        #     if run:
        #         current_session.scan_saving.newsample(name)
        #         return fscan.run(
        #             {'instrument':
        #             {self._name:
        #             {"type": "constant_vel",
        #              "name": name,
        #              "element": element,
        #              "edge": edge,
        #              "e0": e0,
        #              "e1": e1,
        #              "step_ev": step_ev,
        #              "step_s": step_s}
        #             }
        #             })



# def _cleanup():
#     config = get_config()
#     eh1_mpx = config.get('eh1_mpx')
#     eh1_mpx.switch('SEL_TRIG', 'STEP')
#     hook = config.get("fbragghook")
#     hook.clear_scan_traj()


def exafs_fast_fbragg(name, element, edge, step_ev=0.1, step_s=0.1, run=False):#e0, e1, step_ev, step_s, run=False):
    config = get_config()
    eh1_mpx = config.get('eh1_mpx')
    bragg = config.get('bragg')
    fbragg = config.get('fbragg')
    fscan_obj = config.get('d20fscanexafs')

    params = ExafsParameters(element, edge)

    # preset = FastXes5Preset(name, e0, e1, step_ev, step_s)
    # # fscan_obj.add_scan_preset(preset)
    
    fscan = fscan_obj.fscan
    # energy = config.get('energy')
    bragg.sync_hard()
    fbragg.sync_hard()
    fbragg.offset = bragg.offset
    fscan.pars.motor = fbragg
    d_spacing = bragg.controller.d_spacing()

    egy, tm = params._calc_cont()
    e0 = egy[0]
    e1 = egy[-1]
    print(e0, e1)
    # e0 = params.scan_pos[0]
    # e1 = params.scan_pos[-1]

    with cleanup(_cleanup):
        bragg_0 = energy2bragg(e0, d_spacing)
        bragg_1 = energy2bragg(e1, d_spacing)
        # n_points = abs(e1 - e0) / step_ev
        n_points = np.sum(tm) / step_ev
        # npoints = params.scan_pos.shape[0]#abs(e1 - e0) / step_ev

        # print("FAST", e0, e1, bragg_0, bragg_1)

        fscan.pars.start_pos = bragg_0
        fscan.pars.step_size = (bragg_1 - bragg_0) / n_points
        fscan.pars.latency_time = 0.001

        fscan.pars.acc_margin = 0.001 # / bragg.steps_per_unit
        
        fscan.pars.npoints = n_points
        fscan.pars.step_time = 0#step_s + 0.001
        fscan.pars.acq_time = step_s
        fscan.pars.gate_mode = 'TIME'
        fscan.pars.scan_mode = 'TIME'

        hook = config.get("fbragghook")
        fbragg.controller.set_next_scan_params(None)
        fbragg.controller.set_next_scan_params(params)

        # eh1_mpx.switch('SEL_TRIG', 'ZAP')
        fscan.prepare()
        if run:
            current_session.scan_saving.newsample(name)
            return fscan.run(
                {'instrument':
                 {"exafs":
                  {"element": element,
                   "edge": edge}
                 }
                })
        else:
            print(fscan)


# def exafs_constant_fbragg(name, element, edge, step_ev=0.01, step_s=0.1, run=False):
#     config = get_config()
#     eh1_mpx = config.get('eh1_mpx')
#     bragg = config.get('bragg')
#     fbragg = config.get('fbragg')
#     fscan_obj = config.get('d20fscanexafs')

#     # preset = FastXes5Preset(name, e0, e1, step_ev, step_s)
#     # # fscan_obj.add_scan_preset(preset)
#     params = ExafsParameters(element, edge)
#     egy = params.scan_pos
#     e0 = egy[0]
#     e1 = egy[-1]
#     print(e0, e1)

#     fscan = fscan_obj.fscan
#     # energy = config.get('energy')
#     bragg.sync_hard()
#     fbragg.sync_hard()
#     fbragg.offset = bragg.offset
#     fscan.pars.motor = fbragg
#     d_spacing = bragg.controller.d_spacing()

#     with cleanup(_cleanup): 
#         bragg_0 = energy2bragg(e0, d_spacing)
#         bragg_1 = energy2bragg(e1, d_spacing)
#         npoints = abs(e1 - e0) / step_ev

#         fscan.pars.start_pos = bragg_0
#         fscan.pars.step_size = (bragg_1 - bragg_0) / npoints
#         fscan.pars.latency_time = 0.001

#         fscan.pars.acc_margin = 0.001 # / bragg.steps_per_unit
        
#         fscan.pars.npoints = npoints
#         fscan.pars.step_time = 0#step_s + 0.001
#         fscan.pars.acq_time = step_s
#         fscan.pars.gate_mode = 'TIME'
#         fscan.pars.scan_mode = 'TIME'

#         eh1_mpx.switch('SEL_TRIG', 'ZAP')
#         fscan.prepare()
#         if run:
#             current_session.scan_saving.newsample(name)
#             return fscan.run(
#                 {'instrument':
#                  {"exafs":
#                   {"e0": e0,
#                    "e1": e1,
#                    "step_ev": step_ev,
#                    "step_s": step_s}
#                  }
#                 })
#         else:
#             print(fscan)


class FastExafsPreset(ScanPreset):
    def __init__(self):
        super().__init__()
        self._opiom = get_config().get(OPIOM_EH1)
        self._mono = get_config().get(MONO)

    def prepare(self, scan):
        self._opiom.switch('SEL_TRIG', 'ZAP')

    def start(self, scan):
        print(f"Starting scan {scan.name}")
        fbragg = get_config().get('fbragg')
        fbragg.sync_hard()
        # fbragg.controller._upload_exafs_params()
#         print(f"Opening the shutter")
        print("STARTED")

    def stop(self, scan):
        print("STOP")
        self._opiom.switch('SEL_TRIG', 'STEP')
        fbragg = get_config().get('fbragg')
        fbragg.controller.clear_scans_params()
        print("STOPPED")
        # # making sure the xtal2perp motor is in open loop
        # # because it is in vacuum but not cooled.
        # # TODO: add others?
        if not self._mono.pmac_comm.is_open_loop(4):
            self._mono.pmac_comm.open_loop(4)
        print(f"xtal2perp in open loop: {self._mono.pmac_comm.is_open_loop(4)}")

        # saving = scan.scan_saving
        # snun = scan.scan_number
        # data = scan.get_data()

        # dat_dir = os.path.join(saving.base_path, saving.proposal_dirname, saving.beamline, saving.proposal_session_name)
        # dat_file = f"{saving.collection_name}_{saving.dataset_name}_{snun}.dat"
        # data_file = os.path.join(dat_dir, dat_file)

        # columns = ['musstoh:fbragg_energy', "musstoh:fbragg_egy_delta",
        #            'p201_0:ct2_counters_controller:i0', 'xesm4:roi_det0',
        #             'xesm4:realtime_det0',
        #             'xesm4:trigger_livetime_det0', 'xesm4:energy_livetime_det0', 
        #             'xesm4:triggers_det0', 'xesm4:events_det0', 
        #             'xesm4:icr_det0', 'xesm4:ocr_det0', 'xesm4:deadtime_det0']
        # # print(data.keys())
        # # print(scan.scan_info)
        # print('====', (len(data[columns[0]]), len(columns)), data['musstoh:fbragg_energy'].shape)
        # data_ar = np.ndarray((len(data[columns[0]]), len(columns)))
        # for i, col in enumerate(columns):
        #     print(i, col, data[col].shape)
        #     data_ar[:, i] = data[col][:]
        # with open(data_file, "w") as ofile:
        #     ofile.write(tabulate(data_ar, headers=columns, tablefmt="plain"))
        # print(f"File writen: {data_file}")

# def _mkscan(name, element, edge, zap=False, run=True):
#     config = get_config()
#     eh1_mpx = config.get('eh1_mpx')
#     bragg = config.get('bragg')
#     # vbragg = config.get('vbragg')
#     bragg.sync_hard()
#     # vbragg.sync_hard()
#     with cleanup(_cleanup):
#         if zap:
#             fscan_obj = config.get('d20fscan')
#             # energy.settings.set('real_select', 1)
#             energy = config.get('egy_cs')
#             eh1_mpx.switch('SEL_TRIG', 'ZAP')
#             #TODO : sync bragg offset etc
#             vbragg.offset = bragg.offset
#             scan = ExafsContinuousScan(name, element, edge, energy,
#                                        vbragg, fscan_config=fscan_obj)
#         else:
#             # energy.settings.set('real_select', 0)
#             energy = config.get('energy')
#             eh1_mpx.switch('SEL_TRIG', 'STEP')
#             scan = ExafsStepScan(name, element, edge, energy, bragg)
#         energy.sync_hard()
#         return scan.run()


# def mkscan(name,
#            element,
#            edge):
#     return _mkscan(name, element, edge, zap=False)


# def mkscan_fast(name,
#                 element,
#                 edge):
#     return _mkscan(name, element, edge, zap=True)