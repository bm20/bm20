import numpy as np
from bliss.controllers.spectrometers.spectro_base import Analyser, Detector, Spectrometer
from bliss.physics.units import ur, units
from bliss.physics.diffraction import CrystalPlane, _get_all_crystals, MultiPlane, hc, string_to_crystal_plane
from bliss.controllers.motor import CalcController
from bliss.physics.diffraction import MultiPlane, CrystalPlane, Ge, Si, hc, string_to_crystal_plane
from typing import NamedTuple, Any

from bliss.common.protocols import HasMetadataForScan

RADIUS_M = 0.25  # ?
DET_DEG = 64.5


def d_spacing(lattice, h, k, l):
    return lattice / np.sqrt(h**2 + k**2 + l**2)

def bragg_deg(e_kev, refl_order, d_space):
    return np.rad2deg(np.arcsin(6.19926 * refl_order / (d_space * e_kev)))

def xtal2det(radius_m, bragg_deg):
    """
    Distance of:
    - center of xtal analyzer <-> center of detector
    - sample <-> center of the xtal analyzer
    SC in KK AS 2016
    """
    return 2 * radius_m * np.sin(np.deg2rad(bragg_deg)) * 1000

def det_angle(bragg):
    return 2 * bragg

def samp2det(bragg_deg, a2x):
    return 2 * a2x * np.cos(np.deg2rad(bragg_deg))

def _xpi(bragg_deg, radius_m, y_pos):
    return np.sqrt((2 * radius_m * 1000.) ** 2 * np.sin(np.deg2rad(bragg_deg))**4 - y_pos**2)

def someval(bragg_deg, y_pos, xpi):
    """
    cell 5 crystals_edit:N30 to N34
    """
    return np.pi - np.arctan(y_pos / (xpi * np.sin(np.deg2rad(bragg_deg))))

def get_achi(someval):
    return np.rad2deg(someval) - 180.

def someval2(bragg_deg, y_pos, xpi):
    """
    cell 5 crystals_edit:N35 to N39
    """
    bragg_rad = np.deg2rad(bragg_deg)
    return np.arctan(
            np.sqrt(
                y_pos ** 2 + xpi ** 2 * np.sin(bragg_rad) ** 2
            ) /
            (xpi * np.cos(bragg_rad))
        )

def get_ath(someval2):
    return np.rad2deg(someval2)

def get_dth(bragg_deg):
    # return np.pi * 3./2 - 2 * np.deg2rad(bragg_deg)
    return 180 - np.rad2deg(np.pi * 3./2 - 2 * np.deg2rad(bragg_deg))

def detector_theta_2_bragg(dth):
    # 180 - np.rad2deg(
    # np.pi * 3./2 - 2 * np.deg2rad(bragg_deg)
    # )
    return np.rad2deg(-(np.deg2rad(180-dth) - 3/2.*np.pi)/2.)



def get_dlong(bragg_deg, s2d_mm, det_deg):
    return s2d_mm * np.cos(np.deg2rad(det_deg - bragg_deg))

def get_dx(bragg_deg, s2d_mm, det_deg):
    return s2d_mm * np.sin(np.deg2rad(det_deg - bragg_deg))

def get_ax(bragg_deg, radius_m, xpi):
    bragg_rad = np.deg2rad(bragg_deg)
    return 2 * radius_m * 1000 * np.sin(bragg_rad) * np.cos(bragg_rad) ** 2 + xpi * np.sin(bragg_rad)
    
def get_az(bragg_deg, radius_m, xpi):
    bragg_rad = np.deg2rad(bragg_deg)
    return 2 * radius_m * 1000 * np.cos(bragg_rad) * np.sin(bragg_rad) ** 2 - xpi * np.cos(bragg_rad)


# class D20Analyzer(Analyser):
    
#     def compute_bragg_solution(self, bragg):
#         """returns a tuple (bragg, bragg_solution, reals_positions):
#         - bragg: the bragg angle associated to this solution.
#         - bragg_solution: a dict with relevant data for the position and orientation of this positioner.
#           Data expressed in the laboratory referential with an origin at (0,0,0) (i.e. ignoring self.referential_origin).
#         - reals_positions: the theoritical positions of the real axes for the given bragg value.
#           Reals positions expressed in laboratory referential (must take into account self.referential_origin!=(0,0,0))
#         """
#         y_pos = self.ypos
#         # xtal = self.xtal_sel

#         # dsp = d_spacing(lattice, h, k, l)
#         # d_spacing = (xtal.d * ur.meter).to(ur.angstrom)

#         # brg = bragg_deg(egy, reflection, d_spacing)
#         x2d = xtal2det(RADIUS_M, bragg)

#         # deta = det_angle(brg)
#         sd = samp2det(bragg, x2d)

#         xpi = _xpi(bragg, RADIUS_M, y_pos)

#         tmp0 = someval(bragg, y_pos, xpi)

#         achi = get_achi(tmp0)

#         tmp1 = someval2(bragg, y_pos, xpi)
#         ath = get_ath(tmp1)

#         ax = get_ax(bragg, RADIUS_M, xpi)
#         az = get_az(bragg, RADIUS_M, xpi)

#         # dth = get_dth(brg)
#         # dlong = get_dlong(brg, sd, det_deg)
#         # dx = get_dx(brg, sd, det_deg)

#         solution = {
#             "sd": sd,
#             "xpi": xpi,
#         }

#         reals_pos = {
#             "chi": achi,
#             "theta": ath,
#             "xpos": ax,
#             "zpos": az,
#             # "ypos": ay,
#             # "dth": dth,
#             # "dlong": dlong,
#             # "dx": dx
#         }

#         print("============", bragg, reals_pos)

#         return bragg, solution, reals_pos
    
#     def _get_pos(self, tag):
#         if tag == "ypos":
#             return self.config["ypos"]
#         if tag == "rpos":
#             x = self._get_pos("xpos")
#             y = self.config["ypos"]
#             return np.sqrt(x**2 + y**2)
#         if tag == "pitch":
#             return self._get_real_axis_pos("theta")
#         elif tag == "yaw":
#             return self._get_real_axis_pos("chi")
#         return self._get_real_axis_pos(tag)
    
#     # def _get_real_axis_pos(self, tag):
#     #     if self.trajectory_mode:
#     #         bragg = self.trajectory_axis.position
#     #         if np.isnan(bragg):
#     #             return bragg

#     #         if not self._current_bragg_solution or (
#     #             bragg != self._current_bragg_solution[0]
#     #         ):
#     #             self._update_bragg_solution(bragg)
#     #         reals = self._current_bragg_solution[2]
#     #         theo_pos = reals[tag]
#     #         return theo_pos
#     #     else:
#     #         return self.real_axes[tag].position
        

# class D20Detector(Detector):
#     def compute_bragg_solution(self, bragg):
#         """returns a tuple (bragg, bragg_solution, reals_positions):
#         - bragg: the bragg angle associated to this solution.
#         - bragg_solution: a dict with relevant data for the position and orientation of this positioner.
#           Data expressed in the laboratory referential with an origin at (0,0,0) (i.e. ignoring self.referential_origin).
#         - reals_positions: the theoritical positions of the real axes for the given bragg value.
#           Reals positions expressed in laboratory referential (must take into account self.referential_origin!=(0,0,0))
#         """
#         bsolution = self.target.compute_bragg_solution(bragg)[1]
#         bsolution = bsolution.copy()

#         sd = bsolution["sd"]

#         dth = get_dth(bragg)
#         dlong = get_dlong(bragg, sd, DET_DEG)
#         dx = get_dx(bragg, sd, DET_DEG)

#         bsolution.update({"dth": dth,
#                           "dlong": dlong,
#                           "dx": dx})
        
#         reals_pos = {"dth": dth,
#                      "dlong": dlong,
#                      "dx": dx}
#         return bragg, bsolution, reals_pos
    
#     def _get_pos(self, tag):
#         """return one of the position coordinates [xpos, ypos, zpos] or
#         orientation angles (pitch, yaw) expressed in the laboratory referential
#         (i.e taking into account a possible spectrometer origin != [0,0,0]).
#         The returned value must be computed from the actual real axes position to reflect
#         actual positioner situation.
#         args: tag is one of ["xpos", "ypos", "zpos", pitch, "yaw"]
#         """
#         # === !!! motor position is expressed in lab ref (so it already includes referential_origin offset) !!!
#         ypos = self.config.get("ypos", 0) + self.referential_origin[1]
#         # yaw = self.config.get("yaw", 0)
#         if tag == "rpos":
#             x = self._get_real_axis_pos("dx")
#             y = ypos
#             return np.sqrt(x**2 + y**2)
#         elif tag == "ypos":
#             return ypos
#         # elif tag == "yaw":
#         #     return yaw
#         elif tag == "xpos":
#             return self._get_real_axis_pos("dx")
#         elif tag == "zpos":
#             return self._get_real_axis_pos("dlong")
#         elif tag == "pitch":
#             return self._get_real_axis_pos("dth")
#         elif tag == "yaw":
#             return self._get_real_axis_pos("azlong")
#         raise RuntimeError(f"unknown tag {tag}")
    
