import itertools

from bliss.common.axis import Axis
from bliss.common.logtools import *
from bliss.config.static import get_config


from ..macros.motors import (parallel_group_mv, move_to_limit,
                             set_home_position, home_search,
                             is_homed)


def _bm20_slit_blade_names(*slit_names):
    return [f'{slit_name}{blade}'
            for slit_name in slit_names
            for blade in ('t', 'b', 'l', 'r')]


def _bm20_slit_blade_axes(*slit_names):
    config = get_config()
    axes = _bm20_slit_blade_names(*slit_names)
    return [config.get(axis) for axis in axes]


def _bm20_home_slits(slit_names, valid_slits, hutch, motor_group):
    """
    Home EH1 slits.

    Args:
        slit_names: optional, a list of slit names (s5, ...)
    """
    diff = set(slit_names) - set(valid_slits)
    if diff:
        raise ValueError(f'Error: invalid slit name(s):{diff}.'
                         f' Available: {valid_slits}.')

    return _bm20_home_hutch_slits(hutch, motor_group, *slit_names)


def _bm20_home_hutch_slits(hutch, motor_group, *slit_names):
    slit_blades = list(itertools.chain(*[_bm20_slit_blade_axes(slit_name)
                                         for slit_name in slit_names]))

    print('##########################################')
    print('##########################################')
    lprint(f'Homing {slit_names}.')
    blade_names = [blade.name for blade in slit_blades]
    lprint(f'Blades: {blade_names}.')
    print('##########################################')
    print('##########################################')
    return _bm20_home_slit_blades(hutch,
                                  motor_group,
                                  *slit_blades)


def _bm20_home_slit_blades(hutch, motorgroup, *slit_blades):
    if not all(isinstance(elem, (Axis,)) for elem in slit_blades):
        raise ValueError('Some parameters are not Axis objects.')

    # move to lim-
    # search for home+ switch
    # set user/dial/offset
    # move to 0

    print('##########################################')
    move_to_limit(-1, *slit_blades)
    print('##########################################')
    home_search(1, *slit_blades)
    print('##########################################')
    homed_blades = [blade for blade in slit_blades if is_homed(blade)]
    diff_blades = set(slit_blades) - set(homed_blades)
    print('##########################################')
    set_home_position(hutch, motorgroup, *homed_blades)
    print('##########################################')
    # parallel_group_mv(0, *homed_blades)
    # print('##########################################')
    if diff_blades:
        lprint('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        lprint('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('ERROR, some slit blades could not be homed:\n - '
              + '\n - '.join([f'{blade.name}' for blade in diff_blades]))
        lprint('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        lprint('!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    return bm20_status_object(*slit_blades, title='Homed blades')
    

def _bm20_slits_show(*slit_names):

    class _foo():
        def __info__(self):
            config = get_config()
            txt = '============= OH SLITS =============\n'
            for slit_name in slit_names:
                txt += f'  ====== {slit_name} ======\n'
                for s_type in 'vg', 'vo', 'hg', 'ho':
                    calc_name = f'{slit_name}{s_type}'
                    calc = config.get(calc_name)
                    txt += f'    - {calc_name} : {calc.position:> 5.4f}\n'
            return txt

    return _foo()


def _bm20_slits_status_object(*slits, **kwargs):
    slit_blades = _bm20_slit_blade_names(*slits)
    return bm20_status_object(*slit_blades, **kwargs)