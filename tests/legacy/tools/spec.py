import os
import sys
from collections import OrderedDict
from datetime import datetime
from contextlib import contextmanager

import h5py
import numpy as np


def _bliss_counters():
    counters = OrderedDict()
    counters["Energy"] = "energy"
    counters["epoch_trig"] = "Epoch"
    counters["i0"] = "i0"
    counters["i1"] = "i1"
    counters["i2"] = "i2"
    counters.update({
        f"fx_ge18_sum_sca{i}":f"sca{i}"
        for i in range(18)
    })
    counters.update({ f"fx_ge18_sum_icr{i}":f"icr{i}"
        for i in range(18)})
    counters.update({ f"fx_ge18_sum_lft{i}":f"lft{i}"
        for i in range(18)})
    counters["ge18_fluo"] = "fluo"
    counters["sec"] = "Seconds"

    return counters


@contextmanager
def spec_output(f_name=None, mode="w"):
    # Code to acquire resource, e.g.:
    try:
        if f_name is None:
            yield sys.stdout
        else:
            with open(f_name, mode) as file:
                yield file
    except Exception as ex:
        print(ex)
        raise


def split_line(values, header, width):
    text = ""
    n_val = len(values)
    n_lines = n_val // width
    if n_val >= width:
        for i in range(n_lines):
            if header:
                text += f"{header}{i} "
            text += " ".join(values[i*width:(i+1)*width])
            text += "\n"
    if n_val % width != 0:
        if header:
            text += f"{header}{n_lines} "
        idx = n_lines * width
        text += " ".join(values[idx:idx+n_val%7])
        text += "\n"
    return text


def bliss_exafs_to_spec(bliss_f, spec_f, scan_n=None, overwrite=False, stdout=False):
    """
    Converts a bm20 bliss EXAFS hdf5 file into a spec file.
    It will create on spec file per scan present in the hdf5 file.
    Won't work if measurements are 2D+.
    """
    if spec_f is not None and os.path.isfile(spec_f) and not overwrite:
        raise RuntimeError(f"File {spec_f} already exists.")

    # first getting the scans
    with h5py.File(bliss_f, "r") as bliss_h5:
        scans = {key: [key] for key in bliss_h5.keys()}
        if scan_n is not None:
            try:
                sel_scans = list(scan_n)
            except TypeError:
                sel_scans = [scan_n]
            sel_scans = [f"{sc}.1" for sc in sel_scans]
            found_scans = set(sel_scans) & set(scans.keys())
            if len(found_scans) != len(sel_scans):
                not_found = set(sel_scans) - found_scans
                raise RuntimeError(f"Scans not found: {not_found}.")
            scans = {key: [key] for key in sel_scans}
        
        if not scans:
            print("No scans found.")
            return

        # TODO: improve
        seq_scans = []
        for scan in scans.keys():
            # finding sequences
            if scan not in seq_scans:
                try:
                    scan_numbers = bliss_h5[f"{scan}/instrument/scan_numbers/data"][:]
                    scans[scan] = [f"{sc}.1" for sc in scan_numbers]
                    seq_scans += scans[scan]
                except KeyError:
                    pass

        if spec_f is None:
            spec_files = {scan: None for scan in scans}
        else:
            if len(scans) > 1:
                basename, ext = os.path.splitext(spec_f)
                spec_files = {scan: f"{basename}_{scan}{ext}" for scan in scans.keys()}
            else:
                spec_files = {list(scans.keys())[0]: spec_f}

            exists = [spec_file for spec_file in spec_files.values()
                    if os.path.exists(spec_file)]
            if exists:
                print("WARNING! Some output file(s) already exist:")
                for fname in exists:
                    print(f"  - {fname}")
                if not overwrite:
                    print("Cancelled.")
                    return
                else:
                    print("User chose to overwrite.")

            os.makedirs(os.path.dirname(spec_f), exist_ok=True)
        
        for scan, entries in scans.items():
            bliss_cntrs = _bliss_counters()

            n_entries = len(entries)
            scan_n = scan.split(".")[0]
            first = bliss_h5[entries[0]]
            positioners = set(first["instrument/positioners_start"].keys())
            try:
                counters = set(first["measurement"].keys())
            except KeyError:
                counters = None

            command = first["title"][()].decode()

            if n_entries > 1:
                command = [command]

            # check if this is necessary: do sequences
            #   always have the same positioners?
            for entry in entries[1:]:
                entry_h5 = bliss_h5[f"{entry}"]
                if (set(entry_h5["instrument/positioners_start"].keys())
                    != positioners):
                    raise RuntimeError("Can't merge sequence {scan}: scans "
                                       "must have the same positioners.")
                if (set(entry_h5["measurement"].keys())
                    != counters):
                    raise RuntimeError("Can't merge sequence {scan}: scans "
                                       "must have the same counters.")
                command.append(entry_h5["title"][()].decode())

            missing = set(counters) ^ set(bliss_cntrs.keys())
            for name in missing:
                try:
                    del bliss_cntrs[name]
                except KeyError:
                    pass
            
            scan_h5 = bliss_h5[scan]
            start_t = datetime.fromisoformat(scan_h5["start_time"][()].decode())
            start_e = start_t.timestamp()
            start_d = start_t.strftime("%a %b %d %H:%M:%S %Y")
            positioners = list(scan_h5["instrument/positioners_start"].keys())

            spec_file = spec_files[scan]
            with spec_output(spec_file) as spec_f:
                if spec_file is not None:
                    spec_f.write(f"#F {os.path.abspath(spec_file)}\n")
                spec_f.write(f"#E {start_e}\n")
                spec_f.write(f"#D {start_d}\n")
                spec_f.write(split_line(positioners, "#O", 7))
                spec_f.write(split_line(positioners, "#o", 7))

                if counters is None:
                    continue

                # counters = list(first["measurement"].keys())
                counters = list(bliss_cntrs.keys())
                # for counter in ['elapsed_time']:
                #     try:
                #         counters.remove(counter)
                #     except ValueError:
                #         pass

                cnt_names = list(bliss_cntrs.values())
                bliss_cnt_names = list(bliss_cntrs.keys())
                spec_f.write(split_line(cnt_names, "#J", 7))
                spec_f.write(split_line(cnt_names, "#j", 7))

                spec_f.write("\n\n")

                if n_entries == 1:
                    spec_f.write(f"#S {scan_n} {command}\n")
                else:
                    spec_f.write(f"#S MERGE of scans, see comments below\n")

                spec_f.write(f"#D {start_d}\n")

                pos_start_grp = first["instrument/positioners_start"]
                pos_start = [f"{pos_start_grp[pos][()]}"
                             for pos in positioners]
                             
                spec_f.write(split_line(pos_start, "#P", 7))

                spec_f.write("#L " + " ".join(cnt_names) + "\n")
                # from tabulate import tabulate
                for entry in entries:
                    measurement = bliss_h5[entry][f"measurement"]
                    n_data = measurement[f"{bliss_cnt_names[0]}"].shape[0]
                    data = np.ndarray((len(bliss_cnt_names), n_data))
                    for i, counter in enumerate(bliss_cnt_names):
                        if counter == 'epoch_trig':
                            data[i, :] = measurement[f"{counter}"][:] - start_e
                        else:
                            data[i, :] = measurement[f"{counter}"][:]
                    np.savetxt(spec_f, data.T, delimiter=" ", fmt="%f")
                    # spec_f.write(tabulate(data.T))


if __name__ == "__main__":
    bliss_f = "/data/bm20/inhouse/bm202210/bm20/20221001/sample/sample_exafs/sample_exafs.h5"
    spec_f = "/data/bm20/inhouse/bm202210/bm20/20221001/sample/sample_exafs/spec/sample_exafs.spec"
    res = bliss_exafs_to_spec(bliss_f, spec_f, scan_n=[3], overwrite=True)
    # print(res)



# test EXAFS in opd20@ld209:dev/sample_exafs

#F /data/bm20/inhouse/EXAFS/EV-408_3/5mM_Re_3w_ph8_15K_ReL3_000_000.dat
#E 1663166379
#D Wed Sep 14 16:39:39 2022
#C exafs  User = opd20

#O0   Energy  braggangle  monoxtal  supphori  cryovert  ge13det  siddet
#O1 multivert  multigoni  phothori  photvert  s5vertgap  s5vertoff  s5horigap  s5horioff
#O2 micx  lateral  sy  tablevert  cryorot  sz  SY_long  
#o0  energy mono monxtal suph cv ged sdd
#o1 mulv mulg photh photv s5vg s5vo s5hg s5ho
#o2 micx dlat sy tablev crot sz SY_long 
#J0 Seconds  i0  i1  i2  i00  i10  i20  sca00
#J1 sca01  sca02  sca03  sca04  sca05  sca06  sca07  sca08
#J2 sca09  sca10  sca11  sca12  sca13  sca14  sca15  sca16
#J3 sca17  scaSum  scaAvg  icr00  icr01  icr02  icr03  icr04
#J4 icr05  icr06  icr07  icr08  icr09  icr10  icr11  icr12
#J5 icr13  icr14  icr15  icr16  icr17  lft00  lft01  lft02
#J6 lft03  lft04  lft05  lft06  lft07  lft08  lft09  lft10
#J7 lft11  lft12  lft13  lft14  lft15  lft16  lft17  trsample
#J8 trref  fluo  SRCurrent  m_mono  
#j0 sec i0 i1 i2 i00 i10 i20 sca00
#j1 sca01 sca02 sca03 sca04 sca05 sca06 sca07 sca08
#j2 sca09 sca10 sca11 sca12 sca13 sca14 sca15 sca16
#j3 sca17 scaSum scaAvg xicr00 xicr01 xicr02 xicr03 xicr04
#j4 xicr05 xicr06 xicr07 xicr08 xicr09 xicr10 xicr11 xicr12
#j5 xicr13 xicr14 xicr15 xicr16 xicr17 xlt00 xlt01 xlt02
#j6 xlt03 xlt04 xlt05 xlt06 xlt07 xlt08 xlt09 xlt10
#j7 xlt11 xlt12 xlt13 xlt14 xlt15 xlt16 xlt17 trsamp
#j8 trref fluo srcur m_mono 

