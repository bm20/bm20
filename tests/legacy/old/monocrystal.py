"""
BM20 DCM/DMM crystal selection and information
"""


# from bliss.shell.standard import umv
# from bliss.common.axis import AxisState
# from bliss.common.logtools import log_debug, log_error, log_warning, lprint
# from bliss.config.static import get_config
# from bliss.controllers.motor import Controller, CalcController

from tabulate import tabulate

from bliss.common.logtools import (lprint,
                                   log_error,
                                   log_info,
                                   log_debug,
                                   log_warning)

from .modes import get_mono_modes
from .oh_objects import bm20_mono, bm20_mono_wago





class MonoCrystal():
    def __init__(self, name, config):
        self._modes = get_mono_modes()
        self._wago = None
        self._mono = None

    def __info__(self):
        current_mode = self.get_crystal(raise_ex=False)
        text = []
        if current_mode == -1:
            text.append(['ERROR', 'ERROR', 'ERROR', 'ERROR', 'ERROR'])
            text.append(['->>', 'N/A',
                         'ERROR',
                         f'More than one channel set '
                         f'({self._get_active_dcm_channels()}).', '<<-'])
        elif current_mode == 0:
            text.append(['WARNING', 'WARNING', 'WARNING', 'WARNING', 'WARNING'])
            text.append(['->>', 'N/A',
                         'WARNING',
                         f'Mode is NOT set'
                         f'({self._get_active_dcm_channels()}).', '<<-'])
        for mode in self._modes.values():
            text.append(['', f'{mode.address}', mode.d_spacing, mode.description, ''])
            if mode.address == current_mode:
                text[-1][0] = f'->>'
                text[-1][-1] = '<<-'

        text = tabulate(text, headers=['', 'm', 'd_spacing', 'description', ''])
        text = (f'===============\n'
                f'Monocrystal\n'
                f'Current mode: {current_mode}\n'
                f'===============\n' + text)
        return text

    def wago(self):
        if self._wago is None:
            self._wago = bm20_mono_wago()
        return self._wago

    def mono(self):
        if self._mono is None:
            self._mono = bm20_mono()
        return self._mono

    def _get_active_dcm_channels(self):
        """
        Returns the value of the wago channels linked to the crystal selection
        """
        wago = self.wago()
        return [address
                for address, mode in self._modes.items()
                # if self._wago.get(mode.w_channel)]
                if wago.get(mode.r_channel)]

    def get_crystal(self, raise_ex=True):
        current_modes = self. _get_active_dcm_channels()
        if len(current_modes) > 1:
            if raise_ex:
                raise RuntimeError(f'Error, more than one wago channels set '
                                f'to 1. Should be only one: {current_modes}')
            else:
                return -1
        log_debug(self, f'get_crystal: set channel={current_modes}.')
        if len(current_modes) == 0:
            return 0
        return current_modes[0]

    def __call__(self, mode):
        if mode not in self._modes.keys():
            raise ValueError(f'Unknown mode: {mode}. '
                             f'(allowed: {list(self._modes.keys())}).')
        print('TODO')
        # if mode is None:
        #     print(self._config.get('xtal').__info__())
        # else:
        #     umv(self._config.get('mode'), mode)

        # config = get_config()

        # energy = config.get('energy')
        # mono = config.get('d20mono')
        # energy_d_spacing = energy.settings.get('dspace')
        # mono_d_spacing = mono.d_spacing()

        # if mono_d_spacing != energy_d_spacing:
        #     log_warning(self, 'Dspacing values mistmatch, updating...')
        #     mono._update_dspacing()

        # energy_d_spacing = energy.settings.get('dspace')
        # mono_d_spacing = mono.d_spacing()

        # if mono_d_spacing != energy_d_spacing:
        #     log_error(self, f'Dspacing values mistmatch:\n'
        #                     f'- energy controller: {energy_d_spacing}\n'
        #                     f'- mono controller: {mono_d_spacing}.')
        # else:
        #     lprint(f'===\n INFO: D-Spacing is {energy_d_spacing}.\n===')


        #     def mono_mode_lat_position(self, mode):
#         return self._mono_modes[mode].lat_position



#     def read_dcm_mode(self, raise_ex=True):
#         """
#         Returns the current DCM mode.
        
#         Args:
#             raise_ex (bool): if True, raises an exception
#                              if no mode (or several) is set.
#                              if False, returns None on error.
#         """
#         active_chans = self.get_active_dcm_channels()
#         if len(active_chans) > 1:
#             if raise_ex:
#                 raise RuntimeError(f'Error, more than one active channels set to 1'
#                                 f' on {self._wago.name}.')
#             else:
#                 return None
#         if len(active_chans) == 0:
#             if raise_ex:
#                 raise RuntimeError(f'Error, no active channel set '
#                                     f' on {self._wago.name} '
#                                     f'(Try changing the mode. Was the DCM homed?).')
#             else:
#                 return None
#         return active_chans[0]

#     def write_dcm_mode(self, mode):
#         try:
#             mode_info = self._mono_modes[mode]
#         except KeyError:
#             raise ValueError(f'Unknown DCM mode : {mode}.')
#         self.clear_wago()
#         self._wago.set(mode_info.w_channel, 1)