"""
Some helpers to retrieve some often used objects.
Hopefully will avoid hardcoding names everywhere
in generic code.
"""

from ..macros.motor_utils import get_config_object


BM20_OH_SLITS = ('s1', 's2', 's3',)

BM20_OH_M1_JACKS = ('m1jl', 'm1jr', 'm1jf')
BM20_OH_M1_LAT = ('m1latf', 'm1latb')  # keep that order!
BM20_OH_M1_BEND = 'm1bends'

BM20_OH_M2_JACKS = ('m2jl', 'm2jr', 'm2jf')
BM20_OH_M2_LAT = ('m2latf', 'm2latb')  # keep that order!
BM20_OH_M2_BEND = 'm2bends'


def bm20_mono():
    """
    Returns the BM20 PMAC (DCM) BLISS controller instance
    """
    return get_config_object('d20pmac')


def bm20_mono():
    return get_config_object('d20pmac')


def bm20_energy():
    return get_config_object('energy')


def bm20_mirrors_surfaces():
    mirr1 = get_config_object('m1surface')
    mirr2 = get_config_object('m2surface')
    return mirr1, mirr2


def bm20_attenuators():
    atten1 = get_config_object('atten1')
    atten2 = get_config_object('atten2')
    return atten1, atten2


def bm20_monomode():
    return get_config_object('monomode')


def bm20_mono_wago():
    return bm20_mono()._wago #get_config_object('wcbm20i')


def bm20_oh_slits():
    return [get_config_object(name) for name in BM20_OH_SLITS]
