from bliss.common.hook import MotionHook
from bliss.config.static import get_config

from .d20pmac import mono_modes
from .oh_objects import bm20_monomode


class MonomodeHook(MotionHook):
    def __init__(self, name, config):
        super(MonomodeHook, self).__init__()

    def post_move(self, motion_list):
        axis = motion_list[0].axis
        axis.controller._update_dspacing(mode=axis.position)


class MonoModeError(Exception):
    pass


class EnergyHook(MotionHook):
    def __init__(self, name, config):
        super(EnergyHook, self).__init__()
        self._monomode = None

    def monomode(self):
        if self._monomode is None:
            self._monomode = bm20_monomode()
        return self._monomode

    def pre_move(self, motion_list):
        monomode = self.monomode()
        if monomode.position not in monomode.keys():
            raise MonoModeError(f'Invalid DCM/DMM mode {monomode.position}. '
                                f'Can\'t move energy.')
