from enum import Enum

# home positions
# the value is what the axis dial is set to when the axis
# is sitting at the home position


# class HomePositionsMirror1(Enum):
    # M1JF = 8.075499
    # M1JL = 6.159800
    # M1JR = 6.206560
    # M1LATF = -8.939308
    # M1LATB = -9.844916
    # M1BENDS = 0.


# class HomePositionsMirror2(Enum):
#     M2JF = 8.6000004
#     M2JL = 7.6999998
#     M2JR = 7.25
#     M2LATF = -6.900000
#     M2LATB = -6.
#     M2BENDS = 0.

# class HomePositionsAttenuator(Enum):
#     ATTA = -0.089659  # was home_position of att2 in SPEC
#     ATTB = -0.061032  # was home_position of att1 in SPEC
#     # reminder 01/01/2020 = atta (icepap) = att2 (bliss)
#     # reminder 01/01/2020 = attb (icepap) = att1 (bliss)
#     # to stay coherent with SPEC


# class HomePositionsSlits(Enum):
    # S1L = 0.0
    # S1R = 15.4775
    # S1T = 7.9285002
    # S1B = -8.3704996
    # S2L = -13.0605
    # S2R = 12.6565
    # S2T = 14.278
    # S2B = -6.552
    # S3L = 23.150499
    # S3R = -23.088499
    # S3T = 7.1240001
    # S3B = -5.875
    # S5L = 6.7505
    # S5R = -3.859
    # S5T = 26.29
    # S5B = -0.85


# class HomePositionsEH1Slits(Enum):
#     S5L = 6.7505
#     S5R = -3.859
#     S5T = 26.29
#     S5B = -0.85
    # S6L = 5.515
    # S6R = -2.781
    # S6T = -0.9675
    # S6B = -1.929
