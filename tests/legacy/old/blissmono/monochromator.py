from bliss.config.static import get_config
from bliss.controllers.monochromator import EnergyTrackingObject, MonochromatorFixExit

from bm20.oh.mono.energy import energy2bragg, bragg_to_perp
from bm20.config.oh_aliases import OpticsAliases
from bm20.config.mono import D20Crystals
from bm20.oh.mono.d20monowago import D20MonoWago

import numpy as np


# def bragg_to_perp(angle, xtal_ofst):
#     return xtal_ofst / (2 * np.cos(np.radians(angle)))


class D20EnergyTrackingObject(EnergyTrackingObject):
    def _ene2x2perp(self, energy, *args, **kwargs):
        print('=====', energy, args, kwargs)
        return self._mono.energy2dxtal(energy)
        #bragg_to_perp(self._mono.energy2bragg(energy), self._mono.fix_exit_offset)
        # return bragg_to_perp(energy2bragg(energy))


class D20Monochromator(MonochromatorFixExit):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._monowago = None

    @property
    def monowago(self):
        if self._monowago is None:
            pmac = get_config().get(OpticsAliases.MONO.value)
            self._monowago = D20MonoWago(pmac.config.get('wago'))
        return self._monowago

    def _xtal_change(self, xtal):
        return super()._xtal_change(xtal)

    def _xtal_is_in(self, xtal):
        print('IN?', xtal)
        crystal = self.monowago.read_dcm_crystal()
        return crystal.label == xtal