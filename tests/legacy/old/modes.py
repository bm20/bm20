from bliss.config.static import get_config


class MonoMode:
    def __init__(self,
                 name=None,
                 label=None,
                 description=None,
                 address=None,
                 w_channel=None,
                 r_channel=None,
                 p_register=None,
                 d_spacing=None,
                 low_limit=None,
                 high_limit=None):
        self.name = name
        self.label = label
        self.description = description
        self.address = address
        self.w_channel = w_channel
        self.r_channel = r_channel
        self.p_register = p_register
        self.d_spacing = d_spacing
        self.low_limit = low_limit
        self.high_limit = high_limit
        self.lat_position = None


def get_mono_modes():
    modes = get_config().get_config('monomodes')['modes']
    mono_modes = dict((int(mode['address']), MonoMode(**mode))
                      for mode in modes)
    return mono_modes