from bliss.controllers.motor import Controller
from bliss.common.axis import AxisState
from bliss.config.static import get_config
from bliss.config.channels import Cache
from bliss.common import event
from bliss.common.logtools import log_warning

import numpy as np

import gevent

from bm20.config.mono import MonoCageAxis
from bm20.oh.mono.energy import bragg2energy, energy2bragg
from .pmac_cs import CLEAR, INVERSE, FORWARD

"""
Bliss controller for XXX.
"""

PMAC_CS_STATUS = (
    ((1 << (23 + 48), "Z-Axis Used in Feedrate Calculations"),
    (1 << (22 + 48), "Z-Axis Incremental Mode"),
    (1 << (21 + 48), "Y-Axis Used in Feedrate Calculations"),
    (1 << (20 + 48), "Y-Axis Incremental Mode"),
    (1 << (19 + 48), "X-Axis Used in Feedrate Calculations"),
    (1 << (18 + 48), "X-Axis Incremental Mode"),
    (1 << (17 + 48), "W-Axis Used in Feedrate Calculations"),
    (1 << (16 + 48), "W-Axis Incremental Mode"),
    (1 << (15 + 48), "V-Axis Used in Feedrate Calculations"),
    (1 << (14 + 48), "V-Axis Incremental Mode"),
    (1 << (13 + 48), "U-Axis Used in Feedrate Calculations"),
    (1 << (12 + 48), "U-Axis Incremental Mode"),
    (1 << (11 + 48), "C-Axis Used in Feedrate Calculations"),
    (1 << (10 + 48), "C-Axis Incremental Mode"),
    (1 << (9 + 48), "B-Axis Used in Feedrate Calculations"),
    (1 << (8 + 48), "B-Axis Incremental Mode"),
    (1 << (7 + 48), "A-Axis Used in Feedrate Calculations"),
    (1 << (6 + 48), "A-Axis Incremental Mode"),
    (1 << (5 + 48), "Radius Vector Incremental Mode"),
    (1 << (4 + 48), "Continuous Motion Request"),
    (1 << (3 + 48), "Move-Specified-by-Time Mode"),
    (1 << (2 + 48), "Continuous Motion Mode"),
    (1 << (1 + 48), "Single-Step Mode"),
    (1 << (0 + 48), "Running Program"),
    (1 << (23 + 24), "Lookahead in Progress"),
    (1 << (22 + 24), "Run-Time Error"),
    (1 << (21 + 24), "Move In Stack"),
    (1 << (20 + 24), "Amplifier Fault Error"),
    (1 << (19 + 24), "Fatal Following Error"),
    (1 << (18 + 24), "Warning Following Error"),
    (1 << (17 + 24), "In Position"),
    (1 << (16 + 24), "Rotary Buffer Request"),
    (1 << (15 + 24), "Delayed Calculation Flag"),
    (1 << (14 + 24), "End of Block Stop"),
    (1 << (13 + 24), "Synchronous M-variable One-Shot"),
    (1 << (12 + 24), "Dwell Move Buffered"),
    (1 << (11 + 24), "Cutter Comp Outside Corner"),
    (1 << (10 + 24), "Cutter Comp Move Stop Request"),
    (1 << (9 + 24), "Cutter Comp Move Buffered"),
    (1 << (8 + 24), "Pre-jog Move Flag"),
    (1 << (7 + 24), "Segmented Move in Progress"),
    (1 << (6 + 24), "Segmented Move Acceleration"),
    (1 << (5 + 24), "Segmented Move Stop Request"),
    (1 << (4 + 24), "PVT/SPLINE Move Mode"),
    (1 << (3 + 24), "2D Cutter Comp Left/3D Cutter Comp On"),
    (1 << (2 + 24), "2D Cutter Comp On"),
    (1 << (1 + 24), "CCW Circle/Rapid Mode"),
    (1 << (0 + 24), "CIRCLE/SPLINE Move Mode"),
    (1 << (23 + 0), "Lookahead Buffer Wrap"),
    (1 << (22 + 0), "Lookahead Lookback Active"),
    (1 << (21 + 0), "Lookahead Buffer End"),
    (1 << (20 + 0), "Lookahead Synchronous M-variable"),
    (1 << (19 + 0), "Lookahead Synchronous M-variable Overflow"),
    (1 << (18 + 0), "Lookahead Buffer Direction"),
    (1 << (17 + 0), "Lookahead Buffer Stop"),
    (1 << (16 + 0), "Lookahead Buffer Change"),
    (1 << (15 + 0), "Lookahead Buffer Last Segment"),
    (1 << (14 + 0), "Lookahead Buffer Recalculate"),
    (1 << (13 + 0), "Lookahead Buffer Flush"),
    (1 << (12 + 0), "Lookahead Buffer Last Move"),
    (1 << (11 + 0), "Lookahead Buffer Single-Segment Request"),
    (1 << (10 + 0), "Lookahead Buffer Change Request"),
    (1 << (9 + 0), "Lookahead Buffer Movement Request"),
    (1 << (8 + 0), "Lookahead Buffer Direction Request"),
    (1 << (7 + 0), "RFU"),
    (1 << (6 + 0), "RFU"),
    (1 << (5 + 0), "RFU"),
    (1 << (4 + 0), "RFU"),
    (1 << (3 + 0), "Radius Error"),
    (1 << (2 + 0), "Program Resume Error"),
    (1 << (1 + 0), "Desired Position Limit Stop"),
    (1 << (0 + 0), "In-Program PMATCH"))
)


def pmac_cs_status_to_str(state):
    text = "\n".join(
        [
            f"{i//24}-{23-i%24}: {desc}"
            for i, (flag, desc) in enumerate(PMAC_CS_STATUS)
            if flag & state
        ]
    )
    return hex(state) + "\n" + text


PMAC_COORD_SYS = 16


CS_PROG = ["CLOSE",
           "OPEN PROG {prog_num}",
           "CLEAR",
           "{move_type}",
           "LINEAR",
           "F{velocity}",
           "X{target}",
           "CLOSE"]


CS_PROG_LINEAR =["LINEAR",
                 "F{velocity}",
                 "X{target}"]

CS_PROG_START = ["CLOSE",
                 "OPEN PROG {prog_num}",
                 "CLEAR",
                 "{move_type}"]

CS_PROG_END = ["CLOSE"]


class D20VirtualBragg(Controller):
    def __init__(self, *args, **kwargs):
        Controller.__init__(self, *args, **kwargs)
        self._pmac = self.config.get('pmac')
        self._pmac_cs_num = 16
        self._pmac_cs_i_num = 66
        self._prog_num = 666
        self._pmac = self.config.get('pmac')
        self._bragg = None
        # self.axis_settings.config_setting["velocity"] = False
        # self.axis_settings.config_setting["acceleration"] = False
        # self.axis_settings.config_setting["steps_per_unit"] = False
        self._pvt_axis_data = {}
        
    def initialize_hardware(self):
        # print('IH')
        self._pmac._write_multiline(CLEAR, write=True)
        self._pmac._write_multiline(INVERSE, write=True)
        self._pmac._write_multiline(FORWARD, write=True)

    def initialize(self):
        # print('I')
        self._bragg = self.config.get('bragg')
        self._perp = self.config.get('perp')

    def _bragg_offset_changed(self, offset):
        self.axes.get('vbragg').offset = offset
    
    def initialize_hardware_axis(self, axis):
        # print('IHA')
        super().initialize_hardware_axis(axis)
        config = get_config()
        if axis.name == 'vbragg':
            # USE TAGES INSTEAD OF NAME
            self._bragg_offset_changed(self._bragg.offset)
            event.connect(self._bragg, "offset", self._bragg_offset_changed)
        # print('bmh0', axis._bragg.motion_hooks)
        # hook = config.get('bragg2vbragghook')
        # axis._bragg.motion_hooks.append(hook)
        # from bliss.common import event
        # from functools import partial
        # chan = Channel(axis._bragg.state)
        # axis._bragg_callback = partial(self._bragg_state, axis)
        # self._vbragg = axis
        # event.connect(axis._bragg, "state", self._bragg_state)
            if axis.steps_per_unit != self._bragg.steps_per_unit:
                raise RuntimeError('steps_per_unit')
        
    # def _bragg_state(self, state):
    #     print('EV', state)
    #     self._vbragg.sync_hard()

    def initialize_axis(self, axis):
        """
        Reads specific config
        Adds specific methods
        """
        # axis._bragg = axis.config.get('bragg')
        # print('bmh', axis._bragg.motion_hooks)
        # axis._perp = axis.config.get('perp')
        self._pvt_axis_data[axis.name] = {}

    def read_position(self, axis):
        # axis._bragg.sync_hard()
        pos = self._bragg._hw_position * self._bragg.steps_per_unit
        return pos

    def read_velocity(self, axis):
        return self._pvt_axis_data[axis.name].get('velocity', self._bragg.velocity)

    def set_velocity(self, axis, new_velocity):
        self._pvt_axis_data[axis.name]['velocity'] = new_velocity
        
    def read_acceleration(self, axis):
            # TODO
        return self._bragg.acceleration * self._bragg.steps_per_unit
    
    def set_acceleration(self, axis, acceleration):
            # TODO
        pass
        # return axis._bragg.acceleration
        
    def state(self, axis):
        in_pos = f"M{self._pmac_cs_i_num}87"
        prog_running = f"M{self._pmac_cs_i_num}80"
        reply = self._pmac.pmac_comm.sendline_getbuffer(in_pos + prog_running)
        in_pos, prog_running = map(int, reply)
        # print('\n', axis.name, in_pos, prog_running, self._pmac.pmac_comm.sendline_getbuffer('#1?#4?'), '\n')#, axis._bragg.hw_state, self._pmac.pmac_comm.sendline_getbuffer(f"??"))
        # if prog_running or not in_pos:
        #     # TODO: a mix of bragg and xtal2perp
        #     return AxisState("MOVING")
        # return axis._bragg.hw_state
        state = self._bragg.hw_state
        if prog_running or self._perp.hw_state.MOVING:
            state.set("MOVING")
            # state.unset("READY")
        return state

    def prepare_move(self, motion):
        if not motion.type == "move":
            raise ValueError(f"Unsupported motion type: {motion.type}.")
        if motion.target_pos is not None:
            move_type = 'ABS'
            target = motion.target_pos
        elif motion.delta is not None:
            move_type = 'INC'
            raise ValueError(f"Unsupported motion type: {motion.type}.")
            # target = motion.delta + motion.axis.position
        else:
            raise ValueError("target_pos and delta are both None.")
        
        # b_start = motion.axis.position
        # step_ev = abs(target - b_start) / 
        
        # crystal = config.get('crystal')
        # _prog_move_spline1_bragg(axis, crystal.d_spacing(),
        #                          b_start, target, step_ev, step_s,
        #                          cs_num=16, prog_num=666, cs_i_num=66, abs=True,
        #                          run_prog=False)
        # target is in steps
        # target /= motion.axis.steps_per_unit
        # axis.velocity is in engineering units/s
        # CS velocity is in engineering units/isx90 (ms)
        # 0.4 u/s -> 80000st/s -> 80st/ms ->
        isx90 = float(self._pmac.pmac_comm.sendline_getbuffer(f'I{self._pmac_cs_i_num}90'))
        velocity = motion.axis.steps_per_unit * isx90 * motion.axis.velocity / 1000.
        print(motion.target_pos, motion.delta, velocity)
        prog = '\r'.join(CS_PROG)
        prog = prog.format(prog_num=self._prog_num,
                           move_type=move_type,
                           velocity=velocity,
                           target=target)
        print(prog.split('\r'))
        self._pmac.pmac_comm.sendline_getbuffer(prog)

    def start_one(self, motion):
        cmd = f"#4J/&{self._pmac_cs_num}B{self._prog_num}R"
        try:
            pass
            self._pmac.pmac_comm.sendline_getbuffer(cmd)
        except:
            # make sure x2perp is back to open loop
            # because it's in vacuum, and closed loop would 
            # be bad
            self._pmac.pmac_comm.sendline_getbuffer('#4J:0')

    def stop(self, axis):
        print('STOP')
        self._pmac.pmac_comm.sendline_getbuffer(f"&{self._pmac_cs_num}A#4J:0")
        
    def stop_all(self, *motions):
        print('STOPALL')
        self._pmac.pmac_comm.sendline_getbuffer(f"&{self._pmac_cs_num}A#4J:0")



def _prog_move_multi(axis, targets, cs_num=16,prog_num=666, cs_i_num=66, abs=True):
    if abs:
        move_type = "ABS"
    else:
        move_type = "INC"
    # if not motion.type == "move":
    #     raise ValueError(f"Unsupported motion type: {motion.type}.")
    # if motion.target_pos is not None:
    #     move_type = 'ABS'
    #     target = motion.target_pos
    # elif motion.delta is not None:
    #     move_type = 'INC'
    #     target = motion.delta
    # else:
    #     raise ValueError("target_pos and delta are both None.")
    
    # target is in steps
    # target /= motion.axis.steps_per_unit
    # axis.velocity is in engineering units/s
    # CS velocity is in engineering units/isx90 (ms)
    # 0.4 u/s -> 80000st/s -> 80st/ms ->
    isx90 = float(axis.controller._pmac.pmac_comm.sendline_getbuffer(f'I{cs_i_num}90'))

    prog = '\r'.join(CS_PROG_START).format(prog_num=prog_num, move_type=move_type)
    print(targets)
    for (pos, vel) in targets:
        velocity = axis.steps_per_unit * isx90 * vel / 1000.
        print(velocity)
        prog += "\r" + '\r'.join(CS_PROG_LINEAR).format(velocity=velocity,target=pos*axis.steps_per_unit)
    prog += "\r" + '\r'.join(CS_PROG_END)
    print(prog.split('\r'))
    axis.controller._pmac.pmac_comm.sendline_getbuffer(prog)
    cmd = f"#4J/&{cs_num}B{prog_num}R"
    try:
        print('COMMAND=', cmd)
        axis.controller._pmac.pmac_comm.sendline_getbuffer(cmd)
    except:
        # make sure x2perp is back to open loop
        # because it's in vacuum, and closed loop would 
        # be bad
        axis.controller._pmac.pmac_comm.sendline_getbuffer('#4J:0')


def _prog_move_pvt(axis, d_spacing,
                   e_start, e_end, step_ev, step_s,
                   cs_num=16, prog_num=666, cs_i_num=66, abs=True,
                   run_prog=False):
    if abs:
        # move_type = "ABS"
        pass
    else:
        raise NotImplemented()
        # move_type = "INC"
    
    prog = energy_pvt_prog(axis, d_spacing,
                           e_start, e_end,
                           step_ev=step_ev,
                           step_s=step_s,
                           cs_num=cs_num, prog_num=prog_num, cs_i_num=cs_i_num)
    print(prog.split('\r'))    
    if run_prog:
        axis.controller._write_multiline(prog, write=1)
        cmd = f"#4J/&{cs_num}B{prog_num}R"
        try:
            print('COMMAND=', cmd)
            axis.controller.pmac_comm.sendline_getbuffer(cmd)
            while True:
                gevent.sleep(0.5)
                axis.sync_hard()
                if not axis.state.MOVING:
                    print('NOT MOVING')
                    break
                else:
                    print('MOVING', axis.position)
        except Exception as ex:
            # make sure x2perp is back to open loop
            # because it's in vacuum, and closed loop would 
            # mean that the motor is still powered and it
            # will heat
            print('ERROR', ex)
            axis.controller.pmac_comm.sendline_getbuffer('A')
            axis.controller.pmac_comm.sendline_getbuffer('#4J:0')
            print('STOPPED')
            

def energy_to_steps_pvt(e_start,
                        e_end,
                        d_spacing,
                        step_ev,
                        step_s,
                        steps_per_unit,
                        offset,
                        extra_seg_before=0,
                        extra_seg_after=0,
                        segment_t_s=2):
    """
    Computes a PMAC PVT array, with T=segment_t_s
    """
    # by default in PVT mode the segments can't be > 4095.9998
    # at least on the turbo pmac (unless you change some parameter)
    assert segment_t_s < 4.095

    # step in energy over segment_t_s: segment_t_s (s) * speed (eV/s)
    egy_step = segment_t_s * (step_ev / step_s)

    egys = np.arange(e_start - extra_seg_before * egy_step,
                     e_end + extra_seg_after * egy_step, egy_step)

    # bragg angle (with offset)
    braggs = energy2bragg(egys, d_spacing) - offset
    # position in motor steps
    bragg_steps = braggs * steps_per_unit

    # rough estimate of the velocity at the end of each segment
    # slightly != than getting mid steps from mid energy
    # TODO: probably negligible difference, but should test
    mid_braggs = braggs[0:-1] + np.diff(braggs) / 2
    mid_steps = mid_braggs * steps_per_unit

    steps_vel = np.abs(np.diff(mid_steps) / segment_t_s)

    result = np.ndarray((braggs.shape[0], 2))
    result[:, 0] = bragg_steps
    result[1:-1, 1] = steps_vel
    result[0, 1] = 0
    result[-1, 1] = 0

    return result


def energy_pvt_prog(axis, d_spacing,
                    e_start, e_end,
                    step_ev, step_s,
                    segment_t_s=2.,
                    cs_num=16, prog_num=666, cs_i_num=66):
    move_type = "ABS"

    # by default in PVT mode the segments can't be > 4095.9998
    # at least on the turbo pmac (unless you change some parameter)
    assert segment_t_s < 4.095

    offset = axis.offset
    steps_per_unit = axis.steps_per_unit
    # velocity is in units per [isx90] ms
    isx90 = float(axis.controller.pmac_comm.sendline_getbuffer(f'I{cs_i_num}90'))

    pvt = energy_to_steps_pvt(e_start, e_end, d_spacing, step_ev, step_s, steps_per_unit, offset, segment_t_s=2)

    pvt[:,1] *= 100 + isx90 / 1000

    prog = '\r'.join(CS_PROG_START).format(prog_num=prog_num, move_type=move_type)
    prog += f"\rPVT{segment_t_s*1000}"

    # pv_ar = []
    for i in range(pvt.shape[0]):
        prog += f"\rX{pvt[i, 0]}:{pvt[i, 1]}"
    prog += "\r" + '\r'.join(CS_PROG_END)

    return prog


def _prog_move_spline1_bragg(axis, d_spacing,
                             b_start, b_end, step_ev, step_s,
                             cs_num=16, prog_num=666, cs_i_num=66,
                             run_prog=False):
    e_start = bragg2energy(b_start, d_spacing)
    e_end = bragg2energy(b_end, d_spacing)
    _prog_move_spline1(axis, d_spacing,
                       e_start, e_end, step_ev, step_s,
                       cs_num=cs_num, prog_num=prog_num,
                       cs_i_num=cs_i_num,
                       run_prog=run_prog)


def _prog_move_spline1(axis, d_spacing,
                       e_start, e_end, step_ev, step_s,
                       cs_num=16, prog_num=666, cs_i_num=66,
                       run_prog=False):
    prog = energy_spline1_prog(axis, d_spacing,
                           e_start, e_end,
                           step_ev=step_ev,
                           step_s=step_s,
                           cs_num=cs_num, prog_num=prog_num, cs_i_num=cs_i_num)
    print(prog.split('\r'))    
    if run_prog:
        axis.controller._write_multiline(prog, write=1)
        cmd = f"#4J/&{cs_num}B{prog_num}R"
        egy = get_config().get('energy')
        monp = get_config().get('xtal2perp')
        try:
            print('COMMAND=', cmd)
            axis.controller.pmac_comm.sendline_getbuffer(cmd)
            while True:
                gevent.sleep(0.5)
                axis.sync_hard()
                if not axis.state.MOVING:
                    print('NOT MOVING')
                    break
                else:
                    print('MOVING', axis.position, monp.position, monp.state, egy.position)
        except Exception as ex:
            # make sure x2perp is back to open loop
            # because it's in vacuum, and closed loop would 
            # mean that the motor is still powered and it
            # will heat
            print('ERROR', ex)
            axis.controller.pmac_comm.sendline_getbuffer('A')
            axis.controller.pmac_comm.sendline_getbuffer('#4J:0')
            print('STOPPED')


def energy_spline1_prog(axis, d_spacing,
                    e_start, e_end,
                    step_ev, step_s,
                    segment_t_s=2.,
                    cs_num=16, prog_num=666, cs_i_num=66):
    move_type = "ABS"

    # by default in PVT mode the segments can't be > 4095.9998
    # at least on the turbo pmac (unless you change some parameter)
    assert segment_t_s < 4.095

    offset = axis.offset
    steps_per_unit = axis.steps_per_unit
    # velocity is in units per [isx90] ms
    isx90 = float(axis.controller.pmac_comm.sendline_getbuffer(f'I{cs_i_num}90'))

    pvt = energy_to_steps_pvt(e_start, e_end, d_spacing, step_ev, step_s, steps_per_unit, offset, segment_t_s=2)

    pvt[:,1] *= 100 + isx90 / 1000

    prog = '\r'.join(CS_PROG_START).format(prog_num=prog_num, move_type=move_type)
    # prog += f"\rPVT{segment_t_s*1000}"
    prog += f"\rSPLINE1"
    prog += f"\rTM{segment_t_s * 1000}"

    # pv_ar = []
    for i in range(pvt.shape[0]):
        prog += f"\rX{pvt[i, 0]}"
    prog += "\r" + '\r'.join(CS_PROG_END)

    return prog


if __name__ == '__main__':
    config = get_config()
    bragg = config.get('bragg')
    crystal = config.get('crystal')

    prog = energy_pvt_prog(bragg, crystal.d_spacing(), 15000, 15100, 0.2, 0.1)

    print(prog.split('\r'))

    exit(0)