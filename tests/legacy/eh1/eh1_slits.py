from .eh1_objects import BM20_EH1_SLITS
from ..config.groups import Hutch, EH1MotorGroup
from ..macros.motors import bm20_status_object
from ..common.slits import (_bm20_home_slits,
                            _bm20_slits_show,
                            _bm20_slits_status_object)


__all__ = ['show_eh1_slits', 'status_eh1_slits']


def home_eh1_slits(*slit_names):
    """
    Home EH1 slits.

    Args:
        slit_names: optional, a list of slit names (s5, ...)
    """
    valid_names = BM20_EH1_SLITS

    if not slit_names:
        slit_names = valid_names

    hutch = Hutch.EH1
    motor_group = EH1MotorGroup.SLITS

    return _bm20_home_slits(slit_names, valid_names, hutch, motor_group)


show_eh1_slits = _bm20_slits_show(BM20_EH1_SLITS)


status_eh1_slits = _bm20_slits_status_object(*BM20_EH1_SLITS, title='EH1 slits')
