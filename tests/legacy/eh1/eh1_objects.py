from ..macros.bm20_utils import get_config_object


BM20_EH1_SLITS = ('s5',)


def bm20_eh1_slits():
    return [get_config_object(name) for name in BM20_EH1_SLITS]
