from bliss.config.static import get_config

from bm20.oh.mono.energy import energy2bragg
from bm20.config.mono import MonoCageAxis


def d20fs(e0, e1, egy_step, step_time, fscan):
    config = get_config()
    bragg_0 = energy2bragg(e0)
    bragg_1 = energy2bragg(e1)
    # bragg = config.get(MonoCageAxis.BRAGG.value)
    bragg_0_step = abs(energy2bragg(e0 - egy_step) - bragg_0)
    bragg_1_step = abs(energy2bragg(e1 - egy_step) - bragg_1)
    config = get_config()
    vbragg = config.get('vbragg')
    fs = fscan.fscan 
    # fs.motor = bragg
    fs.pars.motor = vbragg
    fs.pars.start_pos = bragg_0   
    # fs.pars.end_pos = 13000
    step_size = min(bragg_0_step, bragg_1_step)
    if e0 < e1:
        step_size = -1. * step_size
    fs.pars.npoints = abs((bragg_1 - bragg_0) / step_size)
    fs.pars.step_size = step_size
    # fs.pars.step_time = 0.1    
    # fs.pars.npoints = 100
    # fs.prepare()   
    fs.pars.step_time = step_time
    fs.pars.acq_time = step_time
    fs.pars.gate_mode = 'POSITION'
    # fs.prepare()   
    # fs.start()
    print(bragg_0, bragg_1, bragg_0_step, bragg_1_step)
    return fs
    
    