import numpy as np
from silx.io import open as h5open
from silx.gui import qt as Qt
from silx.gui.plot import Plot1D



HC_OVER_E = 12.39842

def bragg2energy(bragg_deg, dspace=3.13542, bragg_ofst=0):
    wl = 2 * dspace * np.sin(np.radians(bragg_deg))
    energy = 1000 * HC_OVER_E / wl
    return energy

def encoder2bragg(encoder, enc_ofst=0, bragg_ofst=0):
    return (encoder + enc_ofst) / 200000 + bragg_ofst

def energy2bragg(energy_ev, dspace=3.13542, bragg_ofst=0):
    bragg_deg = np.degrees(np.arcsin(HC_OVER_E / (energy_ev * 2 * dspace / 1000)))
    return bragg_deg


# def energy2encoder(energy_ev, enc_ofst=0, bragg_ofst=0):
#     bragg = energy2bragg(energy_ev)
#     return enc_ofst + bragg * 200000


def encoder2energy(encoder, enc_ofst=0, bragg_ofst=0):
    bragg = encoder2bragg(encoder, enc_ofst=enc_ofst, bragg_ofst=bragg_ofst)
    return bragg2energy(bragg)



def parse_title(title):
    known_cmd = ['ascan']
    
    t_split = title.split()
    
    cmd = t_split[0]
    
    if not cmd in known_cmd:
        raise RuntimeError(f'Unsupported command {cmd}.')
        
    # ascan  energy 9500 11300  35 1
    if cmd == 'ascan':
        motor = t_split[1]
        start_pos = float(t_split[2])
        end_pos = float(t_split[3])
        steps = float(t_split[4])
        acqt_s = float(t_split[5])
        
    return (motor, start_pos, end_pos, steps, acqt_s)


b_offset = -0.1902
e_offset = 38477



app = Qt.QApplication([])

h5file = '/data/bm20/inhouse/blc/mono_step/data/musstscope.h5'
specfile = '/data/bm20/inhouse/blc/mono_step/data/mono_test_16May22.spec'

h5scan = None
specscan = None

with h5open(h5file) as h5f:
    scans = list(h5f.keys())
    if h5scan is None:
        h5scan = scans[-1]
    print(scans)
    print(f'Opening {h5scan}.')

    enc_raw = h5f[h5scan]['measurement/musstoh:enc_bragg_raw'][:]
    trigct_raw = h5f[h5scan]['measurement/musstoh:trigct_raw'][:]
    timer_raw = h5f[h5scan]['measurement/musstoh:timer_raw'][:] / 1000000.


with h5open(specfile) as spec_f:
    scans = list(spec_f.keys())
    if specscan is None:
        specscan = scans[-1]
    print(scans)
    print(f'SPEC: opening {specscan}.')
    bragg_spec = spec_f[specscan]['measurement/braggangle'][:]
    title = spec_f[specscan]['title'][()]
    try:
        title = title.decode()
    except (UnicodeDecodeError, AttributeError):
        pass


print(encoder2energy(1583875), encoder2energy(1583870), encoder2energy(1583880))


# e_start = 14700.
# e_end = 14750.
# e_steps = 20
_, m_start, m_end, m_steps, _ = parse_title(title)

m_ival = (m_end - m_start) / m_steps
m_arr = np.arange(m_steps + 1) * m_ival



plot = Plot1D()

energy = encoder2energy(enc_raw, enc_ofst=e_offset, bragg_ofst=b_offset)
expected_energy = np.repeat(np.nan, energy.shape[0])
spec_energy = np.repeat(np.nan, energy.shape[0])

bragg = encoder2bragg(enc_raw, enc_ofst=e_offset, bragg_ofst=b_offset)
expected_bragg = np.repeat(np.nan, bragg.shape[0])
spec_bragg = np.repeat(np.nan, bragg.shape[0])

# gate_idx = np.where(trigct_raw == 1.)[0]
# steps = gate_idx[1:] - gate_idx[0:-1]
# steps_idx = np.where(steps != 1)[0]

gate_edges = trigct_raw[1:] - trigct_raw[:-1]
gate_up = np.where(gate_edges == 1)[0]
gate_down = np.where(gate_edges == -1)[0]

for idx in range(gate_up.shape[0]):
    expected_bragg[gate_up[idx]:gate_down[idx]] = m_start + idx * m_ival
    spec_bragg[gate_up[idx]:gate_down[idx]] = bragg_spec[idx]

for idx in range(gate_up.shape[0]):
    # energy = encoder2energy(encoder_raw)
    # expected_energy[gate_up[idx]:gate_down[idx]] = e_start + idx * e_ival
    expected_energy = bragg2energy(expected_bragg)
    # spec_energy[gate_up[idx]:gate_down[idx]] = energy_spec[idx]
    spec_energy = bragg2energy(spec_bragg)

# plot.addCurve(timer_raw, enc_raw, legend='enc')
plot.addCurve(timer_raw, bragg, legend='bragg')
plot.addCurve(timer_raw, expected_bragg, legend='expected_bragg')
plot.addCurve(timer_raw, spec_bragg, legend='spec_bragg', color='green')
# plot.addCurve(timer_raw, trigct_raw, legend='trig', yaxis='right')

plot.addCurve(timer_raw, energy, legend='energy', yaxis='right', color='blue')
plot.addCurve(timer_raw, expected_energy, legend='expected_energy', yaxis='right', color='orange')
plot.addCurve(timer_raw, spec_energy, legend='spec_energy', yaxis='right', color='pink')

plot.show()

app.exec()

