import numpy as np
from silx.io import open as h5open
from silx.gui import qt as Qt
from silx.gui.plot import Plot1D



HC_OVER_E = 12.39842

def bragg2energy(bragg_deg, dspace=3.13542, bragg_ofst=0):
    wl = 2 * dspace * np.sin(np.radians(bragg_deg))
    energy = 1000 * HC_OVER_E / wl
    return energy

def encoder2bragg(encoder, enc_ofst=0, bragg_ofst=0):
    return (encoder + enc_ofst) / 200000 + bragg_ofst

def energy2bragg(energy_ev, dspace=3.13542, bragg_ofst=0):
    bragg_deg = np.degrees(np.arcsin(HC_OVER_E / (energy_ev * 2 * dspace / 1000)))
    return bragg_deg


# def energy2encoder(energy_ev, enc_ofst=0, bragg_ofst=0):
#     bragg = energy2bragg(energy_ev)
#     return enc_ofst + bragg * 200000


def encoder2energy(encoder, enc_ofst=0, bragg_ofst=0):
    bragg = encoder2bragg(encoder, enc_ofst=enc_ofst, bragg_ofst=bragg_ofst)
    return bragg2energy(bragg)



def parse_title(title):
    known_cmd = ['ascan']
    
    t_split = title.split()
    
    cmd = t_split[0]
    
    if not cmd in known_cmd:
        raise RuntimeError(f'Unsupported command {cmd}.')
        
    # ascan  energy 9500 11300  35 1
    if cmd == 'ascan':
        motor = t_split[1]
        start_pos = float(t_split[2])
        end_pos = float(t_split[3])
        steps = float(t_split[4])
        acqt_s = float(t_split[5])
        
    return (motor, start_pos, end_pos, steps, acqt_s)


def parse_musst_title(title):
    known_cmd = ['timescope']
    
    t_split = title.split()
    
    cmd = t_split[0]
    
    if not cmd in known_cmd:
        raise RuntimeError(f'Unsupported command {cmd}.')
        
    # ascan  energy 9500 11300  35 1
    if cmd == 'timescope':
        device = t_split[1]
        npts = int(t_split[2])
        ival = float(t_split[3])
        
    return (cmd, device, npts, ival)


b_offset = 0 #-0.1902
e_offset = 32878.9 + 1545838.9696875

app = Qt.QApplication([])

h5file = '/data/bm20/inhouse/blc/mono_step/data/musstscope.h5'
specfile = '/data/bm20/inhouse/blc/mono_step/data/mono_test_18May22.spec'
# h5file = '/data/bm20/inhouse/blc/mono_step/data/fullbliss.h5'
# specfile = '/data/bm20/inhouse/blc/mono_step/data/mono_steps.h5'

h5scan = None#'72_timescope'
specscan = None#'18.1'

sort_key = lambda s: int(s.split('_')[0].split('.')[0])

with h5open(h5file) as h5f:
    scans = sorted(list(h5f.keys()), key=sort_key)
    if h5scan is None:
        h5scan = scans[-1]
    # print(scans)
    print(f'Opening {h5scan}.')

    enc_raw = h5f[h5scan]['measurement/musstoh:enc_bragg_raw'][:]
    trigct_raw = h5f[h5scan]['measurement/musstoh:trigct_raw'][:]
    timer_raw = h5f[h5scan]['measurement/musstoh:timer_raw'][:] / 1000000.
    musst_title = h5f[h5scan]['title'][()]
    try:
        musst_title = musst_title.decode()
    except (UnicodeDecodeError, AttributeError):
        pass
    print(f'MUSST title: {musst_title}')


with h5open(specfile) as spec_f:
    scans = sorted(list(spec_f.keys()), key=sort_key)
    if specscan is None:
        specscan = scans[-1]
    # print(scans)
    print(f'SPEC: opening {specscan}.')
    try:
        energy_spec = spec_f[specscan]['measurement/energy'][:]
    except KeyError:
        energy_spec = spec_f[specscan]['measurement/axis:energy'][:]
    title = spec_f[specscan]['title'][()]
    try:
        title = title.decode()
    except (UnicodeDecodeError, AttributeError):
        pass
    print(f'SPEC title: {title}')


print(encoder2energy(1583875), encoder2energy(1583870), encoder2energy(1583880))


# e_start = 14700.
# e_end = 14750.
# e_steps = 20
_, e_start, e_end, e_steps, _ = parse_title(title)

e_ival = (e_end - e_start) / e_steps
e_arr = np.arange(e_steps + 1) * e_ival

_, _, _, samp_ival = parse_musst_title(musst_title)

plot = Plot1D()

energy = encoder2energy(enc_raw, enc_ofst=e_offset, bragg_ofst=b_offset)
expected_energy = np.repeat(np.nan, energy.shape[0])
spec_energy = np.repeat(np.nan, energy.shape[0])

# gate_idx = np.where(trigct_raw == 1.)[0]
# steps = gate_idx[1:] - gate_idx[0:-1]
# steps_idx = np.where(steps != 1)[0]

gate_edges = trigct_raw[1:] - trigct_raw[:-1]
gate_up = np.where(gate_edges == 1)[0]
gate_down = np.where(gate_edges == -1)[0]

# print(gate_up.shape, gate_down.shape, energy_spec.shape)


for idx in range(gate_up.shape[0]):
    if idx >= energy_spec.shape[0]:
        break
    expected_energy[gate_up[idx]+1:gate_down[idx]+1] = e_start + idx * e_ival
    spec_energy[gate_up[idx]+1:gate_down[idx]+1] = energy_spec[idx]

# plot.addCurve(timer_raw, enc_raw, legend='enc')
plot.addCurve(timer_raw, energy, legend='energy')
plot.addCurve(timer_raw, expected_energy, legend='expected_energy')
plot.addCurve(timer_raw, spec_energy, legend='spec_energy', color='green')
# plot.addCurve(timer_raw, trigct_raw)
plot.getXAxis().setLabel('time(s)')
plot.getYAxis().setLabel('energy')
# plot.addCurve(timer_raw, trigct_raw, legend='trig', yaxis='right')


plot.show()

app.exec()

