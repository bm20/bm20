from bliss.config.static import get_config

from bliss.common.standard import loopscan
from bliss.common.counter import SoftCounter, SamplingMode
from bliss.controllers.motors.turbopmac import PMAC_MOTOR_STATUS


_ecounter = None
_encoder = None
_scounter = None
_status = None


def pmac_status_to_bliss_state_names(pmac_status):
        state_names = set()

        for mask, value_set, value_unset, d in PMAC_MOTOR_STATUS:
            if pmac_status & mask:
                if value_set:
                    state_names |= {value_set}
            elif value_unset:
                state_names |= {value_unset}

        state_names = set(state_names)

        # if "HOMING" in state_names:
        #     state_names |= {"MOVING"}
        #     state_names -= {"READY"}

        # if "READY" in state_names:
        #     state_names -= {"MOVING"}

        return state_names


class PmacEncoder:
    def __init__(self):
        self._pmac = get_config().get('pmacd20')

    @property
    def encoder(self):
        return self._pmac.pmac_comm.sendline_getbuffer('#1P')


class PmacStatus:
    def __init__(self):
        self._pmac = get_config().get('pmacd20')

    @property
    def status(self):
        status = self._pmac.pmac_comm.sendline_getbuffer('#1?')
        print(status)
        status = int(status.lstrip('\x06'), base=16)
        return 'READY' in pmac_status_to_bliss_state_names(status)


def encoder_counter():
    global _ecounter, _encoder
    if _encoder is None:
        _encoder = PmacEncoder()
    if _ecounter is None:
        _ecounter = SoftCounter(_encoder,
                                'encoder',
                                name='encoder',
                                mode=SamplingMode.SINGLE)
    return _ecounter


def status_counter():
    global _scounter, _status
    if _status is None:
        _status = PmacStatus()
    if _scounter is None:
        _scounter = SoftCounter(_status,
                                'status',
                                name='status',
                                mode=SamplingMode.SINGLE)
    return _scounter

ecounter = encoder_counter()
scounter = status_counter()