import numpy as np
from silx.io import open as h5open
from silx.gui import qt as Qt
from silx.gui.plot import Plot1D
from scipy.ndimage.filters import uniform_filter1d



HC_OVER_E = 12.39842

def bragg2energy(bragg_deg, dspace=3.13542, bragg_ofst=0):
    wl = 2 * dspace * np.sin(np.radians(bragg_deg))
    energy = 1000 * HC_OVER_E / wl
    return energy

def encoder2bragg(encoder, enc_ofst=0, bragg_ofst=0):
    return (encoder + enc_ofst) / 200000 + bragg_ofst

def energy2bragg(energy_ev, dspace=3.13542, bragg_ofst=0):
    bragg_deg = np.degrees(np.arcsin(HC_OVER_E / (energy_ev * 2 * dspace / 1000)))
    return bragg_deg


# def energy2encoder(energy_ev, enc_ofst=0, bragg_ofst=0):
#     bragg = energy2bragg(energy_ev)
#     return enc_ofst + bragg * 200000


def encoder2energy(encoder, enc_ofst=0, bragg_ofst=0):
    bragg = encoder2bragg(encoder, enc_ofst=enc_ofst, bragg_ofst=bragg_ofst)
    return bragg2energy(bragg)



def parse_title(title):
    known_cmd = ['ascan']
    
    t_split = title.split()
    
    cmd = t_split[0]
    
    if not cmd in known_cmd:
        raise RuntimeError(f'Unsupported command {cmd}.')
        
    # ascan  energy 9500 11300  35 1
    if cmd == 'ascan':
        motor = t_split[1]
        start_pos = float(t_split[2])
        end_pos = float(t_split[3])
        steps = float(t_split[4])
        acqt_s = float(t_split[5])
        
    return (motor, start_pos, end_pos, steps, acqt_s)


b_offset = -0.1902
e_offset = 38477



app = Qt.QApplication([])

musttfile = '/data/bm20/inhouse/blc/mono_step/data/musstscope.h5'
specfile = '/data/bm20/inhouse/blc/mono_step/data/mono_test_11May22.spec'


h5scan = None
specscan = None


def get_musst_scan(h5file, scan=None):
    with h5open(h5file) as h5f:
        scans = list(h5f.keys())
        if scan is None:
            scan = scans[-1]
        print(f'Musst: opening {scan}.')
        enc_raw = h5f[scan]['measurement/musstoh:enc_bragg_raw'][:]
        trigct_raw = h5f[scan]['measurement/musstoh:trigct_raw'][:]
        timer_raw = h5f[scan]['measurement/musstoh:timer_raw'][:] / 1000000.
    return enc_raw, trigct_raw, timer_raw


def get_spec_scan(file, scan=None):
    with h5open(specfile) as spec_f:
        scans = list(spec_f.keys())
        if scan is None:
            scan = scans[-1]
        print(f'SPEC: opening {scan}.')
        energy_spec = spec_f[specscan]['measurement/energy'][:]
        title = spec_f[specscan]['title'][()]
        try:
            title = title.decode()
        except (UnicodeDecodeError, AttributeError):
            pass
    return title, energy_spec

# _, e_start, e_end, e_steps, _ = parse_title(title)

# e_ival = (e_end - e_start) / e_steps
# e_arr = np.arange(e_steps + 1) * e_ival


enc_raw, trigct_raw, time_row = get_musst_scan(musttfile)

enc_delta = enc_raw[1:] - enc_raw[:-1]
enc_delta_filt = uniform_filter1d(enc_delta, size=30)
start_idx = np.where(enc_delta_filt > 5 )[0]
# print(start_idx)
first_idx = np.where((start_idx[1:] - start_idx[:-1]) != 1)[0]
first_idx = np.append(first_idx, start_idx[-1])
# print(first_idx)
# start_idx = start_idx[first_idx]
# print(start_idx)
# ar = enc_delta[0:first_idx[0]]
# print(ar)
idx_0 = 0
sync_idx = np.zeros(first_idx.shape[0])
for i_idx, idx in enumerate(first_idx):
    enc_idx = start_idx[idx_0:idx+1]
    sync_idx[i_idx] = enc_idx[np.argmin(enc_delta[enc_idx])]
    # print(i_idx, idx, start_idx[idx_0:idx+1], sync_idx[i_idx], enc_delta[start_idx[idx_0:idx+1]])
    idx_0 = idx+1

# print(sync_idx)

sync_idx = np.append(sync_idx, enc_raw.shape[0])

plot = Plot1D()

# x = np.arange(enc_delta.shape[0])
# plot.addCurve(x, enc_delta_filt, legend='filt')
# plot.addCurve(x, enc_delta, legend='raw')


print(f'{sync_idx.shape[0]-1} curves.')
for r_idx in range(sync_idx.shape[0] - 1):
    x0 = int(sync_idx[r_idx])
    x1 = int(sync_idx[r_idx+1])
    # print(x0, x1)
    data = enc_raw[x0:x1]
    plot.addCurve(np.arange(data.shape[0]), data, legend=f'run{r_idx}')

# energy = encoder2energy(enc_raw, enc_ofst=e_offset, bragg_ofst=b_offset)
# expected_energy = np.repeat(np.nan, energy.shape[0])
# spec_energy = np.repeat(np.nan, energy.shape[0])

# # gate_idx = np.where(trigct_raw == 1.)[0]
# # steps = gate_idx[1:] - gate_idx[0:-1]
# # steps_idx = np.where(steps != 1)[0]

# gate_edges = trigct_raw[1:] - trigct_raw[:-1]
# gate_up = np.where(gate_edges == 1)[0]
# gate_down = np.where(gate_edges == -1)[0]

# for idx in range(gate_up.shape[0]):
#     expected_energy[gate_up[idx]:gate_down[idx]] = e_start + idx * e_ival
#     spec_energy[gate_up[idx]:gate_down[idx]] = energy_spec[idx]

# # plot.addCurve(timer_raw, enc_raw, legend='enc')
# plot.addCurve(timer_raw, energy, legend='energy')
# plot.addCurve(timer_raw, expected_energy, legend='expected_energy')
# plot.addCurve(timer_raw, spec_energy, legend='spec_energy', color='green')
# # plot.addCurve(timer_raw, trigct_raw, legend='trig', yaxis='right')

print(energy2bragg(14700, bragg_ofst=b_offset))
print(energy2bragg(14750, bragg_ofst=b_offset))

plot.show()

app.exec()

