# class PmacAxisDebug:
#     def __init__(self, axis):
#         self.axis = axis
#         self.address = axis.address
#         self.pmac = axis.controller

#     def pmac_comm(self):
#         return self.pmac.pmac_comm

#     def encoder(self):
#         return self.pmac_comm.sendline_getbuffer(f'M{self.address}01')

#     def move_to_encoder(self, enc):
#         real = self.encoder()
#         soft = self.pmac_comm.motor_position(self.address)
#         diff = real - enc
#         self.pmac_comm.jo


from bliss.common.standard import SoftAxis


def PmacEncoderAxis(axis):
    address = axis.controller._pmac_address(axis)
    pmac = axis.controller
    comm = pmac.pmac_comm

    def _encoder(*args, **kwargs):
        return int(comm.sendline_getbuffer(f'M{address}01'))

    def _move(enc):
        print('MOVE', enc)
        _enc = _encoder()
        _diff = enc - _enc
        # _real = int(comm.motor_position(address))
        _target = axis.position + (_diff / axis.steps_per_unit)
        print(_diff, axis.position, _target, axis.position - _target)
        axis.move(_target)

    return SoftAxis(f'enc_{axis.name}',
                    axis, position=_encoder,
                    move=_move,
                    tolerance=2)