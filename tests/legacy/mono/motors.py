from bliss.common.scans import ascan
from bliss.config.static import get_config
from bliss.common.counter import SoftCounter

from bm20.config.mono import MonoCageAxis
import datetime

from bliss.common.scans import timescan, loopscan, timescan
from bliss.config.static import get_config

from bliss.shell.standard import move, flint
from bliss.scanning.scan_saving import ScanSaving
from bliss.common.session import get_current_session


def scan_egy(start, stop, ival):
    config = get_config()
    bragg = config.get(MonoCageAxis.BRAGG.value)
    xl2perp = config.get(MonoCageAxis.XTAL2_PERP.value)
    egy = config.get('energy')

    cntegy = SoftCounter(egy, value='position')
    cntbrag = SoftCounter(bragg, value='position')
    cntxl2p = SoftCounter(xl2perp, value='position')

    strtime = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')

    session = get_current_session()
    scan_saving = ScanSaving(session.name)
    cur_filename = scan_saving.data_filename
    try:
        filename = f'dcm_test_{strtime}_{start}_{stop}_{ival}'
        scan_saving.data_filename = filename
        print(f'Writing scan to {scan_saving.base_path}/{filename}')
        ascan(egy, start, stop, ival, 0.01, cntegy, cntbrag, cntxl2p)
    except Exception as ex:
        print(ex)
    scan_saving.data_filename = cur_filename


def scan_pos(delay):
    config = get_config()
    bragg = config.get(MonoCageAxis.BRAGG.value)
    xl2perp = config.get(MonoCageAxis.XTAL2_PERP.value)
    egy = config.get('energy')

    cntegy = SoftCounter(egy, value='position')
    cntbrag = SoftCounter(bragg, value='position')
    cntxl2p = SoftCounter(xl2perp, value='position')

    strtime = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')

    session = get_current_session()
    scan_saving = ScanSaving(session.name)
    cur_filename = scan_saving.data_filename
    try:
        filename = f'scan_pos_dcm_{strtime}_{delay}'
        scan_saving.data_filename = filename
        print(f'Writing scan to {scan_saving.base_path}/{filename}')
        timescan(delay, cntegy, cntbrag, cntxl2p)
    except Exception as ex:
        print(ex)
    scan_saving.data_filename = cur_filename