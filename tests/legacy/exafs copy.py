from collections import OrderedDict

import numpy as np

from bliss.common.cleanup import cleanup
from bliss import current_session
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.scanning.group import Sequence

from bliss.controllers.counter import CalcCounterController, SamplingCounterController, CounterController
from bliss.controllers.simulation_counter import SimulationCounterController
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.mca.base import TriggerMode, BaseMCA
from bm20.oh.mono.energy import energy2bragg


from .params import ExafsParameters


class ExafsScan:
    def __init__(self, config):
        defaults = config.get('defaults')
        ref_config = defaults.get("ref_scan")
        pre_config = defaults.get("pre_scans")
        edge_config = defaults.get("edge_scan")
        k_config = defaults.get("k_scans")

        print(ref_config, pre_config, edge_config, k_config)

        ref_scan_max = ref_config.get('e_max')
        if ref_scan_max is None:
            ref_scan_max = pre_config[-1]['e_min']
        pre_scan_max = pre_config.get('e_max')
        if pre_scan_max is None:
            pre_scan_max = edge_config['e_min']
        edge_scan_max = edge_config.get('e_max')
        if edge_scan_max is None:
            edge_scan_max = k_config['e_min']

        


class ExafsScanBase:
    exafs_params = property(lambda self: self._exafs_params)
    energy_axis = property(lambda self: self._egy_axis)
    bragg_axis = property(lambda self: self._bragg_axis)
    
    def __init__(self,
                 name,
                 element,
                 edge,
                 egy_axis,
                 bragg_axis,
                 shutter=None,
                 stab_time=0.1,
                 *args, **kwargs):
        exafs_params = ExafsParameters(element, edge)
        self._element = element
        self._edge = edge
        self._name = name
        self._egy_axis = egy_axis
        self._exafs_params = exafs_params
        self._bragg_axis = bragg_axis
        self._shutter = shutter
        self._stab_time = stab_time

    def __repr__(self):
        return self._exafs_params.__repr__()

    def run(self, *counters, repeat=1):
        if not counters:
            counters = current_session.active_mg
            if not counters:
                raise RuntimeError('Please provide counters or set '
                                'the active measurement group.')
            self.start(counters, repeat=repeat)
        else:
            self.start(*counters, repeat=repeat)

    def metadata(self):
        return self._exafs_params.metadata()

    def start(self, *counters, repeat=1):
        raise NotImplementedError()
    
    
class ExafsStepScan(ExafsScanBase):
    def start(self, *counters, repeat=1):
        # if len(counters) == 0:    
        #     raise RuntimeError('At least one counter must be provided.')
    #     exafs_param, 
    #     *counters, 
    #     RC=False, 
    #     sleep_time=0.2, 
    #     step=False, 
    #     nbscan=1,
    #     ascii=False,
    # ):
    #     """
    #     Step Scans with variable step size and variable step time
    #     Positions and times are taken from exafs_param object
    #     """
        scan_pos = list(self._exafs_params.scan_pos)
        scan_t = list(self._exafs_params.scan_t)
                    
        # SCAN STATE
        # self._scan_state = "PREPARING"
        # self._scan_name = "exafs_step"
        
        # INIT VARIABLES        
        # positions = scantool_get_step_position2(exafs_param, bragg, step)
        # print(numpy.all(numpy.isfinite(positions)))
        if scan_pos[0] > scan_pos[-1]:
            way = "down"
        else:
            way = "up"
        
        # BEAMLINE SETUP
        # Open beamshutter
        if self._shutter is not None:
            self._shutter.open()

        with cleanup(self._scan_end):
            # MULTIPLEXER
            #self._multpx.switch("SEL_TRIG", "STEP")
            
            # SCAN PARAMETERS
            self._scan_par = scantool_set_scan_parameters(
                motor=self._egy_axis,
                positions=scan_pos,         
                time=scan_t, 
                points=len(scan_pos),
                way=way)
            
            for nscan in range(repeat):
                # MOTOR MASTER
                self._master = VariableStepTriggerMaster(
                    self._egy_axis,
                    scan_pos,
                    stab_time=self._stab_time)
                print(counters)
                # ACQUISITION CHAIN
                self._builder, self._chain = scantool_stepscan_get_acquisition_chain(
                        self._master,
                        self._scan_par,
                        counters,
                        fx_trigger=TriggerMode.GATE
                        #fx_trigger=self._fx_trigger_mode,
                        #fx_block_size=self._fx_block_size,
                    )
                print(self._chain._tree)
            
                
                nbp = self._scan_par["points"]
                # scan_info = {
                #     "title": f"EXAFS {self._name}",
                #     "element": f"{self._element}",
                #     "edge": f"{self._edge}",
                # }
                scan_info = {
                    "title": f"EXAFS {self._name}",
                    "instrument":{"exafs": self._exafs_params.metadata()}
                }
                

                #     "title": f"{self._name}.exafs_step {nbp} points", 
                #     "type": f"{self._name}.exafs_step",
                #     #"instrument": instrument
                # }
                    
                print(f"\nSCAN #{nscan+1}(/{repeat})")

                # SCAN
                self._scan_obj = Scan(
                    self._chain,
                    name=f"{self._name}.exafs_step",
                    scan_info=scan_info
                    # ,
                    # data_watch_callback=scan_display
                )
                # self._scan_state = "RUNNING"
                print(f'Scanning from {scan_pos[0]} to {scan_pos[-1]}')
                self._scan_obj.run()
                #self.plotter.run(self._scan_obj)
                
                # if ascii:
                #     save_exafs_ascii(self._scan_obj)
                    
    def _scan_end(self):
        print('SCAN END')


class ExafsContinuousScan(ExafsScanBase):
    fscan_config = property(lambda self: self._fscan_config)
    # step_size = property(lambda self: self.step_size)
    # step_time = property(lambda self: self.step_time)
    fscans = property(lambda self: self._fscans)
    
    def __init__(self, *args, fscan_config=None, **kwargs):
        super().__init__(*args, **kwargs)
        if fscan_config is None:
            raise RuntimeError('Please provide a FScanConfig object.')
        self._fscan_config = fscan_config
        # self._fscans = OrderedDict()
        # self._fscan = fscan_config.fscan
        # self._setup_scans()
        
    def start(self, *args, **kwargs):
        # self._fscans = OrderedDict()
        # self.bragg_axis.sync_hard()
        # self.energy_axis.sync_hard()
        sequence = Sequence()
        with sequence.sequence_context() as scan_seq:
            for i_params, scan_params in enumerate(self._exafs_params.pre_e_scans):
                fscan = self._setup_scan(scan_params, 0.02, 0.1)
                fscan.prepare()
                scan_seq.add_and_run(fscan.scan)
                # self._fscans[f'pre_{i_params}'] = fscan
                # self.bragg_axis.sync_hard()
                # self.energy_axis.sync_hard()
            for i_params, scan_params in enumerate(self._exafs_params.e_scans):
                fscan = self._setup_scan(scan_params, 0.02, 0.1)
                fscan.prepare()
                scan_seq.add_and_run(fscan.scan)
                # self.bragg_axis.sync_hard()
                # self._fscans[f'edge_{i_params}'] = fscan
            for i_params, scan_params in enumerate(self._exafs_params.k_scans):
                fscan = self._setup_scan(scan_params, 0.05, 0.1)
                fscan.prepare()
                scan_seq.add_and_run(fscan.scan)
                # self.bragg_axis.sync_hard()
                # self._fscans[f'exafs_{i_params}'] = fscan
            
    def _setup_scan(self, scan_params, step_time, step_size):
        from bm20.scans.continuous.fastscan import fastscan_setup
        fscan = self._fscan_config.fscan
        e0 = scan_params.scan_pos[0]
        e1 = scan_params.scan_pos[-1]
        fastscan_setup(fscan, e0, e1, step_size, step_time, self.energy_axis, self.bragg_axis)
        return fscan
        
    # def run(self, *counters, repeat=1):
    #     pass
        
    

        
# """
# EXAFS_CONT Scan
# """
    
# class ExafsContinuousScan(ExafsScanBase):
#     def continuous(
#         self,
#         start,
#         stop,
#         nb_points,
#         time_per_points,
#         *counters,
#         nbscan=1,
#         backNforth=False,
#         comment=None,
#     ):
#         """
#         Continuous scan in energy on mbragg at mbragg constant speed
#         """
        
#         # CHECKS
#         if start == stop:
#             raise RuntimeError("ZAP: Start and Stop positions are the same")
            
#         # # SCAN STATE
#         # self._scan_state = "PREPARING"
#         # self._scan_name = "exafs_cont"

#         # INIT VARIABLES
#         if start > stop:
#             way = "down"
#             way_back = "up"         
#         else:
#             way = "up"
#             way_back = "down"            
            
#         # BEAMLINE SETUP
#         # open beamshutter
#         ##bsh = setup_globals.bsh
#         ##bsh.open()
#         # reset MUSST channels
#         # !!! TODO - This should be standardize adding parameters to musst channels
#         # Step Channel
#         mbragg = setup_globals.mbragg
#         motpos = mbragg.position * mbragg.steps_per_unit
#         self._musst.putget(f"CH CH1 {motpos}")
#         #musst_val = float(self._musst.putget(f"?CH CH1"))
#         #print("Init CH1 mot({motpos}) musst_ch1({musst_val})")
#         # Encoder Channel
#         bragg_enc = setup_globals.bragg_enc
#         ene_enc = setup_globals.ene_enc
#         encpos = (bragg_enc.read() + ene_enc.settings.get()) * bragg_enc.steps_per_unit
#         self._musst.putget(f"CH CH2 {encpos}") 
#         #musst_val = float(self._musst.putget(f"?CH CH2"))
#         #print("Init CH2 mot({encpos}) musst_ch2({musst_val})")
#         # TO BE REMOVE ASAP
#         self._backlash[mbragg] = mbragg.backlash

#         with cleanup(self._scan_end):
            
#             # MULTIPLEXER
#             self._multpx.switch("SEL_TRIG", "CONT")
            
#             # COUNTERS
#             musst_counters = (
#                 self._musst.counters.musst_timer,
#                 self._musst.counters.step,
#                 self._musst.counters.encoder,
#             )
#             counters = counters + musst_counters

#             # METADATA
#             metadata = setup_globals.metadata
#             metadata._state["ExafsElement"].set(False)
        
#             # Scanned motor backlash
#             self._backlash[mbragg] = mbragg.backlash
#             mbragg.backlash = 0.0
            
#             for nscan in range(nbscan):
                
#                 # SCAN PARAMETERS
#                 if not backNforth or (backNforth and nscan%2==0):
#                     self._scan_par = scantool_set_scan_parameters(
#                         motor=setup_globals.mbragg,
#                         start=self._mono.energy2bragg(start) / self._mono.bragg2cbragg,
#                         stop=self._mono.energy2bragg(stop) / self._mono.bragg2cbragg,
#                         points=nb_points,
#                         time=time_per_points,
#                         way=way,
#                         trig_start=self._setting["trigger_start"],
#                         trig_point=self._setting["trigger_point"]
#                     )
#                 else:
#                     self._scan_par = scantool_set_scan_parameters(
#                         motor=setup_globals.mbragg,
#                         start=self._mono.energy2bragg(stop) / self._mono.bragg2cbragg,
#                         stop=self._mono.energy2bragg(start) / self._mono.bragg2cbragg,
#                         points=nb_points,
#                         time=time_per_points,
#                         way=way_back,
#                         trig_start=self._setting["trigger_start"],
#                         trig_point=self._setting["trigger_point"]
#                     )
                    
#                 # MOTOR MASTER
#                 self._master = MotorMaster(
#                     self._scan_par["motor"],
#                     self._scan_par["start"],
#                     self._scan_par["stop"],
#                     self._scan_par["points"] * self._scan_par["time"],
#                 )
#                 #if self._master.undershoot < self._backlash[self._scan_par["motor"]]:
#                 #    self._master.undershoot = self._backlash[self._scan_par["motor"]]
#                 if self.undershoot > self._master.undershoot:
#                     self._master._undershoot = self.undershoot
                
#                 # ACQUISITION CHAIN
#                 (self._builder, self._chain) = scantool_contscan_get_acquisition_chain(
#                     self._master,
#                     self._musst,
#                     self._motors_config,
#                     self._scan_par,
#                     self._musst_calc,
#                     counters
#                 )
#                 print(self._chain._tree)

#                 # # SCAN DISPLAY
#                 # self._display = ScanOutput(self._musst,
#                 #     self._scan_par["points"],
#                 #     self._scan_par["motor"],
#                 #     self._builder,
#                 # )
#                 # timeout = 2.0 * self.undershoot / self._master.velocity + self._timeout
#                 # self._watchdog = ScanWatchdog(self._musst,
#                 #     self._scan_par["points"],
#                 #     self._scan_par["motor"],
#                 #     self._builder,
#                 #     timeout=timeout,
#                 # )
                
#                 if not backNforth or (backNforth and nscan%2==0):
#                     s_start = start
#                     s_stop = stop
#                     s_way = way
#                 else:
#                     s_start = stop
#                     s_stop = start
#                     s_way = way_back
                    
#                 # META DATA
#                 sc_title = f"{self._name}.exafs_cont {s_start} {s_stop} {nb_points} {time_per_points}"
#                 if comment is not None:
#                      sc_title += f" {comment}"
#                 instrument = metadata.get_meta_data()
#                 scan_info = {
#                     "title": sc_title,
#                     "type": f"{self._name}.exafs_cont",
#                     "points": nb_points,
#                     "instrument": instrument
#                 }
                
#                 print(f"\nSCAN #{nscan+1}(/{nbscan}) - {s_way.upper()} direction")
                    
#                 # SCAN
#                 self._scan_obj = Scan(
#                     self._chain,
#                     name=f"{self._name}.exafs_cont",
#                     scan_info=scan_info,
#                     data_watch_callback=self._display,
#                     watchdog_callback=self._watchdog
#                 )
#                 self._scan_state = "RUNNING"
#                 try:
#                     # self._scan_obj.run()
#                     self.plotter.run(self._scan_obj)
#                 except HandelError:
#                     print(RED("fx Error, continue...\n\n"))
#                 except KeyboardInterrupt:
#                     print("KeyboardInterrup, exit...\n\n")
#                     return
#                 except TimeoutError:
#                     print(RED("\Missing points on last scan, continue....\n\n"))
        
        
#######################################################################
#####
##### Store Scan Parameters and create defaults
#####
def scantool_set_scan_parameters(
    motor=None,
    start=np.nan,
    stop=np.nan,
    positions=None,
    points=1,
    time=None,
    way="up",
    trig_start="TIME",
    trig_point="TIME"
):
    scan_par = {
        "motor": motor,
        "positions": positions,
        "start": start,
        "stop": stop,
        "points": points,
        "time": time,
        "way" : way,
        "trig_start": trig_start,
        "trig_point": trig_point,
    }
    
    return scan_par


#######################################################################
#####
##### Build Step scan Acquisition Chain
#####
def scantool_stepscan_get_acquisition_chain(
    master,
    scan_par,
    counters,
    fx_trigger="GATE",
    fx_block_size=5,
):
    
    chain = AcquisitionChain(parallel_prepare=True)
    builder = ChainBuilder(counters)

    ###################################
    # P201
    #
    acq_node_params = {
            "npoints": scan_par["points"],
            "acq_expo_time": scan_par["time"],
    }
    if isinstance(scan_par["time"], (list, np.ndarray)):
        acq_child_params = {
            "count_time": 1,
            "npoints": 1,
        }
    else:
        acq_child_params = {
            "count_time": scan_par["time"],
            "npoints": scan_par["points"],
        }
    
    for node in builder.get_nodes_by_controller_type(CT2Controller):
        node.set_parameters(acq_params=acq_node_params)
        for child_node in node.children:
            child_node.set_parameters(acq_params=acq_child_params)
        node_p201 = node
        chain.add(master, node)
        
    # tmast = SoftwareTimerMaster(0.1, scan_par["points"])
    # chain.add(master, tmast)

    ###################################
    # FalconX
    #
    if fx_trigger == "SOFTWARE":
        acq_params = {
            "npoints": scan_par["points"],
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : scan_par["time"]
        }
    else:
        acq_params = {
            "npoints": scan_par["points"],
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : 0.1
        }
        
    for node in builder.get_nodes_by_controller_type(BaseMCA):
        node.set_parameters(acq_params=acq_params)
        # chain.add(node_p201, node)
        chain.add(master, node)

    ###################################
    # SamplingCounterController
    #
    acq_params = {
            "count_time": scan_par["time"],
            "npoints": scan_par["points"],
        }
    # for node in builder.nodes:  
    for node in builder.get_nodes_by_controller_type(CounterController):  
        if isinstance(node.controller, SamplingCounterController):
            node.set_parameters(acq_params=acq_params)
            # chain.add(node_p201, node)
        

            
    # for node in builder.nodes:
    #     if isinstance(node.controller, SamplingCounterController):
    #         node.set_parameters(acq_params=acq_params)
    # SimulationCounter

    ###################################
    # CalcCounterController
    
    acq_params = {
        "count_time": scan_par["time"],
    }
    for node in builder.nodes:
        if isinstance(node.controller, CalcCounterController):
            node.set_parameters(acq_params=acq_params)
            chain.add(node_p201, node)

    return (builder, chain)



def scantool_stepscan_get_acquisition_chain2(
    master,
    scan_par,
    counters,
    fx_trigger="GATE",
    fx_block_size=5,
):
    
    chain = AcquisitionChain(parallel_prepare=True)
    builder = ChainBuilder(counters)
    
    tmast = SoftwareTimerMaster(0.1, npoints=scan_par["points"])
    chain.add(master, tmast)
    
    ###################################
    # P201
    #
    # acq_node_params = {
    #         "npoints": scan_par["points"],
    #         "acq_expo_time": scan_par["time"],
    # }
    # if isinstance(scan_par["time"], (list, np.ndarray)):
    #     acq_child_params = {
    #         "count_time": 1,
    #         "npoints": 1,
    #     }
    # else:
    #     acq_child_params = {
    #         "count_time": scan_par["time"],
    #         "npoints": scan_par["points"],
    #     }
    
    # for node in builder.get_nodes_by_controller_type(CT2Controller):
    #     node.set_parameters(acq_params=acq_node_params)
    #     for child_node in node.children:
    #         child_node.set_parameters(acq_params=acq_child_params)
    #     node_p201 = node
    #     chain.add(master, node)
        
    

    ###################################
    # FalconX
    #
    print(fx_trigger)
    if fx_trigger == TriggerMode.SOFTWARE:
        acq_params = {
            "npoints": scan_par["points"],
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : scan_par["time"]
        }
    else:
        acq_params = {
            "npoints": scan_par["points"],
            "trigger_mode": fx_trigger,
            "read_all_triggers": False,
            "block_size": fx_block_size,
            "preset_time" : 0.1
        }
        
    for node in builder.get_nodes_by_controller_type(BaseMCA):
        node.set_parameters(acq_params=acq_params)
        chain.add(master, node)

    # ###################################
    # # SamplingCounterController
    # #
    # acq_params = {
    #         "count_time": scan_par["time"],
    #         "npoints": scan_par["points"],
    #     }
    # for node in builder.nodes:
    #     if isinstance(node.controller, SamplingCounterController):
    #         node.set_parameters(acq_params=acq_params)
    #         # chain.add(node_p201, node)
            
    # for node in builder.nodes:
    #     if isinstance(node.controller, SamplingCounterController):
    #         node.set_parameters(acq_params=acq_params)
    # SimulationCounter

    ###################################
    # CalcCounterController
    
    # acq_params = {
    #         "count_time": scan_par["time"],
    #     }
    # for node in builder.nodes:
    #     if isinstance(node.controller, CalcCounterController):
    #         node.set_parameters(acq_params=acq_params)
    #         chain.add(master, node)

    return (builder, chain)


def scantool_contscan_get_acquisition_chain(
    master,
    musst,
    motor_par,
    scan_par,
    musst_calc_channels,
    counters
):
    """
    Create chain for acquisition devices

    Input Params.
        - master
        - musst: Bliss object which synchronize the scan
        - motor_par["motor"]: Bliss motor to be scanned
            "sync_channel" : Musst synchronization channel
        - scan_par = {}
            "start"        : start position of the scan
            "stop"         : stop position of the scan
            "points"       : Number of points in the scan
            "time"         : Integration time(s) pwer points
            "trig_start"   : type of synchronization ("TIME"/"POSITION")
                             for start point
            "trig_point"   : type of synchronization ("TIME"/"POSITION")
                             for all other points
            "way"          : "up" if start<stop, "down" if start>stop
        - musst_calc_chan  : MUSST calc counters to be added
        - counter          : Counter asked by users
    """
    
    chain = AcquisitionChain(parallel_prepare=True)
    builder = ChainBuilder(counters)
        
    ###################################
    # MUSST
    #
    channel_list = []
    name_list = {}
    for node in builder.get_nodes_by_controller_type(
        MusstIntegratingCounterController
    ):
        for cnt in node.counters:
            ch = cnt.channel
            if ch.lower() == "timer":
                ch = 0
            else:
                ch = int(ch.split("CH")[1])
            channel_list.append(ch)
            name_list[ch] = cnt.name
    for node in builder.get_nodes_by_controller_type(
        MusstSamplingCounterController
    ):
        for cnt in node.counters:
            ch = cnt.channel
            if ch.lower() == "timer":
                ch = 0
            else:
                ch = int(ch.split("CH")[1])
            channel_list.append(ch)
            name_list[ch] = cnt.name
                
    musst_master = musst_get_master(musst, motor_par, scan_par, channel_list)

    if len(channel_list) > 0:
        store_list = []
        channel_list.sort()
        for i in range(len(channel_list)):
            store_list.append(name_list[channel_list[i]])
        musst_acq = MusstAcquisitionSlave(musst, store_list=store_list)
        chain.add(musst_master, musst_acq)

        # Add Musst Channels Calc Counters
        for name, calc in musst_calc_channels.items():
            if calc.musst_ch_name in store_list:
                acq_slave = calc.get_acquisition_slave(musst_acq, scan_par["way"])
                chain.add(musst_master, acq_slave)

    # Add musst in the acquisition chain
    if master is not None:
        chain.add(master, musst_master)

    # ###################################
    # # FalconX
    # #
    # acq_params = {
    #     "npoints": scan_par["points"],
    #     "trigger_mode": TriggerMode.SYNC,
    #     "read_all_triggers": True,
    #     "block_size": 10,
    # }
      
    # for node in builder.get_nodes_by_controller_type(BaseMCA):
    #     #print("----------------")
    #     #print(node)
    #     #print("----------------")
        
    #     node.set_parameters(acq_params=acq_params)
    #     chain.add(musst_master, node)

    # ###################################
    # # Lima
    # #
    # acq_params = {
    #     "acq_nb_frames": scan_par["points"] + 1,
    #     "acq_trigger_mode": "EXTERNAL_TRIGGER_MULTI",
    # }

    # for node in builder.get_nodes_by_controller_type(Lima):
    #     acq_params["acq_expo_time"] = node.controller.acquisition.expo_time
    #     ctrl_params = {"saving_frame_per_file": scan_par["points"] + 1}
    #     node.set_parameters(acq_params=acq_params, ctrl_params=ctrl_params)
    #     chain.add(musst_master, node)

    # ###################################
    # # EMH
    # #
    # acq_params = {
    #     "count_time": scan_par["time"],
    #     "npoints": scan_par["points"] + 1,
    #     "trigger_type": "HARDWARE",
    #     "trigger": "DIO_1",
    # }
    # for node in builder.get_nodes_by_controller_type(EMH):
    #     node.set_parameters(acq_params=acq_params)
    #     chain.add(musst_master, node)

    ###################################
    # P201
    # #
    # acq_node_params = {
    #     "npoints": scan_par["points"] + 1,
    #     "acq_mode": AcqMode.ExtTrigReadout,
    #     "acq_expo_time": scan_par["time"],
    #     "read_all_triggers": True,
    # }
    # acq_child_params = {
    #     "count_time": scan_par["time"],
    #     "npoints": scan_par["points"] + 1,
    # }

    # p201_node = musst_master
    # for node in builder.get_nodes_by_controller_type(CT2Controller):
    #     node.set_parameters(acq_params=acq_node_params)
    #     p201_node = node
    #     for child_node in node.children:
    #         child_node.set_parameters(acq_params=acq_child_params)
    #     chain.add(musst_master, node)

    ###################################
    # PEPU
    # #
    # acq_node_params = {
    #     "npoints": scan_par["points"] + 1,
    #     "start": Signal.SOFT,
    #     "trigger": Signal.DI2,
    # }
    # for node in builder.get_nodes_by_controller_type(PEPU):
    #     node.set_parameters(acq_params=acq_node_params)
    #     chain.add(musst_master, node)

    # ###################################
    # # Speedgoat, CalcCounterController
    # #
    # acq_params = {
    #     "count_time": scan_par["time"],
    #     "npoints": scan_par["points"] + 1,
    #     "npoints": scan_par["points"],
    #     "trigger_type": "HARDWARE",
    # }
    # for node in builder.nodes:
    #     if isinstance(
    #         node.controller, (SpeedgoatCountersController, CalcCounterController)
    #     ):
    #         node.set_parameters(acq_params=acq_params)
    #         #chain.add(p201_node, node)
    #         chain.add(musst_master, node)

    return (builder, chain)