from enum import Enum, auto


class Hutch(Enum):
    """
    Hutch enum
    """
    OH = auto()
    EH1 = auto()
    EH2 = auto()


class MotorGroup(Enum):
    """
    Base class for Motor group enumerator
    """
    pass


class OHMotorGroup(MotorGroup):
    """
    Enum for equipments in the OH hutch
    """
    SLITS = auto()  # slits
    MONO = auto()  # monochromator
    ATTEN = auto()  # attenuators 1 and 2
    MIRR1 = auto()  # mirror 1
    MIRR2 = auto()  # mirror 2


class EH1MotorGroup(MotorGroup):
    """
    Enum for equipments in the OH hutch
    """
    SLITS = auto()  # slits