from bliss.config.static import get_config


from bm20.oh.mono.energy import energy2bragg
from bm20.config.mono import MonoCageAxis
from bm20.config.oh_aliases import OpticsAliases


def xes5_fscan(e0, e1, e_step, step_t_s):
    config = get_config()
    fscan_obj = config.get('fscan_xes5')
    return fastscan(e0, e1, e_step, step_t_s, fscan_obj)


def fastscan_setup(fscan, e0, e1, egy_step, step_time, egy_axis, bragg_axis):
    fscan.pars.motor = bragg_axis

    dspacing = egy_axis.controller._dspacing
    
    e0 -= egy_axis.offset
    e1 -= egy_axis.offset
    
    bragg_0 = energy2bragg(e0, dspacing)
    bragg_1 = energy2bragg(e1, dspacing)
    
    fscan.pars.start_pos = bragg_0
    
    bragg_0_step = abs(energy2bragg(e0 - egy_step, dspacing) - bragg_0)
    bragg_1_step = abs(energy2bragg(e1 - egy_step, dspacing) - bragg_1)
    
    # fs.pars.end_pos = 13000
    step_size = min(bragg_0_step, bragg_1_step)
    if e0 < e1:
        step_size = -1. * step_size
    fscan.pars.npoints = abs((bragg_1 - bragg_0) / step_size)
    fscan.pars.step_size = step_size
    # fs.pars.step_time = 0.1    
    # fs.pars.npoints = 100
    # fs.prepare()   
    fscan.pars.step_time = step_time
    fscan.pars.acq_time = step_time
    fscan.pars.gate_mode = 'POSITION'
    # fs.prepare()   
    # fs.start()
    # print(bragg_0, bragg_1, bragg_0_step, bragg_1_step)
    # return fscan
    
    
def d20fastscan(e0, e1, egy_step, step_time):
    config = get_config()
    energy = config.get(OpticsAliases.ENERGY.value)
    vbragg = config.get(MonoCageAxis.VBRAGG.value)
    fscan_obj = config.get('d20fscan')

    dspacing = energy.controller.selected_real.controller._dspacing

    fs = fscan_obj.fscan 
    fs.pars.motor = vbragg
    
    e0 -= energy.offset
    e1 -= energy.offset
    
    bragg_0 = energy2bragg(e0, dspacing)
    bragg_1 = energy2bragg(e1, dspacing)
    
    fs.pars.start_pos = bragg_0
    
    bragg_0_step = abs(energy2bragg(e0 - egy_step) - bragg_0, dspacing)
    bragg_1_step = abs(energy2bragg(e1 - egy_step) - bragg_1, dspacing)
    
    # fs.pars.end_pos = 13000
    step_size = min(bragg_0_step, bragg_1_step)
    if e0 < e1:
        step_size = -1. * step_size
    fs.pars.npoints = abs((bragg_1 - bragg_0) / step_size)
    fs.pars.step_size = step_size
    # fs.pars.step_time = 0.1    
    # fs.pars.npoints = 100
    # fs.prepare()   
    fs.pars.step_time = step_time
    fs.pars.acq_time = step_time
    fs.pars.gate_mode = 'POSITION'
    # fs.prepare()   
    # fs.start()
    print(bragg_0, bragg_1, bragg_0_step, bragg_1_step)
    return fs