from bliss.config.static import get_config
from bliss.config.settings import SimpleSetting
from bliss.controllers.motor import CalcController

from .d20common import bragg_to_perp
from .oh_objects import bm20_pmac


def _motor_moved(val_in, val_out, epsilon=10e-6):
    return (abs(val_in - val_out) / val_in) < 1e-6


# #######################
# real = calc + offset
# #######################

class D20MonoCalc(CalcController):
    def calc_from_real(self, real_pos):
        # mono2ofst = get_config().get('mono2ofst')
        calc_pos = {'bragg': real_pos['braggangle']}
        return calc_pos

    def calc_to_real(self, calc_pos):
        bragg = calc_pos['bragg']
        
        # dcm2offset = get_config().get('mono2ofst')
        real_pos = {'braggangle': bragg,
                    'xtal2perp': bragg_to_perp(bragg)}# + dcm2offset.position}
        return real_pos


class D20Mono2PerpCalc(CalcController):
    def calc_from_real(self, real_pos):
        calc_pos = {'mono2perp': real_pos['xtal2perp']}
        return calc_pos

    def calc_to_real(self, calc_pos):
        real_pos = {'xtal2perp': calc_pos['mono2perp']}
        return real_pos


class D20Mono2OffsetCalc(CalcController):
    def __init__(self, *args, **kwargs):
        super(D20Mono2OffsetCalc, self).__init__(*args, **kwargs)
        self.__olatch = False
        self.__blatch = False
        self.__platch = False
        self.__is_init_setting = SimpleSetting('mono2ofst_init')
        self.__is_init = self.__is_init_setting.get() is True

    def calc_from_real(self, real_pos):
        calc_pos = {}
        x2perp = self._tagged['xtal2perp'][0]
        bragg = self._tagged['braggangle'][0]

        # this method is called first even if the move is on the calc
        # first with is_moving False, then True
        if bragg.is_moving:
            self.__olatch = False
            self.__blatch = True
            self.__platch = False
        elif x2perp.is_moving:
            if not self.__olatch:
                self.__olatch = False
                self.__blatch = False
                self.__platch = True
        else:
            self.__blatch = False
            self.__platch = False

        if self.__blatch:
            # energy command, offset doesnt move
            pass
        elif self.__olatch:
            # perp is moving after an offset command
            mono2ofst = real_pos['xtal2perp'] - bragg_to_perp(real_pos['braggangle'])
            calc_pos['mono2ofst'] = mono2ofst
        elif self.__platch:
            # perp is moving but it s not an energy move nor an offset move
            mono2ofst = real_pos['xtal2perp'] - bragg_to_perp(real_pos['braggangle'])
            calc_pos['mono2ofst'] = mono2ofst
        else:
            # noone is moving, not sure... keep the previous value
            # dirty hack to set a value the first time this motor is initialized
            if not self.__is_init:
                self.__is_init = True
                self.__is_init_setting.set(True)
                calc_pos['mono2ofst'] = 0

        return calc_pos

    def calc_to_real(self, calc_pos):
        # this method is called first when moving the offset
        real_pos = {}
        self.__olatch = True
        self.__blatch = False
        self.__platch = False
        if self._tagged['mono2ofst'][0].is_moving:
            x2perp = get_config().get('xtal2perp')
            bragg = get_config().get('bragg')
            mono2pos = bragg_to_perp(bragg.position) + calc_pos['mono2ofst']
            real_pos['xtal2perp'] = mono2pos
        return real_pos


# class D20MonoModeCalc(CalcController):
#     def __init__(self, *args, **kwargs):
#         super(D20MonoModeCalc, self).__init__(*args, **kwargs)
#         self.__pmac = None
#         self.__wago = None

#     def _pmac(self):
#         if self.__pmac is None:
#             self.__pmac = bm20_pmac()
#         return self.__pmac

#     def _wago(self):
#         if self.__wago is None:
#             self.__wago = self._pmac()._wago
#         return self.__wago

#     def calc_from_real(self, real_pos):
#         axis = self._tagged['monolat'][0]
#         state = axis.controller.state(axis)

#         if 'MOVING' not in axis:
#             mode = self._pmac().read_dcm_mode()
#         else:


#         return {'monomode': mode}

#     def calc_to_real(self, calc_pos):
#         return {}
