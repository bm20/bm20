import time
from datetime import datetime
from collections import OrderedDict

import pandas as pd
from tabulate import tabulate

from bliss.common.axis import Axis
from bliss.config.static import get_config
from bliss.shell.getval import getval_idx_list, getval_yes_no, getval_int_range
from bliss.common.logtools import user_error
from bliss.controllers.multiplepositions import MultiplePositions

# from bm20.config.mono import get_crystal
from bm20.config.names import (SPEC_NAMES_TO_BLISS,
                               ATTEN1,
                               ATTEN2,
                               MONXTAL,
                               MIR_SURF_1,
                               MIR_SURF_2)

# filters saved positions corresponding to config.
#         The config_code is a four-digit string, where the first to fourth digit stand
#         for the experiment hutch (1:RCH, 2:MRH), the first mirror (1:Rh, 2:Si, 3:Pt),
#         the DCM/DMM crystal/ML (1:Si(311), 2:Si(111)/0deg, 3:Si(111)/30deg, 4:ML(25A), 5:ML(37A)),
#         and the second mirror (cf. M1), respectively.
#         A zero (0) on at a certain position acts as a placeholder. Its value is ignored in the
#         search and the motors belonging to this component are NOT restored.
#         Note: leading zeros are not supported, so filter_config(123) == filter_config(0123)


DEFAULT_SETTINGS_PATH = "/users/blissadm/local/spec/userconf/optics/optics_settings"
NOT_MOTORS = ["conf", "epoch"]
MIRR_MATERIALS = ["?", "Rh", "Si", "Pt"]
BASE_MOTOR_GROUPS = ["m1", "dcm", "m2"]

GRP_MIRR1 = "m1"
GRP_MIRR2 = "m2"
GRP_MONO = "dcm"
GRP_OTHER = "other"


IGNORE_ALIAS = ["bpm1x", "bpm1y"]


def atten_to_bliss(atten):
    pos = atten.motor_objs[0]._hw_position
    targets = atten.targets_dict
    for label, conf in targets.items():
        label_pos = conf[0]["destination"]
        label_tol = conf[0]["tolerance"]
        if pos > label_pos - label_tol and pos < label_pos + label_tol:
            return label
    return "UNKNOWN"


def atten_to_spec(atten):
    return atten.motor_objs[0]._hw_position
    

MULTIPOS_SPEC_TO_BLISS = {ATTEN1: atten_to_bliss,
                          ATTEN2: atten_to_bliss}

MULTIPOS_BLISS_TO_SPEC = { ATTEN1: atten_to_spec,
                           ATTEN2: atten_to_spec}


def epoch_to_iso(epoch):
    return datetime.datetime.fromtimestamp(epoch).astimezone().isoformat()


class OpticsConfig:
    """
    A record in the optics setting file.
    """

    desc = property(lambda self: self._df["desc"])
    date = property(lambda self: self._df["date"])

    def __init__(self, df, parent):
        """
        df: DataFrame
        parent: full pandas whose df is an extract of
        """
        self._df = df
        self._parent = parent

    def __info__(self):
        df = self._df
        tab = []
        for group in self._parent.groups:
            if group not in self._parent.ignored:
                tab += [[motor, df[motor], group]
                        for motor in self._parent.group_motors(group)]
        return tabulate(tab)

    def parent(self):
        return self._parent
    
    def positions(self):
        """
        Returns the motors positions recorded in this entry.
        group is ignored if motor is provided.
        """
        positions = OrderedDict([(g_name, OrderedDict())
                                 for g_name in self._parent.groups])
        motors = [(g_name, m_name)
                   for g_name in self._parent.groups
                   for m_name in self._parent.group_motors(g_name)]
        for g_name, m_name in motors:
            pos = self._df[m_name]
            if pos == 'void':
                pos = 'nan'
            positions[g_name][SPEC_NAMES_TO_BLISS.get(m_name, m_name)] = float(pos)
        # positions = OrderedDict((motor, float(self._df[motor])) for motor in motors)
        return positions
    

def get_crystal(index=None):
    crystal = get_config().get(MONXTAL)
    if index is not None:
        return crystal.controller.crystals.from_index(index)
    return crystal.controller.crystal


def conf_to_txt(conf):
    """
    Converts the conf number to a 4 elements array: hutch, mirr1, crystal, mirr2
    """
    conf = int(conf)
    hutch = f"EH{conf // 1000}"
    mirr1 = MIRR_MATERIALS[(conf // 100) % 10]
    crystal = get_crystal((conf // 10) % 10).label
    mirr2 = MIRR_MATERIALS[conf % 10]
    return hutch, mirr1, crystal, mirr2


class OpticsSettings:
    """
    Loads the optics_settings file, and allows user to filter
    and select entries.
    """
    filename = property(lambda self: self._filename)
    columns = property(lambda self: list(self._data.columns))
    groups = property(lambda self: list(self._groups.keys()))
    motors = property(lambda self: list(self._motors.keys()))
    ignored = property(lambda self: self._ignored[:])
    n_filtered = property(lambda self: len(self._filtered))
    n_total = property(lambda self: len(self._data))

    def __init__(self, filename=DEFAULT_SETTINGS_PATH, desc=None, hutch=None):
        self._filename = None
        self._ignored = []
        self._data = None
        self._groups = None
        self._motors = None
        self._filters = {"hutch": None,
                         "m1": None,
                         "m2": None,
                         "crystal": None,
                         "desc": []}
        self._filtered = None
        self._load(filename)
        self.filter(desc=desc, hutch=hutch)

    def _load(self, filename):
        ignored = NOT_MOTORS[:]
        groups = OrderedDict((name, []) for name in BASE_MOTOR_GROUPS)
        motors = OrderedDict()
        with open(filename, 'r') as f:
            header = ""
            while not header.startswith("#"):
                header = f.readline().lstrip()
            # getting comments
            desc = [line.split("#")[-1].strip(' \n"') for line in f]
        col_names = header.lstrip("#").split()
        header = []
        for col_name in col_names:
            try:
                name, group = col_name.split(":")
            except ValueError:
                name = col_name
                group = "other"
            if name.strip().startswith("@"):
                name = name[1:]
                ignored += [name]
            if name not in ignored:
                groups[group] = groups.get(group, []) + [name]
                motors[name] = group
            header.append(name)

        desc_df = pd.DataFrame(desc)
        desc_df = desc_df.iloc[:, 0].str.split(";", expand=True)
        desc_df.columns = ["date", "conf_str", "desc"]

        csv_f = pd.read_csv(filename,
                            sep="\s+",
                            skiprows=0,
                            comment="#",
                            names=header)
        csv_f = pd.concat([csv_f, desc_df], axis=1)
        self._filename = filename

        self._ignored = ignored
        self._data = csv_f
        self._groups = groups
        self._motors = motors
        self._filtered = csv_f

    def show_filters(self):
        """
        Returns a string that can be printed to show infos
        about the applied filters.
        """
        filters = {k: v for k, v in self._filters.items() if v if not None}
        n_filters = len(filters)
        txt = ''
        if self.n_filtered != self.n_total:
            txt += f"  {n_filters} filter(s) applied:\n"
            tab_filters = [[key, value] for key, value in filters.items()]
            txt += tabulate(tab_filters, headers=["key", "value"],
                            tablefmt="grid")
            txt += f"\nFound {self.n_filtered} entries matching the filter(s) ({self.n_total} total entries).\n"
        else:
            txt += "No filters applied."
        return txt

    def __info__(self):
        df = self._filtered
        df.reset_index()
        filters = {k: v for k, v in self._filters.items() if v if not None}
        n_filters = len(filters)
        tab = [(index, row["date"], row["desc"], *conf_to_txt(row["conf"]))
               for index, row in df.iterrows()]
        txt = "==================================\n"
        txt += "==================================\n"
        txt += tabulate(tab, headers=["idx", "date", "description", "hutch", "M1", "Crystal", "M2"],
                        tablefmt="mixed_grid")
        txt += "\n---------------------------------------------\n"
        if n_filters:
            txt += "<< ATTENTION! FILTERS APPLIED >>\n"
            txt += f"Found {self.n_filtered} entries matching the filter(s) ({self.n_total} total entries).\n"
        else:
            txt += f"File contains {self.n_total} entries.\n"
        txt += "---------------------------------------------\n"
        return txt

    def _set_filtered(self, data):
        self._filtered = data
        return data

    def motor_group(self, motor):
        """
        Returns the group of the given motor (m1, dcm, ...).
        """
        return self._motors[motor]
    
    def group_motors(self, group):
        """
        Returns the list of motors belonging to the given group.
        """
        return self._groups[group]
    
    def clear_filter(self, key):
        """
        Clears the given filter.
        key: one of "desc", "hutch"
        """
        self._filters[key] = None
        self._apply_filters()

    def clear_filters(self):
        """
        Clears all filters
        """
        for key in self._filters.keys():
            self._filters[key] = None
        self._apply_filters()
    
    def filter(self, desc=None, hutch=None):
        """
        Filter saved positions matching the given description and/or hutch.
        desc: str, sase is ignored.
        hutch: 1, 2 (rch1, rch2) or 0 (removes the filter)
        """
        if desc:
            # for now only one text supported
            # (well could do more than one, but I need to update the user menu
            #  to be able to delete individual text filters)
            self._filters["desc"] = [desc] if desc else None #(self._filters["desc"] or []) + [desc]
        if hutch is not None:
            if hutch not in (0, 1, 2):
                raise RuntimeError(f"Invalid hutch ID: {hutch} (expected 1 or 2)")
            self._filters["hutch"] = hutch if hutch else None
        return self._apply_filters()

    def _apply_filters(self):
        src = self._data
        for ftype, fvalue in self._filters.items():
            src = self._filter(src, **{ftype:fvalue})
        self._set_filtered(src)

    def _filter(self, src, desc=None, hutch=None, m1=None, m2=None, crystal=None):
        if hutch:
            src = self._filter_conf(hutch*1000, src)
        if desc:
            for txt in desc:
                src = self._filter_desc(txt, src)
        # if hutch:
        #     src = self._filter_hutch(hutch, src)
        return src
    
    def _filter_conf(self, conf, src):
        query = [f"floor(conf/(10**{i}))%10 == {d}"
                 for i, d in enumerate(str(conf)[-1::-1]) if d !="0"]
        query = " and ".join(query)
        return src.query(query)
    
    def _filter_desc(self, desc, src, case=False):
        return src[src["desc"].str.contains(desc, case=case, na=False)]
    
    def select(self, index):
        """
        Select an entry based on its index in the list of entries.
        """
        return OpticsConfig(self._data.loc[index], self)


def main_menu(entries):
    """
    Entry selection menu.
    Allows to filter entries, and select one.
    Returns:
    - q if user wants to quit
    - p if user wants to print the selection
    - an integer if user wants to restore an entry
    - 
    """
    choices = ["select an index to restore",
               "filter entries",
               "show entries",
               "edit filters",
               "quit"]
    while True:
        ans = getval_idx_list(choices, "Command:", default=5)[0]
        if ans == 5:
            return
        if ans == 3:
            print(print(entries.__info__()))
            continue
        if ans == 4:
            print("\n")
            print(entries.show_filters())
            print("\n")
            continue
        if ans == 2:
            filter_menu(entries)
            continue
        if ans == 1:
            ans = getval_int_range("Index of entry to restore",
                                   minimum=-1,
                                   default=-1,
                                   maximum=entries.n_total)
            if ans == -1:
                continue
            try:
                entry = entries.select(ans)
            except KeyError:
                print("\n!!!!!!!!!!!!!!!!!!!!!")
                print(f"Error, entry not found: {ans}.")
                print("!!!!!!!!!!!!!!!!!!!!!\n")
                continue
            return entry
    

def filter_menu(entries):
    """
    Filter menu.
    Allows to filter entries, and select one.
    """
    choices = ("search text within description",
               "hutch selection",
               "clear all filters",
               "print selection",
               "return to main menu")
    while True:
        print(entries.show_filters())
        ans = getval_idx_list(choices,
                              message="Command:",
                              default=5)
        ans = ans[0]
        if ans == 5:
            return
        if ans == 1:
            desc = input("Text to look for in the desc field (empty string to delete the current filter):")
            entries.filter(desc=desc)
            continue
        if ans == 2:
            hutch = -1
            while hutch not in (0, 1, 2):
                hutch = getval_idx_list(["RCH1", "RCH2", "Both"])[0]
                if hutch not in (1, 2):
                    hutch = -1
                entries.filter(hutch=hutch)
            continue
        if ans == 3:
            entries.clear_filters()
            continue
        if ans == 4:
            print(entries.__info__())
            continue


def restore_entry(entry):
    config = get_config()
    entry_pos = entry.positions()
    while True:
        for g_name, m_positions in entry_pos.items():
            mv_tab = OrderedDict([("", ["Current", "Target", "Distance"])])
            for m_name, m_pos in m_positions.items():
                try:
                    axis = config.get(m_name)
                    cur_pos = axis.position
                    if isinstance(axis, (MultiplePositions,)):
                        m_pos = MULTIPOS_SPEC_TO_BLISS[m_name](axis)
                        if m_pos == cur_pos:
                            diff_pos = "OK"
                        else:
                            diff_pos = "NOK"
                    else:
                        diff_pos = m_pos - cur_pos
                        if abs(diff_pos) < axis.tolerance:
                            diff_pos = "OK"
                    mv_tab[m_name] = (cur_pos, m_pos, diff_pos)
                except RuntimeError:
                    user_error(f"WARNING: Could not get axis {m_name}.")
                    mv_tab[m_name] = ("not found", "ERROR", "ERROR")
            print('----------')
            print(g_name)
            print(tabulate(mv_tab, tablefmt="mixed_grid", headers=mv_tab.keys()))
        print('\n----------')
        print(f"Desc: {entry.desc}")
        print(f"Date: {entry.date}")
        print('----------\n')
        choices = ["1st Mirror, only",
                "Monochromator, only",
                "1st Mirror + Monochromator",
                "2nd Mirror, only",
                "1st Mirror + 2nd Mirror",
                "2nd Mirror + Monochromator",
                "Everything, incl. slits etc.",
                "Abort"]
        ans = getval_idx_list(choices,
                            message="Which components do you want to restore?",
                            default=8)
        ans = ans[0]
        if ans == 8:
            return
        grp_list = []
        if ans in (1, 3, 5, 7):
            grp_list += [GRP_MIRR1]
        if ans in (2, 3, 6, 7):
            grp_list += [GRP_MONO]
        if ans in (4, 5, 6, 7):
            grp_list += [GRP_MIRR2]
        if ans == 7:
            grp_list = list(entry_pos.keys())
        
        print("Restoring the following components:")
        complist = '\n  -'.join(grp_list)
        print(f"  -{complist}")
        if getval_yes_no("Please confirm", default='n'):
            print("RESTORING")
            return
        else:
            print("\n=====\nCancelled.\n=====\n")


def restore_optics(optics_file, desc=None, hutch=None, year=None, month=None):
    full = OpticsSettings(optics_file,
                          desc=desc,
                          hutch=hutch)
    
    print(full.__info__())
    while True:
        entry = main_menu(full)
        if entry is None:
            return
        if entry and restore_entry(entry):
            break


def save_optics(optics_file, hutch, desc):
    print(f"Saving to {optics_file}")

    config = get_config()

    with open(optics_file, 'r') as optics_f:
        header = ""
        while not header.startswith("#"):
            header = optics_f.readline().lstrip()
        col_names = header.lstrip("#").split()
        mot_names = [col.lstrip("@").split(":")[0]
                     for col in col_names]

    positions = []
    for m_name in mot_names:
        position = "nan"
        try:
            if m_name not in NOT_MOTORS and m_name not in IGNORE_ALIAS:
                axis = config.get(SPEC_NAMES_TO_BLISS.get(m_name, m_name))
                if not isinstance(axis, (Axis, MultiplePositions)):
                    print(f"WARNING! {m_name} is not an axis, ignoring.")
                else:
                    if m_name in MULTIPOS_BLISS_TO_SPEC:
                        position = MULTIPOS_BLISS_TO_SPEC[m_name](axis)
                    else:
                        position = axis._hw_position
                if m_name == MONXTAL:
                    position = f"{int(position)}"
                else:
                    if position.is_integer():
                        position = f"{int(position)}"
                    else:
                        position = f"{round(position, 5)}"
        except RuntimeError as ex:
            print(ex)
            print(f"ERROR! Object not found: {m_name}")
            axis = None
        positions.append(position)

    # positions = [config.get(bliss2spec.get(m_name, m_name))._hw_position
    #              for m_name in mot_names
    #              if m_name not in NOT_MOTORS]

    xtal = config.get(MONXTAL)._hw_position
    xtal = get_crystal(xtal)
    if xtal is None:
        xtal_name = "unknown"
    else:
        xtal_name = xtal.description
    m1surf = config.get(MIR_SURF_1).position
    m2surf = config.get(MIR_SURF_2).position
    try:
        mir1 = MIRR_MATERIALS.index(m1surf)
    except ValueError:
        raise ValueError("Mirror 1: unknown position, was it homed?")
    try:
        mir2 = MIRR_MATERIALS.index(m2surf)
    except ValueError:
        raise ValueError("Mirror 2: unknown position, was it homed?")
    print(hutch, mir1, xtal, mir2)
    conf = int(hutch * 1000 +
               mir1 * 100 +
               xtal.index * 10 +
               mir2)
    epoch = int(time.time())
    positions[0] = str(conf)
    positions[1] = f"{epoch}"
    date = time.strftime("%a %b %d %H:%M:%S %Y", time.gmtime(epoch))
    comment = (f"# \"{date}; Configured for EH{hutch} hutch: M1:{m1surf}, "
               f"DCM:{xtal_name}, M2:{m2surf}; {desc}\"")
    #"# "Tue Feb 25 09:26:16 2014; Configured for RC hutch: M1:Rh, DCM:Si(111)/0deg, M2:Rh; CH-3934 Christophe Den Auwer"
    positions += [comment]
    print(positions)
    with open(optics_file, 'a') as ofile:
        ofile.write(' '.join(positions))


def save_rch1(optics_file=DEFAULT_SETTINGS_PATH):
    desc = input("Description:")
    save_optics(optics_file, 1, desc)


if __name__ == "__main__":
    save_rch1("/users/opd20/optics_settings")
    # o = restore_optics("/users/blissadm/optics_settings", hutch=1)#, conf=2031)
    # print(o._data)
    # print(o.columns)
    # print(o.groups)
    # print(o.motors)
    # print(o.ignored)
    # ff = o.filter_config(2131)
    # print(ff)
    # # print(o._data["desc"])
    # tt = o._filter_desc("17kev", ff)
    # print(tt)
    # print(o._filter_config(2131, ff))
    # print(epoch_to_iso(1459944801))
    # o.filter(desc='eve')
    # print(o.__info__())
    # print(o.show_filters())
    # # o.clear_filter()
    # # print(o.__info__())
    # s = o.select(396)
    # print(s.__info__())
    # sel_menu()