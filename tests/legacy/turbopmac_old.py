# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

# documentation : Turbo PMAC User Manual.
# Chapter : Turbo PMAC Ethernet Protocol, p. 412


import re
import enum
import socket
import struct
from collections import OrderedDict

import gevent

from bliss import global_map
from bliss.common.axis import Axis
from bliss.common.logtools import (log_debug,
                                   log_error,
                                   log_info,
                                   log_warning)
from bliss.comm.util import get_comm
from bliss.common.axis import AxisState
from bliss.controllers.motor import Controller


# flush timeout in ms
_PMAC_FLUSH_TIMEOUT = 62465
#500


# pmac default port if not set in the config
_PMAC_DEFAULT_PORT = 1025


# max size of PMAC's buffer
_PMAC_MAX_BUFFER_SIZE = 1400


# motor status to bliss axis state
# tuple: status bit, bliss state if bit set, bliss state if bit unset, info
#
# !!!!
# Some more state translation is done
# in the TurboPmac._pmac_state method
# in which READY takes precedence over MOVING
# !!!!
#
# amplifier enabled = 0 : not moving
# in position = 1: not moving
PMAC_MOTOR_STATUS = (
    # first 6 characters (bits 47 to 24)
    # ((1 << 23 + 24), "ON"),
    (1 << (23 + 24), None, 'OFF', 'Motor activated'),
    (1 << (22 + 24), "LIMNEG", None, 'Negative end limit set'),
    (1 << (21 + 24), "LIMPOS", None, 'Positive end limit set'),
    (1 << (20 + 24), None, None, 'Extended servo algorithm enabled'),
    (1 << (19 + 24), None, 'READY', 'Amplifier Enabled'),
    (1 << (18 + 24), None, None, 'Open Loop Mode'),
    (1 << (17 + 24), 'MOVING', None, 'Move Timer Active'),
    (1 << (16 + 24), None, None, 'Integration Mode'),
    (1 << (15 + 24), None, None, 'Dwell in Progress'),
    (1 << (14 + 24), None, None, 'Data Block Error'),
    (1 << (13 + 24), None, 'MOVING', 'Desired Velocity Zero'),
    (1 << (12 + 24), None, None, 'Abort Deceleration'),
    (1 << (11 + 24), None, None, 'Block Request:'),
    (1 << (10 + 24), "HOMING", None, 'Home Search in Progress'),
    (1 << (9 + 24), None, None, 'User-Written Phase Enable'),
    (1 << (8 + 24), None, None, 'User-Written Servo Enable'),
    (1 << (7 + 24), None, None, 'Alternate Source/Destination'),
    (1 << (6 + 24), None, None, 'Phased Motor'),
    (1 << (5 + 24), None, None, 'Following Offset Mode'),
    (1 << (4 + 24), None, None, 'Following Enabled'),
    (1 << (3 + 24), None, None, 'Error Trigger'),
    (1 << (2 + 24), None, None, 'Software Position Capture'),
    (1 << (1 + 24), None, None, 'Alternate Command-Output Mode'),
    (1 << (0 + 24), None, None, 'Maximum Rapid Speed'),

    # last 6 charaters (bits 23 to 0)
    (1 << 23, None, None, 'Coordinate system bit3'),
    (1 << 22, None, None, 'Coordinate system bit2'),
    (1 << 21, None, None, 'Coordinate system bit1'),
    (1 << 20, None, None, 'Coordinate system bit0'),
    (1 << 19, None, None, 'Coordinate definition bit3 '),
    (1 << 18, None, None, 'Coordinate definition bit2'),
    (1 << 17, None, None, 'Coordinate definition bit1'),
    (1 << 16, None, None, 'Coordinate definition bit0'),
    (1 << 15, None, None, 'Assigned to coordinate system'),
    (1 << 14, None, None, 'RFU'),
    (1 << 13, None, None, 'Foreground In-Position'),
    (1 << 12, None, None, 'Stopped on Desired Position Limit'),
    (1 << 11, None, None, 'Stopped on Position Limit'),
    (1 << 10, "HOMED", None, 'Home Complete'),
    (1 << 9, None, None, 'Phasing Search/Read Active'),
    (1 << 8, "FAULT", None, 'Phasing Reference Error'),
    (1 << 7, None, None, 'Trigger Move'),
    (1 << 6, "FAULT", None, 'Integrated Fatal Following Error'),
    (1 << 5, "FAULT", None, 'I2T Amplifier Fault Error'),
    (1 << 4, None, None, 'Backlash Direction Flag'),
    (1 << 3, "FAULT", None, 'Amplifier Fault'),
    (1 << 2, "FAULT", None, 'Fatal Following Error'),
    (1 << 1, "FAULT", None, 'Warning Following Error'),
    (1 << 0, 'READY', 'MOVING', 'In Position')
    )


def pmac_motor_status_to_str(state):
    text = '\n'.join([f'{i//24}-{23-i%24}: {desc}'
            for i, (flag, _, _, desc) in enumerate(PMAC_MOTOR_STATUS)
            if flag & state])
    return hex(state) + '\n' + text


class PmacRequestType(enum.IntEnum):
    VR_DOWNLOAD = 0x40
    VR_UPLOAD = 0xc0


class PmacRequest(enum.IntEnum):
    VR_PMAC_SENDLINE = 0xb0
    VR_PMAC_GETLINE = 0xb1
    VR_PMAC_FLUSH = 0xb3
    VR_PMAC_GETMEM = 0xb4
    VR_PMAC_SETMEM = 0xb5
    VR_PMAC_SETBIT = 0xba
    VR_PMAC_SETBITS = 0xbb
    VR_PMAC_PORT = 0xbe
    VR_PMAC_GETRESPONSE = 0xbf
    VR_PMAC_READREADY = 0xc2
    VR_CTRL_RESPONSE = 0xc4
    VR_PMAC_GETBUFFER = 0xc5
    VR_PMAC_WRITEBUFFER = 0xc6
    VR_PMAC_WRITEERROR = 0xc7
    VR_FWDOWNLOAD = 0xcb
    VR_IPADDRESS = 0xe0


class TurboPmacCommand:
    request_type = None
    request = None
    reply_rx = None
    eol = None

    def __init__(self,
                 data=None,
                 value=None,
                 index=None,
                 data_len=None,
                 **kwargs):
        super(TurboPmacCommand, self).__init__(**kwargs)

        assert self.request_type in PmacRequestType
        assert self.request in PmacRequest

        if data:
            if not isinstance(data, (bytes,)):
                data = f'{data}\r'.encode()
            data_len = len(data)
        else:
            data = ''
            if data_len is None or data_len < 0:
                data_len = 0

        self._data = data
        self._value = value if value else 0
        self._index = index if index else 0
        self._command = struct.pack('BBHHH',
                                    self.request_type,
                                    self.request,
                                    self._value,
                                    self._index,
                                    socket.htons(data_len))
        if data:
            self._command += data
        if self.reply_rx:
            self._reply_rx = re.compile(self.reply_rx)
        else:
            self._reply_rx = None

    def command(self):
        return self._command

    def parse_reply(self, reply):
        match = self._reply_rx.match(reply.strip())
        if match:
            return match.groupdict()
        else:
            raise RuntimeError('Reply does not match regular '
                               'expression: reply={0}, rx={1}'
                               ''.format(reply, self.reply_rx))

    def need_more(self, data):
        return 0


class PmacFlush(TurboPmacCommand):
    request_type = PmacRequestType.VR_DOWNLOAD
    request = PmacRequest.VR_PMAC_FLUSH
    reply_rx = '.'


class PmacSendline(TurboPmacCommand):
    request_type = PmacRequestType.VR_DOWNLOAD
    request = PmacRequest.VR_PMAC_SENDLINE
    reply_rx = '@'


class PmacGetBuffer(TurboPmacCommand):
    request_type = PmacRequestType.VR_UPLOAD
    request = PmacRequest.VR_PMAC_GETBUFFER
    reply_rx = '^(?P<buffer>.*)$'
    eol = '\x06'


class PmacGetResponse(TurboPmacCommand):
    request_type = PmacRequestType.VR_DOWNLOAD
    request = PmacRequest.VR_PMAC_GETRESPONSE
    reply_rx = '^(?P<response>.*)$'
    eol = '\x06'


class _TurboPmacComm:
    """Communication class for the TurboPmac
    """
    def __init__(self, config_dict):
        log_debug(self, 'Initializing _TurboPmacComm.')
        self.pmac_comm = get_comm(config_dict, port=_PMAC_DEFAULT_PORT)

        global_map.register(self,
                            children_list=[self.pmac_comm])

        # I3=2
        # IO handshake control, turbo srm page 85
        # or PMAC user manual p383
        # <CR> command is ack by PMAC with an <ACK>  (ascii 0x06)
        # invalid command is ack with a <BELL>  (ascii 0x07)
        # messages are sent as DATA <CR> [ DATA <CR> ... ] <ACK>
        # <CR> = 0x0D or \r
        # <LF> = 0x0A or \n
        self.write_i_register(3, 2)

        # I4=0
        # communication integrity mode
        # 0 = checksum disabled
        self.write_i_register(4, 0)

        # I100=0
        # deactivate motor X (here X = 1)

        # M161->D:$00008B
        # M register $00008B now points to the motor #1 actual position
        # M162->D:$000088
        # M register $000088 now points to the motor #1 command position

        log_debug(self, 'Initialized _TurboPmacComm.')

    def host(self):
        "Returns a tuple (hostname, port)"
        return self.pmac_comm._host, self.pmac_comm._port

    def flush(self):
        command = PmacFlush(value=_PMAC_FLUSH_TIMEOUT)
        reply = self.pmac_comm.write_read(command.command())
        command.parse_reply(reply.decode())

    def send_command(self, command_klass, *args, **kwargs):
        log_debug(self, f'send_command{command_klass}({args}, {kwargs}).')
        command = command_klass(*args, **kwargs)

        flush = kwargs.get('flush', None)
        # not flushing if the command is a PmacGetBuffer, unless flush is True!
        if (flush or
            (flush is None and not issubclass(command_klass, (PmacGetBuffer,)))):
            log_debug(self, 'FLUSH')
            self.flush()

        command_raw = command.command()
        log_debug(self, f'Sending command {command_raw}.')
        if command.eol:
            reply = self.pmac_comm.write_readline(command_raw, eol=command.eol)
        else:
            reply = self.pmac_comm.write_read(command_raw)

        match = command.parse_reply(reply.decode())
        log_debug(self, f'Ok command sent, reply={reply}.')
        return match

    def sendline(self, data):
        self.send_command(PmacSendline, data=data)

    def sendline_getbuffer(self, data=None, data_len=None, **kwargs):
        self.sendline(data=data, **kwargs)
        data_len = data_len or _PMAC_MAX_BUFFER_SIZE
        return self.send_command(PmacGetBuffer, data_len=data_len)['buffer']

    def pmac_version(self):
        return self.sendline_getbuffer(data="VERSION")

    def pmac_date(self):
        return self.sendline_getbuffer(data="DATE")

    def pmac_type(self):
        return self.sendline_getbuffer(data="TYPE")

    def read_i_register(self, i_reg, axis_address=None, cast_to=None):
        if axis_address is not None:
            axis_address = int(axis_address)
            command = f'I{axis_address}{i_reg}'
        else:
            command = f'I{i_reg}'
        log_debug(self, f'Requesting register {command}')
        reply = self.sendline_getbuffer(data=command)
        if cast_to:
            value = cast_to(reply)
        else:
            value = reply
        log_debug(self, f'Received I register value {command}={value}')
        return value

    def write_i_register(self, i_reg, value, axis_address=None):
        if axis_address is not None:
            axis_address = int(axis_address)
            command = f'I{axis_address}{i_reg:02d}={value}'
        else:
            command = f'I{i_reg}={value}'
        log_debug(self, f'Writing to register {command}')
        self.send_command(PmacSendline, data=command)

    def read_jog_speed(self, address):
        """Reads an axis' jog speed

        Reads the Ixx22 register value.
        WARNING: units are in counts/msec.
        """
        velocity = self.read_i_register(22,
                                        axis_address=address,
                                        cast_to=float)
        return velocity

    def write_jog_speed(self, axis_address, jog_speed):
        """Sets an axis' jog speed

        Writes to the Ixx22 register value.
        WARNING: units are in counts/msec.
        """
        assert jog_speed >= 0
        self.write_i_register(22, jog_speed, axis_address=axis_address)

    def write_home_speed(self, axis_address, home_speed):
        """Sets an axis' home speed and direction.

        Writes to the Ixx23 register value.
        WARNING: units are in counts/msec.
         """
        self.write_i_register(23, home_speed, axis_address=axis_address)

    def read_home_speed(self, axis_address):
        """Reads an axis' home speed and direction

        Reads the Ixx23 register value.
        WARNING: units are in msec.
        """
        home_speed = self.read_i_register(23,
                                          axis_address=axis_address,
                                          cast_to=float)
        return home_speed

    def read_jog_accel_time(self, axis_address):
        """Reads an axis' acceleration time

        Reads the Ixx20 register value.
        WARNING: units are in msec.
        """
        accel_time = self.read_i_register(20,
                                          axis_address=axis_address,
                                          cast_to=float)
        return accel_time

    def write_jog_accel_time(self, axis_address, accel_time):
        """Sets an axis' acceleration time

        Writes to the Ixx20 register value.
        WARNING: units are in msec.
        """
        self.write_i_register(20, accel_time, axis_address=axis_address)

    def motor_status(self, axis_address):
        """Returns the state of the given axis.

        See Turbo SRM documentation on the ? command.
        """
        log_debug(self, f'Requesting state for axis {axis_address}')
        axis_address = int(axis_address)
        command = f'#{axis_address}?'
        status = self.sendline_getbuffer(data=command)
        log_debug(self, f'Received state for axis {command} : {status}')
        return status

    def motor_position(self, axis_address):
        """PMAC's motor position
        """
        log_debug(self, f'Requesting position for axis {axis_address}.')
        axis_address = int(axis_address)
        m_pos = float(self.sendline_getbuffer(f'#{axis_address}P'))
        log_debug(self, f'Received position for axis {axis_address}={m_pos}.')
        return m_pos

    def jog_stop(self, axis_address):
        """Sends a stop command.
        """
        log_debug(self, f'Sending jog stop request for axis {axis_address}.')
        axis_address = int(axis_address)
        self.send_command(PmacSendline, f'#{axis_address}J/')
        log_debug(self, f'Jog stop request sent for motor {axis_address}.')

    def jog_to(self, axis_address, target):
        """
        Sends a "jog to position" command.
        """
        log_debug(self, f'Sending a jog request for axis {axis_address} to '
                         'position {target}.')
        axis_address = int(axis_address)
        # this is just an extra check to make sure that the passed target
        # is a number
        target = float(target)
        self.send_command(PmacSendline, f'#{axis_address}J={target}')
        log_debug(self, f'Jog request sent for motor {axis_address}.')

    def jog_relative(self, axis_address, delta):
        """
        Sends a "jog relative to current position" command.
        """
        log_debug(self, f'Sending a jog request for axis {axis_address}, '
                         'relative move with delta={delta}.')
        axis_address = int(axis_address)
        # this is just an extra check to make sure that the passed target
        # is a number.
        delta = float(delta)
        self.send_command(PmacSendline, f'#{axis_address}J:{delta}')
        log_debug(self, f'Relative jog request sent for motor {axis_address}.')

    def jog(self, axis_address, direction):
        """
        Sends a "jog in the given direction" commnand.
        Direction: positive if direction > 0, negative otherwise.
        """
        log_debug(self, f'Sending a jog request for axis {axis_address}'
                        f' (direction={direction}).')
        axis_address = int(axis_address)
        if direction > 0:
            op = '+'
        else:
            op = '-'
        self.send_command(PmacSendline, f'#{axis_address}J{op}')
        log_debug(self, f'jog{op} request sent for motor {axis_address}.')


class TurboPmac(Controller):
    """Bliss controller for DeltaTau's TurboPmac

       Methods to override:
        - TurboPmac._pmac_state
    """

    def __init__(self, *args, **kwargs):
        super(TurboPmac, self).__init__(*args, **kwargs)

        self._pmac_status = AxisState()
        self._pmac_status.create_state('HOMING', 'Home search in progress')
        self._pmac_status.create_state('HOMED', 'Home search complete')
        # self.pmac_comm = None
        self.pmac_comm = _TurboPmacComm(self.config.config_dict)

        global_map.register(self,
                            parents_list=['controllers'],
                            children_list=[self.pmac_comm])

        # self.axis_settings.config_setting["pmac_address"] = True

        # velocity and acceleration are not mandatory in config
        # self.axis_settings.config_setting["velocity"] = False
        # self.axis_settings.config_setting["acceleration"] = False

    def get_id(self, axis):
        return self.pmac_comm.pmac_version()

    def initialize(self):
        super(TurboPmac, self).initialize()
        log_debug(self, 'Initializing.')
        # self.pmac_comm = _TurboPmacComm(self.config.config_dict)
        self.axis_settings.config_setting["velocity"] = False
        self.axis_settings.config_setting["acceleration"] = False
        self.axis_settings.config_setting["steps_per_unit"] = False
        log_debug(self, 'Initialized.')

    def initialize_hardware(self):
        super(TurboPmac, self).initialize_hardware()
        host, port = self.pmac_comm.host()
        pmac_version = self.pmac_comm.pmac_version()
        pmac_type = self.pmac_comm.pmac_type()
        pmac_date = self.pmac_comm.pmac_date()
        info = (f'PMAC:\n'
                f'   - host : {host}:{port},\n'
                f'   - version : {pmac_version},\n'
                f'   - type : {pmac_type},\n'
                f'   - date : {pmac_date}.')
        log_info(self, f'Connected to {info}.')

    def initialize_axis(self, axis):
        address = axis.config.config_dict.get('pmac_address')
        read_only = axis.config.config_dict.get('read_only', False)

        # if not read only, we should have the default mandatory
        # settings in the yaml 
        # TODO : this needs more thought
        if not read_only:
            try:
                _v = axis.config.get('velocity')
                _s = axis.config.get('steps_per_unit')
                _a = axis.config.get('acceleration')
            except Exception as ex:
                if not ex.args:
                    ex.args = (f'PMAC axis {axis.name}:',)
                else:
                    ex.args = (f'PMAC axis {axis.name} : ' + ex.args[0],) + ex.args[1:]
                raise

        name = axis.name
        log_debug(self, f'Initializing axis {name}, with address {address}.')
        axis.pmac_address = address
        axis.pmac_read_only = read_only
        log_debug(self, f'Initialized axis {name}')

    def initialize_hardware_axis(self, axis):
        super(TurboPmac, self).initialize_hardware_axis(axis)
        # setting the motor maximum jog/home acceleration, forced to 1000
        # TODO : this value is set to 1000 to reproduce the behaviour of
        #   another program.
        pmac_address = self._pmac_address(axis)
        self.pmac_comm.write_i_register(19, 1000., axis_address=pmac_address)
        # setting the motor maximum jog/home s-curve time to 0
        #  so that only Ixx20 is used
        self.pmac_comm.write_i_register(21, 0, axis_address=pmac_address)

    def pmac_motor_status_to_str(self, axis, print_stdout=True):
        pmac_address = self._pmac_address(axis)
        status = self.pmac_comm.motor_status(pmac_address)
        status = int(status, base=16)
        status_str = pmac_motor_status_to_str(status)
        if print_stdout:
            log_info(self, status_str)
        else:
            return status_str

    def _update_deceleration_rate(self, axis):
        """
        Updating the Ixx15 variable (motor abort/limit deceleration rate)

        Warning: should not be set to 0.
        """
        pmac_address = self._pmac_address(axis)
        # accel in steps/s-2
        acceleration = axis.acceleration * abs(axis.steps_per_unit)
        # decel in steps/ms-2
        # TODO : the factor is there to reproduce the behaviour of
        #   another program.
        decel = 2 * acceleration / 1000**2.
        log_debug(self, f'Setting motor #{pmac_address} deceleration'
                        f' rate to {decel}.')
        if decel == 0:
            raise ValueError(f'Deceleration rate should not be {decel}.')
        # TODO
        # self.pmac_comm.write_i_register(15, decel, axis_address=pmac_address)

    def set_velocity(self, axis, new_velocity):
        """
        Sets velocity in steps/s-1
        """
        if self._pmac_axis_read_only(axis):
            log_debug(self, f'Cannot set velocity, '
                            f'axis {axis.name} is read_only.')
            return
        pmac_address = self._pmac_address(axis)
        log_debug(self, f'TurboPmac: setting axis {pmac_address} velocity '
                        f'to {new_velocity}')
        # pmac is in cts/ms, new_velocity is in cts/s, hence the 10**-3 factor
        pmac_velocity = new_velocity / 1000.
        self.pmac_comm.write_jog_speed(pmac_address, pmac_velocity)

    def read_velocity(self, axis):
        """
        Returns velocity in steps/s-1
        """
        pmac_address = self._pmac_address(axis)
        velocity = self.pmac_comm.read_jog_speed(pmac_address)
        # pmac is in cts/ms, bliss expects units/s, hence the 10**3
        velocity = velocity * 1000
        log_debug(self, f'TurboPmac: axis {pmac_address} velocity is {velocity}')
        return velocity

    def set_acceleration(self, axis, new_acceleration):
        log_debug(self, f'Cannot set acceleration, '
                        f'axis {axis.name} is read_only.')
        if self._pmac_axis_read_only(axis):
            log_debug(self, f'set_acceleration: axis {axis.name} '
                            f'defined as read only.')
            return
        pmac_address = self._pmac_address(axis)
        # pmac doesnt have acceleration, but has acceleration time
        # PMAC's acceleration time is in msec
        # new_acceleration is in steps.s-2
        # dt = 1000(ms) * dv / a
        steps_velocity = axis.velocity * abs(axis.steps_per_unit)
        accel_time = int(0.5 + 1000. * steps_velocity / new_acceleration)
        log_debug(self, f'1000*accel_time={steps_velocity}/{new_acceleration}')
        log_debug(self, f'Setting axis {pmac_address} acceleration '
                        f'to {new_acceleration} '
                        f'(i.e: {accel_time}ms of acceleration time).')
        if accel_time == 0:
            i21 = self.pmac_comm.read_i_register(21, axis_address=pmac_address)
            if i21 == 0:
                raise ValueError('Acceleration time should not be 0.')
        self.pmac_comm.write_jog_accel_time(pmac_address, accel_time)
        self._update_deceleration_rate(axis)

    def read_acceleration(self, axis):
        pmac_address = self._pmac_address(axis)
        # pmac doesnt have acceleration, but has acceleration time
        # PMAC's acceleration time is in msec
        # BLISS's acceleration is in unit.s-2
        # a = 1000(ms) * dv / dt
        accel_time = self.pmac_comm.read_jog_accel_time(pmac_address)
        steps_velocity = axis.velocity * abs(axis.steps_per_unit)
        accel = 1000. * steps_velocity / accel_time
        log_debug(self, f'TurboPmac: axis {pmac_address} acceleration '
                        f'is {accel} steps/s^2.')
        return accel

    def state(self, axis):
        # pmac motor status is a 12 characters hexadecimal word (2x24 bits)
        # see TURBO SRM documentation about the "?" (report moto status)
        # command.
        pmac_address = self._pmac_address(axis)
        pmac_status = self.pmac_comm.motor_status(pmac_address)
        pmac_status = int(pmac_status, base=16)
    
        state = self._pmac_status.new()

        state_names = self.pmac_status_to_bliss_state_names(pmac_status)
    

        user_state_names = self._pmac_state(axis, tuple(state_names), pmac_status)
        for state_name in user_state_names:
            state.set(state_name)

        return state

    def pmac_status_to_bliss_state_names(self, pmac_status):
        state_names = set()

        for mask, value_set, value_unset, d in PMAC_MOTOR_STATUS:
            if pmac_status & mask:
                if value_set:
                    state_names |= {value_set}
            elif value_unset:
                state_names |= {value_unset}

        state_names = set(state_names)

        if 'HOMING' in state_names:
            state_names |= {'MOVING'}
            state_names -= {'READY'}

        if 'READY' in state_names:
            state_names -= {'MOVING'}

        return state_names

    def _pmac_state(self, axis, state_names, pmac_status):
        """
        Override this method to customize the axis state.

        Args:
            axis: the axis
            state_names: default bliss state named corresponding the status
                (see PMAC_MOTOR_STATUS)
            pmac_status (int): the 24 bits returned by the '#x?' command, where
                x is the axis address.
        """
        return state_names

    def _pmac_axis_read_only(self, axis):
        return getattr(axis, 'pmac_read_only', False)

    def _pmac_address(self, axis):
        try:
            return axis.pmac_address
        except AttributeError:
            pmac_address = axis.config.get('pmac_address')
            setattr(axis, 'pmac_address', pmac_address)
            return pmac_address

    def read_position(self, axis):
        return self.pmac_comm.motor_position(self._pmac_address(axis))

    def stop(self, axis):
        self.pmac_comm.jog_stop(self._pmac_address(axis))

    def pmac_stop_all(self, init_only=False):
        if init_only:
            axes = self._axes
        else:
            axes = [self.get_axis(axis['name'])
                    for axis in self.config.config_dict.get('axes')]
        for axis in axes:
            log_info(self, f'Stopping {axis.name}.')
            self.stop(axis)

    def prepare_move(self, motion):
        if self._pmac_axis_read_only(motion.axis):
            raise RuntimeError(f'Axis {motion.axis.name}'
                               f' defined as read only.')

    def start_one(self, motion):
        if self._pmac_axis_read_only(motion.axis):
            raise RuntimeError(f'Axis {motion.axis.name} defined as read only.')
        axis_address = self._pmac_address(motion.axis)
        log_debug(self, 'start_on on axis {axis_address}.')
        if not motion.type == 'move':
            raise ValueError(f'Unsupported motion type: {motion.type}.')

        if motion.target_pos is not None:
            self.pmac_comm.jog_to(axis_address, motion.target_pos)
        elif motion.delta is not None:
            self.pmac_comm.jog_relative(axis_address, motion.delta)
        else:
            raise ValueError('target_pos and delta are both None.')

    def start_jog(self, axis, velocity, direction):
        """
        Starts a jog in the given direction.
        Attention: the velocity param is ignored.
        """
        if self._pmac_axis_read_only(axis):
            raise RuntimeError(f'Axis {axis.name} defined as read only.')
        axis_address = self._pmac_address(axis)
        log_debug(self,
                  f'start_jog on axis {axis_address}, '
                  f'direction={direction} (ignored: velocity={velocity}).')
        self.pmac_comm.jog(axis_address, direction)

    def set_on(self, axis):
        """
        Writes 1 to register Ixx00
        """
        address = self._pmac_address(axis)
        log_debug(self, f'Enabling motor {address}.')
        self.pmac_comm.write_i_register(0, 1, axis_address=address)

    def set_off(self, axis):
        """
        Writes 0 to register Ixx00
        """
        address = self._pmac_address(axis)
        log_debug(self, f'Disabling motor {address}.')
        self.pmac_comm.write_i_register(0, 0, axis_address=address)

    def limit_search(self, axis, limit):
        """
        Starts a limit search (sends a JOG in the given direction).
        """
        if self._pmac_axis_read_only(axis):
            raise RuntimeError(f'Axis {axis.name} '
                               f'defined as read only.')
        direction = 1 if limit > 0 else -1
        self.pmac_comm.jog(self._pmac_address(axis), direction)

    def home_search(self, axis, direction):
        if self._pmac_axis_read_only(axis):
            raise RuntimeError(f'Axis {axis.name} defined as read only.')
        address = self._pmac_address(axis)
        log_debug(self, f'Starting a home search for axis {axis.name} '
                        f'(dir={direction}).')
        direction = 1 if direction > 0 else -1
        # home speed in counts/ms !!
        home_speed = direction * abs(axis.velocity * axis.steps_per_unit) / 1000.
        self.pmac_comm.write_home_speed(address, home_speed)
        self.pmac_comm.sendline(f'#{address}HOME')

    def home_state(self, axis):
        axis_state = axis.hw_state

        if 'HOMING' in axis_state:
            axis_state.set('MOVING')
            try:
                axis_state.unset('READY')
            except ValueError:
                pass
        return axis_state

    def pmac_home_states(self, *axis):
        """
        Returns an OrderedDict containing True or False for each given axis,
        depending on the "homed" state.
        Keys: axis name
        Value: True if the axis' state contains HOMED, False otherwise
        """
        states = OrderedDict()
        for ax in axis:
            if not isinstance(ax, (Axis,)):
                ax = self.get_axis(ax)
            axis_name = ax.name
            state = ax.hw_state
            homed = 'HOMED' in state
            states[axis_name] = homed
        return states

    def _pmac_reboot(self, **kwargs):
        """
            Reboots the PMAC.

            Mandatory keyword: confirm=True
        """
        if kwargs.get('confirm') is True:
            delay = 5
            try:
                for i in range(delay):
                    log_info(self, f'Rebooting the PMAC {self.name} in {delay-i}s.')
                    gevent.sleep(1)
                self.pmac_comm.sendline('$$$')
            except:
                log_info(self, f'PMAC {self.name}: reboot cancelled.')
        else:
            log_info(self, f'ERROR: reboot: Pmac {self.name}, missing keyword "confirm".')