import numpy as np
from ...bm20.tools.elements.elements import get_binding_energy, get_natural_width


# Physical constant
E_MASS = 9.1093836e-31
E_CHARGE = 1.602176634e-19
HBAR = 1.0545718176461565e-34
K_TO_E = 2.0 * E_CHARGE * E_MASS * 1e-20 / (np.power(HBAR, 2))


# # Pre-edge 1
# _PRE_E_MIN_0 = -70
# _PRE_E_INC_0 = 10
# _PRE_CTIME_0 = 0.5

# # Pre-edge 2
# _PRE_E_MIN_1 = -30
# _PRE_E_INC_1 = 1
# _PRE_CTIME_1 = 0.5

# # Scan over the absorption edge
# _EDGE_E_MIN = -20
# _EDGE_E_MAX = 30
# _EDGE_E_INC = 1
# _EDGE_CTIME = 0.5

# # Parameters for the k-region 
# _POST_K_END = 13
# _POST_K_INC = 0.05
# _POST_T_MIN = 0.5    # Minimum  counting time at k=0
# _POST_T_MAX = 2   # Maximum counting time at k=K_END
# # Power of the increase in counting time, 
# #   e.g. 1.0= linear, 2.0=square, 3.0=cubic
# _POST_T_POW = 2


# Pre-edge 1
_PRE_E_MIN_0 = -200
_PRE_E_INC_0 = 10
_PRE_CTIME_0 = 1

# Pre-edge 2
_PRE_E_MIN_1 = -40
_PRE_E_INC_1 = 0.5
_PRE_CTIME_1 = 1

# Scan over the absorption edge
_EDGE_E_MIN = -20
_EDGE_E_MAX = 30
_EDGE_E_INC = 0.5
_EDGE_CTIME = 1

# Parameters for the k-region 
_POST_K_END = 7# 14.5
_POST_K_INC = 0.05
_POST_T_MIN = 2    # Minimum  counting time at k=0
_POST_T_MAX = 1.5#14   # Maximum counting time at k=K_END
# Power of the increase in counting time, 
#   e.g. 1.0= linear, 2.0=square, 3.0=cubic
_POST_T_POW = 2


def e_to_k(delta_e_ev):
    """ Given a value of the energy (in eV above the edge)
        as input, returns the corresponding value in k.

    Args:
        delta_e_ev (float): energy above the edge, in eV

    Returns:
        float: k
    """
    # taken from SPEC's kscan
    # even then the code didnt explain where this constant comes from
    # TODO: improve
    # return 0.51242415 * np.sqrt(delta_e_ev)
    
    # taken from GB code
    return np.sqrt(K_TO_E * delta_e_ev)


def k_to_e(k):
    """Given a value of k, returns the corresponding
       value in energy above the edge in eV.

    Args:
        k (float): k

    Returns:
        float: energy above the edge in eV
    """
    # taken from SPEC's kscan
    # even then the code didnt explain where this constant comes from
    # TODO: improve
    # return np.power(k/0.51242415, 2)

    # taken from GB code
    return np.power(k, 2) / K_TO_E


def k_to_s(t_pwr, t_min_s, t_max_s, k_values, k_max=None):
    """
    Computes the integration time for the given K values.
    t_pwr = 1, 2
    t_min_s, t_max_s : min and max integration times
    k_end
    """
    if k_max is None:
        k_max = k_values[-1]
    k_m = (t_max_s - t_min_s) / np.power(k_max, t_pwr)
    return t_min_s + k_m * np.power(k_values, t_pwr)


def k_to_v2(v_pwr, v_min_ev_s, v_max_ev_s, k_values, k_min=None):
    if k_min is None:
        k_min = k_values[0]
    k_m = (v_max_ev_s - v_min_ev_s) * np.power(k_min, v_pwr)
    return v_min_ev_s + k_m / np.power(k_values, v_pwr)


def k_to_v(v_pwr, v_min_ev_s, v_max_ev_s, k_values, k_max=None):
    if k_max is None:
        k_max = k_values[-1]
    k_m = (v_max_ev_s - v_min_ev_s) / np.power(k_max, v_pwr)
    return v_max_ev_s - k_m * np.power(k_values, v_pwr)
        

def k_range_to_k_e(edge_ev, post_edge_ev, k_end, k_inc):
    k_start = e_to_k(post_edge_ev)
    if (k_end - k_start) % k_inc != 0.:
        k_end = k_end + k_inc
    else:
        k_end = k_end
    k_pos = np.arange(k_start, k_end, k_inc)
    return k_pos, k_to_e(k_pos) + edge_ev


class EScanParams:
    """ Constant step/time scan paramters.
    """
    
    _type = 'Scan'
    element = property(lambda self: self._element)
    edge = property(lambda self: self._edge)
    edge_ev = property(lambda self: self._edge_ev)
    e_min = property(lambda self: self._e_min)
    e_max = property(lambda self: self._e_max)
    e_inc = property(lambda self: self._e_inc)
    acq_t_s = property(lambda self: self._acq_t_s)
    scan_pos = property(lambda self: self._scan_pos)
    scan_t = property(lambda self: self._scan_t)
    
    def __init__(self, element, edge, e_min, e_max,
                 e_inc, acq_t_s, edge_ev=None, _type='Scan'):
        # TODO: set a range for e_min and e_max? like +/- 200 max?
        if e_min >= 0:
            raise ValueError(f'e_min should be < 0 [got {e_min}]')
        
        if e_max <= e_min:
            raise ValueError(f'e_max should be > e_min [e_min={e_min}; e_max={e_max}]')
        
        if e_inc <= 0:
            raise ValueError(f'e_inc should be > 0 [got {e_inc}.')
        
        if acq_t_s <= 0:
            raise ValueError(f'acq_t_s should be > 0 [got {acq_t_s}.')
        
        if edge_ev is None:
            edge_ev = get_binding_energy(element, edge)
        
        self._element = element
        self._edge = edge
        self._edge_ev = edge_ev
        
        self._e_min = e_min
        self._e_max = e_max
        self._e_inc = e_inc
        self._acq_t_s = acq_t_s
        
        self._type = _type
        
        self._calc()
        
    def _calc(self):
        # warning: the last value of the E array may not be e_max
        # depending on the increment (last E either = or < that e_max)
        e_pos = np.arange(self._e_min + self._edge_ev,
                          self._e_max + self._edge_ev,
                          self._e_inc)
        acq_t = np.repeat(self._acq_t_s, e_pos.shape[0])
        self._scan_pos = e_pos
        self._scan_t = acq_t

    def _calc_cont(self, ev_start=None, ev_end=None):
        if ev_start is None:
            ev_start = self.scan_pos[0]
            start_idx = 0
        else:
            start_idx = 1
        if ev_end is None:
            ev_end = self.scan_pos[-1]
        vel_ev_s = 1
        t_seg_s_max = 2.
        dt = (ev_end - ev_start) / vel_ev_s
        n_seg = int(np.ceil(dt / t_seg_s_max))
        e_pos = np.linspace(ev_start,
                            ev_end,
                            n_seg + 1)
        t_seg = dt / n_seg
        tm = np.repeat(t_seg, e_pos.shape[0]-1)
        return e_pos[start_idx:], tm
        
    def __repr__(self):
        str = [f' > {self._type} (elem: {self.element:>s}  @  edge: '
                    f'{self.edge:>s} [{self.edge_ev} eV])',
               f'   * e_min = {self.e_min:> 5.1f} '
                    f'[{self.e_min + self.edge_ev:> 5.1f} eV]']
        
        if self.e_min < 0 and self.e_max > 0:
            str += [f'   |  edge         [{self.edge_ev:> 5.1f} eV]']
        
        str += [f'   * e_inc = {self.e_inc:> 5.1f} eV']
        
        str += [f'   * e_max = {self.e_max:> 5.1f} '
                    f'[{self.e_max + self.edge_ev:> 5.1f} eV]',
                f'   * ct    = {self.acq_t_s:> 5.1f} s'
        ]
        return '\n'.join(str)
    
    
class KScanParams:
    element = property(lambda self: self._element)
    edge = property(lambda self: self._edge)
    edge_ev = property(lambda self: self._edge_ev)
    natural_width_ev = property(lambda self: self._natural_width_ev)
    e_start = property(lambda self: self._e_start)
    k_end = property(lambda self: self._k_end)
    k_inc = property(lambda self: self._k_inc)
    t_min_s = property(lambda self: self._t_min_s)
    t_max_s = property(lambda self: self._t_max_s)
    t_pwr = property(lambda self: self._t_pwr)
    scan_pos = property(lambda self: self._scan_pos)
    scan_t = property(lambda self: self._scan_t)
    
    def __init__(self,
                 element,
                 edge,
                 e_start,
                 k_end,
                 k_inc,
                 t_min_s,
                 t_max_s,
                 t_pwr):
        
        edge_ev = get_binding_energy(element, edge)
        natural_width_ev = get_natural_width(element, edge)
        
        # TODO error msg
        assert t_pwr in (0, 1, 2)
        
        self._element = element
        self._edge = edge
        self._edge_ev = edge_ev
        self._natural_width_ev = natural_width_ev
        
        self._e_start = e_start
        self._k_end = k_end
        self._k_inc = k_inc
        self._t_min_s = t_min_s
        self._t_max_s = t_max_s
        self._t_pwr = t_pwr
        
        self._scan_pos = None
        self._scan_t = None
        self._calc()

    def _calc(self):
        k_values, scan_pos = k_range_to_k_e(self.edge_ev,
                                            self.e_start,
                                            self.k_end,
                                            self.k_inc)
        scan_t = k_to_s(self.t_pwr,
                        self.t_min_s,
                        self.t_max_s,
                        k_values)

        # k_m = (self.t_max_s - self.t_min_s) / pow(self.k_end, self.t_pwr)
        # k_start = e_to_k(self.e_start)
        # if (self.k_end - k_start) % self.k_inc != 0.:
        #     k_end = self.k_end + self.k_inc
        # else:
        #     k_end = self.k_end
        # k_pos = np.arange(k_start, k_end, self.k_inc)
        # scan_pos = k_to_e(k_pos) + self._edge_ev
        # scan_t = self.t_min_s + k_m * np.power(k_pos, self.t_pwr)
        
        self._scan_pos = scan_pos
        self._scan_t = scan_t

    def _calc_cont(self, ev_start=None, ev_end=None):
        step_s = 2
        min_vel_ev_s = 1
        max_vel_ev_s = 5
        if ev_start is not None:
            ev_start = self.edge_ev + self.e_start
            e_start = ev_start - self.edge_ev
            start_idx = 1
        else:
            e_start = self.e_start
            start_idx = 0
        if ev_end is not None:
            # ev_end = scan_pos[-1]
            k_end = e_to_k(ev_end - self.edge_ev)
        else:
            k_end = self.k_end

        k_values, scan_pos = k_range_to_k_e(self.edge_ev,
                                            e_start,
                                            k_end,
                                            self.k_inc)
                            
        scan_pos -= self.edge_ev
        e0 = scan_pos[0]
        max_e = scan_pos[-1]
        
        def e_to_v(ev, v_min, v_max, e_max):
            return v_min + v_max * (e_max - ev) / e_max
            #return v_min + e * v_max / e_max
            
        v0 = e_to_v(e0, min_vel_ev_s, max_vel_ev_s, max_e)
        e0 = e0 + v0 * step_s
        scan_pos = [e0]
        while e0 < max_e:
            v0 = e_to_v(e0, min_vel_ev_s, max_vel_ev_s, max_e)
            e0 = e0 + v0 * step_s
            scan_pos.append(e0)
        
        scan_pos = np.array(scan_pos) + self.edge_ev
        scan_tm = np.repeat(step_s, scan_pos.shape[0] - start_idx)
        return scan_pos[start_idx:], scan_tm
        #k0 = e_to_k(max_vel_ev_s)
        #k1 = e_to_k(scan_pos[-1] - self.edge_ev - min_vel_ev_s)

        #print("TOTOTOTOT", e_to_k(0))
        #max_vel_k_s = k0
        #min_vel_k_s = k_values[-1] - k1
        
        #print("VEL", max_vel_k_s, min_vel_k_s,
              #k_to_v(self.t_pwr, min_vel_k_s,
                     #max_vel_k_s, k_values[-1], k_max=k_values[-1]))
         ##e_pos = np.arange(ev_start, ev_end, 0.1)
         ##k_values_cont = e_to_k(e_pos - self.edge_ev)

        #k0 = k_values[0] #e_to_k(ev_start - self.edge_ev)
        #v0 = k_to_v(self.t_pwr, min_vel_k_s, max_vel_k_s, k0, k_max=k_values[-1])
        #k0 = v0 * step_s + k0#@k_to_e(k0) + self.edge_ev
         ##e0 = k_to_e(k0) + self.edge_ev

        #scan_pos = [k0]
        #scan_v = [v0]
        #while k0 < k_end:
             ##k0 = e_to_k(e0 - self.edge_ev)
            #v0 = k_to_v(self.t_pwr, min_vel_k_s, max_vel_k_s, k0, k_max=k_values[-1])
            #k0 = v0 * step_s + k0#@k_to_e(k0) + self.edge_ev
             ##e0 = k_to_e(k0) + self.edge_ev
             ##print(k0, v0, e0)
             ##print(e0)
            #scan_pos.append(k0)
            #scan_v.append(v0)


        #scan_pos = np.array(scan_pos)
         ##print(scan_pos[-1], k_values[-1],
               ##k_to_v(self.t_pwr, min_vel_k_s,
                      ##max_vel_k_s, scan_pos[-1], k_max=k_values[-1]))
         ##print(np.diff(scan_pos))
        #return k_to_e(scan_pos) + self.edge_ev, np.repeat(2, scan_pos.shape[0])


    def __repr__(self) -> str:
        str = [f' > EXAFS scan (elem: {self.element:>s}  @  edge: {self.edge:>s} [{self.edge_ev} eV])',
               f'   * e_start = {self.e_start:> 5.1f} [{self.e_start + self.edge_ev:> 5.1f} eV]',
               f'   * k_end = {self.k_end} (={self.edge_ev + k_to_e(self.k_end)})',
               f'   * k_inc = {self._k_inc}',
               f'   * t_min = {self._t_min_s} s',
               f'   * t_max = {self._t_max_s} s',
               f'   * t_pwr = {self._t_pwr}']
        return '\n'.join(str)
        

class ExafsParameters:
    # TODO: do some check on E min/max
    #   to make sure that scans dont overlap...
        
    scans_params = property(lambda self: self._pre_e_scans + self._e_scans + self._k_scans)
    pre_e_scans = property(lambda self: self._pre_e_scans)
    e_scans = property(lambda self: self._e_scans)
    k_scans = property(lambda self: self._k_scans)
    scan_pos = property(lambda self: self._scan_pos)
    scan_t = property(lambda self: self._scan_t)
    
    def __init__(self, element, edge):
        self._scans = []#OrderedDict()
        
        pre_0 = self._append_escan(element, edge,
                            _PRE_E_MIN_0,
                            _PRE_E_MIN_1,
                            _PRE_E_INC_0,
                            _PRE_CTIME_0,
                            _type='Pre edge 0')
        
        
        pre_1 = self._append_escan(element, edge,
                            _PRE_E_MIN_1,
                            _EDGE_E_MIN,
                            _PRE_E_INC_1,
                            _PRE_CTIME_1,
                            _type='Pre edge 1')
        
        edge_1 = self._append_escan(element, edge,
                                _EDGE_E_MIN,
                                _EDGE_E_MAX,
                                _EDGE_E_INC,
                                _EDGE_CTIME,
                                _type='Edge')
        
        exafs = self._append_kscan(element,
                            edge,
                            _EDGE_E_MAX,
                            _POST_K_END,
                            _POST_K_INC,
                            _POST_T_MIN,
                            _POST_T_MAX,
                            _POST_T_POW)
        
        self._pre_e_scans = [pre_0, pre_1]
        self._e_scans = [edge_1]
        self._k_scans = [exafs]
        
        self._calc()

    def _append_escan(self,
                      element,
                      edge,
                      edge_ev_0,
                      edge_ev_1,
                      e_inc,
                      acq_time,
                      _type='n/a'):
        # if self._scans:
        #     scan = self._scans[-1]
        #     edge_ev_0 = scan.scan_pos[-1] - scan.edge_ev + e_inc
        scan = EScanParams(element, edge,
                           edge_ev_0,
                           edge_ev_1,
                           e_inc,
                           acq_time,
                           _type=_type)
        self._scans.append(scan)
        return scan

    def _append_kscan(self,
                      element,
                      edge,
                      edge_ev_0,
                      k_end,
                      k_inc,
                      t_min,
                      t_max,
                      t_pow,
                      _type='n/a'):
        if self._scans:
            scan = self._scans[-1]
            edge_ev_0 = scan.scan_pos[-1] - scan.edge_ev + scan.e_inc
        scan = KScanParams(element, edge,
                           edge_ev_0,
                           k_end,
                           k_inc,
                           t_min,
                           t_max,
                           t_pow)
        self._scans.append(scan)
        return scan

    def metadata(self):
        mdata = {"element": self._k_scans[0].element,
                 "edge": self._k_scans[0].edge,
                 "edge_ev": self._k_scans[0].edge_ev,
                 "e_start": self._pre_e_scans[0].e_min,
                 "e_end": self._pre_e_scans[-1].scan_pos[-1],
                 "k_end": self._k_scans[-1].k_end,
                 "n_pre_e_scans": len(self._pre_e_scans),
                 "n_e_scans": len(self._e_scans),
                 "n_k_scans": len(self._k_scans)}
        for i, scan in enumerate(self._pre_e_scans):
            mdata[f"pre_{i}"] = {"e_start": scan.e_min,
                                 "e_end": scan.e_max,
                                 "e_inc": scan.e_inc}
        for i, scan in enumerate(self._e_scans):
            mdata[f"e_scan_{i}"] = {"e_start": scan.e_min,
                                    "e_end": scan.e_max,
                                    "e_inc": scan.e_inc}

        for i, scan in enumerate(self._k_scans):
            mdata[f"k_scan_{i}"] = {"e_start": scan.e_start,
                                    "k_end": scan.k_end,
                                    "k_inc": scan.k_inc,
                                    "t_start": scan.t_min_s,
                                    "t_end": scan.t_max_s,
                                    "t_pwr": scan.t_pwr}
        return mdata
        
    def __repr__(self):
        str = []
        for scan in self.scans_params:
            str += [f'{scan}']
        return '\n'.join(str)
        
    def _calc(self):
        idx_list = []
        new_len = 0
        scans_params = self.scans_params
        for scan_idx in range(len(scans_params[:-1])):
            scan0 = scans_params[scan_idx]
            scan1 = scans_params[scan_idx + 1]
            idx = np.searchsorted(scan0.scan_pos, scan1.scan_pos[0])
            new_len += idx
            # TODO: check that idx > 0
            idx_list += [(0, idx)]
            
        idx_list += [(0, scans_params[-1].scan_pos.shape[0])]
        new_len += scans_params[-1].scan_pos.shape[0]
        
        scan_pos = np.ndarray(new_len, dtype=np.float64)
        scan_t = np.ndarray(new_len, dtype=np.float64)
        i_start = 0
        i_end = 0
        for scan_idx, scan in enumerate(scans_params):
            i0, i1 = idx_list[scan_idx]
            i_end = i_start + i1
            scan_pos[i_start:i_end] = scan.scan_pos[i0:i1]
            scan_t[i_start:i_end] = scan.scan_t[i0:i1]
            i_start += i1

        self._scan_pos = scan_pos
        self._scan_t = scan_t

    def _calc_cont(self, seg_t_s_max=2, vel_ev_s=0.1, ev_start=None, ev_end=None):
        scan_tm = []
        scan_pos = []
        n_points = 0

        if ev_start is None:
            ev_start = self.scan_pos[0]

        scans_params = self.scans_params
        for scan_idx in range(len(scans_params[:-1])):
            scan0 = scans_params[scan_idx]
            scan1 = scans_params[scan_idx + 1]

            pos, tm = scan0._calc_cont(ev_start=ev_start,
                                       ev_end=scan1.scan_pos[0])

            scan_tm += [tm]
            scan_pos += [pos]
            n_points += len(tm)
            ev_start = pos[-1]

            # print(scan_idx, len(tm), len(pos))

        pos, tm = self.scans_params[-1]._calc_cont(ev_start=ev_start)
        scan_tm += [tm]
        scan_pos += [pos]
        n_points += len(tm)

        tm_arr = np.empty((n_points,))
        pos_arr = np.empty((n_points,))

        # print(np.sum([a.shape[0] for a in scan_pos]))
        # print(np.sum([a.shape[0] for a in scan_tm]))
        
        idx = 0
        # print(n_points)
        for pos, tm in zip(scan_pos, scan_tm):
        #     print(pos[0:2], tm[0:2])
        #     print(pos[-2:-1], tm[-2:-1])
        #     print(idx, tm.shape, pos.shape)
            pos_arr[idx:idx+pos.shape[0]] = pos
            tm_arr[idx:idx+tm.shape[0]] = tm
            idx += pos.shape[0]
        return pos_arr, tm_arr

    # def _calc_cont(self):
    #     idx_list = []
    #     new_len = 0
    #     scans_params = self.scans_params
    #     for scan_idx in range(len(scans_params[:-1])):
    #         scan0 = scans_params[scan_idx]
    #         scan1 = scans_params[scan_idx + 1]

    #         if isinstance(scan0, (KScanParams,)):
    #         else:


    #         scan0 = scans_params[scan_idx]
    #         scan1 = scans_params[scan_idx + 1]
    #         idx = np.searchsorted(scan0.scan_pos, scan1.scan_pos[0])
    #         new_len += idx
    #         # TODO: check that idx > 0
    #         idx_list += [(0, idx)]
            
    #     idx_list += [(0, scans_params[-1].scan_pos.shape[0])]
    #     new_len += scans_params[-1].scan_pos.shape[0]
        
    #     scan_pos = np.ndarray(new_len, dtype=np.float64)
    #     scan_t = np.ndarray(new_len, dtype=np.float64)
    #     i_start = 0
    #     i_end = 0
    #     for scan_idx, scan in enumerate(scans_params):
    #         i0, i1 = idx_list[scan_idx]
    #         i_end = i_start + i1
    #         scan_pos[i_start:i_end] = scan.scan_pos[i0:i1]
    #         scan_t[i_start:i_end] = scan.scan_t[i0:i1]
    #         i_start += i1

    #     self._scan_pos = scan_pos
    #     self._scan_t = scan_t
