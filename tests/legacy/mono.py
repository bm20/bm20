# from enum import Enum
# from typing import NamedTuple
# from .names import *


# def get_crystal(index):
#     """
#     Returns the D20Crystal corresponding to the given integer index.
#     If index is already a D20Crystal: returns index.
#     If index is a D20Crystals enum, returns its value.
#     """
#     if isinstance(index, (D20Crystal,)):
#         return index
#     if isinstance(index, (D20Crystals,)):
#         return index.value
#     for crystal in D20Crystals:
#         if crystal.value.index == index:
#             return crystal.value
#     raise RuntimeError(f'Unknown crystal index {index}.')


# #####################################################
# #####################################################
# Monochromator
# #####################################################
# #####################################################

# MONO = 'd20pmac'


# class D20Crystal(NamedTuple):
#     index: int
#     mode: str         # DCM or DMM
#     register: str     # PMAC P register with the lateral axis position
#     d_spacing: float
#     label: str
#     chan_out: str     # wago (write to pmac)
#     chan_in: str      # wago (read from pmac)
#     description: str



# class D20Crystals(Enum):
#     DCM1 = D20Crystal(1, 'DCM', 'P951', 1.163742, 'Si311', 'outmode1', 'inmode1', 'Si 311')
#     DCM2 = D20Crystal(2, 'DCM', 'P952', 3.13542, 'Si111', 'outmode2', 'inmode2', 'Si 111')
#     DCM3 = D20Crystal(3, 'DCM', 'P953', 3.13542, 'Si111_30', 'outmode3', 'inmode3', 'Si 111 (30deg)')
#     DMM1 = D20Crystal(4, 'DMM', 'P954', 1.163742, 'ml24', 'outmode4', 'inmode4', 'Multilayer (24.8A)')
#     DMM2 = D20Crystal(5, 'DMM', 'P955', 1.163742, 'ml36', 'outmode5', 'inmode5', 'Multilayer (36.8A)')


# class MonoWChannelsWrite(Enum):
#     ABORT_CHAN = 'abort'
#     HOMELAT_CHAN = 'homelat'
#     HOMEYAW_CHAN = 'homeyaw'


# class MonoWChannelsRead(Enum):
#     YAW_ENABLE_CHAN = 'yawenable'
#     LAT_BRAKE_CHAN = 'lat_brake'
#     ERROR_CHAN = 'error'


# class MonoDmmAxisSimu(Enum):
#     ML2_LONG = 'simu_ml2long'  # mon_z in SPEC
#     ML2_PERP = 'simu_ml2perp'  # mon_y in SPEC
#     ML2_PITCH = 'simu_ml2ptch'
#     ML2_ROLL = 'simu_ml2roll'

# P variables for Lateral & Yaw Homing Offset:
# Lateral Homing Offset
# P217= 2,141,765 (Lateral position in motor steps from Negative limit position)
# P218= 19,683 (Yaw position in motor steps from Negative limit position)
# P variables for Lateral Positions in Different Modes:
# Mode 1 Request – DCM Crystal 1
# P951= 2,000,000 (Lateral position in motor steps from HOME reference point)
# Mode 2 Request – DCM Crystal 2
# P952= 0 (Lateral position in motor steps from HOME reference point)
# Mode 3 Request – DCM Crystal 3
# P953= -2,000,000 (Lateral position in motor steps from HOME reference point)
# Mode 4 Request – DMM Mirror 1
# P954= 2,000,000 (Lateral position in motor steps from HOME reference point)
# Mode 5 Request – DMM Mirror 2
# P955= 250,000 (Lateral position in motor steps from HOME reference point)
# P variables for Yaw axis operation window on Crystal 2
# DCM Crystal 2 upper limit
# P957= 250,050 (Lateral position in motor steps from HOME reference point)
# DCM Crystal 2 lower limit
# P958= -250,050 (Lateral position in motor steps from HOME reference point)