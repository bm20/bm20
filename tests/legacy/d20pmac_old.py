

#     def initialize(self):
#         super(D20Pmac, self).initialize()
#         self._mono_modes = mono_modes()

#     def initialize_hardware(self):
#         super(D20Pmac, self).initialize_hardware()
#         for mode_info in self._mono_modes.values():
#             lat_pos = self.pmac_comm.sendline_getbuffer(mode_info.p_register)
#             mode_info.lat_position = float(lat_pos)

#     def mono_modes(self):
#         return self._mono_modes.copy()

#     def mono_mode_lat_position(self, mode):
#         return self._mono_modes[mode].lat_position

#     def get_active_dcm_channels(self):
#         """
#         Returns the value of the wago channels linked to the crystal selection
#         """
#         return [address
#                 for address, mode in self._mono_modes.items()
#                 # if self._wago.get(mode.w_channel)]
#                 if self._wago.get(mode.r_channel)]

#     def read_dcm_mode(self, raise_ex=True):
#         """
#         Returns the current DCM mode.
        
#         Args:
#             raise_ex (bool): if True, raises an exception
#                              if no mode (or several) is set.
#                              if False, returns None on error.
#         """
#         active_chans = self.get_active_dcm_channels()
#         if len(active_chans) > 1:
#             if raise_ex:
#                 raise RuntimeError(f'Error, more than one active channels set to 1'
#                                 f' on {self._wago.name}.')
#             else:
#                 return None
#         if len(active_chans) == 0:
#             if raise_ex:
#                 raise RuntimeError(f'Error, no active channel set '
#                                     f' on {self._wago.name} '
#                                     f'(Try changing the mode. Was the DCM homed?).')
#             else:
#                 return None
#         return active_chans[0]

#     def write_dcm_mode(self, mode):
#         try:
#             mode_info = self._mono_modes[mode]
#         except KeyError:
#             raise ValueError(f'Unknown DCM mode : {mode}.')
#         self.clear_wago()
#         self._wago.set(mode_info.w_channel, 1)

#     def clear_wago(self):
#         for mode in self._mono_modes.values():
#             self._wago.set(mode.w_channel, 0)
#         self._wago.set(MonoWagoChannels.ABORT_CHAN.value, 0)
#         self._wago.set(MonoWagoChannels.HOMELAT_CHAN.value, 0)
#         self._wago.set(MonoWagoChannels.HOMEYAW_CHAN.value, 0)

#     def pmac_error(self, raise_ex=False):
#         error = self._wago.get('error')
#         if error and raise_ex:
#             raise RuntimeError(f'ERROR: the PMAC set the wago error '
#                                        'channel ({self._wago.name}:error), '
#                                        'please investigate.')
#         return error

#     def is_lateral_homed(self):
#         return int(self.pmac_comm.sendline_getbuffer(_LATERAL_HOMED_REG)) == 1

#     def is_yaw_homed(self):
#         return int(self.pmac_comm.sendline_getbuffer(_YAW_HOMED_REG)) == 1

#     def home_lateral(self, force=False):
#         """
#         Starts the homing procedure.

#         Will not do anything if the axis is already homed,
#         unless force=True
#         """
#         self._pmac_home_vessel(MonoVesselAxis.LATERAL.value,
#                                MonoWagoChannels.HOMELAT_CHAN.value,
#                                 _LATERAL_HOMED_REG,
#                                 force=force)

#         if self.is_lateral_homed():
#             print('SUCCESS: LATERAL homing successful.')
#         else:
#             print('ERROR: LATERAL homing not complete.')

#     def home_yaw(self, force=False):
#         """
#         Starts the yaw homing procedure.

#         Will not do anything if the axis is already homed,
#         unless force=True
#         """
#         # This check is supposed to be done by the PLC, but adding a little
#         # redundancy doesnt hurt.
#         if not self.is_lateral_homed():
#             print('ERROR: LATERAL axis must be homed before the yaw axis '
#                   'homing can be done.')
#             return

#         self._pmac_home_vessel(MonoVesselAxis.YAW.value,
#                                MonoWagoChannels.HOMEYAW_CHAN.value,
#                                _YAW_HOMED_REG,
#                                force=force)

#         if self.is_yaw_homed():
#             print('SUCCESS: YAW homing complete.')
#         else:
#             print('ERROR: YAW homing not complete.')

#     def _pmac_home_vessel(self, axis_name, wago_chan, reg_address, force=False):
#         """
#         LATERAL and YAW common homing procedure.
#         Will not do anything if the axis is already homed, unless force=True
#         """
#         homed = int(self.pmac_comm.sendline_getbuffer(reg_address))
#         if homed and not force:
#             log_warning(self, f'{axis_name} axis is already homed. Set the '
#                               f'"force" keyword to True to force the homing.')
#             return
#         axis = self.get_axis(axis_name)

#         pre_position = axis.position

#         try:
#             print(f'Homing {axis_name} axis.')
#             print('Clearing Wago channels.')
#             self.clear_wago()

#             print('Sending home request.')
#             self._wago.set(wago_chan, 1)

#             post_position = axis.position

#             gevent.sleep(2)

#             self.pmac_error(raise_ex=True)

#             homed = int(self.pmac_comm.sendline_getbuffer(reg_address))

#             timeout = monotonic() + _HOMING_TIMEOUT

#             while homed == 0:
#                 print(f'Homing {axis_name} ({post_position:13.2f}, homed={homed})', end='\r.')
#                 if pre_position == post_position:
#                     if monotonic() >= timeout:
#                         raise RuntimeError('Homing timeout reached, it seems '
#                                            'that the axis is not moving?')
#                 else:
#                     timeout = monotonic() + _HOMING_TIMEOUT
#                 gevent.sleep(0.5)
#                 homed = int(self.pmac_comm.sendline_getbuffer(reg_address))
#                 pre_position = post_position
#                 post_position = axis.position

#                 self.pmac_error(raise_ex=True)

#             print('')
#             print(f'INFO: Lateral homing completed ({post_position}, homed={homed})')

#         except KeyboardInterrupt:
#             print('WARNING: aborting.')
#             self.dcm_abort()
#             print('WARNING: homing interrupted by user.')
#         except Exception as ex:
#             print(ex)
#             log_error(self, 'Homing aborted.')
#             self.dcm_abort()
#         finally:
#             self._wago.set('homelat', 0)

#     def home_cage_motors(self):
#         motors_names = [elem.value for elem in MonoCageAxis]
#         self._pmac_home_axis(*motors_names)

#     def _pmac_home_axis(self, *args):
#         """
#         args: axis names or axis objects
#         Homing procedure for the axis 1, 3, 4, 5, 6 (i.e: all but lat and yaw)
#         """
#         homed_complete = False
#         self.dcm_abort(clear=True)
#         self.pmac_error(raise_ex=True)

#         self.clear_wago()

#         axis_list = [self.get_axis(axis)
#             if not isinstance(axis, (Axis,)) else axis
#             for axis in args]
#         axis_names = [axis.name for axis in axis_list]

#         limit_reached = False

#         def _home_abort(*args, **kwargs):
#             lprint('WARNING! Aborting the homing procedure.')
#             self.dcm_abort()
#             lprint('Aborted')
#             self.pmac_stop_all()
#             lprint('Stopped all')
#             gevent.sleep(1)
#             lprint('Sync')
#             for axis in axis_list:
#                 axis.sync_hard()
#             lprint('Abort done.')

#         t0 = monotonic()
#         lprint('Moving to lim-')

#         # with error_cleanup(*axis_list, _home_abort):
#         try:
#             for axis in axis_list:
#                 # FIXME
#                 # workaround a bug where state/position is not updated when
#                 # running a home.
#                 # https://gitlab.esrf.fr/bliss/bliss/issues/1359
#                 axis.sync_hard()
#                 if 'LIMNEG' not in axis.state:
#                     axis.hw_limit(-1, wait=False)
#                 else:
#                     lprint(f'Axis {axis.name} is already at lim-.')

#             gevent.sleep(1)

#             self.pmac_error(raise_ex=True)

#             print('============ LIM- ============')
#             print('time\t' + '\t'.join(axis_names))

#             limit_reached = False
#             while not limit_reached:
#                 txt = '\t'.join([f'{axis.position:.4f}'
#                                    for axis in axis_list])
#                 m, s = divmod(monotonic() - t0, 60)
#                 print(f'{int(m):02d}:{int(s):02d}\t\t{txt}', end='\r')

#                 limit_reached = all(['LIMNEG' in axis.state
#                                      for axis in axis_list])

#                 # if pre_position == post_position:
#                 #     if monotonic() >= timeout:
#                 #         raise RuntimeError('Homing timeout reached, it seems '
#                 #                            'that the axis is not moving?')
#                 # else:
#                 #     timeout = monotonic() + _HOMING_TIMEOUT
#                 gevent.sleep(0.5)
#                 # homed = int(self.pmac_comm.sendline_getbuffer(reg_address))
#                 # pre_position = post_position
#                 # post_position = axis.position

#                 self.pmac_error(raise_ex=True)
#         except Exception as ex:
#             print('############################', ex)
#             _home_abort()
#             print('######')
#             return

#         lprint('Searching for home+')

#         if not limit_reached:
#             log_error(self, 'Homing not completed.')

#         # with error_cleanup(*axis_list, _home_abort):
#         try:
#             for axis in axis_list:
#                 axis.home(1, wait=False)

#             gevent.sleep(1)

#             self.pmac_error(raise_ex=True)

#             print('============ HOME+ ============')
#             print('time\t' + '\t'.join(axis_names))

#             home_reached = False
#             while not home_reached:
#                 # FIXME
#                 # workaround a bug where state/position is not updated when
#                 # running a home.
#                 # https://gitlab.esrf.fr/bliss/bliss/issues/1359
#                 for axis in axis_list:
#                     axis.sync_hard()

#                 txt = '\t'.join([f'{axis.position:.4f}' for axis in axis_list])
#                 m, s = divmod(monotonic() - t0, 60)
#                 print(f'{int(m):02d}:{int(s):02d}\t\t{txt}', end='\r')

#                 home_reached = all(self.pmac_home_states(*axis_names).values())

#                 gevent.sleep(0.5)

#                 self.pmac_error(raise_ex=True)

#             home_complete = True
#         except Exception as ex:
#             print('############################', ex)
#             _home_abort()
#             print('######')
#             return

#         # pmac needs some time to settle down
#         print('sleeping 2', home_complete)
#         gevent.sleep(2)
#         print('slept')

#         if home_complete:
#             # FIXME: this is just to keep compatibility with the values
#             # stored by the SPEC macro "save/restore optics"
#             # maybe this could eventually be removed once everything is properly
#             # tested (adjust the values)
#             for axis in axis_list:
#                 # FIXME
#                 # workaround a bug where state/position is not updated when
#                 # running a home.
#                 # https://gitlab.esrf.fr/bliss/bliss/issues/1359
#                 axis.sync_hard()
#                 home_position = axis.config.get('home_position', default=0)
#                 lprint(f'Setting axis {axis.name} offset to '
#                        f'{home_position}.')
#                 axis.position = home_position

#             if limit_reached and home_reached:
#                 lprint('Homing done.')
#             else:
#                 lprint('Error! Homing not complete.')

#             # FIXME
#             # workaround a bug where state/position is not updated when
#             # running a home.
#             # https://gitlab.esrf.fr/bliss/bliss/issues/1359
#             for axis in axis_list:
#                 axis.sync_hard()
#         else:
#             log_error(self, 'Homing did not complete.')

#         print('out')
