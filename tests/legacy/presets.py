
from bliss.config.static import get_config
from bliss.scanning.scan import ScanPreset

from bm20.config.names import OPIOM_EH1, MONO


class D20Preset(ScanPreset):
    def __init__(self, is_fscan=False):
        super().__init__()
        self._is_fscan = is_fscan
        self._opiom = get_config().get(OPIOM_EH1)
        self._mono = get_config().get(MONO)

    def prepare(self, scan):
        if self._is_fscan:
            self._opiom.switch('SEL_TRIG', 'ZAP')
        else:
            self._opiom.switch('SEL_TRIG', 'STEP')

    def start(self, scan):
#         print(f"Starting scan {scan.name}")
#         print(f"Opening the shutter")

    def stop(self, scan):
        self._opiom.switch('SEL_TRIG', 'STEP')
        # making sure the xtal2perp motor is in open loop
        # because it is in vacuum but not cooled.
        # TODO: add others?
        if not self._mono.pmac_comm.is_open_loop(4):
            self._mono.pmac_comm.open_loop(4)


# class Xes5Preset(ScanPreset):
#     def prepare(self, scan):
#         print(f"Preparing scan 2 {scan.name}\n")

#     def start(self, scan):
#         print(f"Starting scan {scan.name}")
#         print(f"Opening the shutter")

#     def stop(self, scan):
#         print(f"{scan.name} scan is stopped")
#         print(f"Closing the shutter")