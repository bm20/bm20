"""
TurboPmac for the BM20 beamline
"""

from enum import Enum
import re
import gevent

from bliss.common.standard import sync
from bliss.common.logtools import log_warning
from bliss.controllers.motors.turbopmac import TurboPmac, _PMAC_MAX_BUFFER_SIZE

from bm20.config.names import LATERAL, YAW


# lateral and yaw home flags. Set to one by their respective PLC
# after homing.
LATERAL_HOMED_REG = 'M745'
YAW_HOMED_REG = 'M845'


class MonoHomeOffsetRegisters(Enum):
    LATERAL = 'P217'
    YAW = 'P218'


class D20Pmac(TurboPmac):
    """
    BM20 Pmac controller.
    - overrides the HOMED state for lateral and yaw motors:
        ignores the HOMED motor status flag,
        only takes into account the register set by the homing PLC running
        on the PMAC (started by setting the homelat or homeyaw channel on
        the wago box.)
    """

    wago = property(lambda self: self._wago)

    def initialize(self):
        self._wago = self.config.get("wago")
        return super().initialize()
    
    def state(self, axis):
        state = super().state(axis)

        # The PMAC sets the HOMED axis flag before the end of the homing PLC.
        # The PLC sets a register to 1 (see .is_XXX_homed methods) when its done.
        if axis.name == LATERAL:
            if self.is_lateral_homed():
                state.set('HOMED')
            else:
                try:
                    state.unset('HOMED')
                except ValueError:
                    pass

        elif axis.name == YAW:
            if self.is_yaw_homed():
                state.set('HOMED')
            else:
                try:
                    state.unset('HOMED')
                except ValueError:
                    pass
        
        return state

    def is_lateral_homed(self):
        """
        Reads the "homed" pmac register set by the homing PLC.
        """
        return int(self.raw_write_read(LATERAL_HOMED_REG)) == 1

    def is_yaw_homed(self):
        """
        Reads the "homed" pmac register set by the homing PLC.
        """
        return int(self.raw_write_read(YAW_HOMED_REG)) == 1
    
    def abort_mono(self):
        """
        Aborts all operations on the mono.
        Writes to the wago "abort" channel.
        """
        self._wago.abort()
        log_warning(self, 'Aborting!')
        print(f'Abort!')
        gevent.sleep(1)
        sync()

    # def _write_multiline(self, data, write=False):
    #     # print("\n".join(data.split("\r")))
    #     multilines = []
    #     for line in re.split("\r|\n", data):
    #         match = re.match("^([^;]+)", line.lstrip())
    #         if match:
    #             multilines += [match.groups()[0].rstrip()]
    #     if not write:
    #         print("The following data would be sent to the PMAC:")
    #         print("================================")
    #         print(multilines)
    #         print("================================")
    #     else:
    #         for line in multilines:
    #             if len(line) > _PMAC_MAX_BUFFER_SIZE:
    #                 raise NotImplementedError(
    #                     f"Data of more than {_PMAC_MAX_BUFFER_SIZE} chars not supported yet."
    #                 )
    #             self.raw_write_read(line)

    # def raw_write(self, com):
    #     self.pmac_comm.sendline(com)

    # def raw_write_read(self, com):
    #     return self.pmac_comm.sendline_getbuffer(com)