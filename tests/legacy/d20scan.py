import datetime
import tabulate
from collections import OrderedDict()

from bliss.config import settings
from bliss.config.static import get_config
from bliss.shell.standard import newproposal, newcollection, newdataset, ascan
from bliss.scanning.toolbox import get_active_mg
from bliss import setup_globals

from bm20.config.names import *


class D20ScanRunner:
    # proposal = property(lambda self: self._proposal)

    def __init__(self, config):#, proposal=None, collection_name=None):
        # TODO: make sure there's an active MG ready
        self._scans = OrderedDict()

        # config = get_config()
        # self._proposal = proposal
        # self._energy = config.get(ENERGY)
        # setup_globals.SCAN_SAVING
        # if collection_name is None:
        #     collection_name = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
        # newcollection(collection_name)

    def __info__(self):
        ss = setup_globals.SCAN_SAVING
        # info = " XES PARAMS:\n"
        info = [["Proposal", ":", f"{ss.proposal_name}"],
               ["Collection", ":", f"{ss.collection_name}"],
               ["Dataset", ":", f"{ss.dataset_name}"],
               ["Current file", ":", f"{ss.icat_data_fullpath}"]]
        return tabulate.tabulate(info)
    
    def _init_scan(self, *args, **kwargs):
        kwargs['run'] = False
        return ascan(*args, **kwargs)

    def __call__(self, *args, name=None, run=True, append=False, collection=None, dataset=None, comment=None, **kwargs):
        scan = self._init_scan(*args, **kwargs)
        i = 1
        new_name = name
        while new_name in self._scans:
            new_name = f"{name}_{i}"
            i+=1
        self._scans[new_name] = {"scan": scan, "name":name, "collection":collection, "dataset":dataset, "comment":comment}
        if run:
            self._run_scan(new_name)

    def _run_scan(self, name, run=True, append=False, collection=None, dataset=None, comment=None):

    def run_all(self):
        pass

    def run_next(self):
        pass

    # def scan(self,
    #          e_start_ev,
    #          e_end_ev,
    #          n_steps,
    #          acq_time_s,
    #          collection_name,
    #          dataset_name=None,
    #          repeat=1,
    #          run=True):


class D20Scan:
    def __init__(self, config):
        pass

