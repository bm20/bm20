"""
Monochromator controller for the BM20 beamline.
Axis: mode
"""

import re
import enum
import socket
import struct


from bliss import global_map
from bliss.config.static import get_config
from bliss.common import session
from bliss.common.logtools import *
from bliss.comm.util import get_comm
from bliss.common.axis import AxisState
from bliss.controllers.motor import Controller, CalcController
from bliss.config.static import get_config


# from ..config.groups import MonoWagoChannels
from .oh_objects import bm20_energy


class D20Mono(Controller):

    _monomode = property(lambda self: self._tagged['monomode'][0])
    # _dcm2perp_offset = property(lambda self: self._tagged['dcm2perp_ofst'][0])
    # _wago = property(lambda self: get_config().get(self.config.get('wago')))
    # _monolat = property(lambda self:
    #                     get_config().get(self._pmac.get_axis('monolat')))

    _pmac = property(lambda self: get_config().get(self.config.get('pmac')))

    def __init__(self, *args, **kwargs):
        super(D20Mono, self).__init__(*args, **kwargs)

        # self._wago = None
        self._mono_status = AxisState()
        self._mode_move = None
        self.__dcm2perp = None
        self.__energy_axis = None

    @property
    def _dcm2perp(self):
        if not self.__dcm2perp:
            self.__dcm2perp = get_config().get('dcm2perp')
        return self.__dcm2perp

    def _monolat(self):
        return self._pmac.get_axis('monolat')

    def _energy(self):
        if self.__energy_axis is None:
            self.__energy_axis = bm20_energy()
        return self.__energy_axis

    def initialize(self):
        # reading the config to get the defined modes.
        super(D20Mono, self).initialize_hardware()
        self._mono_modes = self._pmac.mono_modes()
        log_debug(self, f'Initialized {len(self._mono_modes)} modes.')

    def initialize_axis(self, axis):
        pass

    def state(self, axis):
        state = self._mono_status.new()
        # if axis.name == self._dcm2perp_offset.name:
        #     return axis.state()
        if axis.name == self._monomode.name:
            lat_state = self._monolat().state
            # the mono state depends mostly on the monolat state, and the error
            # wago channel
            # it is only move via the wago box

            if 'FAULT' in lat_state:
                # error = self._wago.get(ERROR_CHAN)
                # if error:
                #     log_debug(self, f'MonoCrystal: {axis.name} ERROR_CHAN={error}.')
                #     state.set('FAULT')
                state.set('FAULT')
            else:
                channels = self._pmac.get_active_dcm_channels()
                n_set = len(channels)
                if n_set > 1:
                    log_error(self, f'MonoCrystal: {axis.name} multiple '
                                    f'channels set. {channels}.')
                    state.set('FAULT')
                else:
                    state = lat_state
        return state

    def read_position(self, axis):
        # if axis.name == self._dcm2perp_offset.name:
        #     dcm2perp_pos = self._dcm2perp.position - 
        #     return 666
        if axis.name == self._monomode.name:
            if 'MOVING' in self._monolat().state and self._mode_move is not None:
                start_mode, target_mode, start_pos, end_pos = self._mode_move
                if target_mode == start_mode:
                    position = target_mode
                else:
                    position = start_mode + ((target_mode - start_mode) * (self._monolat().position - start_pos) / (end_pos - start_pos))
            else:
                channels = self._pmac.get_active_dcm_channels()
                if len(channels) > 1:
                    raise RuntimeError('More than one mode selected, '
                                    'there should be only one.')
                if len(channels) == 0:
                    return 0
                    # raise RuntimeError('Crystal mode is not set yet.'
                    #                    ' Did you home the mono?')
                position = channels[0]
        else:
            position = super(D20Mono, self).read_position(axis)
        return position

    def d_spacing(self):
        mode_info = self._mono_modes.get(self._monomode.position)
        if mode_info:
            return mode_info.d_spacing
        return None

    def _update_dspacing(self, mode=None):
        if mode is None:
            mode = self._monomode.position
        try:
            mode_info = self._mono_modes[mode]
            energy_axis = self._energy()
            energy_axis.settings.set('dspace', mode_info.d_spacing)
            energy_axis.settings.set('low_limit', mode_info.low_limit)
            energy_axis.settings.set('high_limit', mode_info.high_limit)
        except KeyError:
            log_error(self,
                        f'Failed to set d_spacing (mode={mode}).')
        # d_spacing = self._energy.settings.get('dspace')
        # lprint(f'======> INFO : Setting energy axis d-spacing to {d_spacing} <======')

    def start_one(self, motion):
        axis = motion.axis
        # if axis.name == self._dcm2perp_offset.name:
        #     print('BLA')
        #     return 666
        if axis.name == self._monomode.name:

            target_mode = motion.target_pos
            if target_mode not in self._mono_modes.keys():
                raise ValueError(f'Crystal position {target_mode} must be one the defined '
                                f'addresses: {list(self._mono_modes.keys())}')

            log_debug(self, f'Request to move crystal to {motion.target_pos}')
            if not motion.type == 'move':
                raise ValueError(f'Unsupported motion type: {motion.type}.')

            if target_mode is None:
                raise ValueError('Only absolute move is allowed.')

            mode_info = self._mono_modes[target_mode]
            start_mode = axis.position
            pmac_comm = self._pmac.pmac_comm
            end_pos = float(pmac_comm.sendline_getbuffer(mode_info.p_register))
            start_pos = self._monolat().position

            self._mode_move = [start_mode, target_mode, start_pos, end_pos]

            mode_info = self._mono_modes[target_mode]

            # self._pmac.clear_wago()
            # self._wago.set(mode_info.w_channel, 1)
            self._pmac.write_dcm_mode(target_mode)#mode_info.w_channel)

        else:
            super(D20Mono, self).start_one(axis)

    def stop(self, axis):
        # send abort
        print('STOP', axis.name)
        # if axis.name == self._dcm2perp_offset.name:
        #     print('BLA')
        if axis.name == self._monomode.name:
            self._pmac.dcm_abort()
        else:
            super(D20Mono, self).stop(axis)
