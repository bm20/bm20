from .oh_objects import BM20_OH_SLITS
from ..config.groups import Hutch, OHMotorGroup
from ..common.slits import (_bm20_home_slits,
                            _bm20_slits_show,
                            _bm20_slits_status_object)


__all__ = ['show_oh_slits', 'status_oh_slits']



def home_oh_slits(*slit_names):
    """
    Home OH slits.

    Args:
        slit_names: optional, a list of slit names (s5, ...)
    """
    valid_names = BM20_OH_SLITS

    if not slit_names:
        slit_names = BM20_OH_SLITS

    hutch = Hutch.OH
    motor_group = OHMotorGroup.SLITS

    return _bm20_home_slits(slit_names, valid_names, hutch, motor_group)


show_oh_slits = _bm20_slits_show(*BM20_OH_SLITS)


status_oh_slits = _bm20_slits_status_object(*BM20_OH_SLITS, title='OH slits')
