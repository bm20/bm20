from math import radians, cos


g_mo_s = 18.

def bragg_to_perp(angle, xtal_ofst=g_mo_s):
    return xtal_ofst / (2 * cos(radians(angle)))
