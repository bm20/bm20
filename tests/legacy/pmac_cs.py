"""
Coordinate system calculations for the PMAC.
Tooltip: Bragg
Joints: Bragg and Xtal2Perp

Doc: Turbo PMAC user manual, setting up a coordinate system. p.255.
"""

# ========================================
# virtual axis is in eV
# ========================================

# joints to tooltip
# Bragg and Perp to Energy in eV
# actually only using Bragg
# wl = 2 * dspace * np.sin(np.radians(bragg))
# energy = 1000 * HC_OVER_E / wl
FORWARD_E2B = \
"""
; Setup for program
I15=0                       ; Trig calculations in degrees
&16                         ; Address CS 16
M145->Y:$0000C0,10,1        ; Motor 1 (Bragg) home complete bit
M445->Y:$000240,10,1        ; Motor 4 (X2Perp) home complete bit
M6682->Y:$002F3F,22,1       ; CS 16 run-time error bit
Q91=200000                  ; Counts per degree for Bragg
Q92=1000*12.39842/(2*3.13542) ; 1000 * HC_OVER_E / (2 * dspace)

&16 OPEN FORWARD             ; Forward kinematics for CS 16
CLEAR                       ; Erase existing contents
IF (M145=1 AND M445=1)      ; Properly position referenced?
Q7=Q92/SIN(P1/Q91)          ; X=Energy in eV
ELSE                        ; Not valid; halt operation
M6682=1                     ; Set run-time error bit
ENDIF
CLOSE
"""

# tooltip to joints
# energy in eV to Bragg/Perp
# np.degrees(np.arcsin(1000 * HC_OVER_E / (energy * 2 * dspace)))
# xtal_ofst / (2 * np.cos(np.radians(angle)))
INVERSE_E2B = \
"""
; Setup for program
I15=0                       ; Trig calculations in degrees
&16                         ; Address CS 16
#1->I
#4->I

M145->Y:$0000C0,10,1        ; Motor 1 (Bragg) home complete bit
M445->Y:$000240,10,1        ; Motor 4 (X2Perp) home complete bit
M6682->Y:$002F3F,22,1       ; CS 16 run-time error bit
Q91=200000                  ; Counts per degree for Bragg
Q93=20000                  ; Counts per degree for xtal2perp
Q92=1000*12.39842/(2*3.13542) ; 1000 * HC_OVER_E / (2 * dspace)

&16 OPEN INVERSE             ; Inverse kinematics for CS 16
CLEAR                       ; Erase existing contents
Q96=M145
Q97=M445
Q98=12
IF (M145=1 AND M445=1)      ; Properly position referenced?
Q21=ASIN(Q92/Q7)
P1=Q91*ASIN(Q92/Q7)               ; X=Energy in eV
# P1=Q91*Q21
Q94=P1
Q95=18/(2*COS(Q21))
P4=Q93*18/(2*COS(Q21))
ELSE                        ; Not valid; halt operation
M6682=1                     ; Set run-time error bit
ENDIF
CLOSE
"""


# ========================================
# virtual axis is in bragg units (degrees)
# ========================================

# joints to tooltip
# actually only using Bragg
FORWARD = \
"""
; Setup for program
I15=0                       ; Trig calculations in degrees
&16                         ; Address CS 16
M145->Y:$0000C0,10,1        ; Motor 1 (Bragg) home complete bit
M445->Y:$000240,10,1        ; Motor 4 (X2Perp) home complete bit
M6682->Y:$002F3F,22,1       ; CS 16 run-time error bit
Q91=200000                  ; Counts per degree for Bragg

&16 OPEN FORWARD             ; Forward kinematics for CS 16
CLEAR                       ; Erase existing contents
IF (M145=1 AND M445=1)      ; Properly position referenced?
Q7=P1 ;/Q91
ELSE                        ; Not valid; halt operation
M6682=1                     ; Set run-time error bit
ENDIF
;CMD"#1J/"
;CMD"#4J/"
CLOSE
"""

# tooltip to joints
# xtal2perp = 18.0 / (2 * np.cos(np.radians(angle))) ; 18.0 is the crystal offset
#           = 9.0/ cos(rad(bragg))
INVERSE = \
"""
; Setup for program
I15=0                       ; Trig calculations in degrees
&16                         ; Address CS 16
#1->I
; #4->I

M145->Y:$0000C0,10,1        ; Motor 1 (Bragg) home complete bit
M445->Y:$000240,10,1        ; Motor 4 (X2Perp) home complete bit
M6682->Y:$002F3F,22,1       ; CS 16 run-time error bit
Q91=200000                  ; Counts per degree for Bragg
Q93=20000                  ; Counts per degree for xtal2perp

&16 OPEN INVERSE             ; Inverse kinematics for CS 16
CLEAR                       ; Erase existing contents
IF (M145=1 AND M445=1)      ; Properly position referenced?
P1=Q7  ;*Q91
; P101=70000 ;Q17
P4=Q93*18/(2*COS(Q7/Q91))
ELSE                        ; Not valid; halt operation
M6682=1                     ; Set run-time error bit
ENDIF
CLOSE
"""


TRAJ = \
"""

"""

CLEAR = \
"""
CLOSE
&16 OPEN FORWARD             ; Inverse kinematics for CS 16
CLEAR                       ; Erase existing contents
CLOSE
&16 OPEN INVERSE             ; Inverse kinematics for CS 16
CLEAR                       ; Erase existing contents
CLOSE
"""


# def pmac_trajectory(nrg_start, nrg_stop):
    

# # joints to tooltip
# # Bragg and Perp to Energy in eV
# # actually only using Bragg
# # wl = 2 * dspace * np.sin(np.radians(bragg))
# # energy = 1000 * HC_OVER_E / wl
# FORWARD = \
# """
# ; Setup for program
# I15=0
# &16
# &16 OPEN FORWARD
# CLEAR
# Q7=P1
# CLOSE
# """

# # tooltip to joints
# # energy in eV to Bragg/Perp
# # np.degrees(np.arcsin(1000 * HC_OVER_E / (energy * 2 * dspace)))
# INVERSE = \
# """
# I15=0
# &16
# #1->I
# &16 OPEN INVERSE
# CLEAR
# P1=Q7
# CLOSE
# """


# F = 1
# 63822  4446
# 63711  4540
# 91 94s

# F = 100
# 63703  4820
# 61186  4845

# 2517   25s
#  user length units per user time units
# PMAC_DEV [98]: pmacd20.pmac_comm.sendline_getbuffer('I6690')
# Out [98]: '1000'
# 100 units per 1000ms?


# PMAC_DEV [48]: pmacd20.pmac_comm.sendline_getbuffer('i6650=1')
#      Out [48]: []


FORWARD_E2B2 = \
"""
; Setup for program
I15=0                       ; Trig calculations in degrees
&16                         ; Address CS 16
M145->Y:$0000C0,10,1        ; Motor 1 (Bragg) home complete bit
M445->Y:$000240,10,1        ; Motor 4 (X2Perp) home complete bit
M6682->Y:$002F3F,22,1       ; CS 16 run-time error bit
Q91=200000                  ; Counts per degree for Bragg
Q92=1000*12.39842/(2*3.13542) ; 1000 * HC_OVER_E / (2 * dspace)
&16 OPEN FORWARD             ; Forward kinematics for CS 16
CLEAR                       ; Erase existing contents
Q103=0
IF (M145=1 AND M445=1)      ; Properly position referenced?
; Q7=Q92/SIN(P1/Q91)          ; X=Energy in eV
Q7=9
Q103=1111
ELSE                        ; Not valid; halt operation
M6682=1                     ; Set run-time error bit
Q103=2222
ENDIF
CLOSE
"""

# tooltip to joints
# energy in eV to Bragg/Perp
# np.degrees(np.arcsin(1000 * HC_OVER_E / (energy * 2 * dspace)))
# xtal_ofst / (2 * np.cos(np.radians(angle)))
INVERSE_E2B2 = \
"""
; Setup for program
I15=0                       ; Trig calculations in degrees
&16                         ; Address CS 16
; #1->I
#4->I
Q102=3333
&16 OPEN INVERSE             ; Forward kinematics for CS 16
CLEAR     
Q105=0
Q102=4444
CLOSE
"""