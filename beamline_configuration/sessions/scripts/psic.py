
def tth_eta_ascan(tth_start, tth_end, npts=None, count_time=1, step_size=None, eta_offset=0):
    """ 
    theta-2theta absolute scan, 
    step_size is for tth,
    npts takes priority over step_size
    """
    if npts == None and step_size == None:
        print('Need either npts or step_size!')
        return
    if npts == None:
        npts = int((tth_end-tth_start-(step_size/2))//step_size) + 1
        print('Using',npts,'points in scan')
    a2scan(tth, tth_start, tth_end, eta, (tth_start/2)+eta_offset, (tth_end/2)+eta_offset, npts, count_time)

import os

def tth_eta_dscan(tth_start, tth_end, npts=None, count_time=1, step_size=None, eta_offset=None):
    """ 
    theta-2theta relative scan, 
    step_size is for tth,
    npts takes priority over step_size
    eta_offset is caluclated from current position if not defined
    """
    if eta_offset == None: eta_offset = eta.position - (tth.position/2)
    tth_initial, eta_initial = tth.position, eta.position
    try:
        tth_eta_ascan(tth.position+tth_start, tth.position+tth_end, npts=npts,
                  count_time=count_time, step_size=step_size, eta_offset=eta_offset)
        umv(tth, tth_initial, eta, eta_initial)
    except:
        umv(tth, tth_initial, eta, eta_initial)       
                  
	
def tth_eta_umv(tthpos, eta_offset=0):
    umv(tth, tthpos, eta, (tthpos/2)+eta_offset)
    
def tth_eta_umvr(tthpos, eta_offset=0):
    umvr(tth, tthpos, eta, (tthpos/2)+eta_offset)

def get_eta_offset():
    return eta.position - (tth.position/2)

def set_eiger_energy(energy_in_eV):
    """sets the eiger500k energy, should be integer in eV"""
    if energy_in_eV < 4000 or energy_in_eV > 80000:
        print("Energy must be between 4000 and 80000")
        return
    en_string = "curl -X PUT -H \"Content-Type: application/json\" -d \'{\"value\":%i}\' http://d20pilatus500k.esrf.fr/detector/api/1.8.0/config/photon_energy"%energy_in_eV
    print(en_string)
    os.system(en_string)
    print(f"Eiger500k energy set to {energy_in_eV}")

def get_eiger_energy():
    print("curl -X GET -H \"Content-Type: application/json\" -d \'value\' http://d20pilatus500k.esrf.fr/detector/api/1.8.0/config/photon_energy")

def set_eiger_countingmode(mode='normal'):
    """sets the eiger500k counting mode to 'normal' or 'retrigger'"""
    if mode == 'normal':
        m_string = "curl -X PUT -H \"Content-Type: application/json\" -d \'{\"value\":\"normal\"}\' http://d20pilatus500k.esrf.fr/detector/api/1.8.0/config/counting_mode"
        print("Setting eiger 500k counting mode to normal")
        os.system(m_string)
        
    elif mode == 'retrigger':
        m_string = "curl -X PUT -H \"Content-Type: application/json\" -d \'{\"value\":\"retrigger\"}\' http://d20pilatus500k.esrf.fr/detector/api/1.8.0/config/counting_mode"
        print("Setting eiger 500k coutning mode to retrigger")
        os.system(m_string)
        
    else:
        print("Did not understand the mode...")


