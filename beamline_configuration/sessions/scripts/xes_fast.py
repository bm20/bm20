import numpy as np
import time

def run_test_scans():
    samplerate = 0.0002
    start = 3500
    stop = 3900
    for stepsize in [0.01,0.02,0.03,0.05]:
        d20fs._fscan(start, stop, stepsize, samplerate)
        d20fs._fscan(stop, start, stepsize, samplerate)
        
def timemove(energypos):
    st = time.time()
    umv(energy,energypos)
    print(time.time() - st)
    
def mock_scan():
    samplerate = 0.0002
    start = 3710
    stop = 3760
    for i in range(100):
        print('######  Loop',str(i))
        d20fs._fscan(start, stop, 0.002, samplerate)
        d20fs._fscan(stop, start, 0.05, samplerate)
