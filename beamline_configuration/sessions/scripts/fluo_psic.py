
def collect_XRF(count_time=1, iterations=1):
	ACTIVE_MG.enable("fx_vortex*")
	ACTIVE_MG.disable("eiger500k*")
	try:
		sheh2.open()
		for i in range(iterations):
			sct(count_time)
	except:
		print('Something went wrong, XRf stopped...')
	ACTIVE_MG.disable("fx_vortex*")
	ACTIVE_MG.enable("eiger500k*")
	
def collect_XANES(energy_start, energy_end, energy_step, count_time, iterations=1):
	startingpos = energy.position
	steps = int( ((energy_end - energy_start)+(energy_step*0.99)) //energy_step)
	sheh2.open()
	try:
		for i in range(iterations):
			print(f"XANES collection #{i+1}")
			ascan(energy, energy_start, energy_end, steps, count_time)
	except:
		print(f"Something went wrong... moving energy back to {startingpos}")
		umv(energy, startingpos)
	umv(energy, startingpos)



def xrd_to_xas():
	ACTIVE_MG.enable("fx_vortex*")
	ACTIVE_MG.disable("eiger500k*")
	
def xrd_and_xas():
	ACTIVE_MG.enable("fx_vortex*")
	ACTIVE_MG.enable("eiger500k*")	
	
def xas_to_xrd():
	ACTIVE_MG.disable("fx_vortex*")
	ACTIVE_MG.enable("eiger500k*")
