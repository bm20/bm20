from collections import OrderedDict
import bliss.shell.getval as _gv
from prompt_toolkit.formatted_text import FormattedText
from time import time, ctime

optics_file = '/users/opd20/test_opset.txt'
motordict = OrderedDict([('conf', None),
                  ('epoch', None),
                  ('energy', energy),
                  ('bpm1x', None),
                  ('bpm1y', None),
                  ('att1', attb),
                  ('att2', atta),
                  ('bpm1v', bpm1v),
                  ('s1hg', s1hg),
                  ('s1ho', s1ho),
                  ('s1vg', s1vg),
                  ('s1vo', s1vo),
                  ('m1hgt', m1hgt),
                  ('m1ptch', m1ptch),
                  ('m1roll', m1roll),
                  ('m1lat', m1lat),
                  ('m1yaw', m1yaw),
                  ('m1bend', m1bend),
                  ('s2hg', s2hg),
                  ('s2ho', s2ho),
                  ('s2vg', s2vg),
                  ('s2vo', s2vo),
                  ('monxtal', monxtal),
                  ('monhgt', monhgt),
                  ('monyaw', monyaw),
                  ('xl1roll', xl1roll),
                  ('mono', bragg),
                  ('monp', xl2perp),
                  ('xl2ptch', xl2ptch),
                  ('xl2roll', xl2roll),
                  ('ml2ptch', ml2ptch),
                  ('ml2roll', ml2roll),
                  ('mon_y', ml2perp),
                  ('mon_z', ml2long),
                  ('bpm3v', bpm3v),
                  ('s3hg', s3hg),
                  ('s3ho', s3ho),
                  ('s3vg', s3vg),
                  ('s3vo', s3vo),
                  ('m2hgt', m2hgt),
                  ('m2ptch', m2ptch),
                  ('m2roll', m2roll),
                  ('m2lat', m2lat),
                  ('m2yaw', m2yaw),
                  ('m2bend', m2bend),
                  ('bpm4h', bpm4h),
                  ('bpm4v', bpm4v),
                  ('s5hg', s5hg),
                  ('s5ho', s5ho),
                  ('s5vg', s5vg),
                  ('s5vo', s5vo)])

def _read_motorlist():
    with open(optics_file,"r") as fh:
        content = fh.readlines()
        line = content[0].split('#')[-1]
        motorstr = [mot.split('@')[-1].split(':')[0] for mot in line.strip().split()]
    return motorstr
    
def save_optics():
    hutch = _gv.getval_idx_list(["RCH (EXAFS)","MRH (XES, XRD)"],"Enter hutch")
    comment = _gv.getval_name("Enter comment")
    positionstr = f"{_build_optics_config(hutch[0])} "
    positionstr += f"{time()} "
    nopos = "void"
    for mot in motordict.keys():
        if mot == 'epoch' or mot == 'conf':
            continue
        elif motordict[mot]:
            try:
                print(mot,motordict[mot].position)
                positionstr += f"{motordict[mot].position:.6} "
            except:
                print("could not read",mot)
                positionstr += f"{nopos} "
        else:
            positionstr += f"{nopos} "
            
    positionstr += f"# {ctime()}; Configured for {hutch[1][:3]} hutch; "
    positionstr += f"M1:{m1surface.position}, DCM:{_DCMXTALNAME[int(monxtal.position)-1]}, M2:{m2surface.position}; "
    if comment != "":
        positionstr += comment
    positionstr += "\n"
    print(comment)
    with open(optics_file, "a+") as fh:
        fh.write(positionstr)
    print(f"Optics configuration saved in {optics_file}\n")
    
def _build_optics_config(hutch):
    m1 = _MIRRNAME.index(m1surface.position)+1
    mo = int(monxtal.position)
    m2 = _MIRRNAME.index(m2surface.position)+1
    return f"{hutch}{m1}{mo}{m2}"
    
    if default is not None:
        default_text = f" [{default}]"
    else:
        default_text = ""

    
def _list_saved_versions(gens,poslist,posctr):
    print(f"Found {posctr} entries, which matches your query:")
    for key in range(posctr):
        print(f"{key}\tEnergy: {gens[key]: <7} eV;\t{poslist[key].split('#')[-1]}")
    #key = _gv.getval_name("\nWhich configuration do you want to restore?", default=str(posctr-1))
    print("Give one number to select one saved config, or a list of numbers (separated by ,) to compare configs",end="")
    text = FormattedText([("class:question", "\nWhich configuration do you want to restore?"), ("", " "),
            ("class:valid_input", f" [{str(posctr-1)}]"),
            ("class:prompt_char", ": "),] )
    key = _gv.bliss_prompt(text)
    if key == "": key = str(posctr-1)
    print(key+"\n")
    if ',' in key:
        return [int(k) for k in key.split(',')]
    else:
        return int(key)
    
def _get_optics_options(searchstr, wanted_config):
    gcoms = []
    gmots = []
    gkeys = []
    gengs = []
    with open(optics_file, "r") as fh:
        content = fh.readlines()
        motstr = content[0]
        for i, line in enumerate(content[1:]):
            motline, comment = line.split('#')
            motorstr = [mot.split('@')[-1].split(':')[0] for mot in motline.strip().split()]
            if searchstr in comment.split('M2:')[1][4:] and _check_config(motorstr[0], wanted_config):
                gcoms.append(comment)
                gmots.append(motline)
                gkeys.append(i)
                gengs.append(motorstr[2])
    userkey = _list_saved_versions(gengs,gcoms,len(gkeys))
    if isinstance(userkey, int):
        print(f"Loading configuration {userkey}: {gcoms[userkey]}")
        return gmots[userkey], gcoms[userkey], [mot for mot in motstr.strip().split()], False
    else:
        return [gmots[u] for u in userkey], [gcoms[u] for u in userkey], [mot for mot in motstr.strip().split()], True
        

def _show_components(gmot, gcom, comps):
    print(f"Loading configuration {gcom}")
    print(f"* motor position does not change so will not be moved")
    motorstr = [mot.split('@')[-1].split(':')[0] for mot in gmot.strip().split()]
    print("{: <16}| {: <16}| {: <16}| {: <16}".format("spec name","bliss name","saved pos.","current pos."))
    print("----------------------------------------------------------------------")
    mot_to_move = []
    pos_to_move = []
    for i, mot in enumerate(motordict.keys()):
        if mot in comps:
            row = [f"{mot}",f"{motordict[mot].name}",f"{motorstr[i]}",f"{motordict[mot].position:.6}"]
            print("{: <16}| {: <16}| {: <16}| {: <16}".format(*row), end='')
            if abs(float(motorstr[i]) - motordict[mot].position) < 0.0001:
                print(' *')
            else:
                print(' ')
                mot_to_move.append(mot)
                pos_to_move.append(float(motorstr[i]))
    return mot_to_move, pos_to_move

def _check_config(saved_config, wanted_config):
    match = True
    for si, wi in zip(saved_config, wanted_config):
        print(si,wi)
        if wi>0:
            if wi != int(si):
                match = False
    return match


def _compare_components(gmots, gcoms, comps):
    c2c = len(gmots)
    [print(f"Loading configuration {i+1}: {gcoms[i]}") for i in range(c2c)]
    motorstr = [[mot.split('@')[-1].split(':')[0] for mot in gmot.strip().split()] for gmot in gmots]
    row = ["spec name","bliss name"]
    fmt = "{: <16}| {: <16}| {: <16}"
    for i in range(c2c): 
        row.append(f"saved pos. {i+1}")
        fmt += "| {: <16}"
    row.append("current pos.")
    print(fmt.format(*row))
    row = "------------------------------------------------------"
    for i in range(c2c): row += "----------------"
    print(row)
    mot_to_move = []
    pos_to_move = []
    for i, mot in enumerate(motordict.keys()):
        if mot in comps:
            row = [f"{mot}",f"{motordict[mot].name}"]
            for j in range(c2c):
                row.append( f"{motorstr[j][i]}")
            row.append(f"{motordict[mot].position:.6}")
            print(fmt.format(*row))
    compkey = _gv.getval_int_range("\nWhich configuration do you want to restore?", minimum=1, maximum=c2c+1, default=c2c)
    print(compkey,"\n")
    return gmots[compkey-1], gcoms[compkey-1]


_MIRRNAME = ['Rh','Si','Pt']
_DCMXTALNAME = ["Si(311)","Si(111)/0deg","Si(111)/30deg","ML(24.8A)","ML(36.8A)"]
def _query_conf():
    op_list = ["RCH","MRH","ignore"]
    hu = _gv.getval_idx_list(op_list, "Enter Hutch Number", default=3)
    print(hu)
    hu = hu[0] if hu[0] < 3 else 0 

    op_list = [_MIRRNAME[0],_MIRRNAME[1],_MIRRNAME[2],"ignore"]
    m1 = _gv.getval_idx_list(op_list, "Enter surface of mirror 1", default=4)
    print(m1)
    m1 = m1[0] if m1[0] < 4 else 0 

    op_list = [_DCMXTALNAME[0], _DCMXTALNAME[1], _DCMXTALNAME[2], _DCMXTALNAME[3], _DCMXTALNAME[4],"ignore"]
    mo = _gv.getval_idx_list(op_list, "Enter crystal of monochromator", default=6)
    print(mo)
    mo = mo[0] if mo[0] < 6 else 0 

    op_list = [_MIRRNAME[0],_MIRRNAME[1],_MIRRNAME[2],"ignore"]
    m2 = _gv.getval_idx_list(op_list, "Enter surface of mirror 2", default=4)
    print(m2)
    m2 = m2[0] if m2[0] < 4 else 0 

    return [hu,m1,mo,m2]

def _get_comps(motstr):
    comp_list = ["1st Mirror only",
        "Monochromator only",
        "2nd Mirror only",
        "1st Mirror + Monochromator",
        "1st Mirror + 2nd Mirror",
        "Monochromator + 2nd Mirror",
        "1st Mirror + Monochromator + 2nd Mirror",
        "Everything including slits"]    
    comp_strings = [[":m1"],[":dcm"],[":m2"],[":m1",":dcm"],[":m1",":m2"],[":dcm",":m2"],[":m1",":dcm",":m2"],["all"]]

    comp = _gv.getval_idx_list(comp_list, "Which components do you want to restore?", default=7)
    print(comp,'\n')
    comp = comp[0]-1
    mots_to_move = []
    if comp < 3:
        for mot in motstr:
            if comp_strings[comp][0] in mot:
                mots_to_move.append(mot.split(':')[0].split('@')[-1])
    elif comp < 6:
        for mot in motstr:
            if comp_strings[comp][0] in mot or comp_strings[comp][1] in mot:
                mots_to_move.append(mot.split(':')[0].split('@')[-1])
    elif comp == 6:
        for mot in motstr:
            if comp_strings[comp][0] in mot or comp_strings[comp][1] in mot or comp_strings[comp][2] in mot:
                mots_to_move.append(mot.split(':')[0].split('@')[-1])
    elif comp == 7:
        for mot in motstr:
            if ':m1' in mot or ':dcm' in mot or ':m2' in mot or 's1' in mot or 's3' in mot:
                mots_to_move.append(mot.split(':')[0].split('@')[-1])
    return mots_to_move
    
def _move_components(mots_to_move,pos_to_move):
    print("")
    for i in range(len(mots_to_move)):
        print(f"umv({motordict[mots_to_move[i]].name}, {pos_to_move[i]})")

def restore_optics():  
    wanted_config = _query_conf()
    print(wanted_config)
    searchstr = _gv.getval_name("Enter search text",default="")
    gmot, gcom, motstr, compare = _get_optics_options(searchstr, wanted_config)
    mot_to_move = _get_comps(motstr)
    if compare: gmot, gcom = _compare_components(gmot, gcom, mot_to_move)
    mot_to_move, pos_to_move = _show_components(gmot, gcom, mot_to_move)
    print("\nPlease check if this is what you want.\n")
    if _gv.getval_yes_no("Do you want to proceed?", default="no"):
        _move_components(mots_to_move,pos_to_move)
    else:
        print("abort...")
    
    
    
def rc2(counter='p201_0:ct2_counters_controller:i0', scanrange=0.004, npts=30, counttime=0.1):
    plotselect(counter)
    dscan(xl2ptch, -scanrange, scanrange, npts, counttime)
    goto_cen()
        
def rc2_xes(scanrange=0.004, npts=30, counttime=0.1):
    rc2('p201_0:ct2_counters_controller:i0_xes', scanrange, npts, counttime)
        
def rc2_xrd1(scanrange=0.004, npts=30, counttime=0.1):
    rc2('p201_1:ct2_counters_controller:ic0', scanrange, npts, counttime)
        
def rc2_xrd2(scanrange=0.004, npts=30, counttime=0.1):
    rc2('p201_1:ct2_counters_controller:i0_xes', scanrange, npts, counttime)
        
    
    
    
    
    
            
        
