import pyFAI, fabio
from pyFAI.goniometer import Goniometer, GoniometerRefinement
import json
import glob
import os
import h5py
import numpy as np

def collect_PXRD(sample_name=None, dataset_name='PXRD', tth_start=0, tth_end=69, step_size=1, time_per_step=3, energy_eV=None, integratedata=True, plot=True, move_ty=True):
    """
    Performs PXRD data collection and integrates data
    """
    if sample_name:
        if not isinstance(sample_name, str): 
            print("Sample and dataset name must be a string (in \"  \")")
            return
    if dataset_name:
        if not isinstance(dataset_name, str): 
            print("Sample and dataset name must be a string (in \"  \")")
            return
    print(f"\n\nCurrently at Energy = {energy_eV} \n")
    
    if energy_eV: 
        print(f"Moving energy to {energy_eV} eV")
        umv(energy, energy_eV)

    umv(ty,0)
    plotselect('ic0')
    
    try:
        sheh2.open()
    except:
        print("Could not open EH2 shutter, has the search been completed?")
        return

    try: 
        if sample_name: newsample(sample_name)
        if dataset_name: newdataset(dataset_name)
        steps = int( ((tth_end - tth_start)+(step_size*0.99)) //step_size)
        print(f"Scanning tth from {tth_start} to {tth_end} in {steps} steps of {step_size}")
        ascan(tth, tth_start, tth_end, steps, time_per_step)
    except:
        print("Problem executing scan...")
        sheh2.close()
        return

    sheh2.close()
    print("\n##########################################################\n\n"+
          "\n                     Scan complete!\n\n"+
          "\n##########################################################\n")
    if integratedata:
        integrate_PXRD(sample_name,dataset_name, plot=plot)
    if move_ty:
        umv(ty, 30, tth, tth_start)
    else:
        umv(tth, tth_start)


def integrate_PXRD(sample_name=None, dataset_name=None, scan=None, calib_file=None, mask_file=None, plot=True, radial_range=[None, None], n_points = 49998, norm_counter = 'ic0', norm_factor = 1e6, detector = "eiger2_500k", int_filext='dat'):
    """
    integrates PXRD scan
    if sample, dataset, scan are not given, the last scan will be used
    calib_file: should be a .json, if not given it will be searched for in SCRIPTS
    radial_range: range on integration - if the first element is None, 0 degrees will be used,if the second element is None, the last image's angle will be used
    n_points: size of the integrated array
    """
    exp_path = os.path.join('/data/visitor',SCAN_SAVING.proposal_dirname,SCAN_SAVING.beamline,SCAN_SAVING.proposal_session_name)
    if not sample_name: sample_name = SCAN_SAVING.collection_name
    if not dataset_name: dataset_name = SCAN_SAVING.dataset_name
    try:
        if not scan: scan = int(SCANS[-1].scan_number)
    except:
        scan = 1
    print("Integrating",exp_path, sample_name, dataset_name, scan)
    if not calib_file:
        calib_files = glob.glob(os.path.join(exp_path, 'SCRIPTS/*json'))
        if len(calib_files) > 1: 
            print("More than one option for the calib file!!!!")
            print(calib_files)
            return
        elif len(calib_files) == 0:
            print("No calib file found!!")
            return
        else:
            calib_file = calib_files[0]
            print("Using",calib_file,"as calibration file")
    
    #mask_file = os.path.join(exp_path, 'SCRIPTS', 'msk_2023June16.edf')
    #if 'msk_2023June16.edf' not in glob.glob(os.path.join(exp_path, 'SCRIPTS','/*.edf')):
    #    os.system(f'cp XRD1/msk_2023June16.edf {mask_file}')
    
    if not mask_file:
        mask_files = glob.glob(os.path.join(exp_path, 'SCRIPTS','*edf'))
        if len(mask_files) > 1: 
            print("More than one option for the mask file!!!!")
            print(mask_files)
            mask_file = 'XRD1/msk_2023June16.edf'
        else:
            mask_file = mask_files[0]
    print("Using",mask_file,"as mask file")
        
    with fabio.open(mask_file) as mask_f:
        mask_data = mask_f.data[:]
    
    pilatus = pyFAI.detector_factory(detector)
    gonio = Goniometer.sload(calib_file)

    data = _load_data_h5(os.path.join(exp_path, 'RAW_DATA'), sample_name, dataset_name, scan, norm_counter=norm_counter, norm_factor=norm_factor)
    mg = gonio.get_mg(data['enc_angles'])
    rad_rng = radial_range[0] or 0, radial_range[1] or np.floor(data['enc_angles'][-1] + 0.5)
    mg.radial_range = rad_rng[0], rad_rng[1]

    res = mg.integrate1d([fabio.open(i).data for i in data['images']],
                          n_points,
                          normalization_factor=data['norm_values'],
                          lst_mask=mask_data)


    res_short = mg.integrate1d([fabio.open(i).data for i in data['images']],
                          14998,
                          normalization_factor=data['norm_values'],
                          lst_mask=mask_data)
                          
    savepath = os.path.join(exp_path, 'PROCESSED_DATA', sample_name, f'{sample_name}_{dataset_name}')
    os.makedirs(savepath, exist_ok=True) 
    #fname = os.path.join(output_dir, f'{name}.xy') #  => adds normalisation counter to the file name
    
    myheader = ''
    for metname in ['sample','dataset','scan','path','energy','norm_label']:
    	myheader += f'\t {metname}: {data[metname]}\n'
    myheader += f'tth, intensity'
    	
    print(myheader)
    fname = os.path.join(savepath,f'{sample_name}_{dataset_name}_scan{scan}_integrated.{int_filext}')
    np.savetxt(fname, np.array(res).T, fmt="%16.5f", newline='\r\n', header=myheader)
    
    fname_short = os.path.join(savepath,f'{sample_name}_{dataset_name}_scan{scan}_integrated.x_y')
    np.savetxt(fname_short, np.array(res_short).T, fmt="%16.5f", newline='\r\n', header=myheader)
    print("Saved integrated data in",fname)

    if plot:
        f = flint()
        p = f.get_plot("curve", name=f'{sample_name}_{dataset_name}_scan{scan}')
        p.add_curve_item("intensity", "tth", legend=f'{sample_name}_{dataset_name}_scan{scan}', linewidth=1)
        p.set_data(tth=res[1], intensity=res[0])
    return 



def _load_data_h5(data_dir, sample, dataset, scan, img_ext='edf', norm_counter='ic0', norm_factor=1e6):
    im_path = os.path.join(data_dir,sample,sample+'_'+dataset,'scan%04i'%scan)
    result = {
        'sample': sample,
        'dataset': dataset,
        'scan': scan,
        'path': im_path,
        'images': [],
        'energy': None,
        'enc_angles': None,
        'norm_factor': norm_factor,
        'norm_label': None,
        'norm_values': None,
        'error': '',
        'warning': ''
    }
    enc_angles = None

    images = sorted(glob.glob(f"{im_path}/*.{img_ext}"))
    n_images = len(images)
    if n_images == 0:
        print('No images found!')
        return result
    result['images'] = images
    
    h5_filepath = os.path.join(data_dir,sample,sample+'_'+dataset)
    h5files = glob.glob(f'{h5_filepath}/*.h5')
    print(h5files[0],'scan',scan,'loading')
    if len(h5files) > 1:
        raise RuntimeError(f'In directory {data_dir}: Expected to find ONE h5 file, found {len(h5files)}.')

    else:
        with h5py.File(h5files[0], 'r') as h5:
            try:
                enc_angles = h5[f'{scan}.1']['measurement/deltaenc'][:]
                n_images = len(images)
                n_angles = len(enc_angles)
                if n_images != n_angles:
                    raise RuntimeError(f'In directory {data_dir}: found {n_images} images but {n_angles} encoder values found in the SPEC file.')
            except KeyError:
                print('WARNING: no delenc found in h5 file.')            
                enc_angles = None
            result['enc_angles'] = enc_angles
            try:
                result['energy'] = h5[f'{scan}.1']['instrument/positioners/energy'][()]
            except KeyError:
                print('WARNING: could not read energy, using current value')            
                result['energy'] = energy.position
                
            if norm_counter:
                try:
                    norm_values = h5[f'{scan}.1'][f'measurement/{norm_counter}'][:]
                    n_norm = len(norm_values)
                    if n_images != n_norm:
                        result['error'] = f'{n_images} images != {n_norm} {norm_counter} values in h5.'
                        return result
                    if max(norm_values) <= 0.:
                        result['error'] += f'{norm_counter} max value is <= 0.'
                        return
                    result['norm_label'] = norm_counter

                except KeyError:
                    if norm_factor is not None:
                        result['warning'] += f'no {norm_counter} found in h5 file, using fallback value: {norm_factor};'
                        norm_values = np.full((n_images,), norm_factor)
                        result['norm_label'] = f'{norm_factor}'
                    else:
                        result['warning'] += f'no {norm_counter} found in h5 file, and no fallback (norm_factor) set, NOT normalizing.'
            elif norm_factor is not None:
                norm_values = np.full((1,n_images), norm_factor)
                
            result['norm_values'] = norm_values        
            result['h5'] = h5files[0]    
 
    return result



def collect_PXRD_energy_steps(energy_start, energy_end, energy_step, tth_start=0, tth_end=69, step_size=1, time_per_step=3, **kwargs):
    """
    Does collect_PXRD for a range of energies
    Energy in eV!!!
    """
    energies = np.arange(energy_start, energy_end+energy_step, energy_step)
    print("Energies at which PXRD will be measured:")
    print(energies)

    for ene in energies:
        try:
            ds = '%i_eV'%ene
            print("\nSetting eiger 500k energy to",ds,"\n")
            set_eiger_energy(ene)
            sleep(1)
            collect_PXRD(dataset_name=ds, energy_eV=ene, tth_start=tth_start, tth_end=tth_end, step_size=step_size, time_per_step=time_per_step, move_ty=False, integrate=False, plot=False, **kwargs)
            integrate_PXRD(plot=False, radial_range=[tth_start-1, tth_end+1], n_points = 49998)
        except:
            print("Problem with something, exiting energy loop...")
            return
        
    return



def collect_DAFS(energy_start, energy_end, energy_step, tth_position=0, time_per_step=3):
    """
    Does energy scan at constant tth position
    """
    energies = np.arange(energy_start, energy_end+energy_step, energy_step)
    steps = int(len(energies)-1)
    #print("Energies at which DAFS will be measured:")
    #print(energies)

    print(f"\nSetting eiger 500k energy to {energy_start} \n")
    set_eiger_energy(energy_start)
    ACTIVE_MG.enable("fx_vortex*")
    ACTIVE_MG.enable("eiger500k*")
    
    umv(tth, tth_position)
    sheh2.open()
    
    newdataset(f"DAFS_{energy_start}eV")
    #print(f"ascan(energy, {energy_start}, {energy_end}, {steps}, {time_per_step})")
    try:
        ascan(energy, energy_start, energy_end, steps, time_per_step)
        umv(energy, energy_start)
    except:
        umv(energy, energy_start)
            
    return

