#from bm20.macros.bm20 import *

def ws( num ):
    """ where slits """
    sl = "s%d"%(num)
    blades = [sl+bl for bl in ("y","z") if sl+bl in config.names_list]
    if len(blades):    wm(*blades)
    blades = [sl+bl for bl in 'udftblr' if sl+bl in config.names_list]
    if len(blades):    wm(*blades)
    blades = [sl+bl for bl in ("vg","vo","hg","ho") if sl+bl in config.names_list]
    if len(blades):    wm(*blades)