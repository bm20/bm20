
### Thursday Evening 18/01/24	
	
def longHERFD_RIXSrange(XES_energy):
	mv(xes_en,XES_energy) 	# moves the XES energy to the required energy
	sheh2.open()			# opens the shutter
	d20fs.fscan(3713, 3751, 0.2, 2) # HERFD(0.1eV / 1s)
	sheh2.close()			# closes the shutter
	
def RIXS():
	sheh2.open()
	d20fs.fscan2d(xes_en, 3329.5, 3345.7, 0.3, 3713, 3751, 0.2, 2)# RIXS on a single spot
	sheh2.close()
	


def Thursday_night():
	
	# go to CWU1
	umv(sampx, -11.69, sampy, 2.45, sampz, 6.935)

	# HERFDs before RIXS
	newsample('SH1_Sample1_CWU1_HERFDpreRIXS')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)
	
	# RIXS map
	newsample('SH1_Sample1_CWU1_RIXS')
	RIXS()
	
	# HERFD after RIXS
	newsample('SH1_Sample1_CWU1_HERFDpostRIXS')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)
	
	###
	
	# Go to CWU8 
	umv(sampx, 5.6950, sampy, -15.52, sampz, -8.5) 
	
	# HERFDs before RIXS
	newsample('SH2_Sample6_CWU8_HERFDpreRIXS')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)
	
	# RIXS map
	newsample('SH2_Sample6_CWU8_RIXS')
	RIXS()
	
	# HERFDs after RIXS
	newsample('SH2_Sample6_CWU8_HERFDpostRIXS')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)	

