# Useful functions scripts

print('EV555_protocols.py loaded')


############################# Sample positions #####################################








############################# Scan protocols ###########################################

def bragg_correction():
	bragg.hw_state;
	bragg._set_move_done();
	sync()
	
def shortHERFD(XES_energy):
	mv(xes_en,XES_energy) 	# moves the XES energy to the required energy
	bragg_correction()		
	sheh2.open()			# opens the shutter
	d20fs.fscan(3721, 3736, 0.2, 0.5) # HERFD(0.1eV / 1s) 
	sheh2.close()			# closes the shutter
	
def longHERFD(XES_energy):
	mv(xes_en,XES_energy) 	# moves the XES energy to the required energy
	sheh2.open()			# opens the shutter
	d20fs.fscan(3710, 3780, 0.2, 0.5) # HERFD(0.1eV / 1s)
	sheh2.close()			# closes the shutter
	
def longHERFD_RIXSrange(XES_energy):
	mv(xes_en,XES_energy) 	# moves the XES energy to the required energy
	sheh2.open()			# opens the shutter
	d20fs.fscan(3713, 3751, 0.2, 2) # HERFD(0.1eV / 1s)
	sheh2.close()			# closes the shutter
	
	
	
def normal_XES():
	umv(energy,3780)
	sheh2.open()
	ascan(xes_en,3335,3340,16,10)
	sheh2.close()
	
def RIXS():
	sheh2.open()
	d20fs.fscan2d(xes_en, 3329.5, 3345.7, 0.3, 3713, 3751, 0.2, 2)# RIXS on a single spot
	sheh2.close()




########################## User Functions ########################################

## Thursday Evening 18/01/24	
	
def CWU1_HERFD_test():
	umv(sampx, -11.69, sampy, 2.45, sampz, 6.935) # new center
	newsample('SH1_Sample1_CWU1_HERFDpreRIXS')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)
	

	
def CWU8_align():
	umv(energy,3726.5)
	#umv(sampx, 5.6950, sampy, -15.52, sampz, -8.5) new center
	newsample('CWU8_align')
	#sheh2.open();dscan(syy, -3, 3, 12, 0.5);sheh2.close()
	#sheh2.open();dscan(sampz, -3, 3, 24, 0.5);sheh2.close()
	#sheh2.open();dscan(sampy, -7, 7, 24, 0.5);sheh2.close()
	
	
def Thursday_night():
	
	# CWU1
	umv(sampx, -11.69, sampy, 2.45, sampz, 6.935)
	
	# RIXS
	newsample('SH1_Sample1_CWU1_RIXS')
	RIXS()
	newsample('SH1_Sample1_CWU1_HERFDpostRIXS')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)
	
	# CWU8
	umv(sampx, 5.6950, sampy, -15.52, sampz, -8.5) 
	newsample('SH2_Sample6_CWU8_HERFDpreRIXS')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)
	
	# RIXS
	newsample('SH2_Sample6_CWU8_RIXS')
	RIXS()
	newsample('SH2_Sample6_CWU8_HERFDpostRIXS')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)	
	

def Friday_morning():
	
	# back to CWU1 because of shift during RIXS measurement
	# new sample position 0.1mm in z direction
	umv(sampx, -11.69, sampy, 2.45, sampz, 6.935) # center
	umvr(sampz, 0.1)
	
	# HERFD before RIXS
	newsample('SH1_Sample1_CWU1_HERFDpreRIXS_2')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)
	
	# RIXS
	newsample('SH1_Sample1_CWU1_RIXS_2')
	RIXS()
	
	# HERFD after RIXS
	newsample('SH1_Sample1_CWU1_HERFDpostRIXS_2')
	longHERFD_RIXSrange(3337.8)
	longHERFD_RIXSrange(3337.8)
	
	
	
	
	
	
