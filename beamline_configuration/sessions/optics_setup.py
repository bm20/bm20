
from bliss.setup_globals import *

load_script("bm20_common.py")
load_script("optics.py")

# print('\n\n')
# print('#########################')
# print('###### BM20 Status ######')
# print('#########################')


from bm20.macros.homing import *

#print(bm20_status())

print("")
print("Welcome to your new 'optics' BLISS session !! ")
print("")
print("You can now customize your 'optics' session by changing files:")
print("   * /optics_setup.py ")
print("   * /optics.yml ")
print("   * /scripts/optics.py ")
print("")
