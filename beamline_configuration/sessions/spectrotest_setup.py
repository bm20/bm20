
from bliss.setup_globals import *

load_script("spectrotest.py")

print("")
print("Welcome to your new 'spectrotest' BLISS session !! ")
print("")
print("You can now customize your 'spectrotest' session by changing files:")
print("   * /spectrotest_setup.py ")
print("   * /spectrotest.yml ")
print("   * /scripts/spectrotest.py ")
print("")