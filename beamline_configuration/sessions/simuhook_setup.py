
from bliss.setup_globals import *

load_script("simuhook.py")

print("")
print("Welcome to your new 'simuhook' BLISS session !! ")
print("")
print("You can now customize your 'simuhook' session by changing files:")
print("   * /simuhook_setup.py ")
print("   * /simuhook.yml ")
print("   * /scripts/simuhook.py ")
print("")