
from bliss.setup_globals import *

load_script("bm20_common.py")
load_script("xrd2.py")

print("")
print("Welcome to your new 'xrd2' BLISS session !! ")
print("")
print("You can now customize your 'xrd2' session by changing files:")
print("   * /xrd2_setup.py ")
print("   * /xrd2.yml ")
print("   * /scripts/xrd2.py ")
print("")