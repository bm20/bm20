
from bliss.setup_globals import *

load_script("wagotest.py")

print("")
print("Welcome to your new 'wagotest' BLISS session !! ")
print("")
print("You can now customize your 'wagotest' session by changing files:")
print("   * /wagotest_setup.py ")
print("   * /wagotest.yml ")
print("   * /scripts/wagotest.py ")
print("")